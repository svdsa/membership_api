from http import HTTPStatus
from typing import Tuple, Union

from flask import Blueprint
from flask.wrappers import Response
from pydantic.main import BaseModel

from membership.auth.errors import InvalidUserError
from membership.models.authz import PrincipalAuthContext
from membership.schemas.formatters.users import format_user
from membership.web.auth import requires_auth
from membership.web.util import validate_with_pydantic

users_api = Blueprint("users_api", __name__)


@users_api.route("/users/@me", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth(hide_presence=False)
def get_self_user(ctx: PrincipalAuthContext) -> Union[Tuple[BaseModel, int], Response]:
    user = ctx.principal.user

    if user is None:
        raise InvalidUserError(hide_presence=False)

    return format_user(user, needs=ctx.needs), HTTPStatus.OK
