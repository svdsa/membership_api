from typing import Optional

import sentry_sdk
import structlog
import werkzeug.exceptions
from flask import Flask, jsonify, request
from flask_cors import CORS
from flask_principal import Identity, Principal
from jwt.exceptions import ExpiredSignatureError
from sentry_sdk.integrations.flask import FlaskIntegration

import membership.util.structlog
from config import USE_FEATURE_COLLECTIVE_DUES
from config.sentry_config import SENTRY_DSN
from membership.web.util import CustomEncoder

# Initialize logging before anything else
membership.util.structlog.init()
logger = structlog.stdlib.get_logger()


# NOTE: Don't import from the membership package outside of this init function!
#
# Many parts of the code will reference "app" for the logger and this will cause
# cyclic dependencies of this file and the app. By putting all membership imports
# in this function, we can be sure that the app will initialize before anything
# references it.
def init(app: Flask):
    """
    Initializes the app after it has been constructed.

    Note: This is where you can import from the membership package
    """
    from membership.database.base import check_migrations, engine
    from membership.web.assets import asset_api
    from membership.web.committees import committee_api
    from membership.web.contacts import contacts_api
    from membership.web.custom_fields import custom_fields_api
    from membership.web.elections import election_api
    from membership.web.imports import imports_api
    from membership.web.meetings import meeting_api
    from membership.web.members import member_api
    from membership.web.schema import schema_api
    from membership.web.search import search_api
    from membership.web.security_principals import security_principals_api
    from membership.web.tags import tag_api
    from membership.web.users import users_api

    # Insert middleware into the app request pipeline
    logger.info("initializing middleware")

    membership.util.structlog.register_flask_before_request(app)
    principal = Principal(app, use_sessions=False)
    CORS(app, expose_headers=["Link"])

    # Register blueprints
    app.register_blueprint(asset_api)
    app.register_blueprint(member_api)
    app.register_blueprint(meeting_api)
    app.register_blueprint(election_api)
    app.register_blueprint(committee_api)
    app.register_blueprint(tag_api)
    app.register_blueprint(imports_api)
    app.register_blueprint(schema_api)
    app.register_blueprint(contacts_api)
    app.register_blueprint(custom_fields_api)
    app.register_blueprint(users_api)
    app.register_blueprint(search_api)
    app.register_blueprint(security_principals_api)

    if USE_FEATURE_COLLECTIVE_DUES:
        from membership.web.payments import payments_api

        app.register_blueprint(payments_api)

    # Add error handlers
    handle_errors(app)

    # Check migrations are completed
    check_migrations(engine)

    logger.info("initialization complete")

    @principal.identity_loader
    def on_identity_loaded() -> Optional[Identity]:
        from config import USE_AUTH
        from membership.auth.authentication import get_security_principal
        from membership.auth.needs import MemberRoleNeed, NdCommitteesList
        from membership.auth.needs_derived import contact_full_needs_by_id
        from membership.database.base import db_session
        from membership.schemas.rest.contacts import IdContact

        auth = request.headers.get("authorization")
        session = db_session()
        principal = get_security_principal(session, auth_header=auth, use_auth=USE_AUTH)
        if principal is None:
            return None

        # Set the identity user object
        identity = Identity(principal.id, auth_type="jwt" if USE_AUTH else "no_auth")

        # Add default needs
        if principal.user is not None:
            for need in contact_full_needs_by_id(IdContact(principal.user.contact.id)):
                identity.provides.add(need)

        # Enumerate roles associated with principal and assign needs
        for role in principal.roles:
            for need in role.needs:
                identity.provides.add(need)

        # XXX MEMBER Remove when deprecating member authentication
        if principal.member is not None:
            roles = principal.member.roles
            for role in roles:
                identity.provides.add(
                    MemberRoleNeed(role=role.role, committee_id=role.committee_id)
                )
            identity.provides.add(NdCommitteesList())

        return identity


def handle_errors(app: Flask):
    import pydantic.error_wrappers
    from phonenumbers.phonenumberutil import NumberParseException

    import membership.web.errors as errors
    from membership.auth.errors import (
        InvalidJWTError,
        InvalidJWTSecretError,
        InvalidUserError,
    )
    from membership.database.base import InvalidIdError
    from membership.errors.exceptions import (
        AssociationExistsError,
        BulkResourceMissingError,
        CreateAlreadyExistsError,
        CreateArchivedExistsError,
        CreateExclusiveParameterConflictError,
        ParameterMissingError,
        ParameterValidationError,
        ResourceMissingError,
        UpdateTargetNotFoundError,
    )
    from membership.errors.handlers import (
        handle_archived_resource_exists_error,
        handle_association_exists_error,
        handle_bulk_resource_missing_error,
        handle_create_conflict_error,
        handle_parameter_missing_error,
        handle_pydantic_validation_error,
        handle_resource_already_exists,
        handle_resource_missing_error,
        handle_update_target_not_found_error,
        handle_validation_error,
    )
    from membership.util.pagination import (
        PaginationCursorFunctionRequiredError,
        PaginationCursorNotFoundError,
        PaginationMultipleCursorsForbiddenError,
    )
    from membership.web.auth import (
        InsufficientPermissionsError,
        InvalidSecurityPrincipalError,
        RequestRequiresAuthenticationError,
        handle_authentication_error,
        handle_invalid_jwt_error,
    )

    ###################
    # App-wide handlers
    app.register_error_handler(
        PaginationMultipleCursorsForbiddenError,
        errors.handle_multiple_pagination_cursors,
    )
    app.register_error_handler(
        PaginationCursorNotFoundError, errors.handle_pagination_cursor_not_found
    )
    app.register_error_handler(
        PaginationCursorFunctionRequiredError,
        errors.handle_pagination_cursor_function_required,
    )
    app.register_error_handler(InvalidIdError, errors.handle_invalid_id_format)
    app.register_error_handler(
        RequestRequiresAuthenticationError, handle_authentication_error
    )
    app.register_error_handler(
        InvalidSecurityPrincipalError, handle_authentication_error
    )
    app.register_error_handler(
        InsufficientPermissionsError, handle_authentication_error
    )
    app.register_error_handler(InvalidUserError, handle_authentication_error)
    app.register_error_handler(ExpiredSignatureError, handle_invalid_jwt_error)
    app.register_error_handler(InvalidJWTError, handle_invalid_jwt_error)
    app.register_error_handler(InvalidJWTSecretError, handle_invalid_jwt_error)
    app.register_error_handler(
        CreateExclusiveParameterConflictError, handle_create_conflict_error
    )
    app.register_error_handler(ParameterMissingError, handle_parameter_missing_error)
    app.register_error_handler(CreateAlreadyExistsError, handle_resource_already_exists)
    app.register_error_handler(
        UpdateTargetNotFoundError, handle_update_target_not_found_error
    )
    app.register_error_handler(ResourceMissingError, handle_resource_missing_error)
    app.register_error_handler(
        BulkResourceMissingError, handle_bulk_resource_missing_error
    )
    app.register_error_handler(AssociationExistsError, handle_association_exists_error)
    app.register_error_handler(ParameterValidationError, handle_validation_error)
    app.register_error_handler(
        pydantic.error_wrappers.ValidationError, handle_pydantic_validation_error
    )
    app.register_error_handler(
        CreateArchivedExistsError, handle_archived_resource_exists_error
    )

    ###################
    # Data validation handlers
    app.register_error_handler(NumberParseException, errors.handle_invalid_phone_number)

    ##################
    # Generic handlers
    app.register_error_handler(
        werkzeug.exceptions.NotFound, errors.handle_404_not_found
    )
    app.register_error_handler(
        werkzeug.exceptions.MethodNotAllowed, errors.handle_405_method_not_allowed
    )
    app.register_error_handler(
        werkzeug.exceptions.InternalServerError, errors.handle_500_server_error
    )

    ##############
    # must be last
    app.register_error_handler(Exception, errors.handle_generic_exception)


if SENTRY_DSN is not None:
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        integrations=[FlaskIntegration()],

        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production,
        traces_sample_rate=1.0
    )

app = Flask(__name__, static_folder='../../mnt/static')
app.json_encoder = CustomEncoder

init(app)


@app.teardown_appcontext
def shutdown_session(exception=None):
    from membership.database.base import db_session

    db_session.remove()


@app.route("/health", methods=["GET"])
def health_check():
    from membership.database.base import db_session
    from membership.database.models import Contact

    db_session.query(Contact).first()
    return jsonify({'health': True})
