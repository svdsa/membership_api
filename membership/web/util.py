import json
import logging
from datetime import date, datetime
from decimal import Decimal
from functools import wraps
from http import HTTPStatus
from typing import Any, Callable, Dict, List, Optional, Tuple, Type

from flask import request
from flask.helpers import url_for
from flask.json import JSONEncoder
from flask.wrappers import Response
from flask_pydantic.core import (
    convert_query_params,
    is_iterable_of_models,
    make_json_response,
    unsupported_media_type_response,
    validate_many_models,
)
from flask_pydantic.exceptions import (
    InvalidIterableOfModelsException,
    JsonBodyParsingError,
    ManyModelValidationError,
)
from jsonschema import ValidationError as JSONValidationError
from jsonschema import validate
from pydantic import ValidationError as PydanticValidationError
from pydantic.error_wrappers import ValidationError
from pydantic.main import BaseModel
from pydantic.tools import parse_obj_as

from membership.errors.exceptions import ParameterValidationError
from membership.schemas import Json
from membership.schemas.rest.pagination import SxList
from membership.util.arrays import compact
from membership.web.errors import ErrorCode, ErrorResponse, ErrorType

logger = logging.getLogger(__name__)


class Ok(Response):
    def __init__(self, data: Optional[Json] = None) -> None:
        body = None if data is None else json.dumps(data)
        mimetype = None if body is None else 'application/json'
        super(Ok, self).__init__(body, status=200, mimetype=mimetype)


class Created(Response):
    def __init__(self, data: Optional[Json] = None, location: Optional[str] = None) -> None:
        body = None if data is None else json.dumps(data)
        mimetype = None if body is None else 'application/json'
        super(Created, self).__init__(body, status=201, mimetype=mimetype)
        if location is not None:
            self.headers['Location'] = location


class Accepted(Response):
    def __init__(self, data: Optional[Json] = None) -> None:
        body = None if data is None else json.dumps(data)
        mimetype = None if body is None else 'application/json'
        super(Accepted, self).__init__(body, status=202, mimetype=mimetype)


class BadRequest(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(BadRequest, self).__init__(
            json.dumps(payload), status=400, mimetype='application/json'
        )


class Unauthorized(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(Unauthorized, self).__init__(
            json.dumps(payload), status=401, mimetype='application/json'
        )


class PaymentRequired(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(PaymentRequired, self).__init__(
            json.dumps(payload), status=402, mimetype='application/json'
        )


class Forbidden(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(Forbidden, self).__init__(
            json.dumps(payload),
            status=HTTPStatus.FORBIDDEN,
            mimetype='application/json',
        )


class NotFound(Response):
    def __init__(self, err: str = 'Not Found') -> None:
        payload = {'status': 'failed', 'err': err}
        super(NotFound, self).__init__(
            json.dumps(payload), status=404, mimetype='application/json'
        )


class NotAcceptable(Response):
    def __init__(self, err: str) -> None:
        payload = {"status": "failed", "err": err}
        super(NotAcceptable, self).__init__(
            json.dumps(payload),
            status=HTTPStatus.NOT_ACCEPTABLE,
            mimetype="application/json",
        )


class Conflict(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(Conflict, self).__init__(
            json.dumps(payload), status=409, mimetype='application/json'
        )


class Gone(Response):
    """410 Gone

    Indicates that the resource requested is no longer available and will not be
    available again. This should be used when a resource has been intentionally
    removed and the resource should be purged. Upon receiving a 410 status code,
    the client should not request the resource in the future.

    Clients such as search engines should remove the resource from their indices."""

    def __init__(self, err: str) -> None:
        payload = {"status": "failed", "err": err}
        super(Gone, self).__init__(
            json.dumps(payload), status=HTTPStatus.GONE, mimetype="application/json"
        )


class UnprocessableEntity(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(UnprocessableEntity, self).__init__(
            json.dumps(payload), status=422, mimetype='application/json'
        )


class RateLimitExceeded(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(RateLimitExceeded, self).__init__(
            json.dumps(payload), status=429, mimetype='application/json'
        )


class ServerError(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(ServerError, self).__init__(
            json.dumps(payload), status=500, mimetype='application/json'
        )


class NotImplemented(Response):
    def __init__(self, err: str) -> None:
        payload = {"status": "failed", "err": err}
        super(NotImplemented, self).__init__(
            json.dumps(payload),
            status=HTTPStatus.NOT_IMPLEMENTED,
            mimetype="application/json",
        )


class ServiceUnavailable(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(ServiceUnavailable, self).__init__(
            json.dumps(payload), status=503, mimetype='application/json'
        )


def requires_json(f: Callable):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not request.is_json:
            return BadRequest("Expected 'Content-Type:application/json' header")
        else:
            return f(*args, **kwargs)

    # Blueprint methods have to have unique names
    decorated.__name__ = f.__name__
    return decorated


def validate_request(schema: Dict):
    def decorator(f: Callable):
        @wraps(f)
        def decorated(*args, **kwargs):
            try:
                validate(request.json, schema)
                return f(*args, **kwargs)
            except JSONValidationError as ve:
                msg = f"Request did not match expected schema: {ve}"
                logger.error(msg)
                return BadRequest(msg)

        return decorated

    return decorator


def validate_response(schema: Dict):
    def decorator(f: Callable):
        @wraps(f)
        def decorated(*args, **kwargs):
            try:
                response = f(*args, **kwargs)
                validate(response.json, schema)
                return response
            except JSONValidationError as ve:
                msg = f"Response did not match expected schema: {ve}"
                logger.error(msg)
                return ServerError(msg)

        return decorated

    return decorator


def validate_pydantic_path_params(func: Callable, kwargs: dict) -> Tuple[dict, list]:
    errors = []
    validated = {}
    for name, type_ in func.__annotations__.items():
        if name in {"query", "body", "return"}:
            continue
        if name not in kwargs:
            continue
        try:
            value = parse_obj_as(type_, kwargs.get(name))
            validated[name] = value
        except ValidationError as e:
            err = e.errors()[0]
            err["loc"] = [name]
            errors.append(err)
    kwargs = {**kwargs, **validated}
    return kwargs, errors


def validate_with_pydantic(
    body: Optional[Type[BaseModel]] = None,
    query: Optional[Type[BaseModel]] = None,
    on_success_status: int = 200,
    exclude_none: bool = False,
    response_many: bool = False,
    request_body_many: bool = False,
    response_by_alias: bool = False,
    exclude_path_args: Optional[List[str]] = None,
    ignore_path_args: bool = False,
):
    """
    From flask_pydantic

    Decorator for route methods which will validate query and body parameters
    as well as serialize the response (if it derives from pydantic's BaseModel
    class).

    Request parameters are accessible via flask's `request` variable:
        - request.query_params
        - request.body_params

    Or directly as `kwargs`, if you define them in the decorated function.

    `exclude_none` whether to remove None fields from response
    `response_many` whether content of response consists of many objects
        (e. g. List[BaseModel]). Resulting response will be an array of serialized
        models.
    `request_body_many` whether response body contains array of given model
        (request.body_params then contains list of models i. e. List[BaseModel])

    example::

        from flask import request
        from flask_pydantic import validate
        from pydantic import BaseModel

        class Query(BaseModel):
            query: str

        class Body(BaseModel):
            color: str

        class MyModel(BaseModel):
            id: int
            color: str
            description: str

        ...

        @app.route("/")
        @validate(query=Query, body=Body)
        def test_route():
            query = request.query_params.query
            color = request.body_params.query

            return MyModel(...)

        @app.route("/kwargs")
        @validate()
        def test_route_kwargs(query:Query, body:Body):

            return MyModel(...)

    -> that will render JSON response with serialized MyModel instance
    """

    def decorate(func: Callable) -> Callable:
        @wraps(func)
        def wrapper(*args, **kwargs):
            q = None
            b = None
            err: Dict[str, Any] = {}
            if not ignore_path_args:
                filtered_kwargs = None
                if exclude_path_args is not None:
                    exclude_args = set(exclude_path_args)
                    filtered_kwargs = {
                        k: v for k, v in kwargs.items() if k not in exclude_args
                    }
                else:
                    filtered_kwargs = kwargs
                filtered_kwargs, path_err = validate_pydantic_path_params(
                    func, filtered_kwargs
                )
                if path_err:
                    err["status_code"] = HTTPStatus.NOT_FOUND
                    err["path_params"] = path_err
            query_in_kwargs = func.__annotations__.get("query")
            query_model = query_in_kwargs or query
            if query_model:
                query_params = convert_query_params(request.args, query_model)
                try:
                    q = query_model(**query_params)
                except PydanticValidationError as ve:
                    err["status_code"] = HTTPStatus.BAD_REQUEST
                    err["query_params"] = ve.errors()
            body_in_kwargs = func.__annotations__.get("body")
            body_model = body_in_kwargs or body
            if body_model:
                body_params = request.get_json(silent=True)
                if body_params is None:
                    logger.error(
                        f"Error parsing json for request {request.method} {request.path}"
                        f" with {request.content_length=}"
                    )
                    return ErrorResponse(
                        type=ErrorType.invalid_request_error,
                        code=ErrorCode.invalid_or_empty_request,
                        status_code=HTTPStatus.BAD_REQUEST,
                    )

                if "__root__" in body_model.__fields__:
                    try:
                        b = body_model(__root__=body_params).__root__
                    except PydanticValidationError as ve:
                        err["status_code"] = HTTPStatus.BAD_REQUEST
                        err["body_params"] = ve.errors()
                elif request_body_many:
                    try:
                        b = validate_many_models(body_model, body_params)
                    except ManyModelValidationError as e:
                        err["status_code"] = HTTPStatus.BAD_REQUEST
                        err["body_params"] = e.errors()
                else:
                    try:
                        b = body_model(**body_params)
                    except TypeError:
                        content_type = request.headers.get("Content-Type", "").lower()
                        media_type = content_type.split(";")[0]
                        if media_type != "application/json":
                            return unsupported_media_type_response(content_type)
                        else:
                            raise JsonBodyParsingError()
                    except PydanticValidationError as ve:
                        err["status_code"] = HTTPStatus.BAD_REQUEST
                        err["body_params"] = ve.errors()
            request.query_params = q
            request.body_params = b
            if query_in_kwargs:
                kwargs["query"] = q
            if body_in_kwargs:
                kwargs["body"] = b

            if len(err) > 0:
                if err["status_code"] == HTTPStatus.BAD_REQUEST:
                    params = err.get("query_params") or err.get("body_params")

                    friendly_message = None
                    if params is not None:
                        if len(params) == 1:
                            friendly_message = params[0]["msg"]
                        else:
                            friendly_message = (
                                "There were multiple validation "
                                "issues with your request"
                            )

                    raise ParameterValidationError(
                        message=friendly_message, param=params
                    )

                elif err["status_code"] == HTTPStatus.NOT_FOUND:
                    error_obj = ErrorResponse(
                        code=ErrorCode.resource_missing,
                        status_code=HTTPStatus.NOT_FOUND,
                        type=ErrorType.invalid_request_error,
                        params=err.get("path_params"),
                    )
                    logger.error(error_obj)
                    return error_obj

            res = func(*args, **kwargs)

            if response_many:
                if is_iterable_of_models(res):
                    return make_json_response(
                        res,
                        on_success_status,
                        by_alias=response_by_alias,
                        exclude_none=exclude_none,
                        many=True,
                    )
                else:
                    raise InvalidIterableOfModelsException(res)

            if isinstance(res, BaseModel):
                return make_json_response(
                    res,
                    on_success_status,
                    exclude_none=exclude_none,
                    by_alias=response_by_alias,
                )

            if (
                isinstance(res, tuple)
                and len(res) == 2
                and isinstance(res[0], BaseModel)
            ):
                return make_json_response(
                    res[0],
                    res[1],
                    exclude_none=exclude_none,
                    by_alias=response_by_alias,
                )

            return res

        return wrapper

    return decorate


# TODO: We should always convert datetime to a number of millis from epoch
class CustomEncoder(JSONEncoder):
    """Custom encoder class converts Decimals to strings and datetime objects into ISO
    formatted strings."""

    def default(self, obj):
        if isinstance(obj, Decimal):
            return str(obj)
        if isinstance(obj, datetime):
            return obj.isoformat()
        if isinstance(obj, date):
            return obj.isoformat()
        return JSONEncoder.default(self, obj)


def make_paginated_list_response(
    list: SxList,
    *,
    next_cursor: Optional[str] = None,
    prev_cursor: Optional[str] = None,
    route_args: Dict[str, str] = {},
    limit: Optional[int],
    route: str,
) -> Response:
    response = make_json_response(
        list,
        HTTPStatus.OK,
        by_alias=False,
    )

    # attach pagination headers when needed
    link_headers = compact(
        [
            f'<{url_for(route, starting_after=next_cursor, limit=limit, **route_args)}>; rel="next"'  # noqa: E501
            if next_cursor is not None
            else None,
            f'<{url_for(route, ending_before=prev_cursor, limit=limit, **route_args)}>; rel="prev"'  # noqa: E501
            if prev_cursor is not None
            else None,
        ]
    )

    if len(link_headers) > 0:
        response.headers["Link"] = ", ".join(link_headers)

    return response
