from flask import Blueprint, jsonify

from membership.models import MemberAuthContext
from membership.schemas.rest.contacts import (
    SxContactCreateRequest,
    SxListContactsResponse,
)
from membership.schemas.rest.custom_fields import (
    SxCustomFieldCreateRequest,
    SxListCustomFieldsResponse,
)
from membership.web.auth import DEPRECATED_requires_legacy_auth

schema_api = Blueprint("schema_api", __name__)


@schema_api.route("/schemas/contacts", methods=["GET"])
@DEPRECATED_requires_legacy_auth()
def get_contacts_schema(ctx: MemberAuthContext):
    return jsonify(SxListContactsResponse.schema_json())


@schema_api.route("/schemas/contacts", methods=["POST"])
@DEPRECATED_requires_legacy_auth()
def post_contacts_schema(ctx: MemberAuthContext):
    return jsonify(SxContactCreateRequest.schema_json())


@schema_api.route("/schemas/custom_fields", methods=["GET"])
@DEPRECATED_requires_legacy_auth()
def get_custom_fields_schema(ctx: MemberAuthContext):
    return jsonify(SxListCustomFieldsResponse.schema_json())


@schema_api.route("/schemas/custom_fields", methods=["POST"])
@DEPRECATED_requires_legacy_auth()
def post_custom_fields_schema(ctx: MemberAuthContext):
    return jsonify(SxCustomFieldCreateRequest.schema_json())
