from http import HTTPStatus
from typing import Tuple, Union

import structlog
from flask import Blueprint
from flask.helpers import url_for
from flask.wrappers import Response
from pydantic.main import BaseModel

from membership.auth.permissions import (
    PmCustomFieldsCreate,
    PmCustomFieldsDelete,
    PmCustomFieldsRead,
    PmCustomFieldsUpdate,
)
from membership.errors.exceptions import CreateAlreadyExistsError, ParameterMissingError
from membership.models.authz import PrincipalAuthContext
from membership.schemas.formatters.custom_fields import (
    format_custom_field_definition,
    format_custom_field_list,
)
from membership.schemas.rest.custom_fields import (
    IdCustomField,
    SxCustomFieldCreateRequest,
    SxCustomFieldUpdateRequest,
    SxListCustomFieldsQuery,
)
from membership.services import custom_field_service
from membership.services.custom_field_service import (
    CustomFieldAssociationExistsError,
    InvalidCustomFieldOptionsError,
    MissingCustomFieldOptionsError,
)
from membership.web.auth import requires_auth_permissions
from membership.web.errors import ErrorCode, ErrorResponse, ErrorType
from membership.web.util import make_paginated_list_response, validate_with_pydantic

custom_fields_api = Blueprint("custom_fields_api", __name__)
logger = structlog.stdlib.get_logger()


@custom_fields_api.route("/custom_fields", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmCustomFieldsRead, hide_presence=False)
def list_custom_fields(
    ctx: PrincipalAuthContext, query: SxListCustomFieldsQuery
) -> Response:
    custom_fields = custom_field_service.list_(
        ctx.session,
        limit=query.limit,
        starting_after=query.starting_after,
        ending_before=query.ending_before,
    )

    return make_paginated_list_response(
        format_custom_field_list(
            custom_fields, url_for(".list_custom_fields", **query.dict())
        ),
        limit=query.limit,
        next_cursor=custom_fields.next_cursor,
        prev_cursor=custom_fields.prev_cursor,
        route="contacts_api.list_contacts",
    )


@custom_fields_api.route("/custom_fields", methods=["POST"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmCustomFieldsCreate, hide_presence=False)
def create_custom_field(
    ctx: PrincipalAuthContext, body: SxCustomFieldCreateRequest
) -> Union[Tuple[BaseModel, int], Response]:
    try:
        cfd = custom_field_service.create(
            ctx.session,
            body,
            defer_commit=False,
        )
        return (format_custom_field_definition(cfd), HTTPStatus.CREATED)

    except MissingCustomFieldOptionsError:
        raise ParameterMissingError(params="options")

    except CustomFieldAssociationExistsError:
        raise CreateAlreadyExistsError(object="custom_field")


@custom_fields_api.route("/custom_fields/<id>", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmCustomFieldsRead)
def get_custom_field(
    ctx: PrincipalAuthContext, id: IdCustomField
) -> Union[Tuple[BaseModel, int], Response]:
    custom_field = custom_field_service.from_id_or_raise(ctx.session, id=id)

    return format_custom_field_definition(custom_field), HTTPStatus.OK


@custom_fields_api.route("/custom_fields/<id>", methods=["PATCH"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmCustomFieldsUpdate)
def update_custom_field(
    ctx: PrincipalAuthContext, id: IdCustomField, body: SxCustomFieldUpdateRequest
) -> Union[Tuple[BaseModel, int], Response]:
    try:
        custom_field = custom_field_service.update_from_id(
            ctx.session, id=id, update_request=body, defer_commit=False
        )

        return format_custom_field_definition(custom_field), HTTPStatus.OK

    except MissingCustomFieldOptionsError:
        raise ParameterMissingError(params="options")


@custom_fields_api.route("/custom_fields/<id>", methods=["DELETE"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmCustomFieldsDelete)
def delete_custom_field(
    ctx: PrincipalAuthContext,
    id: IdCustomField,
) -> Union[Tuple[BaseModel, int], Response]:
    custom_field = custom_field_service.delete_from_id(
        ctx.session, id=id, defer_commit=False
    )

    return format_custom_field_definition(custom_field), HTTPStatus.OK


def handle_invalid_custom_field_options_error(
    error: InvalidCustomFieldOptionsError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.parameter_invalid,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.BAD_REQUEST,
        friendly_message="Invalid custom field options",
        # Follow pydantic error format
        params=[
            {
                "loc": ["options"],
                "msg": "Invalid custom field options",
                "type": "value_error",
            }
        ],
    )
    logger.error(**response.as_dict())
    return response


custom_fields_api.register_error_handler(
    InvalidCustomFieldOptionsError, handle_invalid_custom_field_options_error
)
