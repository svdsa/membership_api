import logging
from typing import IO, cast

import boto3
from flask import Blueprint, jsonify, request
from jsonschema import ValidationError, validate

import config
from membership.auth.permissions import PmAssetsAdmin
from membership.database.models import Asset, AssetType
from membership.models.authz import PrincipalAuthContext
from membership.schemas.rest.assets import format_resolved_asset
from membership.services.asset_service import (
    AssetLabelService,
    AssetService,
    DuplicateAssetPathError,
    FileSystemAssetResolver,
    S3AssetResolver,
)
from membership.web.auth import requires_auth, requires_auth_permissions
from membership.web.util import BadRequest

logger = logging.getLogger(__file__)

asset_api = Blueprint('asset_api', __name__)
asset_tag_service = AssetLabelService()

if config.ASSETS_ENGINE == 's3':
    s3_client = boto3.client('s3')
    asset_resolver = S3AssetResolver(
        client=s3_client,
        bucket=config.ASSETS_BUCKET,
        path_prefix=config.ASSETS_PATH_PREFIX,
        region=config.ASSETS_BUCKET_REGION
    )
else:
    from os.path import dirname, join, normpath
    root_dir = normpath(join(dirname(__file__), '..', '..'))
    asset_resolver = FileSystemAssetResolver(
        join(root_dir, 'mnt', 'static'),
        config.ASSETS_PATH_PREFIX
    )
asset_service = AssetService(asset_tag_service, asset_resolver)


@asset_api.route('/assets', methods=['GET'])
@requires_auth()
def get_assets(ctx: PrincipalAuthContext):
    filter_ids_query = \
        request.args.get('id') or request.args.get('ids') or request.args.get('filter_ids')
    filter_ids = set(filter_ids_query.split(',')) if filter_ids_query else None
    filter_labels_query = \
        request.args.get('label') or request.args.get('labels') or request.args.get('filter_labels')
    filter_labels = set(filter_labels_query.split(',')) if filter_labels_query else None
    filter_paths_query = \
        request.args.get('path') or request.args.get('paths') or request.args.get('filter_paths')
    filter_paths = set(filter_paths_query.split(',')) if filter_paths_query else None
    assets = asset_service.search(
        ctx.session,
        filter_labels=filter_labels,
        filter_ids=filter_ids,
        filter_paths=filter_paths,
    )
    resolved_assets = (asset_service.resolve(asset) for asset in assets)
    return jsonify({
        'status': 'success',
        'data': [format_resolved_asset(asset) for asset in resolved_assets]
    })


@asset_api.route('/assets/upload_url', methods=['GET'])
@requires_auth_permissions(PmAssetsAdmin)
def get_upload_url(ctx: PrincipalAuthContext):
    relative_path = request.args.get('relative_path')
    if not relative_path:
        return BadRequest('Missing "relative_path" query parameter')
    url = asset_resolver.get_upload_url(relative_path)
    return jsonify({
        'status': 'success',
        'data': {
            'url': url
        }
    })


@asset_api.route('/assets/view_url', methods=['GET'])
@requires_auth_permissions(PmAssetsAdmin)
def get_view_url(ctx: PrincipalAuthContext):
    relative_path = request.args.get('relative_path')
    if not relative_path:
        return BadRequest('Missing "relative_path" query parameter')
    # TODO: Do validation of authorization and existence of the asset in s3
    url = asset_resolver.get_download_url(relative_path)
    return jsonify({
        'status': 'success',
        'data': {
            'url': url
        }
    })


@asset_api.route('/assets/upload', methods=['PUT'])
@requires_auth_permissions(PmAssetsAdmin)
def upload_asset(ctx: PrincipalAuthContext):
    relative_path = request.args.get('relative_path')
    if not relative_path:
        return BadRequest('Missing "relative_path" query parameter')
    asset_resolver.save(relative_path, cast(IO, request.stream))
    return jsonify({
        'status': 'success'
    })


@asset_api.route('/assets', methods=['PUT', 'POST'])
@requires_auth_permissions(PmAssetsAdmin)
def save_asset(ctx: PrincipalAuthContext):
    body = request.json
    asset: Asset
    try:
        validate(body, {
            'asset_type': 'required',
            'content_type': 'required',
        })
        asset_type = AssetType[body['asset_type']]
        if asset_type is AssetType.EXTERNAL_URL:
            validate(body, {'external_url': 'required'})
            external_url = body.get('external_url')
            if not external_url:
                return BadRequest(
                    f"'external_url' cannot be empty when 'asset_type' is {asset_type}."
                )
            asset = asset_service.find_by_external_url(ctx.session, external_url) or Asset()
            if not asset.external_url:
                asset.asset_type = asset_type
                asset.external_url = external_url
        elif asset_type is AssetType.PRESIGNED_URL:
            validate(body, {'relative_path': 'required'})
            relative_path = body.get('relative_path')
            if not relative_path:
                return BadRequest(
                    f"'relative_path' cannot be empty when 'asset_type' is {asset_type}."
                )
            asset = asset_service.find_by_relative_path(ctx.session, relative_path) or Asset()
            if not asset.relative_path:
                asset.asset_type = asset_type
                asset.relative_path = relative_path
        else:
            raise ValidationError(f"Unrecognized 'asset_type' value: '{asset_type}'")
    except ValidationError as ve:
        return BadRequest(ve.message)

    labels_list = body.get('labels')
    labels = set(str(lbl) for lbl in labels_list) if labels_list else None
    asset.content_type = body.get('content_type')
    asset.title = body.get('title')

    try:
        asset_service.update(ctx.session, asset, labels)
    except DuplicateAssetPathError as err:
        return BadRequest(err.msg)

    return jsonify({
        'status': 'success',
        'data': format_resolved_asset(asset_service.resolve(asset))
    })


@asset_api.route('/asset/<int:asset_id>', methods=['DELETE'])
@requires_auth_permissions(PmAssetsAdmin)
def delete_asset(ctx: PrincipalAuthContext, asset_id: int):
    asset_service.delete_by_id(ctx.session, asset_id)
    return jsonify({
        'status': 'success'
    })
