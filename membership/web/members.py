import csv
import datetime
import logging
import os
import zipfile
from io import BytesIO, StringIO
from typing import List, Optional, cast  # NOQA: F401

from flask import Blueprint, jsonify, request
from sqlalchemy.exc import IntegrityError, SQLAlchemyError
from sqlalchemy.orm import joinedload
from sqlalchemy.sql.functions import now

import config  # NOQA
import membership.util.shims  # NOQA
from config.auth_config import (
    ENABLE_FEATURE_AUTO_IMPORT_ENDPOINT,
    MAILGUN_HTTP_WEBHOOK_KEY,
)
from jobs.auth0_invites import SendAuth0Invites
from jobs.import_membership import ImportNationalMembership
from jobs.roster.session_cache import RosterImportSessionCache
from membership.database.base import Session
from membership.database.models import (  # NOQA: F401
    ImportEventType,
    Meeting,
    Member,
    Role,
    Tag,
    TagAssociation,
)
from membership.models import MemberAuthContext
from membership.repos import MeetingRepo
from membership.schemas.rest.members import (
    add_email_address_request_schema,
    add_member_role_request_schema,
    add_or_remove_tags,
    add_phone_number_request_schema,
    delete_email_address_request_schema,
    delete_phone_number_request_schema,
    format_eligible_member_list,
    format_email_addresses,
    format_member_basics,
    format_member_full,
    format_member_query_result,
    format_merge_member_flags,
    format_phone_numbers_full,
    update_member_details_request_schema,
    update_notes_request_schema,
)
from membership.services.attendee_service import AttendeeService
from membership.services.crm.crm_service import CrmService
from membership.services.eligibility import SanFranciscoEligibilityService
from membership.services.errors import NotFoundError
from membership.services.gdrive_service import GdriveService
from membership.services.mailchimp.mailchimp_action_generator import (
    MailchimpActionGenerator,
)
from membership.services.mailchimp.mailchimp_service import MailchimpService
from membership.services.meeting_service import MeetingService
from membership.services.member_service import MemberService
from membership.services.onboarding.onboarding_service import OnboardingService
from membership.services.onboarding.onboarding_status import OnboardingStatus
from membership.services.phone_number_service import PhoneNumberExistsError
from membership.services.role_service import RoleService
from membership.services.slack.slack_service import SlackService
from membership.util.email.mailgun_email_connector import MailgunEmailConnector
from membership.util.logging import LoggerIO
from membership.web.auth import CreateAuth0UserError, DEPRECATED_requires_legacy_auth
from membership.web.util import (
    BadRequest,
    Conflict,
    Forbidden,
    NotAcceptable,
    NotFound,
    NotImplemented,
    Ok,
    ServerError,
    Unauthorized,
    requires_json,
    validate_request,
)

logger = logging.getLogger(__name__)
member_api = Blueprint("member_api", __name__)
meeting_service = MeetingService()
meeting_repository = MeetingRepo(Meeting)
attendee_service = AttendeeService()
eligibility_service = SanFranciscoEligibilityService(
    meeting_repository, attendee_service
)
if config.ENABLE_GDRIVE:
    gdrive_service = GdriveService(credentials_file=config.GDRIVE_CREDENTIALS_FILE)
role_service = RoleService()
mailchimp_service = MailchimpService()
mailchimp_action_gen = MailchimpActionGenerator()
crm_service = CrmService()
slack_service = SlackService()
onboarding_service = OnboardingService(
    mailchimp_service, mailchimp_action_gen, crm_service, slack_service)
member_service = MemberService(eligibility_service, role_service, onboarding_service)


@member_api.route("/member/list", methods=["GET"])
@DEPRECATED_requires_legacy_auth("admin")
def get_members(ctx: MemberAuthContext):
    member_query_result = member_service.query(query_str=None, session=ctx.session)

    return jsonify(format_member_query_result(member_query_result, ctx.az))


@member_api.route("/member/search", methods=["GET"])
@DEPRECATED_requires_legacy_auth()
def search_members(ctx: MemberAuthContext):
    if not any(role.role == "admin" for role in ctx.requester.roles):
        return Forbidden("must be admin to query")

    try:
        page_size_param = request.args.get("page_size")
        cursor_param = request.args.get("cursor")
        query = request.args.get("query")
        page_size = int(page_size_param) if page_size_param is not None else None
        cursor = int(cursor_param) if cursor_param is not None else None
    except ValueError:
        return BadRequest("unable to parse pagination params (page_size, cursor)")

    member_query_result = member_service.query(
        cursor=cursor, page_size=page_size, query_str=query, session=ctx.session
    )
    return jsonify(format_member_query_result(member_query_result, ctx.az))


@member_api.route("/member", methods=["GET"])
@DEPRECATED_requires_legacy_auth()
def get_member(ctx: MemberAuthContext):
    member = format_member_basics(ctx.requester, ctx.az)
    return jsonify(member)


# TODO(ageiduschek): Kill this code path once we support
# pagination results on the frontend
def get_members_list_deprecated(ctx: MemberAuthContext):
    members_result = member_service.all(ctx.session)
    eligible_members = eligibility_service.members_as_eligible_to_vote(
        ctx.session, members_result.members
    )
    return format_eligible_member_list(ctx.az)(eligible_members)


@member_api.route("/member/details", methods=["GET"])
@DEPRECATED_requires_legacy_auth()
def get_member_details(ctx: MemberAuthContext):
    if ctx.az.member_id != ctx.requester.id:
        return Unauthorized("User cannot request other members' personal information.")

    member = format_member_full(ctx.session, ctx.requester, ctx.az)
    return jsonify(member)


@member_api.route("/admin/member/details", methods=["GET"])
@DEPRECATED_requires_legacy_auth("admin")
def get_member_info(ctx: MemberAuthContext):
    other_member = ctx.session.query(Member).get(request.args["member_id"])
    return jsonify(format_member_full(ctx.session, other_member, ctx.az))


@member_api.route("/member", methods=["POST"])
@DEPRECATED_requires_legacy_auth("admin")
@requires_json
def add_member(ctx: MemberAuthContext):
    email_address: Optional[str] = request.json["email_address"].strip() or None
    first_name: Optional[str] = request.json["first_name"].strip() or None
    last_name: Optional[str] = request.json["last_name"].strip() or None
    phone_number: Optional[str] = request.json.get("phone_number")
    phone_numbers: Optional[List[str]] = request.json.get("phone_numbers")
    make_member = request.json.get("give_chapter_member_role", False)
    if not email_address and not (first_name and last_name):
        return BadRequest(
            "You must supply either an email or a full name to add a member"
        )

    upgraded = False
    try:
        member = member_service.create_member(
            ctx.session,
            email_address=email_address,
            first_name=first_name,
            last_name=last_name,
            phone_number=phone_number,
            phone_numbers=phone_numbers,
            defer_commit=False,
        )

        if make_member:
            upgraded = member_service.upgrade_to_national_member(member, True, ctx.session)

        results = onboarding_service.onboard_member(member, "Admin", make_member)
        logger.info(results.to_json())
    except IntegrityError:
        return Conflict(f"Member with email already exists: {email_address}")
    except CreateAuth0UserError as e:
        logger.error(e.message)
        return ServerError("Error creating user in Auth0")

    return jsonify(
        {
            "status": "success",
            "data": {
                "member": format_member_basics(member, ctx.az),
                "account_created": results.auth0_result == OnboardingStatus.SUCCESS,
                "email_sent": results.portal_email_result == OnboardingStatus.SUCCESS,
                "role_created": upgraded
            },
        }
    )


@member_api.route("/member", methods=["PATCH", "PUT"])
@DEPRECATED_requires_legacy_auth()
@requires_json
def update_member(ctx: MemberAuthContext):
    member = ctx.requester

    if request.json.get("do_not_call", None) is not None:
        member.do_not_call = request.json["do_not_call"]
    if request.json.get("do_not_email", None) is not None:
        member.do_not_email = request.json["do_not_email"]

    ctx.session.add(member)
    ctx.session.commit()

    return jsonify(
        {
            "status": "success",
            "data": format_member_full(ctx.session, ctx.requester, ctx.az),
        }
    )


@member_api.route("/member/upgrade/<int:member_id>", methods=["PUT"])
@DEPRECATED_requires_legacy_auth("admin")
def upgrade_to_member(ctx: MemberAuthContext, member_id: int):
    found_member = member_service.find_by_id(ctx.session, member_id)
    if not found_member:
        return NotFound()

    upgraded = member_service.upgrade_to_national_member(found_member, True, ctx.session)
    results = onboarding_service.onboard_member(found_member, "Upgrade Existing", True)

    return jsonify(
        {
            "status": "success",
            "data": {
                "auth0_account_created": results.auth0_result.status == OnboardingStatus.SUCCESS,
                "email_sent": results.portal_email_result == OnboardingStatus.SUCCESS,
                "role_created": upgraded,
            },
        }
    )


@member_api.route("/member/downgrade/<int:member_id>", methods=["POST"])
@DEPRECATED_requires_legacy_auth("admin")
def cancel_dues_override_member(ctx: MemberAuthContext, member_id: int):
    member = member_service.find_by_id(ctx.session, member_id)
    if not member:
        return NotFound()

    member.dues_paid_overridden_on = None
    ctx.session.commit()

    return jsonify(
        {
            "status": "success"
        }
    )


@member_api.route("/member/suspend/<int:member_id>", methods=["POST"])
@DEPRECATED_requires_legacy_auth("admin")
def suspend_member(ctx: MemberAuthContext, member_id: int):
    member = member_service.find_by_id(ctx.session, member_id)
    if not member:
        return NotFound()

    member.suspended_on = now()
    ctx.session.commit()

    return jsonify(
        {
            "status": "success"
        }
    )


@member_api.route("/member/exit/<int:member_id>", methods=["POST"])
@DEPRECATED_requires_legacy_auth("admin")
def exit_member(ctx: MemberAuthContext, member_id: int):
    member = member_service.find_by_id(ctx.session, member_id)
    if not member:
        return NotFound()

    member.left_chapter_on = now()
    member.do_not_email = True
    member.do_not_call = True
    ctx.session.commit()

    return jsonify(
        {
            "status": "success"
        }
    )


@member_api.route("/admin/member", methods=["PATCH"])
@DEPRECATED_requires_legacy_auth("admin")
@requires_json
@validate_request(update_member_details_request_schema)
def update_member_details(ctx: MemberAuthContext):
    member = ctx.session.query(Member).get(request.args["member_id"])
    json_body = request.get_json(silent=True, force=True)

    # TODO use the existing schema and setattr / getattr
    if json_body.get("first_name") is not None:
        member.first_name = json_body.get("first_name")
    if json_body.get("last_name") is not None:
        member.last_name = json_body.get("last_name")
    if json_body.get("pronouns") is not None:
        member.pronouns = json_body.get("pronouns")
    if json_body.get("address") is not None:
        member.address = json_body.get("address")
    if json_body.get("city") is not None:
        member.city = json_body.get("city")
    if json_body.get("zipcode") is not None:
        member.zipcode = json_body.get("zipcode")
    if json_body.get("biography") is not None:
        member.biography = json_body.get("biography")
    if json_body.get("do_not_call") is not None:
        member.do_not_call = json_body.get("do_not_call")
    if json_body.get("do_not_email") is not None:
        member.do_not_email = json_body.get("do_not_email")

    ctx.session.add(member)
    ctx.session.commit()

    return jsonify(
        {
            "status": "success",
            "data": format_member_full(ctx.session, ctx.requester, ctx.az),
        }
    )


@member_api.route("/import", methods=["PUT"])
@DEPRECATED_requires_legacy_auth("admin")
def import_members(ctx: MemberAuthContext):
    import_file = request.files.get("file").read().decode("utf-8")
    rows = csv.DictReader(StringIO(import_file))
    job = ImportNationalMembership()
    output = LoggerIO(
        logging.INFO, logger=logging.getLogger("membership.web.import_members")
    )

    csv_records = job.parse_records(iter(rows), output)

    cache = RosterImportSessionCache(Session)

    results = job.import_all(
        csv_records,
        cache,
        SendAuth0Invites(),
        out=output)

    cache = cache.refresh_session()
    member_cache = {m.id: m for m in cache.chapter_records}
    return jsonify({"status": "success", "data": results.to_json(member_cache)})


@member_api.route("/import/auto/natl", methods=["POST"])
def import_national_members_from_email():
    """
    Performs an import from a forwarded national membership email.

    Optionally uploads the attached csv to external storage if configured
    """

    if not ENABLE_FEATURE_AUTO_IMPORT_ENDPOINT or MAILGUN_HTTP_WEBHOOK_KEY is None:
        return NotImplemented("Automatic import feature not enabled")

    if (
        "signature" not in request.form
        or "token" not in request.form
        or "timestamp" not in request.form
    ):
        return Unauthorized("No signature or token provided")

    if not MailgunEmailConnector.verify_signature(
        signing_key=MAILGUN_HTTP_WEBHOOK_KEY,
        signature=request.form["signature"],
        timestamp=request.form["timestamp"],
        token=request.form["token"],
    ):
        return Forbidden("Unauthorized API key given")

    if len(request.files) != 1:
        return NotAcceptable("Missing attachment")

    attachment = request.files["attachment-1"]
    filename = attachment.filename
    csv_filename = f"{os.path.splitext(filename)[0]}.csv"

    if filename is None:
        return NotAcceptable("Missing filename")

    try:
        if attachment.mimetype == "text/csv":
            bytes_body_reader = cast(BytesIO, attachment.stream)
        elif attachment.mimetype == "application/zip":
            with zipfile.ZipFile(attachment.stream, "r") as zipfp:
                csv_file = zipfp.read(csv_filename)
                bytes_body_reader = BytesIO(csv_file)
        else:
            return NotAcceptable("Unsupported attachment")
    except Exception as e:
        logger.exception("Malformed zip attachment", e)
        return NotAcceptable("Malformed zip attachment")

    response_body = {}

    if config.ENABLE_GDRIVE:
        dir_id = gdrive_service.get_dir_id(config.GDRIVE_NATL_DIR_NAME)
        if not dir_id:
            return ServerError("upload directory not accessible")
        gdrive_filename = (
            f"{datetime.datetime.now().strftime('%Y-%m-%d')}"
            "silicon_valley_membership_list.csv"
        )
        file_id = gdrive_service.upload_bytes_to_dir(
            body=bytes_body_reader,
            filename=gdrive_filename,
            dir_id=dir_id,
            mime_type="text/csv"
        )
        response_body['gdrive_result'] = {
            "file_id": file_id,
            "dir_id": dir_id,
            "filename": gdrive_filename,
            "dirname": config.GDRIVE_NATL_DIR_NAME
        }

    # merging logic here
    # try:
    #     participants = []
    #     csv_reader = csv.DictReader(TextIOWrapper(bytes_body_reader, encoding="utf-8"))
    #     for row in csv_reader:
    #         participant = ImportEventParticipant(
    #             first_name=row["first_name"],
    #             last_name=row["last_name"],
    #             email_address=row["Email"],
    #             city=row["Mailing_City"] or row["Billing_City"],
    #             zipcode=row["Mailing_Zip"] or row["Billing_Zip"],
    #         )

    #         phone_numbers = []
    #         if row["Mobile_Phone"] != "":
    #             phone_numbers.extend(
    #                 [num.strip() for num in row["Mobile_Phone"].split(",")]
    #             )
    #         if row["Home_Phone"] != "":
    #             phone_numbers.extend(
    #                 [num.strip() for num in row["Home_Phone"].split(",")]
    #             )
    #         if row["Work_Phone"] != "":
    #             phone_numbers.extend(
    #                 [num.strip() for num in row["Work_Phone"].split(",")]
    #             )
    #         participant.phone_numbers = phone_numbers

    #         participant.more_info = row
    #         participants.append(participant)
    # except KeyError as e:
    #     return NotAcceptable(f"Missing required columns: {e}")

    # if len(participants) == 0:
    #     return NotAcceptable("No participants found")

    # try:
    #     slug = f"""natl-csv-{datetime.datetime.now().isoformat(timespec='seconds')}"""
    #     title = f"""National CSV {datetime.datetime.now().strftime("%Y-%m-%d")}"""
    #     session = Session()
    #     event = attendance_service.create(
    #         session,
    #         slug=slug,
    #         title=title,
    #         event_info=None,  # None for now
    #         participants=participants,
    #         event_type=ImportEventType.DSAUSA_EMAIL,
    #         date_created=datetime.datetime.now(),
    #         defer_commit=False,
    #     )
    #     response_body["status"] = "success"
    #     response_body["event"] = format_single_event(event, participants, event.roster)

    #     return Ok(response_body)
    # except IntegrityError as e:
    #     logging.exception("Unable to create reviewable event", e)
    #     return ServerError("Unable to create reviewable event")
    # except Exception as e:
    #     logging.exception("unexpected error", e)
    #     return ServerError("Unexpected error")


@member_api.route("/admin", methods=["POST"])
@DEPRECATED_requires_legacy_auth("admin")
@requires_json
def make_admin(ctx: MemberAuthContext):
    member = (
        ctx.session.query(Member)
        .filter_by(email_address=request.json["email_address"])
        .one()
    )
    committee_id = request.json["committee"] if request.json["committee"] > 0 else None
    role = Role(member_id=member.id, role="admin", committee_id=committee_id)
    ctx.session.add(role)
    ctx.session.commit()
    return jsonify({"status": "success"})


@member_api.route("/member/role", methods=["POST"])
@DEPRECATED_requires_legacy_auth()
@requires_json
@validate_request(add_member_role_request_schema)
def add_role(ctx: MemberAuthContext):
    member_id = request.json.get("member_id", ctx.requester.id)
    cid = request.json["committee_id"]
    role_name = request.json["role"]
    committee_id = cid if cid is not None and cid > 0 else None
    ctx.az.verify_admin(committee_id)

    if not role_service.add(
        ctx.session,
        member_id=member_id,
        role=role_name,
        committee_id=committee_id,
        commit=True,
    ):
        return Conflict("Member already has this role")
    else:
        return jsonify({"status": "success"})


@member_api.route("/member/role", methods=["DELETE"])
@DEPRECATED_requires_legacy_auth()
@requires_json
def remove_role(ctx: MemberAuthContext):
    member_id = request.json.get("member_id", ctx.requester.id)
    role = request.json["role"]
    cid = request.json["committee_id"]
    committee_id = cid if cid is not None and cid > 0 else None
    ctx.az.verify_admin(committee_id)

    if not role_service.remove(
        ctx.session, member_id=member_id, role=role, committee_id=committee_id,
    ):
        return NotFound("Member does not have this role")
    else:
        return jsonify({"status": "success"})


@member_api.route("/member/<int:member_to_keep_id>/merge", methods=["POST"])
@DEPRECATED_requires_legacy_auth("admin")
def merge_member(member_to_keep_id: int, ctx: MemberAuthContext):
    """
    Performs a merge of two members.

    This requires modifying or deleting a number of records across many tables. Any record belonging
    to the member_to_remove that also "duplicates" a record belonging to the member_to_keep will be
    removed. If said record is not a "duplicate" it will be kept and member_id will be updated to
    the member_to_keep_id. Each table has a unique definition for when a record "duplicates"
    another, see comments for method "perform_merge_for_all_tables" for further details.

    The merge will only be executed if 'force' is true or if there are no sensitive deletes that
    would be committed. Currently, the only sensitive deletes are:
        - There is a duplicate EligibleVoter record, as removing this record would alter vote
          counts.
        - There is a duplicate ProxyToken record, as removing this record would alter vote counts.

    If 'force' is false and a delete is flagged, the merge will not happen and the flagged items
    will be returned. Otherwise, method will return None.

    Parameters:
    member_id_to_keep (int): The member_id being merged into. These records will remain after the
                             merge.
    member_to_remove_id (int): The member_id of the user that will be merged (i.e. will no longer
                               exist after merge operation is complete)
    force (bool): If false, will not perform merge if any deteles are flagged. If true, will perform
                  merge regardless of any flagged deletes.
    """
    member_to_remove_arg = request.args.get("member_to_remove_id")
    force_arg = request.args.get("force")

    if member_to_remove_arg is None:
        return BadRequest(
            "Caller must supply a member_to_remove_id as a query parameter."
        )
    if force_arg is None:
        force_arg = "False"

    try:
        member_to_remove_id = int(member_to_remove_arg)
    except ValueError:
        return BadRequest(
            "Caller must supply an integer id for member_to_remove_id parameter."
        )
    force = force_arg.lower() == "true"

    try:
        data = member_service.merge_member_fetch_data(
            member_to_keep_id, member_to_remove_id, ctx.session
        )
        if data.member_to_keep is None:
            return NotFound(
                "The provided member to keep does not exist. Merge could not be performed."
            )
        if data.member_to_remove is None:
            return NotFound(
                "The provided member to remove does not exist. Merge could not be performed."
            )

        if not force:
            flags = member_service.check_for_merge_member_flags(
                member_to_keep_id, member_to_remove_id, data
            )
            if flags is not None:
                return jsonify(format_merge_member_flags(flags))

        res = member_service.perform_merge_for_all_tables(
            member_to_keep_id, member_to_remove_id, data, ctx.session
        )
        return jsonify({"number_of_updates": res[0], "number_of_deletions": res[1]})
    except Exception as e:
        logger.exception(e)
        return ServerError(str(e))


@member_api.route("/admin/member/notes", methods=["PUT"])
@DEPRECATED_requires_legacy_auth("admin")
@requires_json
@validate_request(update_notes_request_schema)
def update_notes(ctx: MemberAuthContext):
    notes: str = request.json["notes"]
    member = ctx.session.query(Member).get(request.args["member_id"])
    try:
        member.notes = notes.strip()
        ctx.session.add(member)
        ctx.session.commit()
        return jsonify({"notes": member.notes})
    except SQLAlchemyError:
        ctx.session.rollback()
        return ServerError("Error when updating notes for member")


@member_api.route("/admin/member/phone_numbers", methods=["POST"])
@DEPRECATED_requires_legacy_auth("admin")
@requires_json
@validate_request(add_phone_number_request_schema)
def add_phone_number(ctx: MemberAuthContext):
    member = ctx.session.query(Member).get(request.args["member_id"])
    json_response = request.get_json()

    if member is None:
        return NotFound("Member does not exist")

    try:
        phone_numbers = member_service.add_phone_number(
            ctx.session,
            member=member,
            phone_number=json_response.get("phone_number"),
            name=json_response.get("name"),
            defer_commit=False,
        )
        return jsonify(format_phone_numbers_full(phone_numbers))
    except (IntegrityError, PhoneNumberExistsError) as e:
        ctx.session.rollback()
        logging.exception("error adding phone number", e)
        return Conflict(
            f"Member with phone number already exists: {request.json['phone_number']}"
        )


@member_api.route("/admin/member/phone_numbers", methods=["DELETE"])
@DEPRECATED_requires_legacy_auth("admin")
@requires_json
@validate_request(delete_phone_number_request_schema)
def delete_phone_number(ctx: MemberAuthContext):
    member = ctx.session.query(Member).get(request.args["member_id"])

    if member is None:
        return NotFound("Member does not exist")

    try:
        phone_numbers = member_service.delete_phone_number(
            ctx.session,
            member=member,
            phone_number=request.json['phone_number'],
            defer_commit=False,
        )
        return jsonify(format_phone_numbers_full(phone_numbers))
    except KeyError:
        return NotFound(
            f"Member does not have this phone number: {request.json['phone_number']}"
        )
    except Exception as e:
        ctx.session.rollback()
        logging.exception("error deleting phone number", e)
        return Conflict(f"Error deleting phone number {request.json['phone_number']}")

    return jsonify(format_phone_numbers_full(member.phone_numbers))


@member_api.route("/admin/member/email_addresses", methods=["POST"])
@DEPRECATED_requires_legacy_auth("admin")
@requires_json
@validate_request(add_email_address_request_schema)
def add_email_address(ctx: MemberAuthContext):
    member = ctx.session.query(Member).get(request.args["member_id"])
    json_response = request.get_json()

    if member is None:
        return NotFound("Member does not exist")

    try:
        addresses = member_service.add_email_address(
            ctx.session,
            member=member,
            email_address=json_response.get("email_address"),
            name=json_response.get("name"),
            preferred=json_response.get("preferred") or False,
            defer_commit=False,
        )
        return jsonify(format_email_addresses(addresses))
    except IntegrityError as e:
        logging.exception("error adding email address", e)
        ctx.session.rollback()
        return Conflict(
            f"Member with email_address already exists: {request.json['email_address']}"
        )


@member_api.route("/admin/member/email_addresses", methods=["DELETE"])
@DEPRECATED_requires_legacy_auth("admin")
@requires_json
@validate_request(delete_email_address_request_schema)
def delete_email_address(ctx: MemberAuthContext):
    member = ctx.session.query(Member).get(request.args["member_id"])

    if member is None:
        return NotFound("Member does not exist")

    try:
        addresses = member_service.delete_email_address(
            ctx.session,
            member=member,
            email_address=request.json["email_address"],
            defer_commit=False,
        )
        return jsonify(format_email_addresses(addresses))
    except (IntegrityError, NotFoundError):
        ctx.session.rollback()
        return NotFound(
            f"Couldn't find member with email address: {request.json['email_address']}"
        )


@member_api.route("/member/<int:member_id>/tags", methods=["PUT"])
@DEPRECATED_requires_legacy_auth("admin")
@requires_json
@validate_request(add_or_remove_tags)
def add_tags(member_id: int, ctx: MemberAuthContext):
    member = ctx.session.query(Member) \
        .options(joinedload(Member.tags)) \
        .get(member_id)

    if not member:
        return NotFound("Member does not exist")

    existing = {tag for tag in member.tags}

    try:
        tags_to_add = [tag for tag in request.json["tags"] if tag not in existing]
        for tag_name in tags_to_add:
            db_tag = ctx.session.query(Tag).filter(Tag.name == tag_name).first()

            if not db_tag:
                new_tag = Tag(name=tag_name)
                new_tassoc = TagAssociation(tag=new_tag, member=member)
                ctx.session.add_all([new_tag, new_tassoc])
                member.tag_associations.append(new_tassoc)

        ctx.session.commit()

        return Ok()
    except SQLAlchemyError as e:
        ctx.session.rollback()
        logging.exception(e)
        return ServerError("Error when updating notes for member")


@member_api.route("/member/<int:member_id>/tags", methods=["DELETE"])
@DEPRECATED_requires_legacy_auth("admin")
@requires_json
@validate_request(add_or_remove_tags)
def remove_tags(member_id: int, ctx: MemberAuthContext):
    member: Member = (
        ctx.session.query(Member).options(joinedload(Member.tags)).get(member_id)
    )

    if not member:
        return NotFound("Member does not exist")

    tags_to_remove = request.json["tags"]

    try:
        for tag in tags_to_remove:
            tag_rec = next(
                filter(lambda t: t.tag.name == tag, member.tag_associations), None
            )
            if tag_rec:
                member.tag_associations.remove(tag_rec)

        ctx.session.commit()

        return Ok()
    except SQLAlchemyError:
        ctx.session.rollback()
        return ServerError("Error when updating notes for member")
