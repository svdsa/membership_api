from http import HTTPStatus
from typing import Tuple, Union

from flask import Blueprint
from flask.helpers import url_for
from flask.wrappers import Response

from membership.auth.permissions import (
    PmContactsAdmin,
    PmCustomFieldsAdmin,
    PmTagsAdmin,
    PmTagsCreate,
)
from membership.errors.exceptions import (
    BulkResourceMissingError,
    ParameterValidationError,
)
from membership.models.authz import PrincipalAuthContext
from membership.schemas.formatters.contacts import format_contact
from membership.schemas.formatters.custom_fields import format_custom_field_definition
from membership.schemas.formatters.tags import format_tag, format_tags_list
from membership.schemas.rest.contacts import IdContact, SxContact
from membership.schemas.rest.custom_fields import IdCustomField, SxCustomFieldDefinition
from membership.schemas.rest.tags import (
    IdTag,
    SxListTagsQuery,
    SxTag,
    SxTagAttachContactRequest,
    SxTagAttachCustomFieldRequest,
    SxTagCreateRequest,
    SxTagUpdateRequest,
)
from membership.services import (
    contact_service,
    contact_tag_associations,
    custom_field_service,
    tag_service,
)
from membership.services.custom_field_tag_associations import (
    CustomFieldTagAssociationService,
)
from membership.web.auth import requires_auth_permissions
from membership.web.util import make_paginated_list_response, validate_with_pydantic

tag_api = Blueprint('tag_api', __name__)


@tag_api.route("/tags", methods=["GET"])
# TODO this permission is overly restrictive
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmTagsAdmin, hide_presence=False)
def list_tags(ctx: PrincipalAuthContext, query: SxListTagsQuery) -> Response:
    contacts = tag_service.list_(
        ctx.session,
        limit=query.limit,
        starting_after=query.starting_after,
        ending_before=query.ending_before,
    )

    return make_paginated_list_response(
        format_tags_list(contacts, url_for(".list_tags", **query.dict())),
        limit=query.limit,
        next_cursor=contacts.next_cursor,
        prev_cursor=contacts.prev_cursor,
        route="tag_api.list_tags",
    )


@tag_api.route("/tags", methods=["POST"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmTagsCreate, hide_presence=False)
def create_tag(
    ctx: PrincipalAuthContext, body: SxTagCreateRequest
) -> Union[Tuple[SxTag, int], Response]:
    name = body.name
    contact_ids = body.contacts
    custom_field_ids = body.custom_fields

    tag = tag_service.create_or_raise(ctx.session, tag=name)

    try:
        if contact_ids is not None and len(contact_ids) > 0:
            contacts = contact_service.bulk_from_id_or_raise(ctx.session, contact_ids)
            for contact in contacts:
                contact_tag_associations.attach_to_contact(
                    ctx.session, contact=contact, tag=tag
                )
    except BulkResourceMissingError:
        raise ParameterValidationError(object="tag", param="contacts")

    try:
        if custom_field_ids is not None and len(custom_field_ids) > 0:
            custom_fields = custom_field_service.bulk_from_id_or_raise(
                ctx.session, custom_field_ids
            )
            for custom_field in custom_fields:
                CustomFieldTagAssociationService.attach_to_custom_field(
                    ctx.session, custom_field=custom_field, tag=tag
                )
    except BulkResourceMissingError:
        raise ParameterValidationError(object="tag", param="custom_fields")

    ctx.session.commit()

    return format_tag(tag), HTTPStatus.CREATED


@tag_api.route("/tags/<tag_id>", methods=["GET"])
# TODO this permission is overly restrictive
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmTagsAdmin, hide_presence=False)
def get_tag(
    ctx: PrincipalAuthContext, tag_id: IdTag
) -> Union[Tuple[SxTag, int], Response]:
    tag = tag_service.from_id_or_raise(ctx.session, tag_id)
    return format_tag(tag), HTTPStatus.OK


@tag_api.route("/tags/<tag_id>", methods=["PATCH"])
# TODO this permission is overly restrictive
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmTagsAdmin, hide_presence=False)
def update_tag(
    ctx: PrincipalAuthContext, tag_id: IdTag, body: SxTagUpdateRequest
) -> Union[Tuple[SxTag, int], Response]:
    updated_tag = tag_service.update_from_id(
        ctx.session, tag_id, update_request=body, defer_commit=False
    )
    return format_tag(updated_tag), HTTPStatus.OK


@tag_api.route("/tags/<tag_id>/contacts", methods=["POST"])
# TODO this permission is overly restrictive
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmTagsAdmin, hide_presence=False)
def attach_contact_to_tag(
    ctx: PrincipalAuthContext, tag_id: IdTag, body: SxTagAttachContactRequest
) -> Union[Tuple[SxContact, int], Response]:
    contact_id = body.contact
    _, contact = contact_tag_associations.attach_to_contact_from_id(
        ctx.session, tag_id=tag_id, contact_id=contact_id, defer_commit=False
    )
    return format_contact(contact), HTTPStatus.OK


@tag_api.route("/tags/<tag_id>/contacts/<contact_id>", methods=["DELETE"])
# TODO this permission is overly restrictive
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmTagsAdmin, PmContactsAdmin, hide_presence=False)
def detach_contact_from_tag(
    ctx: PrincipalAuthContext, tag_id: IdTag, contact_id: IdContact
) -> Union[Tuple[SxContact, int], Response]:
    _, contact = contact_tag_associations.detach_contact_from_id(
        ctx.session, tag_id=tag_id, contact_id=contact_id, defer_commit=False
    )
    return format_contact(contact), HTTPStatus.OK


@tag_api.route("/tags/<tag_id>/custom_fields", methods=["POST"])
# TODO this permission is overly restrictive
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmTagsAdmin, PmCustomFieldsAdmin, hide_presence=False)
def attach_custom_field_to_tag(
    ctx: PrincipalAuthContext, tag_id: IdTag, body: SxTagAttachCustomFieldRequest
) -> Union[Tuple[SxCustomFieldDefinition, int], Response]:
    custom_field_id = body.custom_field
    (
        _,
        custom_field,
    ) = CustomFieldTagAssociationService.attach_to_custom_field_from_id(
        ctx.session,
        tag_id=tag_id,
        custom_field_id=custom_field_id,
        defer_commit=False,
    )
    return format_custom_field_definition(custom_field), HTTPStatus.OK


@tag_api.route("/tags/<tag_id>/custom_fields/<custom_field_id>", methods=["DELETE"])
# TODO this permission is overly restrictive
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmTagsAdmin, PmCustomFieldsAdmin, hide_presence=False)
def detach_custom_field_from_tag(
    ctx: PrincipalAuthContext, tag_id: IdTag, custom_field_id: IdCustomField
) -> Union[Tuple[SxCustomFieldDefinition, int], Response]:
    _, field = CustomFieldTagAssociationService.detach_custom_field_from_id(
        ctx.session,
        tag_id=tag_id,
        custom_field_id=custom_field_id,
        defer_commit=False,
    )
    return format_custom_field_definition(field), HTTPStatus.OK


@tag_api.route("/tags/<tag_id>", methods=["DELETE"])
# TODO this permission is overly restrictive
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmTagsAdmin, hide_presence=False)
def delete_tag(
    ctx: PrincipalAuthContext, tag_id: IdTag
) -> Union[Tuple[SxTag, int], Response]:
    deleted_tag = tag_service.delete_from_id(ctx.session, tag_id, defer_commit=False)
    return format_tag(deleted_tag), HTTPStatus.OK
