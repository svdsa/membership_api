from http import HTTPStatus
from typing import Tuple, Union

import structlog
from flask import Blueprint, request
from flask.helpers import url_for
from flask.wrappers import Response
from flask_principal import Permission
from pydantic.main import BaseModel

from membership.auth.errors import InsufficientPermissionsError, InvalidUserError
from membership.auth.needs import (
    NdContactsAdmin,
    NdContactsPrivilegedInfoRead,
    NdFullAdmin,
)
from membership.auth.needs_derived import read_self_contact_need
from membership.auth.permissions import (
    PmContactsArchive,
    PmContactsCreate,
    PmContactsList,
    build_get_contact_custom_fields_permission,
    build_get_contact_email_addresses_permission,
    build_get_contact_mailing_addresses_permission,
    build_get_contact_permission,
    build_get_contact_phone_numbers_permission,
    build_get_contact_tags_permission,
    build_update_contact_custom_fields_permission,
    build_update_contact_email_addresses_permission,
    build_update_contact_mailing_addresses_permission,
    build_update_contact_permission,
    build_update_contact_phone_numbers_permission,
    build_update_contact_tags_permission,
)
from membership.errors.exceptions import (
    CreateAlreadyExistsError,
    CreateExclusiveParameterConflictError,
    ParameterMissingError,
    UpdateTargetNotFoundError,
)
from membership.models.authz import PrincipalAuthContext
from membership.schemas.formatters.contacts import format_contact, format_contacts_list
from membership.schemas.formatters.custom_fields import (
    format_custom_field_association,
    format_custom_field_association_list,
)
from membership.schemas.formatters.email_addresses import (
    format_email_address,
    format_email_address_list,
)
from membership.schemas.formatters.phone_numbers import (
    format_phone_number,
    format_phone_number_list,
)
from membership.schemas.formatters.postal_addresses import (
    format_postal_address,
    format_postal_address_list,
)
from membership.schemas.formatters.tags import format_tag, format_tags_list
from membership.schemas.id import (
    IdContact,
    IdCustomField,
    IdEmailAddress,
    IdPhoneNumber,
    IdPostalAddress,
)
from membership.schemas.rest.contacts import (
    SxContactArchiveRequest,
    SxContactCreateRequest,
    SxContactUpdateRequest,
    SxListContactsQuery,
)
from membership.schemas.rest.custom_fields import (
    SxCustomFieldAssociation,
    SxCustomFieldAssociationCreateRequest,
    SxCustomFieldAssociationUpdateRequest,
    SxListCustomFieldAssociationsResponse,
)
from membership.schemas.rest.email_addresses import (
    SxEmailAddressArchiveRequest,
    SxEmailAddressCreateRequest,
    SxEmailAddressUpdateRequest,
    SxListEmailAddressesQuery,
)
from membership.schemas.rest.phone_numbers import (
    SxListPhoneNumbersQuery,
    SxPhoneNumberArchiveRequest,
    SxPhoneNumberCreateRequest,
    SxPhoneNumberUpdateRequest,
)
from membership.schemas.rest.postal_addresses import (
    SxListPostalAddressesQuery,
    SxPostalAddressArchiveRequest,
    SxPostalAddressCreateRequest,
    SxPostalAddressUpdateRequest,
)
from membership.schemas.rest.tags import (
    IdTag,
    SxContactTagAssociationRequest,
    SxListTagsResponse,
    SxTag,
)
from membership.services import (
    contact_custom_field_associations,
    contact_service,
    contact_tag_associations,
    email_address_service,
    phone_number_service,
    postal_address_service,
)
from membership.services.contact_custom_field_associations import (
    CustomFieldAssociationExistsError,
    InvalidCustomFieldValueError,
)
from membership.services.custom_field_service import CustomFieldNotFoundError
from membership.services.email_address_service import (
    ContactEmailAddressExistsError,
    EmailAddressExistsError,
    EmailAddressNotFoundError,
)
from membership.services.phone_number_service import (
    PhoneNumberExistsError,
    PhoneNumberNotFoundError,
)
from membership.services.postal_address_service import (
    ContactMailingAddressExistsError,
    PostalAddressNotFoundError,
)
from membership.services.tag_service import TagAssociationExistsError, TagNotFoundError
from membership.util.arrays import compact
from membership.util.pagination import PaginatedList
from membership.web.auth import requires_auth, requires_auth_permissions
from membership.web.errors import ErrorCode, ErrorResponse, ErrorType
from membership.web.util import make_paginated_list_response, validate_with_pydantic

contacts_api = Blueprint("contacts_api", __name__)
logger = structlog.stdlib.get_logger()


@contacts_api.route("/contacts", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmContactsList, hide_presence=False)
def list_contacts(ctx: PrincipalAuthContext, query: SxListContactsQuery) -> Response:
    contacts = contact_service.list_(
        ctx.session,
        limit=query.limit,
        starting_after=query.starting_after,
        ending_before=query.ending_before,
    )

    return make_paginated_list_response(
        format_contacts_list(contacts, url_for(".list_contacts", **query.dict())),
        limit=query.limit,
        next_cursor=contacts.next_cursor,
        prev_cursor=contacts.prev_cursor,
        route="contacts_api.list_contacts",
    )


@contacts_api.route("/contacts", methods=["POST"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmContactsCreate, hide_presence=False)
def create_contact(
    ctx: PrincipalAuthContext, body: SxContactCreateRequest
) -> Union[Tuple[BaseModel, int], Response]:
    try:
        contact = contact_service.create(
            ctx.session,
            create_request=body,
            defer_commit=False,
        )
        return (format_contact(contact), HTTPStatus.CREATED)

    except (EmailAddressExistsError, ContactEmailAddressExistsError):
        raise CreateExclusiveParameterConflictError(params="email_addresses")

    except CustomFieldNotFoundError:
        raise ParameterMissingError(params="custom_fields")

    except TagNotFoundError:
        raise ParameterMissingError(params="tags")


@contacts_api.route("/contacts/<id>", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def get_contact(
    ctx: PrincipalAuthContext, id: IdContact
) -> Union[Tuple[BaseModel, int], Response]:
    contact = contact_service.from_id_or_raise(ctx.session, id)
    permission = build_get_contact_permission(id, ctx.principal)

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    return format_contact(contact), HTTPStatus.OK


@contacts_api.route("/contacts/@me", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth(hide_presence=False)
def get_contact_for_self(
    ctx: PrincipalAuthContext,
) -> Union[Tuple[BaseModel, int], Response]:
    user = ctx.principal.user

    if user is None:
        raise InvalidUserError(hide_presence=False)

    contact_id = user.contact.id
    permission = Permission(
        *(
            compact(
                [
                    NdFullAdmin(),
                    NdContactsAdmin(),
                    NdContactsPrivilegedInfoRead(),
                    read_self_contact_need(IdContact(contact_id), ctx.principal),
                ]
            )
        )
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=False)

    contact = contact_service.from_id_or_raise(ctx.session, IdContact(contact_id))
    return format_contact(contact), HTTPStatus.OK


@contacts_api.route("/contacts/<id>", methods=["PATCH"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def update_contact(
    ctx: PrincipalAuthContext, id: IdContact, body: SxContactUpdateRequest
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_update_contact_permission(id, ctx.principal)

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    contact = contact_service.from_id_or_raise(ctx.session, id)
    updated_contact = contact_service.update(
        ctx.session, contact, update_request=body, defer_commit=False
    )
    return format_contact(updated_contact), HTTPStatus.OK


@contacts_api.route("/contacts/<id>/archive", methods=["PUT"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmContactsArchive)
def archive_contact(
    ctx: PrincipalAuthContext, id: IdContact, body: SxContactArchiveRequest
) -> Union[Tuple[BaseModel, int], Response]:
    archived_contact = contact_service.archive_from_id(
        ctx.session,
        id,
        archived=True,
        reason_archived=body.reason_archived,
        defer_commit=False,
    )
    return format_contact(archived_contact), HTTPStatus.OK


@contacts_api.route("/contacts/<id>/archive", methods=["DELETE"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmContactsArchive)
def unarchive_contact(
    ctx: PrincipalAuthContext, id: IdContact
) -> Union[Tuple[BaseModel, int], Response]:
    unarchived = contact_service.archive_from_id(
        ctx.session, id, archived=False, defer_commit=False
    )
    return format_contact(unarchived), HTTPStatus.OK


@contacts_api.route("/contacts/<id>/email_addresses", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def list_email_addresses_for_contact(
    ctx: PrincipalAuthContext, id: IdContact, query: SxListEmailAddressesQuery
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_get_contact_email_addresses_permission(id, ctx.principal)

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    email_addresses = email_address_service.list_(
        ctx.session,
        contact_id=id,
        show_archived=query.show_archived,
        limit=query.limit,
        starting_after=query.starting_after,
        ending_before=query.ending_before,
    )
    return make_paginated_list_response(
        format_email_address_list(
            email_addresses,
            url_for(".list_email_addresses_for_contact", id=id, **query.dict()),
        ),
        limit=query.limit,
        next_cursor=email_addresses.next_cursor,
        prev_cursor=email_addresses.prev_cursor,
        route="contacts_api.list_email_addresses_for_contact",
        route_args={"id": id},
    )


@contacts_api.route("/contacts/<id>/email_addresses", methods=["POST"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def create_email_address(
    ctx: PrincipalAuthContext, id: IdContact, body: SxEmailAddressCreateRequest
) -> Union[Tuple[BaseModel, int], Response]:
    try:
        permission = build_update_contact_email_addresses_permission(id, ctx.principal)

        if not permission.can():
            raise InsufficientPermissionsError(hide_presence=True)

        contact = contact_service.from_id_or_raise(ctx.session, id)
        email_address = email_address_service.create(
            ctx.session,
            create=body,
            contact=contact,
            defer_commit=False,
        )
        return (
            format_email_address(email_address),
            HTTPStatus.CREATED,
        )

    except EmailAddressExistsError:
        raise CreateAlreadyExistsError(object="email_address")


@contacts_api.route(
    "/contacts/<contact_id>/email_addresses/<email_address_id>", methods=["GET"]
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def get_email_address_for_contact(
    ctx: PrincipalAuthContext, contact_id: IdContact, email_address_id: IdEmailAddress
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_get_contact_email_addresses_permission(contact_id, ctx.principal)

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    email_address = email_address_service.from_id_or_raise(
        ctx.session, email_address_id
    )
    if email_address.contact.id != contact_id:
        raise EmailAddressNotFoundError()

    return (
        format_email_address(
            email_address,
        ),
        HTTPStatus.OK,
    )


@contacts_api.route(
    "/contacts/<contact_id>/email_addresses/<email_address_id>", methods=["PATCH"]
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def update_email_address_for_contact(
    ctx: PrincipalAuthContext,
    contact_id: IdContact,
    email_address_id: IdEmailAddress,
    body: SxEmailAddressUpdateRequest,
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_update_contact_email_addresses_permission(
        contact_id, ctx.principal
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    email_address = email_address_service.update_from_id(
        ctx.session,
        email_address_id,
        contact_id=contact_id,
        update_request=body,
        defer_commit=False,
    )

    return (
        format_email_address(
            email_address,
        ),
        HTTPStatus.OK,
    )


@contacts_api.route(
    "/contacts/<contact_id>/email_addresses/<email_address_id>/archive", methods=["PUT"]
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def archive_email_address_for_contact(
    ctx: PrincipalAuthContext,
    contact_id: IdContact,
    email_address_id: IdEmailAddress,
    body: SxEmailAddressArchiveRequest,
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_update_contact_email_addresses_permission(
        contact_id, ctx.principal
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    email_address = email_address_service.archive_from_id(
        ctx.session,
        email_address_id,
        contact_id=contact_id,
        reason_archived=body.reason_archived,
        archived=True,
        defer_commit=False,
    )

    return (
        format_email_address(
            email_address,
        ),
        HTTPStatus.OK,
    )


@contacts_api.route(
    "/contacts/<contact_id>/email_addresses/<email_address_id>/archive",
    methods=["DELETE"],
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def unarchive_email_address_for_contact(
    ctx: PrincipalAuthContext, contact_id: IdContact, email_address_id: IdEmailAddress
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_update_contact_email_addresses_permission(
        contact_id, ctx.principal
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    email_address = email_address_service.archive_from_id(
        ctx.session,
        email_address_id,
        contact_id=contact_id,
        reason_archived=None,
        archived=False,
        defer_commit=False,
    )

    return (
        format_email_address(
            email_address,
        ),
        HTTPStatus.OK,
    )


@contacts_api.route(
    "/contacts/<contact_id>/email_addresses/<email_address_id>/confirm",
    methods=["PUT", "DELETE"],
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def confirm_email_address_for_contact(
    ctx: PrincipalAuthContext, contact_id: IdContact, email_address_id: IdEmailAddress
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_update_contact_email_addresses_permission(
        contact_id, ctx.principal
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    email_address = email_address_service.mark_confirmed_from_id(
        ctx.session,
        email_address_id,
        contact_id=contact_id,
        confirmed=True if request.method == "PUT" else False,
        defer_commit=False,
    )

    return (
        format_email_address(
            email_address,
        ),
        HTTPStatus.OK,
    )


@contacts_api.route("/contacts/<id>/phone_numbers", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def list_phone_numbers_for_contact(
    ctx: PrincipalAuthContext, id: IdContact, query: SxListPhoneNumbersQuery
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_get_contact_phone_numbers_permission(id, ctx.principal)

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    phone_numbers = phone_number_service.list_(
        ctx.session,
        contact_id=id,
        show_archived=query.show_archived,
        limit=query.limit,
        starting_after=query.starting_after,
        ending_before=query.ending_before,
    )
    return make_paginated_list_response(
        format_phone_number_list(
            phone_numbers,
            url_for(".list_phone_numbers_for_contact", id=id, **query.dict()),
        ),
        limit=query.limit,
        next_cursor=phone_numbers.next_cursor,
        prev_cursor=phone_numbers.prev_cursor,
        route="contacts_api.list_phone_numbers_for_contact",
        route_args={"id": id},
    )


@contacts_api.route("/contacts/<contact_id>/phone_numbers", methods=["POST"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def create_phone_number_for_contact(
    ctx: PrincipalAuthContext, contact_id: IdContact, body: SxPhoneNumberCreateRequest
) -> Union[Tuple[BaseModel, int], Response]:
    try:
        permission = build_update_contact_phone_numbers_permission(
            contact_id, ctx.principal
        )

        if not permission.can():
            raise InsufficientPermissionsError(hide_presence=True)

        contact = contact_service.from_id_or_raise(ctx.session, contact_id)
        phone_number = phone_number_service.create(
            ctx.session, create=body, contact=contact, defer_commit=False
        )

        return format_phone_number(phone_number), HTTPStatus.CREATED

    except PhoneNumberExistsError:
        raise CreateAlreadyExistsError(object="phone_number")


@contacts_api.route(
    "/contacts/<contact_id>/phone_numbers/<phone_number_id>", methods=["GET"]
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def get_phone_number_for_contact(
    ctx: PrincipalAuthContext, contact_id: IdContact, phone_number_id: IdPhoneNumber
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_get_contact_phone_numbers_permission(contact_id, ctx.principal)

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    phone_number = phone_number_service.from_id_or_raise(ctx.session, phone_number_id)
    if phone_number.contact.id != contact_id:
        raise PhoneNumberNotFoundError()

    return format_phone_number(phone_number), HTTPStatus.OK


@contacts_api.route(
    "/contacts/<contact_id>/phone_numbers/<phone_number_id>", methods=["PATCH"]
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def update_phone_number_for_contact(
    ctx: PrincipalAuthContext,
    contact_id: IdContact,
    phone_number_id: IdPhoneNumber,
    body: SxPhoneNumberUpdateRequest,
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_update_contact_phone_numbers_permission(
        contact_id, ctx.principal
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    phone_number = phone_number_service.update_from_id(
        ctx.session,
        phone_number_id,
        contact_id=contact_id,
        update_request=body,
        defer_commit=False,
    )

    return (
        format_phone_number(
            phone_number,
        ),
        HTTPStatus.OK,
    )


@contacts_api.route(
    "/contacts/<contact_id>/phone_numbers/<phone_number_id>/archive", methods=["PUT"]
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def archive_phone_number_for_contact(
    ctx: PrincipalAuthContext,
    contact_id: IdContact,
    phone_number_id: IdPhoneNumber,
    body: SxPhoneNumberArchiveRequest,
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_update_contact_phone_numbers_permission(
        contact_id, ctx.principal
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    phone_number = phone_number_service.archive_from_id(
        ctx.session,
        phone_number_id,
        contact_id=contact_id,
        reason_archived=body.reason_archived,
        archived=True,
        defer_commit=False,
    )

    return (
        format_phone_number(
            phone_number,
        ),
        HTTPStatus.OK,
    )


@contacts_api.route(
    "/contacts/<contact_id>/phone_numbers/<phone_number_id>/archive", methods=["DELETE"]
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def unarchive_phone_number_for_contact(
    ctx: PrincipalAuthContext, contact_id: IdContact, phone_number_id: IdPhoneNumber
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_update_contact_phone_numbers_permission(
        contact_id, ctx.principal
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    phone_number = phone_number_service.archive_from_id(
        ctx.session,
        phone_number_id,
        contact_id=contact_id,
        reason_archived=None,
        archived=False,
        defer_commit=False,
    )

    return (
        format_phone_number(
            phone_number,
        ),
        HTTPStatus.OK,
    )


@contacts_api.route(
    "/contacts/<contact_id>/phone_numbers/<phone_number_id>/confirm",
    methods=["PUT", "DELETE"],
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def confirm_phone_number_for_contact(
    ctx: PrincipalAuthContext, contact_id: IdContact, phone_number_id: IdPhoneNumber
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_update_contact_phone_numbers_permission(
        contact_id, ctx.principal
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    phone_number = phone_number_service.mark_confirmed_from_id(
        ctx.session,
        phone_number_id,
        contact_id=contact_id,
        confirm=True if request.method == "PUT" else False,
        defer_commit=False,
    )

    return (
        format_phone_number(
            phone_number,
        ),
        HTTPStatus.OK,
    )


@contacts_api.route("/contacts/<id>/mailing_addresses", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def list_mailing_addresses_for_contact(
    ctx: PrincipalAuthContext, id: IdContact, query: SxListPostalAddressesQuery
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_get_contact_mailing_addresses_permission(id, ctx.principal)

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    mailing_addresses = postal_address_service.list_mailing_addresses_for_contact(
        ctx.session,
        contact_id=id,
        show_archived=query.show_archived,
        limit=query.limit,
        starting_after=query.starting_after,
        ending_before=query.ending_before,
    )
    return make_paginated_list_response(
        format_postal_address_list(
            mailing_addresses,
            url_for(".list_mailing_addresses_for_contact", id=id, **query.dict()),
        ),
        limit=query.limit,
        next_cursor=mailing_addresses.next_cursor,
        prev_cursor=mailing_addresses.prev_cursor,
        route="contacts_api.list_mailing_addresses_for_contact",
        route_args={"id": id},
    )


@contacts_api.route("/contacts/<contact_id>/mailing_addresses", methods=["POST"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def create_mailing_address_for_contact(
    ctx: PrincipalAuthContext, contact_id: IdContact, body: SxPostalAddressCreateRequest
) -> Union[Tuple[BaseModel, int], Response]:
    try:
        permission = build_update_contact_mailing_addresses_permission(
            contact_id, ctx.principal
        )

        if not permission.can():
            raise InsufficientPermissionsError(hide_presence=True)

        contact = contact_service.from_id_or_raise(ctx.session, contact_id)
        postal_address = postal_address_service.create(
            ctx.session, body, contact=contact, defer_commit=False
        )

        return format_postal_address(postal_address), HTTPStatus.CREATED

    except ContactMailingAddressExistsError:
        raise CreateAlreadyExistsError(object="postal_address")


@contacts_api.route(
    "/contacts/<contact_id>/mailing_addresses/<postal_address_id>", methods=["GET"]
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def get_mailing_address_for_contact(
    ctx: PrincipalAuthContext, contact_id: IdContact, postal_address_id: IdPostalAddress
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_get_contact_mailing_addresses_permission(
        contact_id, ctx.principal
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    postal_address = postal_address_service.from_id_or_raise(
        ctx.session, postal_address_id
    )
    if postal_address.contact.id != contact_id:
        raise PostalAddressNotFoundError()

    return format_postal_address(postal_address), HTTPStatus.OK


@contacts_api.route(
    "/contacts/<contact_id>/mailing_addresses/<postal_address_id>", methods=["PATCH"]
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def update_mailing_address_for_contact(
    ctx: PrincipalAuthContext,
    contact_id: IdContact,
    postal_address_id: IdPostalAddress,
    body: SxPostalAddressUpdateRequest,
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_update_contact_mailing_addresses_permission(
        contact_id, ctx.principal
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    postal_address = postal_address_service.update_from_id(
        ctx.session,
        postal_address_id,
        contact_id=contact_id,
        update_request=body,
        defer_commit=False,
    )

    return (
        format_postal_address(
            postal_address,
        ),
        HTTPStatus.OK,
    )


@contacts_api.route(
    "/contacts/<contact_id>/mailing_addresses/<postal_address_id>/archive",
    methods=["PUT"],
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def archive_mailing_address_for_contact(
    ctx: PrincipalAuthContext,
    contact_id: IdContact,
    postal_address_id: IdPostalAddress,
    body: SxPostalAddressArchiveRequest,
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_update_contact_mailing_addresses_permission(
        contact_id, ctx.principal
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    postal_address = postal_address_service.archive_from_id(
        ctx.session,
        postal_address_id,
        contact_id=contact_id,
        reason_archived=body.reason_archived,
        archived=True,
        defer_commit=False,
    )

    return (
        format_postal_address(
            postal_address,
        ),
        HTTPStatus.OK,
    )


@contacts_api.route(
    "/contacts/<contact_id>/mailing_addresses/<postal_address_id>/archive",
    methods=["DELETE"],
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def unarchive_mailing_address_for_contact(
    ctx: PrincipalAuthContext, contact_id: IdContact, postal_address_id: IdPostalAddress
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_update_contact_mailing_addresses_permission(
        contact_id, ctx.principal
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    postal_address = postal_address_service.archive_from_id(
        ctx.session,
        postal_address_id,
        contact_id=contact_id,
        reason_archived=None,
        archived=False,
        defer_commit=False,
    )

    return (
        format_postal_address(
            postal_address,
        ),
        HTTPStatus.OK,
    )


@contacts_api.route(
    "/contacts/<contact_id>/mailing_addresses/<postal_address_id>/confirm",
    methods=["PUT", "DELETE"],
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def confirm_mailing_address_for_contact(
    ctx: PrincipalAuthContext, contact_id: IdContact, postal_address_id: IdPostalAddress
) -> Union[Tuple[BaseModel, int], Response]:
    permission = build_update_contact_mailing_addresses_permission(
        contact_id, ctx.principal
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    postal_address = postal_address_service.mark_confirmed_from_id(
        ctx.session,
        postal_address_id,
        contact_id=contact_id,
        confirmed=True if request.method == "PUT" else False,
        defer_commit=False,
    )

    return (
        format_postal_address(
            postal_address,
        ),
        HTTPStatus.OK,
    )


@contacts_api.route("/contacts/<contact_id>/custom_fields", methods=["POST"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def assign_custom_field_value(
    ctx: PrincipalAuthContext,
    contact_id: IdContact,
    body: SxCustomFieldAssociationCreateRequest,
) -> Union[Tuple[SxCustomFieldAssociation, int], Response]:
    try:
        permission = build_update_contact_custom_fields_permission(
            contact_id, ctx.principal
        )

        if not permission.can():
            raise InsufficientPermissionsError(hide_presence=True)

        cfa = contact_custom_field_associations.attach_to_contact_by_id(
            ctx.session,
            contact_id=contact_id,
            custom_field_id=body.custom_field,
            value=body.value,
            defer_commit=False,
        )
        return (format_custom_field_association(cfa), HTTPStatus.CREATED)

    except CustomFieldNotFoundError:
        raise UpdateTargetNotFoundError(params="custom_fields")


@contacts_api.route("/contacts/<contact_id>/custom_fields", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def list_custom_field_associations_for_contact(
    ctx: PrincipalAuthContext,
    contact_id: IdContact,
) -> Union[Tuple[SxListCustomFieldAssociationsResponse, int], Response]:
    permission = build_get_contact_custom_fields_permission(contact_id, ctx.principal)

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    associations = contact_custom_field_associations.list_for_contact(
        ctx.session, contact_id=contact_id
    )
    return (
        format_custom_field_association_list(
            PaginatedList.new(associations, has_more=False, limit=len(associations)),
            url_for(
                ".list_custom_field_associations_for_contact",
                contact_id=contact_id,
            ),
        ),
        HTTPStatus.OK,
    )


@contacts_api.route(
    "/contacts/<contact_id>/custom_fields/<custom_field_id>", methods=["GET"]
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def get_custom_field_value(
    ctx: PrincipalAuthContext,
    contact_id: IdContact,
    custom_field_id: IdCustomField,
) -> Union[Tuple[SxCustomFieldAssociation, int], Response]:
    permission = build_get_contact_custom_fields_permission(contact_id, ctx.principal)

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    cfa = contact_custom_field_associations.from_ids_or_raise(
        ctx.session,
        contact_id=contact_id,
        custom_field_id=custom_field_id,
    )
    return (format_custom_field_association(cfa), HTTPStatus.OK)


@contacts_api.route(
    "/contacts/<contact_id>/custom_fields/<custom_field_id>", methods=["PATCH"]
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def update_custom_field_value(
    ctx: PrincipalAuthContext,
    contact_id: IdContact,
    custom_field_id: IdCustomField,
    body: SxCustomFieldAssociationUpdateRequest,
) -> Union[Tuple[SxCustomFieldAssociation, int], Response]:
    permission = build_update_contact_custom_fields_permission(
        contact_id, ctx.principal
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    cfa = contact_custom_field_associations.update_by_id(
        ctx.session,
        contact_id=contact_id,
        custom_field_id=custom_field_id,
        value=body.value,
        defer_commit=False,
    )
    return (format_custom_field_association(cfa), HTTPStatus.OK)


@contacts_api.route(
    "/contacts/<contact_id>/custom_fields/<custom_field_id>", methods=["DELETE"]
)
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def delete_custom_field_value(
    ctx: PrincipalAuthContext, contact_id: IdContact, custom_field_id: IdCustomField
) -> Union[Tuple[SxCustomFieldAssociation, int], Response]:
    permission = build_update_contact_custom_fields_permission(
        contact_id, ctx.principal
    )

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    cfa = contact_custom_field_associations.delete_by_id(
        ctx.session,
        contact_id=contact_id,
        custom_field_id=custom_field_id,
        defer_commit=False,
    )
    return (format_custom_field_association(cfa), HTTPStatus.OK)


@contacts_api.route("/contacts/<contact_id>/tags", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def list_tags_for_contact(
    ctx: PrincipalAuthContext, contact_id: IdContact
) -> Union[Tuple[SxListTagsResponse, int], Response]:
    permission = build_get_contact_tags_permission(contact_id, ctx.principal)

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    contact = contact_service.from_id_or_raise(ctx.session, contact_id)
    return (
        format_tags_list(
            PaginatedList.new(contact.tags, has_more=False, limit=len(contact.tags)),
            route=url_for(".list_tags_for_contact", contact_id=contact_id),
        ),
        HTTPStatus.OK,
    )


@contacts_api.route("/contacts/<contact_id>/tags", methods=["POST"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def assign_tag(
    ctx: PrincipalAuthContext,
    contact_id: IdContact,
    body: SxContactTagAssociationRequest,
) -> Union[Tuple[SxTag, int], Response]:
    try:
        permission = build_update_contact_tags_permission(contact_id, ctx.principal)

        if not permission.can():
            raise InsufficientPermissionsError(hide_presence=True)

        cta, _ = contact_tag_associations.attach_to_contact_from_id(
            ctx.session,
            contact_id=contact_id,
            tag_id=body.tag,
            defer_commit=False,
        )
        tag = cta.tag
        return (format_tag(tag), HTTPStatus.CREATED)

    except TagNotFoundError:
        raise UpdateTargetNotFoundError(params="tag")


@contacts_api.route("/contacts/<contact_id>/tags/<tag_id>", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def get_assigned_tag(
    ctx: PrincipalAuthContext, contact_id: IdContact, tag_id: IdTag
) -> Union[Tuple[SxTag, int], Response]:
    permission = build_get_contact_tags_permission(contact_id, ctx.principal)

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    tag = contact_service.get_tag_from_id_or_raise(
        ctx.session, contact_id=contact_id, tag_id=tag_id
    )
    return (format_tag(tag), HTTPStatus.OK)


@contacts_api.route("/contacts/<contact_id>/tags/<tag_id>", methods=["DELETE"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth()
def unassign_tag(
    ctx: PrincipalAuthContext, contact_id: IdContact, tag_id: IdTag
) -> Union[Tuple[SxTag, int], Response]:
    permission = build_update_contact_tags_permission(contact_id, ctx.principal)

    if not permission.can():
        raise InsufficientPermissionsError(hide_presence=True)

    cta, _ = contact_tag_associations.detach_contact_from_id(
        ctx.session,
        contact_id=contact_id,
        tag_id=tag_id,
        defer_commit=False,
    )
    tag = cta.tag
    return (format_tag(tag), HTTPStatus.OK)


def handle_create_conflict_error(
    error: CreateExclusiveParameterConflictError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.resource_create_conflict_exclusive,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.CONFLICT,
        friendly_message=(
            "Can't create contact: "
            f"one or more {error.params or 'parameters'} "
            "already belongs to another contact"
        ),
    )
    logger.error(**response.as_dict())
    return response


def handle_create_parameter_not_found_error(
    error: ParameterMissingError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.required_parameter_missing,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.BAD_REQUEST,
        friendly_message=(
            "Can't create contact: "
            f"one or more {error.params or 'parameters'} "
            "could not be found"
        ),
    )
    logger.error(**response.as_dict())
    return response


def handle_invalid_custom_field_value_error(
    error: InvalidCustomFieldValueError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.resource_update_invalid_value,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.BAD_REQUEST,
        friendly_message=(
            f"Invalid value '{error.value}' for custom field "
            f"'{error.name}' of type '{error.field_type}': {error.message}"
        ),
    )
    logger.error(**response.as_dict())
    return response


def handle_custom_field_association_exists_error(
    error: CustomFieldAssociationExistsError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.resource_already_exists,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.CONFLICT,
        friendly_message=(
            "Can't attach custom field to contact: another value already exists"
        ),
    )
    logger.error(**response.as_dict())
    return response


def handle_tag_association_exists_error(
    error: TagAssociationExistsError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.resource_already_exists,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.CONFLICT,
        friendly_message=("Can't attach tag to contact: tag already exists"),
    )
    logger.error(**response.as_dict())
    return response


contacts_api.register_error_handler(
    CreateExclusiveParameterConflictError, handle_create_conflict_error
)
contacts_api.register_error_handler(
    ParameterMissingError, handle_create_parameter_not_found_error
)
contacts_api.register_error_handler(
    InvalidCustomFieldValueError, handle_invalid_custom_field_value_error
)
contacts_api.register_error_handler(
    CustomFieldAssociationExistsError, handle_custom_field_association_exists_error
)
contacts_api.register_error_handler(
    TagAssociationExistsError, handle_tag_association_exists_error
)
