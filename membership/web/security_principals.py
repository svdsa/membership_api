from http import HTTPStatus
from typing import Tuple, Union

from flask.blueprints import Blueprint
from flask.helpers import url_for
from flask.wrappers import Response
from pydantic.main import BaseModel

from membership.auth.permissions import (
    PmSecurityPrincipalsList,
    PmSecurityPrincipalsRead,
)
from membership.models.authz import PrincipalAuthContext
from membership.schemas.formatters.security_principals import (
    format_security_principal,
    format_security_principal_roles_list,
    format_security_principals_list,
)
from membership.schemas.id import IdSecurityPrincipal
from membership.schemas.rest.security_principals import (
    SxListSecurityPrincipalRolesQuery,
    SxListSecurityPrincipalsQuery,
)
from membership.services import security_principal_service
from membership.web.auth import requires_auth_permissions
from membership.web.util import make_paginated_list_response, validate_with_pydantic

security_principals_api = Blueprint("security_principals_api", __name__)


@security_principals_api.route("/security_principals", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmSecurityPrincipalsList, hide_presence=False)
def list_security_principals(
    ctx: PrincipalAuthContext, query: SxListSecurityPrincipalsQuery
) -> Union[Tuple[BaseModel, int], Response]:
    sps = security_principal_service.list_(
        ctx.session,
        limit=query.limit,
        starting_after=query.starting_after,
        ending_before=query.ending_before,
    )

    return make_paginated_list_response(
        format_security_principals_list(
            sps, url_for(".list_security_principals", **query.dict())
        ),
        limit=query.limit,
        next_cursor=sps.next_cursor,
        prev_cursor=sps.prev_cursor,
        route="security_principals_api.list_security_principals",
    )


@security_principals_api.route("/security_principals/<sp_id>", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmSecurityPrincipalsRead)
def get_security_principal(
    ctx: PrincipalAuthContext, sp_id: IdSecurityPrincipal
) -> Union[Tuple[BaseModel, int], Response]:
    sp = security_principal_service.from_id_or_raise(ctx.session, sp_id)
    return format_security_principal(sp), HTTPStatus.OK


@security_principals_api.route("/security_principals/<sp_id>/roles", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmSecurityPrincipalsRead)
def list_roles_for_security_principal(
    ctx: PrincipalAuthContext,
    sp_id: IdSecurityPrincipal,
    query: SxListSecurityPrincipalRolesQuery,
) -> Union[Tuple[BaseModel, int], Response]:
    sp = security_principal_service.from_id_or_raise(ctx.session, sp_id)
    sprs = security_principal_service.list_roles(
        ctx.session,
        sp,
        limit=query.limit,
        starting_after=query.starting_after,
        ending_before=query.ending_before,
    )

    return make_paginated_list_response(
        format_security_principal_roles_list(
            sprs,
            url_for(".list_roles_for_security_principal", sp_id=sp_id, **query.dict()),
        ),
        limit=query.limit,
        next_cursor=sprs.next_cursor,
        prev_cursor=sprs.prev_cursor,
        route="security_principals_api.list_roles_for_security_principal",
    )
