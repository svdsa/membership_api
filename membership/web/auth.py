import logging
import random
import string
from contextlib import AbstractContextManager
from datetime import datetime, timedelta
from functools import wraps
from http import HTTPStatus
from json import loads
from typing import Optional, Union, cast

import requests
from flask import g, request
from flask.wrappers import Response
from flask_principal import Identity, Permission
from jwt.exceptions import ExpiredSignatureError
from sqlalchemy import orm

from config import (
    ADMIN_CLIENT_ID,
    ADMIN_CLIENT_SECRET,
    AUTH_CONNECTION,
    AUTH_URL,
    PORTAL_URL,
    USE_AUTH,
)
from membership.auth.authentication import decode_email_from_bearer
from membership.auth.errors import (
    ApiAuthenticationError,
    CreateAuth0UserError,
    InsufficientPermissionsError,
    InsufficientRoleError,
    InvalidJWTError,
    InvalidJWTSecretError,
    InvalidSecurityPrincipalError,
    InvalidUserError,
    RequestRequiresAuthenticationError,
    UnauthorizedError,
)
from membership.database.base import Session, db_session
from membership.database.models import Member
from membership.models import MemberAuthContext
from membership.models.authz import PrincipalAuthContext
from membership.services import security_principal_service
from membership.web.errors import ErrorCode, ErrorResponse, ErrorType
from membership.web.util import NotFound, ServerError, Unauthorized

logger = logging.getLogger(__name__)

PASSWORD_CHARS = string.ascii_letters + string.digits


def DEPRECATED_requires_legacy_auth(*roles: str):
    """ This defines a decorator which when added to a route function in flask requires authorization to
    view the route. Authorization includes being logged in and having the roles listed.

    example: @requires_auth('admin') requires the 'admin' user role.

    @param roles: list of role names as strings.
    """

    def decorator(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            # Find and validate our requestor's email.
            with cast(AbstractContextManager[orm.Session], Session()) as session:
                auth = request.headers.get("authorization")

                # Derive a complete AuthContext and forward to our request.
                try:
                    email = decode_email_from_bearer(auth, USE_AUTH)

                    member = (
                        session.query(Member)
                        .filter_by(email_address=email)
                        .one_or_none()
                    )

                    if member is None:
                        raise UnauthorizedError(f"No member with email {email}")

                    ctx = MemberAuthContext(member, session)

                    for role in roles:
                        ctx.az.verify_role(role)
                    kwargs["ctx"] = ctx
                    return f(*args, **kwargs)

                except (InvalidJWTError, InvalidJWTSecretError) as e:
                    logger.error(e)
                    return Unauthorized("bad credentials")

                except (UnauthorizedError, InsufficientRoleError) as e:
                    logger.error(e)
                    return NotFound("Not found")

                except Exception as e:
                    logger.exception(e)
                    return ServerError("authentication error")

        return decorated

    return decorator


def requires_auth_permissions(*perms: Permission, hide_presence: bool = True):
    """Decorator that restricts access to a user that satisfies all permissions provided.

    @requires_user_with_perms(AdminPermission)
    """

    def decorator(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            identity: Identity = g.identity
            if identity is None or identity.id is None:
                # Caller has provided no authentication or invalid authentication
                raise RequestRequiresAuthenticationError(hide_presence=hide_presence)

            session = db_session()
            sp = security_principal_service.from_id(session, identity.id)
            if sp is None:
                # Forged? or old JWT of an SP that has been removed?
                raise InvalidSecurityPrincipalError(hide_presence=hide_presence)

            for perm in perms:
                if not perm.allows(identity):
                    raise InsufficientPermissionsError(hide_presence=hide_presence)

            ctx = PrincipalAuthContext(
                principal=sp, session=session, needs=identity.provides
            )
            kwargs["ctx"] = ctx

            try:
                return f(*args, **kwargs)
            except Exception as e:
                ctx.session.rollback()
                raise e

        return decorated

    return decorator


def requires_auth(*, hide_presence: bool = True):
    return requires_auth_permissions(hide_presence=hide_presence)


current_token = {}


def get_auth0_token() -> str:
    if not current_token or datetime.now() > current_token["expiry"]:
        current_token.update(generate_auth0_token())
    return current_token["token"]


def generate_auth0_token() -> dict:
    payload = {
        "grant_type": "client_credentials",
        "client_id": ADMIN_CLIENT_ID,
        "client_secret": ADMIN_CLIENT_SECRET,
        "audience": AUTH_URL + "api/v2/",
    }
    response = requests.post(AUTH_URL + "oauth/token", json=payload).json()
    return {
        "token": response["access_token"],
        "expiry": datetime.now() + timedelta(seconds=response["expires_in"]),
    }


def get_auth0_email_verification_link(user_id: int) -> str:
    headers = {"Authorization": "Bearer " + get_auth0_token()}

    # get a password change URL
    payload = {"result_url": PORTAL_URL, "user_id": user_id}
    r = requests.post(
        AUTH_URL + "api/v2/tickets/password-change", json=payload, headers=headers
    )
    if r.status_code > 299:
        msg = "Failed to get password url. " "Received status={} content={}".format(
            r.status_code, r.content
        )
        raise Exception(msg)
    reset_url = r.json()["ticket"]

    # get email verification link
    payload = {"result_url": reset_url, "user_id": user_id}
    r = requests.post(
        AUTH_URL + "api/v2/tickets/email-verification", json=payload, headers=headers
    )
    if r.status_code > 299:
        msg = "Failed to get verify url. " "Received status={} content={}".format(
            r.status_code, r.content
        )
        raise Exception(msg)
    validate_url = r.json()["ticket"]
    return validate_url


def create_auth0_user(email: str) -> Optional[str]:
    """
    Create the Auth0 user for the given email address.

    @returns None if a user with that email address already exists,
             otherwise the email verification url.
    """
    if not USE_AUTH:
        return PORTAL_URL

    # create the user
    payload = {
        "connection": AUTH_CONNECTION,
        "email": email,
        "password": "".join(
            random.SystemRandom().choice(PASSWORD_CHARS) for _ in range(12)
        ),
        "user_metadata": {},
        "email_verified": False,
        "verify_email": False,
    }
    headers = {"Authorization": "Bearer " + get_auth0_token()}
    r = requests.post(AUTH_URL + "api/v2/users", json=payload, headers=headers)

    if r.status_code > 299:
        logger.warning(f"Failed to create user: {r.content.decode()}")

        try:
            error_response = loads(r.content)
            if error_response["code"] == "user_exists":
                return None
        except Exception:
            # Don't handle other Auth0 issues
            logger.warning("Couldn't parse response from auth0 failure message")

        raise CreateAuth0UserError(r.status_code, r.content.decode())

    user_id = r.json()["user_id"]
    return get_auth0_email_verification_link(user_id)


def handle_authentication_error(error: ApiAuthenticationError) -> Response:
    if error.hide_presence:
        return ErrorResponse(
            code=ErrorCode.resource_missing,
            status_code=HTTPStatus.NOT_FOUND,
            type=ErrorType.invalid_request_error,
        )
    else:
        if isinstance(error, RequestRequiresAuthenticationError):
            return ErrorResponse(
                code=ErrorCode.requires_auth,
                status_code=HTTPStatus.UNAUTHORIZED,
                type=ErrorType.invalid_request_error,
            )

        if isinstance(error, InvalidSecurityPrincipalError):
            return ErrorResponse(
                code=ErrorCode.invalid_security_principal,
                status_code=HTTPStatus.UNAUTHORIZED,
                type=ErrorType.api_error,
            )

        if isinstance(error, InvalidUserError):
            return ErrorResponse(
                code=ErrorCode.invalid_user,
                status_code=HTTPStatus.UNAUTHORIZED,
                type=ErrorType.api_error,
            )

        if isinstance(error, InsufficientPermissionsError):
            return ErrorResponse(
                code=ErrorCode.insufficient_permissions,
                status_code=HTTPStatus.FORBIDDEN,
                type=ErrorType.api_error,
            )

        return ErrorResponse(
            code=ErrorCode.server_error,
            status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
            type=ErrorType.api_error,
        )


def handle_invalid_jwt_error(
    error: Union[InvalidJWTError, InvalidJWTSecretError, ExpiredSignatureError]
) -> Response:
    return ErrorResponse(
        code=ErrorCode.jwt_invalid,
        status_code=HTTPStatus.UNAUTHORIZED,
        type=ErrorType.invalid_request_error,
    )
