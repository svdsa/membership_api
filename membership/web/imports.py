from http import HTTPStatus
from typing import Tuple, Union

import structlog
from flask import Blueprint
from flask.helpers import url_for
from flask.wrappers import Response
from pydantic.main import BaseModel

from membership.auth.errors import InvalidUserError
from membership.auth.permissions import PmImportEventsCreate, PmImportEventsReview
from membership.models.authz import PrincipalAuthContext
from membership.schemas.formatters.import_events import (
    contact_import_create_fn,
    format_import_event,
    format_import_event_review_actions_list,
    format_import_events_list,
)
from membership.schemas.id import IdSecurityPrincipal
from membership.schemas.rest.import_events import (
    IdImportEvent,
    SxImportEventCreateRequest,
    SxImportEventReviewActionsUpdateRequest,
    SxListImportEventReviewActionsQuery,
    SxListImportEventsQuery,
)
from membership.services import import_event_service
from membership.services.import_event_service import (
    InvalidImportEventReviewActionStateError,
    MissingImportEventParticipantsError,
)
from membership.util.pagination import PaginatedList
from membership.web.auth import requires_auth_permissions
from membership.web.errors import ErrorCode, ErrorResponse, ErrorType
from membership.web.util import make_paginated_list_response, validate_with_pydantic

imports_api = Blueprint("imports_api", __name__)
logger = structlog.stdlib.get_logger()


class ReviewAlreadyCompletedError(Exception):
    pass


@imports_api.route("/imports", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmImportEventsReview, hide_presence=False)
def list_import_events(
    ctx: PrincipalAuthContext, query: SxListImportEventsQuery
) -> Union[Tuple[BaseModel, int], Response]:
    events = import_event_service.list_import_events(
        ctx.session,
        limit=query.limit,
        starting_after=query.starting_after,
        ending_before=query.ending_before,
    )
    return make_paginated_list_response(
        format_import_events_list(
            events, url_for(".list_import_events", **query.dict())
        ),
        limit=query.limit,
        next_cursor=events.next_cursor,
        prev_cursor=events.prev_cursor,
        route="imports_api.list_import_events",
    )


@imports_api.route("/imports", methods=["POST"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmImportEventsCreate, hide_presence=False)
def create_import_event(
    ctx: PrincipalAuthContext, body: SxImportEventCreateRequest
) -> Union[Tuple[BaseModel, int], Response]:
    event = import_event_service.create(
        ctx.session,
        body,
        creator=ctx.principal,
        create_fn=contact_import_create_fn,
        defer_commit=False,
    )

    return format_import_event(event), HTTPStatus.CREATED


@imports_api.route("/imports/<event_id>", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmImportEventsReview)
def get_import_event(
    ctx: PrincipalAuthContext, event_id: IdImportEvent
) -> Union[Tuple[BaseModel, int], Response]:
    event = import_event_service.from_id_or_raise(ctx.session, id=event_id)
    return format_import_event(event), HTTPStatus.OK


@imports_api.route("/imports/<event_id>/review_actions", methods=["PATCH"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmImportEventsReview)
def update_review_actions(
    ctx: PrincipalAuthContext,
    event_id: IdImportEvent,
    body: SxImportEventReviewActionsUpdateRequest,
) -> Union[Tuple[BaseModel, int], Response]:
    event = import_event_service.from_id_or_raise(ctx.session, id=event_id)

    if event.date_reviewed is not None or event.reviewer is not None:
        raise ReviewAlreadyCompletedError()

    if ctx.principal.user is None:
        raise InvalidUserError(hide_presence=True)

    updated_review_actions = import_event_service.update_review_actions(
        ctx.session,
        review_update=body,
        event=event,
        defer_commit=False,
    )

    return (
        format_import_event_review_actions_list(
            PaginatedList.new(
                updated_review_actions, limit=len(updated_review_actions)
            ),
            route=url_for(".list_import_event_review_actions", event_id=event_id),
        ),
        HTTPStatus.OK,
    )


@imports_api.route("/imports/<event_id>/review_actions", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmImportEventsReview)
def list_import_event_review_actions(
    ctx: PrincipalAuthContext,
    event_id: IdImportEvent,
    query: SxListImportEventReviewActionsQuery,
) -> Union[Tuple[BaseModel, int], Response]:
    event = import_event_service.from_id_or_raise(ctx.session, event_id)
    review_actions = import_event_service.list_review_actions(
        ctx.session,
        event,
        limit=query.limit,
        starting_after=query.starting_after,
        ending_before=query.ending_before,
    )
    return make_paginated_list_response(
        format_import_event_review_actions_list(
            review_actions,
            url_for(".get_import_event_review_actions", **query.dict()),
        ),
        limit=query.limit,
        next_cursor=review_actions.next_cursor,
        prev_cursor=review_actions.prev_cursor,
        route="imports_api.list_import_event_review_actions",
    )


@imports_api.route("/imports/<event_id>/confirm", methods=["PUT"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmImportEventsReview)
def confirm_import_event(
    ctx: PrincipalAuthContext, event_id: IdImportEvent
) -> Union[Tuple[BaseModel, int], Response]:
    """Confirm an import event and commit review actions to the underlying
    resources in the database."""
    event = import_event_service.from_id_or_raise(ctx.session, id=event_id)

    if event.date_reviewed is not None or event.reviewer is not None:
        raise ReviewAlreadyCompletedError()

    if ctx.principal.user is None:
        raise InvalidUserError(hide_presence=True)

    committed_event = import_event_service.apply_import_event(
        ctx.session,
        event=event,
        reviewer_id=IdSecurityPrincipal.validate(ctx.principal.id),
        defer_commit=False,
    )

    return (
        format_import_event(committed_event),
        HTTPStatus.OK,
    )


def handle_missing_import_participants_error(
    error: MissingImportEventParticipantsError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.required_parameter_missing,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.BAD_REQUEST,
        friendly_message="Missing import participants",
        params="participants",
    )
    logger.error(**response.as_dict())
    return response


def handle_review_already_completed_error(
    error: ReviewAlreadyCompletedError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.import_event_already_completed,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.FORBIDDEN,
    )
    logger.error(**response.as_dict())
    return response


def handle_invalid_review_state_error(
    error: InvalidImportEventReviewActionStateError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.parameter_invalid,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.BAD_REQUEST,
        friendly_message=str(error),
    )
    logger.error(**response.as_dict())
    return response


imports_api.register_error_handler(
    MissingImportEventParticipantsError, handle_missing_import_participants_error
)
imports_api.register_error_handler(
    ReviewAlreadyCompletedError, handle_review_already_completed_error
)
imports_api.register_error_handler(
    InvalidImportEventReviewActionStateError, handle_invalid_review_state_error
)
