import logging

import dateutil.parser
from flask import Blueprint, jsonify, request

from membership.auth.permissions import (
    PmCommitteesAdmin,
    PmCommitteesCreate,
    PmCommitteesList,
    PmCommitteesListLeadership,
    build_administer_committee_permission,
    build_administer_committee_permission_from_id,
)
from membership.database.models import (
    Committee,
    CommitteeLeadershipRole,
    LeadershipRole,
)
from membership.models.authz import PrincipalAuthContext
from membership.schemas.rest.committees import (
    add_leadership_role_request_schema,
    add_leadership_role_response_schema,
    format_committee,
    format_committee_details,
    format_committee_leadership_role,
    get_leaders_response_schema,
    replace_leader_request_schema,
    replace_leader_response_schema,
)
from membership.services.committee_leadership_service import CommitteeLeadershipService
from membership.services.committee_service import CommitteeService
from membership.services.errors import NotFoundError, ValidationError
from membership.web.auth import requires_auth, requires_auth_permissions
from membership.web.util import (
    BadRequest,
    NotFound,
    ServerError,
    ServiceUnavailable,
    validate_request,
    validate_response,
)

logger = logging.getLogger(__name__)

committee_api = Blueprint('committee_api', __name__)
committee_service = CommitteeService()
leadership_service = CommitteeLeadershipService()


@committee_api.route("/committee/list", methods=["GET"])
@requires_auth_permissions(PmCommitteesList)
def get_committees(ctx: PrincipalAuthContext):
    is_admin = PmCommitteesAdmin.can()
    committees = committee_service.list(ctx.session, show_inactive=is_admin)

    return jsonify([format_committee(c, viewable=is_admin) for c in committees])


@committee_api.route("/committee", methods=["POST"])
@requires_auth_permissions(PmCommitteesCreate)
def add_committee(ctx: PrincipalAuthContext):
    is_admin = PmCommitteesAdmin.can()
    committee = committee_service.create(
        ctx.session,
        name=request.json["name"],
        provisional=request.json.get("provisional", False),
        admins=request.json.get("admin_list", []),
        defer_commit=False,
    )

    return jsonify(
        {"status": "success", "created": format_committee_details(committee, is_admin)}
    )


@committee_api.route("/committee/<int:committee_id>", methods=["GET"])
@requires_auth()
def get_committee(ctx: PrincipalAuthContext, committee_id: int):
    committee = committee_service.from_id(ctx.session, committee_id=committee_id)
    if committee is None:
        return NotFound("Not found")

    admin_permission = build_administer_committee_permission(committee)
    is_admin = admin_permission.can()

    return jsonify(format_committee_details(committee, is_admin))


@committee_api.route("/committee/<int:committee_id>", methods=["PATCH"])
@requires_auth()
def edit_committee(ctx: PrincipalAuthContext, committee_id: int):
    try:
        committee = committee_service.from_id_or_raise(
            ctx.session, committee_id=committee_id
        )

        admin_permission = build_administer_committee_permission(committee)
        is_admin = admin_permission.can()
        if not is_admin:
            raise NotFoundError("Not found")

        if "inactive" not in request.json or len(request.json) > 1:
            raise ValueError(
                "Only editing committee inactive status is allowed at this time"
            )

        committee = committee_service.edit(
            ctx.session,
            committee_id,
            inactive=request.json.get("inactive"),
            defer_commit=False,
        )

        return jsonify(format_committee_details(committee, is_admin))

    except NotFoundError as e:
        logger.error(e)
        return NotFound("Not found")

    except ValueError as e:
        logger.error(e)
        return BadRequest(str(e))

    except Exception as e:
        logger.exception(e)
        return ServerError("Unexpected error")


@committee_api.route('/committee/<int:committee_id>/member_request', methods=['POST'])
@requires_auth()
def request_committee_membership(ctx: PrincipalAuthContext, committee_id: int):
    return ServiceUnavailable(
        "Membership portal overhaul does not support sending email yet"
    )

    # try:
    #     if ctx.principal.user:
    #         requester = ctx.principal.user.contact
    #         name = requester.full_name
    #         best_email = contact_service.get_best_email_address_or_raise(requester)
    #     else:
    #         # XXX MEMBER remove when member authentication is phased out
    #         requester = ctx.principal.member
    #         if requester is None:
    #             raise InvalidPrincipalTypeError("Not authenticated as a user or member")
    #         name = requester.name
    #         best_email = requester.email_address

    #     result = committee_service.request_membership(
    #             ctx.session,
    #             committee_id=committee_id,
    #             name=name,
    #             email=best_email
    #     )

    #     if result is None:
    #         return BadRequest('There is no email address associated with this committee')

    #     return jsonify(
    #         {
    #             'status': 'success',
    #             'data': {
    #                 'email_sent': result
    #             }
    #         }
    #     )


@committee_api.route("/committee/<int:committee_id>/leaders", methods=["GET"])
@requires_auth_permissions(PmCommitteesListLeadership)
@validate_response(get_leaders_response_schema)
def get_leaders(ctx: PrincipalAuthContext, committee_id):
    try:
        committee_leadership_roles = leadership_service.get_leadership_roles(
            ctx.session, committee_id
        )
        if committee_leadership_roles is None:
            raise NotFoundError("Not found")
        return jsonify(
            [format_committee_leadership_role(r) for r in committee_leadership_roles]
        )
    except NotFoundError as e:
        return NotFound(e.message)


@committee_api.route("/committee/<int:committee_id>/leaders", methods=["POST"])
@requires_auth()
@validate_request(add_leadership_role_request_schema)
@validate_response(add_leadership_role_response_schema)
def add_leadership_role(ctx: PrincipalAuthContext, committee_id):
    if not request.json["role_title"]:
        return BadRequest("No role_title was provided")

    admin_permission = build_administer_committee_permission_from_id(committee_id)
    is_admin = admin_permission.can()
    if not is_admin:
        return NotFound("Not found")

    leadership_role = leadership_service.add_leadership_role(
        ctx.session,
        committee_id,
        request.json["role_title"],
        term_limit_months=request.json.get("term_limit_months"),
        defer_commit=False,
    )

    return jsonify(format_committee_leadership_role(leadership_role))


@committee_api.route(
    "/committee/<int:committee_id>/leaders/<int:leadership_id>", methods=["PATCH"]
)
@requires_auth()
@validate_request(replace_leader_request_schema)
@validate_response(replace_leader_response_schema)
def replace_leader(ctx: PrincipalAuthContext, committee_id, leadership_id):
    # verify committee_id matches leadership_id (also verify leadership_id is valid)
    committee = ctx.session.query(Committee).get(committee_id)
    if committee is None:
        return NotFound("Not found")

    admin_permission = build_administer_committee_permission(committee)
    is_admin = admin_permission.can()
    if not is_admin:
        return NotFound("Not found")

    committee_leadership_role = ctx.session.query(CommitteeLeadershipRole).get(
        leadership_id
    )
    if committee_leadership_role is None:
        return NotFound(f"Committee leadership role with id {leadership_id} not found")

    if committee_leadership_role.committee_id != committee_id:
        return NotFound(
            f"Committee leadership role {leadership_id} isn't part of committee {committee_id}"
        )

    try:
        term_start_date = None
        if 'term_start_date' in request.json:
            term_start_date = dateutil.parser.parse(request.json['term_start_date'])
        term_end_date = None
        if "term_end_date" in request.json:
            term_end_date = dateutil.parser.parse(request.json["term_end_date"])

        leadership_role = leadership_service.replace_leader(
            ctx.session,
            leadership_id,
            member_id=request.json.get("member_id"),
            term_start_date=term_start_date,
            term_end_date=term_end_date,
            defer_commit=False,
        )
        return jsonify(format_committee_leadership_role(leadership_role))
    except NotFoundError as e:
        return NotFound(e.message)
    except ValidationError as e:
        return BadRequest(e.message)


@committee_api.route("/committee-leadership-roles", methods=["GET"])
@requires_auth_permissions(PmCommitteesListLeadership)
def get_role_titles(ctx: PrincipalAuthContext):
    roles = ctx.session.query(LeadershipRole).all()
    return jsonify([role.title for role in roles])
