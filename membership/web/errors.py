import json
from enum import Enum
from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union

import structlog
import werkzeug.exceptions
from flask.wrappers import Response
from phonenumbers.phonenumberutil import NumberParseException

from membership.database.base import InvalidIdError
from membership.util.pagination import (
    PaginationCursorFunctionRequiredError,
    PaginationCursorNotFoundError,
    PaginationMultipleCursorsForbiddenError,
)

logger = structlog.stdlib.get_logger()


class ErrorType(Enum):
    invalid_request_error = "invalid_request_error"
    api_error = "api_error"


class ErrorCode(Enum):
    invalid_or_empty_request = "There was an issue with your request"
    parameter_invalid = "One or more parameters don't meet our validation requirements"
    resource_missing = "No such resource with that id"
    unknown_route = "Unknown API endpoint"
    server_error = "Unexpected server error"

    pagination_multiple_cursors = (
        "When getting pages, must only provide one of "
        "`starting_after` or `ending_before`, not both"
    )
    pagination_cursor_not_found = (
        "We couldn't find a page starting or ending with the resource id you provided"
    )
    invalid_id_format = "Invalid ID format"
    requires_auth = "Requires authentication"
    invalid_security_principal = "Invalid credentials"
    invalid_user = "Invalid user"
    insufficient_permissions = "Not enough permissions"
    jwt_invalid = "Invalid JWT or secret"
    phone_number_invalid = "Invalid phone number"
    association_exists = "Resource already associated with this item"
    resource_create_conflict_exclusive = (
        "Can't create resource: one or more parameters "
        "already belongs to another resource"
    )
    required_parameter_missing = "One or more required parameters not provided"
    resource_already_exists = "Resource already exists"
    resource_update_target_not_found = "Can't edit resource: target not found"
    resource_update_invalid_value = "Can't edit resource: invalid value"
    resource_archived_exists = "An archived resource with that value already exists"

    import_event_already_completed = "Import event already completed"


class ErrorResponse(Response):
    def __init__(
        self,
        *,
        status_code: HTTPStatus,
        type: ErrorType,
        code: ErrorCode,
        friendly_message: Optional[str] = None,
        params: Optional[
            Union[Dict[str, Any], str, List[str], List[Dict[str, Any]]]
        ] = None,
    ):
        """
        Error response base class for standard error messages.

        :param type {ErrorType}: what "group" of errors this type of error belongs to
        :param code {ErrorCode}: the specific type of error. should be unique to the failure path
        :param status_code {HTTPStatus}: the HTTP status code closest to the error
        :param friendly_message {str}: a message describing the error that devs can display to users
        :param params {str}: displays any request params that the error might be specific to
        """
        self.status_code = status_code
        self.type = type
        self.code = code
        self.friendly_message = friendly_message or code.value
        self.params = params

        body = self.as_json()

        super().__init__(body, status=status_code, mimetype="application/json")

    def as_json(self) -> str:
        return json.dumps(self.as_dict())

    def as_dict(self) -> Dict[str, Any]:
        return {
            "object": "error",
            "type": self.type.name,
            "error_code": self.code.name,
            "status_code": self.status_code,
            "friendly_message": self.friendly_message,
            "params": self.params,
        }


def handle_404_not_found(error: werkzeug.exceptions.NotFound) -> Response:
    response = ErrorResponse(
        code=ErrorCode.unknown_route,
        status_code=HTTPStatus.NOT_FOUND,
        type=ErrorType.invalid_request_error,
    )
    # we should get routes as part of the request handler
    logger.error(**response.as_dict())
    return response


def handle_405_method_not_allowed(
    error: werkzeug.exceptions.MethodNotAllowed,
) -> Response:
    response = ErrorResponse(
        friendly_message="Developer implementation issue - method not allowed",
        code=ErrorCode.invalid_or_empty_request,
        status_code=HTTPStatus.METHOD_NOT_ALLOWED,
        type=ErrorType.invalid_request_error,
    )
    logger.error(**response.as_dict())
    return response


def handle_500_server_error(error: werkzeug.exceptions.InternalServerError) -> Response:
    response = ErrorResponse(
        code=ErrorCode.server_error,
        status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
        type=ErrorType.api_error,
    )
    logger.exception(**response.as_dict(), error=error)
    return response


def handle_generic_exception(error: Exception):
    # pass through HTTP errors. You wouldn't want to handle these generically.
    if isinstance(error, werkzeug.exceptions.HTTPException):
        return error

    response = ErrorResponse(
        code=ErrorCode.server_error,
        status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
        type=ErrorType.api_error,
    )
    logger.exception(**response.as_dict(), error=error)
    return response


def handle_multiple_pagination_cursors(
    error: PaginationMultipleCursorsForbiddenError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.pagination_multiple_cursors,
        status_code=HTTPStatus.BAD_REQUEST,
        type=ErrorType.invalid_request_error,
    )
    logger.error(**response.as_dict())
    return response


def handle_pagination_cursor_not_found(
    error: PaginationCursorNotFoundError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.pagination_cursor_not_found,
        status_code=HTTPStatus.BAD_REQUEST,
        type=ErrorType.invalid_request_error,
    )
    logger.error(**response.as_dict())
    return response


def handle_pagination_cursor_function_required(
    error: PaginationCursorFunctionRequiredError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.server_error,
        status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
        type=ErrorType.api_error,
    )
    # This is usually a developer implementation issue
    logger.exception(**response.as_dict(), error=error)
    return response


def handle_invalid_id_format(
    error: InvalidIdError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.invalid_id_format,
        status_code=HTTPStatus.BAD_REQUEST,
        type=ErrorType.invalid_request_error,
    )
    logger.error(**response.as_dict())
    return response


def handle_invalid_phone_number(error: NumberParseException) -> Response:
    response = ErrorResponse(
        code=ErrorCode.phone_number_invalid,
        status_code=HTTPStatus.BAD_REQUEST,
        type=ErrorType.invalid_request_error,
    )
    logger.error(**response.as_dict())
    return response
