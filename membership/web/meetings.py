import datetime
import logging
from typing import List, Optional, cast  # NOQA

from flask import Blueprint, jsonify, request
from flask.helpers import make_response
from sqlalchemy.sql.schema import Column

from config import EMAIL_CONNECTOR
from membership.auth.permissions import (
    PmMeetingsAdmin,
    PmMeetingsCreate,
    PmMeetingsRead,
    build_update_meeting_permission_from_id,
)
from membership.database.base import Session
from membership.database.models import (
    Attendee,
    AttendeeEmailVerifyToken,
    Meeting,
    MeetingInvitation,
    MeetingInvitationStatus,
    Member,
    ProxyToken,
    ProxyTokenState,
)
from membership.models.authz import Authorization, NoAuthContext, PrincipalAuthContext
from membership.repos import MeetingRepo
from membership.schemas.rest.meetings import (
    bulk_send_meeting_invitations_response_schema,
    edit_meeting_agenda_request_schema,
    edit_meeting_agenda_response_schema,
    format_invitation,
    format_meeting_agenda,
    get_invitation_response_schema,
    get_meeting_agenda_response_schema,
    get_meeting_invitations_response_schema,
    get_my_invitations_response_schema,
    respond_to_meeting_invitation_response_schema,
    send_meeting_invitation_request_schema,
    send_meeting_invitation_response_schema,
    update_meeting_attendee_request_schema,
    verify_email_request_schema,
    verify_email_response_schema,
)
from membership.schemas.rest.members import format_member_info
from membership.services import phone_number_service
from membership.services.attendee_service import AttendeeService
from membership.services.crm.crm_service import CrmService
from membership.services.eligibility import SanFranciscoEligibilityService
from membership.services.errors import ValidationError
from membership.services.mailchimp.mailchimp_action_generator import (
    MailchimpActionGenerator,
)
from membership.services.mailchimp.mailchimp_service import MailchimpService
from membership.services.meeting_agenda_service import MeetingAgendaService
from membership.services.meeting_invitation_service import MeetingInvitationService
from membership.services.meeting_service import MeetingService
from membership.services.member_service import MemberService
from membership.services.onboarding.onboarding_service import OnboardingService
from membership.services.role_service import RoleService
from membership.services.slack.slack_service import SlackService
from membership.util.email import (
    get_email_connector,
    send_meeting_confirmation_email,
    send_proxy_nomination_action,
)
from membership.web.auth import requires_auth, requires_auth_permissions
from membership.web.util import (
    BadRequest,
    Forbidden,
    NotFound,
    ServerError,
    Unauthorized,
    requires_json,
    validate_request,
    validate_response,
)

meeting_api = Blueprint("meeting_api", __name__)
meeting_service = MeetingService()
meeting_repository = MeetingRepo(Meeting)
attendee_service = AttendeeService()
eligibility_service = SanFranciscoEligibilityService(
    meetings=meeting_repository, attendee_service=attendee_service
)
role_service = RoleService()
mailchimp_service = MailchimpService()
mailchimp_action_gen = MailchimpActionGenerator()
crm_service = CrmService()
slack_service = SlackService()
onboarding_service = OnboardingService(
    mailchimp_service, mailchimp_action_gen, crm_service, slack_service)
member_service = MemberService(eligibility_service, role_service, onboarding_service)
email_connector = get_email_connector(EMAIL_CONNECTOR)

MAX_NUM_PROXIES = 2


def meeting_to_dict(meeting: Meeting):
    return {
        "id": meeting.id,
        "name": meeting.name,
        "committee_id": meeting.committee_id,
        "code": meeting.short_id,
        "landing_url": meeting.landing_url,
        "start_time": meeting.start_time,
        "end_time": meeting.end_time,
        "owner": meeting.owner.name,
        "published": bool(meeting.published),
    }


@meeting_api.route("/meeting", methods=["POST"])
@requires_auth_permissions(PmMeetingsCreate, hide_presence=False)
@requires_json
def LEGACY_create_meeting(ctx: PrincipalAuthContext):
    # XXX member security principal
    if ctx.principal.member is None:
        return Unauthorized(
            "WORK IN PROGRESS: Need a member-based security principal to call this route"
        )

    try:
        name = request.json["name"]
        committee_id = request.json.get("committee_id")

        owner_id = ctx.principal.member.id
        published = request.json.get("published", True)
        meeting = meeting_service.add_meeting(
            name, committee_id, owner_id, published, ctx.session
        )
        meeting = meeting_service.set_meeting_fields(meeting, request.json, ctx.session)
    except ValidationError as e:
        ctx.session.rollback()
        return BadRequest(e.message)

    return jsonify({"status": "success", "meeting": meeting_to_dict(meeting)})


@meeting_api.route("/meeting/list", methods=["GET"])
@requires_auth_permissions(PmMeetingsRead, hide_presence=False)
def LEGACY_get_meetings(ctx: PrincipalAuthContext):
    meetings: List[Meeting] = (
        ctx.session.query(Meeting).order_by(cast(Column, Meeting.id).desc()).all()
    )
    result = [meeting_to_dict(m) for m in meetings]
    return jsonify(result)


@meeting_api.route("/meeting", methods=["PATCH"])
@requires_auth(hide_presence=False)
@requires_json
def LEGACY_update_meeting(ctx: PrincipalAuthContext):
    # TODO: Do better generic model validation
    meeting_id = int(request.json["meeting_id"])
    if not meeting_id:
        return BadRequest('Missing "meeting_id"')

    meeting = meeting_service.find_meeting_by_id(meeting_id, ctx.session)
    committee_id = None if meeting is None else meeting.committee_id
    if not meeting:
        return NotFound("Not found")

    update_permission = build_update_meeting_permission_from_id(
        meeting_id=meeting_id, committee_id=committee_id
    )
    if not update_permission.can():
        # Don't leak presence
        return NotFound("Not found")

    try:
        meeting_service.set_meeting_fields(meeting, request.json, ctx.session)
    except ValidationError as e:
        ctx.session.rollback()
        return BadRequest(e.message)

    return jsonify({"status": "success", "meeting": meeting_to_dict(meeting)})


@meeting_api.route("/meeting/attend", methods=["POST"])
@requires_auth(hide_presence=False)
@requires_json
def LEGACY_attend_meeting(ctx: PrincipalAuthContext):
    # XXX member security principal
    if ctx.principal.member is None:
        return Unauthorized(
            "WORK IN PROGRESS: Need a member-based security principal to call this route"
        )

    try:
        short_id: int = int(request.json["meeting_short_id"])
    except ValueError as e:
        return BadRequest("Cannot parse 'meeting_short_id' as int: {}".format(str(e)))

    member_id = request.json.get("member_id", ctx.principal.member.id)

    if ctx.principal.member.id != member_id:
        if not PmMeetingsAdmin.can():
            return Forbidden(
                "Must be a meeting administrator to sign in as another member"
            )

    # HACK refactor this flow
    meeting: Optional[Meeting] = None

    try:
        meeting: Optional[Meeting] = attendee_service.attend_meeting_with_short_id(
            member_id, short_id, ctx.session,
        )

        if meeting is None:
            return NotFound("Meeting with short_id={} does not exist".format(short_id))

    except ValidationError as e:
        # Attendance may be recorded retroactively; although the method which
        # sets eligibility during attendance will raise an error if the meeting
        # has already ended, we will quietly ignore it and leave the
        # eligibility status untouched.
        if e.key != SanFranciscoEligibilityService.ErrorCodes.MEETING_ENDED:
            return BadRequest(e.message)

    return jsonify(
        {
            "status": "success",
            **({"landing_url": meeting.landing_url} if meeting is not None else {}),
        }
    )


@meeting_api.route("/meetings/<int:meeting_id>", methods=["GET"])
@requires_auth_permissions(PmMeetingsRead)
def get_meeting(ctx: PrincipalAuthContext, meeting_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    if not meeting:
        return NotFound("Not found")
    return jsonify(meeting_to_dict(meeting))


@meeting_api.route("/meetings/<int:meeting_id>/attendees", methods=["GET"])
@requires_auth()
def get_meeting_attendees(ctx: PrincipalAuthContext, meeting_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    committee_id = None if meeting is None else meeting.committee_id

    if not meeting:
        return NotFound("Not found")

    permission = build_update_meeting_permission_from_id(meeting_id, committee_id)
    if not permission.can():
        return NotFound("Not found")

    result = [
        {"id": attendee.member_id, "name": attendee.member.name}
        for attendee in meeting.attendees
    ]

    return jsonify(result)


@meeting_api.route(
    "/meetings/<int:meeting_id>/attendees/<int:member_id>", methods=["PATCH"]
)
@requires_auth()
def update_meeting_attendance(
    ctx: PrincipalAuthContext, meeting_id: int, member_id: int
):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    committee_id = None if meeting is None else meeting.committee_id

    if not meeting:
        return NotFound("Not found")

    permission = build_update_meeting_permission_from_id(meeting_id, committee_id)
    if not permission.can():
        return NotFound("Not found")

    attendees = (
        ctx.session.query(Attendee)
        .filter_by(meeting_id=meeting_id, member_id=member_id,)
        .all()
    )

    try:
        for attendee in attendees:
            if request.json.get("update_eligibility_to_vote", False):
                eligibility_service.update_eligibility_to_vote_at_attendance(
                    ctx.session, attendee=attendee
                )
    except ValidationError as e:
        return BadRequest(e.message)

    return jsonify({"status": "success"})


@meeting_api.route(
    "/meetings/<int:meeting_id>/attendees/<int:member_id>", methods=["DELETE"]
)
@requires_auth()
def remove_meeting_attendee(ctx: PrincipalAuthContext, meeting_id: int, member_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    committee_id = None if meeting is None else meeting.committee_id

    if not meeting:
        return NotFound("Not found")

    permission = build_update_meeting_permission_from_id(meeting_id, committee_id)
    if not permission.can():
        return NotFound("Not found")

    attendees = (
        ctx.session.query(Attendee)
        .filter_by(meeting_id=meeting_id, member_id=member_id,)
        .all()
    )
    for attendee in attendees:
        ctx.session.delete(attendee)
    ctx.session.commit()

    return jsonify({"status": "success"})


@meeting_api.route("/meetings/<int:meeting_id>/attendee", methods=["POST"])
@requires_json
@validate_request(update_meeting_attendee_request_schema)
@requires_auth()
def add_attendee_from_kiosk(ctx: PrincipalAuthContext, meeting_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    if not meeting:
        return NotFound("Not found")

    email_address = request.json.get("email_address")
    if not email_connector.is_valid_email(email_address):
        return BadRequest("You must supply a valid email_address to check in")

    verified = request.json.get("verified") or False

    phone_number_str = request.json.get("phone_number")
    phone_number = None
    if phone_number_str:
        phone_number = phone_number_service.new(
            number=phone_number_str, name="Kiosk",
        )

    emergency_number_str = request.json.get("emergency_phone_number")
    emergency_phone_number = None
    if emergency_number_str:
        emergency_phone_number = phone_number_service.new(
            number=emergency_number_str, name="Emergency",
        )

    guardian_number_str = request.json.get("guardian_phone_number")
    guardian_phone_number = None
    if guardian_number_str:
        guardian_phone_number = phone_number_service.new(
            number=guardian_number_str, name="Parent/Guardian",
        )

    member = member_service.find_by_email(email_address, ctx.session)
    existing_member = member is not None

    if member:
        member_changed = False

        if not member.first_name:
            member.first_name = request.json.get("first_name")
            member_changed = True

        if not member.last_name:
            member.last_name = request.json.get("last_name")
            member_changed = True

        if not member.pronouns:
            member.pronouns = request.json.get("pronouns")
            member_changed = True

        member_phone_number_set = set([p.number for p in member.phone_numbers])

        if phone_number and phone_number.number not in member_phone_number_set:
            member.phone_numbers.append(phone_number)
            member_changed = True

        if (
            emergency_phone_number
            and emergency_phone_number.number not in member_phone_number_set
        ):
            member.phone_numbers.append(emergency_phone_number)
            member_changed = True

        if (
            guardian_phone_number
            and guardian_phone_number.number not in member_phone_number_set
        ):
            member.phone_numbers.append(guardian_phone_number)
            member_changed = True

        if member_changed:
            ctx.session.add(member)
            ctx.session.commit()
    else:
        if not request.json.get("first_name") and not request.json.get("last_name"):
            # Did not provide first_name or last_name in the form, probably
            # was email-only sign in
            # SECURITY: Return "success" message to avoid leaking membership discovery
            return jsonify({"status": "success"})

        phone_number_list = [
            phone_number,
            emergency_phone_number,
            guardian_phone_number,
        ]
        member = member_service.create_member(
            session=ctx.session,
            first_name=request.json.get("first_name"),
            last_name=request.json.get("last_name"),
            email_address=email_address,
            pronouns=request.json.get("pronouns"),
            defer_commit=True
        )
        member.phone_numbers = [
            number for number in phone_number_list if number is not None
        ]
        ctx.session.add(member)
        ctx.session.commit()

        results = onboarding_service.onboard_member(member, f"Meeting - {meeting.name}", False)
        logging.info(results.to_json())

    attendee = attendee_service.attend_meeting(
        member.id, meeting, ctx.session, verified=verified, return_none_on_error=True,
    )

    if not verified and attendee is not None:
        # send confirmation email with token
        email_address_override = (
            email_address if member.email_address != email_address else None
        )

        try:
            confirmation_token = attendee_service.create_attendee_token(
                session=ctx.session,
                member=member,
                attendee=attendee,
                commit=False,
                email_address_override=email_address_override,
            )
            greeting = "Welcome back" if existing_member else "Great to meet you"
            send_meeting_confirmation_email(
                email_connector=email_connector,
                member=member,
                meeting=meeting,
                token=confirmation_token.token,
                email_address_override=email_address_override,
                greeting=greeting,
            )
            ctx.session.commit()
        except Exception as e:
            ctx.session.rollback()
            logging.error(e)
            return ServerError("Error when preparing verification email")

    return jsonify({"status": "success"})


@meeting_api.route("/meetings/<int:meeting_id>/invitations", methods=["GET"])
@validate_response(get_meeting_invitations_response_schema)
@requires_auth()
def get_meeting_invitations(ctx: PrincipalAuthContext, meeting_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    committee_id = None if meeting is None else meeting.committee_id

    if not meeting:
        return NotFound("Not found")

    permission = build_update_meeting_permission_from_id(meeting_id, committee_id)
    if not permission.can():
        return NotFound("Not found")

    return jsonify(
        [format_invitation(invitation) for invitation in meeting.invitations]
    )


@meeting_api.route("/meetings/<int:meeting_id>/invitation", methods=["POST"])
@requires_json
@validate_request(send_meeting_invitation_request_schema)
@validate_response(send_meeting_invitation_response_schema)
@requires_auth()
def send_meeting_invitation(ctx: PrincipalAuthContext, meeting_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    committee_id = None if meeting is None else meeting.committee_id

    if not meeting:
        return NotFound("Not found")

    permission = build_update_meeting_permission_from_id(meeting_id, committee_id)
    if not permission.can():
        return NotFound("Not found")

    member_id = request.json.get("member_id")
    try:
        invitation = MeetingInvitationService.send_meeting_invitation(
            meeting_id=meeting_id, member_id=member_id, session=ctx.session,
        )
    except ValidationError as e:
        ctx.session.rollback()
        return BadRequest(e.message)
    return make_response(
        jsonify({"status": "success", "invitation": format_invitation(invitation)}), 201
    )


@meeting_api.route(
    "/meetings/<int:meeting_id>/invite-all-committee-members", methods=["POST"]
)
@requires_auth()
@validate_response(bulk_send_meeting_invitations_response_schema)
def bulk_send_meeting_invitations(ctx: PrincipalAuthContext, meeting_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    committee_id = None if meeting is None else meeting.committee_id

    if not meeting:
        return NotFound("Not found")

    if not committee_id:
        return NotFound("Can only bulk-invite members to committee meetings")

    permission = build_update_meeting_permission_from_id(meeting_id, committee_id)
    if not permission.can():
        return NotFound("Not found")

    try:
        invitations = MeetingInvitationService.bulk_send_meeting_invitations(
            meeting_id=meeting_id, committee_id=committee_id, session=ctx.session,
        )

        return make_response(
            jsonify(
                {
                    "status": "success",
                    "invitations": [
                        format_invitation(invitation) for invitation in invitations
                    ],
                }
            ),
            201,
        )

    except ValidationError as e:
        ctx.session.rollback()
        return BadRequest(e.message)


@meeting_api.route(
    "/meetings/<int:meeting_id>/invitation", methods=["GET"],
)
@requires_auth()
@validate_response(get_invitation_response_schema)
def get_invitation(ctx: PrincipalAuthContext, meeting_id: int):
    # XXX member security principal
    if ctx.principal.member is None:
        return Unauthorized(
            "WORK IN PROGRESS: Need a member-based security principal to call this route"
        )

    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()

    if not meeting:
        return NotFound("Not found")

    invitation = (
        ctx.session.query(MeetingInvitation)
        .filter_by(meeting_id=meeting_id, member_id=ctx.principal.member.id)
        .one_or_none()
    )
    if not invitation:
        # Don't leak presence of meeting
        return NotFound("Not found")

    return jsonify(format_invitation(invitation))


@meeting_api.route(
    "/meetings/<int:meeting_id>/invitation/<string:verb>", methods=["POST"],
)
@requires_auth()
@validate_response(respond_to_meeting_invitation_response_schema)
def respond_to_meeting_invitation(
    ctx: PrincipalAuthContext, meeting_id: int, verb: str
):
    # XXX member security principal
    if ctx.principal.member is None:
        return Unauthorized(
            "WORK IN PROGRESS: Need a member-based security principal to call this route"
        )

    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()

    if not meeting:
        return NotFound("Not found")

    invitation = (
        ctx.session.query(MeetingInvitation)
        .filter_by(meeting_id=meeting_id, member_id=ctx.principal.member.id)
        .one_or_none()
    )

    if not invitation:
        # Don't leak presence of meeting
        return NotFound("Not found")

    try:
        new_status = MeetingInvitationStatus(verb)
    except ValueError:
        # Don't leak presence of meeting
        return NotFound("Not found")

    invitation.status = new_status
    ctx.session.commit()
    return jsonify({"status": "success", "invitation": format_invitation(invitation)})


@meeting_api.route("/invitations", methods=["GET"])
@requires_auth()
@validate_response(get_my_invitations_response_schema)
def get_my_invitations(ctx: PrincipalAuthContext):
    # XXX member security principal
    if ctx.principal.member is None:
        return Unauthorized(
            "WORK IN PROGRESS: Need a member-based security principal to call this route"
        )

    return jsonify(
        [
            format_invitation(invitation)
            for invitation in ctx.principal.member.meeting_invitations
        ]
    )


@meeting_api.route("/meetings/<int:meeting_id>/proxy-token", methods=["POST"])
@requires_auth()
def generate_meeting_proxy_token(ctx: PrincipalAuthContext, meeting_id: int):
    # XXX member security principal
    if ctx.principal.member is None:
        return Unauthorized(
            "WORK IN PROGRESS: Need a member-based security principal to call this route"
        )

    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    if not meeting:
        return NotFound("Not found")

    if not eligibility_service.is_member_eligible(
        ctx.session, ctx.principal.member
    ).is_eligible:
        return BadRequest(
            "You are not eligible to vote, so you cannot nominate a proxy to vote for you",
        )

    existing_tokens = (
        ctx.session.query(ProxyToken)
        .filter(ProxyToken.member_id == ctx.principal.member.id)
        .filter(ProxyToken.meeting_id == meeting_id)
        .all()
    )
    if existing_tokens:
        return BadRequest(
            "You have already nominated a proxy for {}".format(meeting_id)
        )

    proxy_token = ProxyToken(
        member_id=ctx.principal.member.id,
        meeting_id=meeting_id,
    )
    ctx.session.add(proxy_token)
    ctx.session.commit()

    return jsonify({"proxy_token_id": proxy_token.id, "state": proxy_token.state.value})


@meeting_api.route(
    "/meetings/<int:meeting_id>/proxy-token/<string:proxy_token_id>", methods=["GET"],
)
@requires_auth()
def check_proxy_token_state(
    ctx: PrincipalAuthContext, meeting_id: int, proxy_token_id: str
):
    # XXX member security principal
    if ctx.principal.member is None:
        return Unauthorized(
            "WORK IN PROGRESS: Need a member-based security principal to call this route"
        )

    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    if not meeting:
        return NotFound("Not found")

    proxy_token = (
        ctx.session.query(ProxyToken).filter_by(id=proxy_token_id).one_or_none()
    )
    if not proxy_token:
        return NotFound("Not found")

    member = ctx.session.query(Member).filter_by(id=proxy_token.member_id).one_or_none()
    if not member:
        return NotFound("Not found")

    return jsonify(
        {
            "proxy_token_id": proxy_token.id,
            "nominator_id": member.id,
            "nominator_name": member.name,
            "recently_acted_member_id": proxy_token.receiving_member_id,
            "state": proxy_token.state.value,
        }
    )


@meeting_api.route(
    "/meetings/<int:meeting_id>/proxy-token/<string:proxy_token_id>/<string:verb>",
    methods=["POST"],
)
@requires_auth()
def act_on_meeting_proxy_token(
    ctx: PrincipalAuthContext, meeting_id: int, proxy_token_id: str, verb: str
):
    # XXX member security principal
    if ctx.principal.member is None:
        return Unauthorized(
            "WORK IN PROGRESS: Need a member-based security principal to call this route"
        )

    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    if not meeting:
        return NotFound("Not found")

    # XXX fix nullable
    if (
        meeting.end_time is not None
        and meeting.end_time
        < datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)
    ):
        return BadRequest("Meeting {} has already ended".format(meeting_id))

    proxy_token = (
        ctx.session.query(ProxyToken).filter_by(id=proxy_token_id).one_or_none()
    )

    if not proxy_token:
        return NotFound("Not found")

    if proxy_token.member_id == ctx.principal.member.id:
        return BadRequest("You cannot act on your own proxy token")

    if proxy_token.state == ProxyTokenState.ACCEPTED:
        return BadRequest(
            "Proxy token {} has already been accepted".format(proxy_token_id)
        )

    if verb == "accept":
        proxy_count = (
            ctx.session.query(ProxyToken)
            .filter_by(
                receiving_member_id=ctx.principal.member.id,
                meeting_id=meeting_id,
                state=ProxyTokenState.ACCEPTED,
            )
            .count()
        )

        if proxy_count >= MAX_NUM_PROXIES:
            return BadRequest(f"You are already a proxy for {proxy_count} people")

        proxy_token.state = ProxyTokenState.ACCEPTED
    elif verb == "reject":
        proxy_token.state = ProxyTokenState.REJECTED
    else:
        # TODO: add a REVOKE flow, where the nominator can (no matter the .state)
        # act as if they never nominated anyone
        return BadRequest(
            "Action {} is not supported, you may only accept or reject".format(verb)
        )
    proxy_token.receiving_member_id = ctx.principal.member.id
    ctx.session.add(proxy_token)

    nominating_member = (
        ctx.session.query(Member).filter_by(id=proxy_token.member_id).one()
    )

    ctx.session.commit()

    try:
        send_proxy_nomination_action(
            email_connector,
            meeting,
            nominating_member,
            ctx.principal.member,
            proxy_token,
        )
        email_sent = True
    except Exception as e:
        email_sent = False
        logging.error(
            f"Could not send proxy token update email to member.id={nominating_member.id}: {e}",
        )

    return jsonify(
        {
            "proxy_token_id": proxy_token.id,
            "nominator_id": nominating_member.id,
            "nominator_name": nominating_member.name,
            "state": proxy_token.state.value,
            "email_sent": email_sent,
        }
    )


@meeting_api.route("/verify-email", methods=["POST"])
@requires_json
@validate_request(verify_email_request_schema)
@validate_response(verify_email_response_schema)
def verify_email():
    token = request.json["token"]

    ctx = NoAuthContext(Session())

    try:
        found_token = (
            ctx.session.query(AttendeeEmailVerifyToken)
            .filter_by(token=token)
            .one_or_none()
        )
        if found_token is None:
            return NotFound("Token does not exist or has been claimed already")
        else:
            found_attendee = found_token.attendee
            found_member = found_token.member
            found_attendee.provisional = False

            member_response = format_member_info(
                found_member, Authorization(member_id=found_member.id, roles=[])
            )

            ctx.session.add(found_attendee)
            ctx.session.delete(found_token)
            ctx.session.commit()
            return make_response(
                jsonify({"status": "success", "member": member_response}), 201
            )
    except Exception as e:
        ctx.session.rollback()
        logging.error(e)
        return ServerError("Encountered an error while verifying token")
    finally:
        ctx.session.close()


@meeting_api.route("/meetings/<int:meeting_id>/agenda", methods=["GET"])
@requires_auth_permissions(PmMeetingsRead)
@validate_response(get_meeting_agenda_response_schema)
def get_meeting_agenda(ctx: PrincipalAuthContext, meeting_id: int):
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one_or_none()
    if not meeting:
        return NotFound("Not found")

    agenda = MeetingAgendaService.get_or_create_agenda(
        meeting=meeting, session=ctx.session
    )

    return jsonify(format_meeting_agenda(agenda))


@meeting_api.route("/meetings/<int:meeting_id>/agenda", methods=["POST", "PATCH"])
@requires_auth()
@validate_request(edit_meeting_agenda_request_schema)
@validate_response(edit_meeting_agenda_response_schema)
def edit_meeting_agenda(ctx: PrincipalAuthContext, meeting_id: int):
    meeting = meeting_service.find_meeting_by_id(meeting_id, ctx.session)
    committee_id = None if meeting is None else meeting.committee_id

    if not meeting:
        return NotFound("Not found")

    permission = build_update_meeting_permission_from_id(meeting_id, committee_id)
    if not permission.can():
        return NotFound("Not found")

    try:
        agenda = MeetingAgendaService.create_or_update_agenda(
            meeting=meeting,
            text=request.json["text"],
            checkout_timestamp_str=request.json["checkout_timestamp"],
            session=ctx.session,
        )
    except ValidationError as e:
        ctx.session.rollback()
        return BadRequest(e.message)
    return jsonify({"status": "success", "agenda": format_meeting_agenda(agenda)})
