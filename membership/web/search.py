import structlog
from flask import Blueprint
from flask.helpers import url_for
from flask.wrappers import Response
from sqlalchemy.exc import IntegrityError

from membership.auth.permissions import PmContactsList, PmSearch, PmTagsAdmin
from membership.models.authz import PrincipalAuthContext
from membership.schemas.formatters.contacts import format_contacts_list
from membership.schemas.formatters.tags import format_tags_list
from membership.schemas.rest.contacts import SxSearchContactsQuery
from membership.schemas.rest.search import SxSearchTagsQuery
from membership.services import contact_service, tag_service
from membership.web.auth import requires_auth_permissions
from membership.web.util import (
    ServerError,
    make_paginated_list_response,
    validate_with_pydantic,
)

search_api = Blueprint("search_api", __name__)
logger = structlog.stdlib.get_logger()


@search_api.route("/search/tags", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmSearch, PmTagsAdmin)
def search_tags(ctx: PrincipalAuthContext, query: SxSearchTagsQuery) -> Response:
    try:
        tags = tag_service.search(
            ctx.session,
            query=query,
            limit=query.limit,
            starting_after=query.starting_after,
            ending_before=query.ending_before,
        )

        return make_paginated_list_response(
            format_tags_list(tags, url_for(".search_tags", **query.dict())),
            limit=query.limit,
            next_cursor=tags.next_cursor,
            prev_cursor=tags.prev_cursor,
            route="search_api.search_tags",
        )

    except IntegrityError as e:
        logger.exception("searching tags", error=e)
        return ServerError("database error when searching tags")


@search_api.route("/search/contacts", methods=["GET"])
@validate_with_pydantic(exclude_path_args=["ctx"])
@requires_auth_permissions(PmSearch, PmContactsList)
def search_contacts(
    ctx: PrincipalAuthContext, query: SxSearchContactsQuery
) -> Response:
    try:
        contacts = contact_service.search(
            ctx.session,
            query=query,
            limit=query.limit,
            starting_after=query.starting_after,
            ending_before=query.ending_before,
        )
        return make_paginated_list_response(
            format_contacts_list(contacts, url_for(".search_contacts", **query.dict())),
            limit=query.limit,
            next_cursor=contacts.next_cursor,
            prev_cursor=contacts.prev_cursor,
            route="search_api.search_contacts",
        )

    except IntegrityError as e:
        logger.exception("searching contacts", error=e)
        return ServerError("database error when searching contacts")
