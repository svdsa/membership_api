from http import HTTPStatus

import pydantic.error_wrappers
import structlog
from flask.wrappers import Response

from membership.errors.exceptions import (
    AssociationExistsError,
    BulkResourceMissingError,
    CreateAlreadyExistsError,
    CreateArchivedExistsError,
    CreateExclusiveParameterConflictError,
    ParameterMissingError,
    ParameterValidationError,
    ResourceMissingError,
    UpdateTargetNotFoundError,
)
from membership.web.errors import ErrorCode, ErrorResponse, ErrorType

logger = structlog.stdlib.get_logger()


def handle_create_conflict_error(
    error: CreateExclusiveParameterConflictError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.resource_create_conflict_exclusive,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.CONFLICT,
        params=error.params,
    )
    logger.error(**response.as_dict())
    return response


def handle_parameter_missing_error(
    error: ParameterMissingError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.required_parameter_missing,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.BAD_REQUEST,
        params=error.params,
    )
    logger.error(**response.as_dict())
    return response


def handle_validation_error(error: ParameterValidationError) -> Response:
    response = ErrorResponse(
        code=ErrorCode.parameter_invalid,
        status_code=HTTPStatus.BAD_REQUEST,
        type=ErrorType.invalid_request_error,
        friendly_message=error.message,
        params=error.param,
    )
    logger.error(**response.as_dict())
    return response


def handle_pydantic_validation_error(
    error: pydantic.error_wrappers.ValidationError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.parameter_invalid,
        status_code=HTTPStatus.BAD_REQUEST,
        type=ErrorType.invalid_request_error,
        friendly_message=str(error),
        params=error.json(),
    )
    logger.error(**response.as_dict())
    return response


def handle_resource_missing_error(error: ResourceMissingError) -> Response:
    response = ErrorResponse(
        code=ErrorCode.resource_missing,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.NOT_FOUND,
    )
    logger.error(**response.as_dict())
    return response


def handle_bulk_resource_missing_error(error: BulkResourceMissingError) -> Response:
    response = ErrorResponse(
        code=ErrorCode.resource_missing,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.NOT_FOUND,
        friendly_message="No such resources for one or more ids",
        params={"items": error.items},
    )
    logger.error(**response.as_dict())
    return response


def handle_resource_already_exists(error: CreateAlreadyExistsError) -> Response:
    response = ErrorResponse(
        code=ErrorCode.resource_already_exists,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.CONFLICT,
        params=error.object,
    )
    logger.error(**response.as_dict())
    return response


def handle_update_target_not_found_error(
    error: UpdateTargetNotFoundError,
) -> Response:
    response = ErrorResponse(
        code=ErrorCode.resource_update_target_not_found,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.BAD_REQUEST,
        params=error.params,
    )
    logger.error(**response.as_dict())
    return response


def handle_association_exists_error(error: AssociationExistsError) -> Response:
    response = ErrorResponse(
        code=ErrorCode.association_exists,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.CONFLICT,
        params={"target": error.target_object, "association": error.association_object},
    )
    logger.error(**response.as_dict())
    return response


def handle_archived_resource_exists_error(error: CreateArchivedExistsError) -> Response:
    response = ErrorResponse(
        code=ErrorCode.resource_archived_exists,
        type=ErrorType.invalid_request_error,
        status_code=HTTPStatus.CONFLICT,
        params={"object": error.object, "existing_id": error.existing_id},
    )
    logger.error(**response.as_dict())
    return response
