from typing import Any, List, Optional


class CreateAlreadyExistsError(Exception):
    def __init__(self, *, object: Optional[str] = None):
        super().__init__(object)
        self.object = object


class CreateArchivedExistsError(Exception):
    def __init__(
        self, *, object: Optional[str] = None, existing_id: Optional[str] = None
    ):
        super().__init__(object)
        self.object = object
        self.existing_id = existing_id


class CreateExclusiveParameterConflictError(Exception):
    def __init__(self, *, params: Optional[str] = None):
        super().__init__(params)
        self.params = params


class AssociationExistsError(Exception):
    def __init__(
        self,
        *,
        target_object: Optional[str] = None,
        association_object: Optional[str] = None
    ) -> None:
        super().__init__(target_object, association_object)
        self.target_object = target_object
        self.association_object = association_object


class ParameterMissingError(Exception):
    def __init__(self, *, params: Optional[str] = None):
        super().__init__(params)
        self.params = params


class UpdateTargetNotFoundError(Exception):
    def __init__(self, *, params: Optional[str] = None):
        super().__init__(params)
        self.params = params


class ResourceMissingError(Exception):
    def __init__(self, *, object: Optional[str] = None):
        super().__init__(object)
        self.object = object


class BulkResourceMissingError(ResourceMissingError):
    items: Optional[List[Any]] = None

    def __init__(
        self, *, object: Optional[str] = None, items: Optional[List[Any]] = None
    ):
        super().__init__(object=object)
        self.items = items


class ParameterValidationError(Exception):
    param: Optional[str]
    object: Optional[str]
    message: Optional[str]

    def __init__(
        self,
        *,
        object: Optional[str] = None,
        message: Optional[str] = None,
        param: Optional[str] = None,
    ) -> None:
        super().__init__(object, param)
        self.message = message or f"Validation error on {object or 'your request'}"
        self.object = object
        self.param = param
