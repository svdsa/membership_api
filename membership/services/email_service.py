"""Operations related to email addresses."""

from typing import List

from sqlalchemy import orm

from membership.database.models import AdditionalEmailAddress
from membership.database.statements import insert_or_ignore


class AdditionalEmailAddressService:
    def add_all(self, session: orm.Session, addresses: List[AdditionalEmailAddress]):
        statement = insert_or_ignore(session, AdditionalEmailAddress)
        session.execute(
            statement,
            [
                {
                    'member_id': address.member_id,
                    'name': address.name,
                    'email_address': address.email_address,
                }
                for address in addresses
            ],
        )
        session.commit()
