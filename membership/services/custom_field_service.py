from datetime import datetime
from typing import Any, List, Optional, cast

from sqlalchemy import orm, select
from sqlalchemy.sql.schema import Column

from membership.database.models import Contact, CustomFieldDefinition, CustomFieldType
from membership.errors.exceptions import (
    AssociationExistsError,
    BulkResourceMissingError,
    ResourceMissingError,
)
from membership.schemas.rest.custom_field_options import (
    SxCustomFieldChoiceOptions,
    SxCustomFieldOptionsUnion,
    SxCustomFieldRangeOptions,
)
from membership.schemas.rest.custom_fields import (
    IdCustomField,
    SxCustomFieldCreateRequest,
    SxCustomFieldUpdateRequest,
)
from membership.util.pagination import (
    DEFAULT_PAGE_LIMIT,
    PaginatedList,
    list_pagable_elements,
)


class CustomFieldAssociationExistsError(AssociationExistsError):
    pass


class CustomFieldNotFoundError(ResourceMissingError):
    def __init__(self):
        super().__init__(object="custom_field")


class BulkCustomFieldNotFoundError(BulkResourceMissingError):
    def __init__(
        self,
        *,
        message: Optional[str] = None,
        items: Optional[List[Any]] = None,
    ):
        super(BulkCustomFieldNotFoundError, self).__init__(
            object="custom_field", items=items
        )


class InvalidCustomFieldOptionsError(ValueError):
    pass


class MissingCustomFieldOptionsError(ValueError):
    pass


def from_id(
    session: orm.Session,
    id: IdCustomField,
) -> Optional[CustomFieldDefinition]:
    raw_id = CustomFieldDefinition.parse_id(id)
    existing_def = session.query(CustomFieldDefinition).get(raw_id)
    return existing_def


def from_id_or_raise(session: orm.Session, id: IdCustomField) -> CustomFieldDefinition:
    existing_def = from_id(session, id)
    if existing_def is None:
        raise CustomFieldNotFoundError()

    return existing_def


def bulk_from_id(
    session: orm.Session, ids: List[IdCustomField]
) -> List[Optional[CustomFieldDefinition]]:
    parsed_ids = [CustomFieldDefinition.parse_id(id) for id in ids]
    results: List[CustomFieldDefinition] = (
        session.execute(
            select(CustomFieldDefinition).where(
                cast(Column, CustomFieldDefinition._raw_id).in_(parsed_ids)
            )
        )
        .scalars()
        .all()
    )
    keyed_by_id = {row.id: row for row in results}
    return [keyed_by_id.get(cf_id) for cf_id in ids]


def bulk_from_id_or_raise(
    session: orm.Session, ids: List[IdCustomField]
) -> List[CustomFieldDefinition]:
    results = bulk_from_id(session, ids)

    if not all(v is not None for v in results):
        missing = [ids[i] for i in range(len(results)) if results[i] is None]
        raise BulkCustomFieldNotFoundError(items=missing)

    return cast(List[CustomFieldDefinition], results)


def create(
    session: orm.Session,
    create: SxCustomFieldCreateRequest,
    *,
    date_created: Optional[datetime] = None,
    defer_commit: bool = True,
) -> CustomFieldDefinition:
    date_created = date_created or create.date_created or datetime.utcnow()

    existing_field = (
        session.execute(
            select(CustomFieldDefinition).where(
                cast(Column, CustomFieldDefinition.name).ilike(create.name)
            )
        )
        .scalars()
        .one_or_none()
    )
    if existing_field is not None:
        raise CustomFieldAssociationExistsError()

    parsed_options = _parse_options_for_type(create.field_type, create.options)

    cfd = CustomFieldDefinition(
        name=create.name,
        description=create.description,
        field_type=create.field_type,
        field_options=parsed_options if parsed_options else None,
        date_created=date_created,
    )

    session.add(cfd)
    session.flush()
    session.refresh(cfd)

    if not defer_commit:
        session.commit()

    return cfd


def _parse_options_for_type(
    type: CustomFieldType, options: Optional[SxCustomFieldOptionsUnion]
) -> Optional[SxCustomFieldOptionsUnion]:
    if type == CustomFieldType.choice:
        if options is not None:
            return SxCustomFieldChoiceOptions.validate(options)
        else:
            raise MissingCustomFieldOptionsError(
                "must provide choices as field options"
            )
    if type == CustomFieldType.range:
        if options is not None:
            return SxCustomFieldRangeOptions.validate(options)
        else:
            raise MissingCustomFieldOptionsError(
                "must provide range info (start, end) as field options"
            )
    if type == CustomFieldType.array or type == CustomFieldType.object:
        raise InvalidCustomFieldOptionsError(
            "options are not implemented for array or object types"
        )
    elif (
        type == CustomFieldType.boolean
        or type == CustomFieldType.date
        or type == CustomFieldType.number
        or type == CustomFieldType.text
        or type == CustomFieldType.textarea
        or type == CustomFieldType.url
    ):
        if options is not None:
            raise InvalidCustomFieldOptionsError(
                "options are not valid for simple types"
            )
        else:
            return None
    else:
        raise InvalidCustomFieldOptionsError("couldn't parse custom field options")


def list_(
    session: orm.Session,
    *,
    limit: Optional[int] = DEFAULT_PAGE_LIMIT,
    starting_after: Optional[str] = None,
    ending_before: Optional[str] = None,
) -> PaginatedList[CustomFieldDefinition]:
    limit = limit if limit is not None else DEFAULT_PAGE_LIMIT
    return list_pagable_elements(
        session,
        CustomFieldDefinition,
        from_id_fn=from_id,
        limit=limit,
        starting_after=starting_after,
        ending_before=ending_before,
    )


# update should only handle name and description rn
# changing field type should be impossible or severely restricted (e.g. text -> textarea only)
# changing options should be non-destructive and only apply to values moving forward
# - expanding range or adding choices is straightforward
# - restricting range should not clamp or alter values
# - restricting choices should not alter values
# may offer front-end to bulk edit options later
def update(
    session: orm.Session,
    to_modify: CustomFieldDefinition,
    *,
    update_request: SxCustomFieldUpdateRequest,
    defer_commit: bool = True,
) -> CustomFieldDefinition:
    for key, value in update_request.dict(
        exclude_unset=True, exclude=set(["options"])
    ).items():
        setattr(to_modify, key, value)

    if update_request.options is not None:
        parsed_options = _parse_options_for_type(
            to_modify.field_type, update_request.options
        )
        to_modify.field_options = parsed_options
    else:
        set_attributes = update_request.dict(exclude_unset=True)
        if "options" in set_attributes and set_attributes.get("options") is None:
            raise MissingCustomFieldOptionsError("cannot clear options")

    session.add(to_modify)
    session.flush()
    session.refresh(to_modify)

    if not defer_commit:
        session.commit()

    return to_modify


def update_from_id(
    session: orm.Session,
    id: IdCustomField,
    *,
    update_request: SxCustomFieldUpdateRequest,
    defer_commit: bool = True,
) -> Contact:
    to_modify = from_id_or_raise(session, id)
    return update(
        session, to_modify, update_request=update_request, defer_commit=defer_commit
    )


def delete(
    session: orm.Session,
    to_delete: CustomFieldDefinition,
    *,
    defer_commit: bool = True,
) -> CustomFieldDefinition:
    session.delete(to_delete)
    session.flush()

    if not defer_commit:
        session.commit()

    return to_delete


def delete_from_id(
    session: orm.Session, id: IdCustomField, *, defer_commit: bool = True
) -> CustomFieldDefinition:
    to_delete = from_id_or_raise(session, id)
    return delete(session, to_delete, defer_commit=defer_commit)


# TODO later: stubs for attaching to contacts and tags

# TODO later: query
