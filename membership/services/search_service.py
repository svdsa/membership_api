import shlex
from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import Dict, List, Optional

from sqlalchemy import and_, or_
from sqlalchemy.orm.query import Query

from membership.database.models import Member, NationalMembershipData, PhoneNumber

# Arbitrarily chosen to prevent excessive query / SQL length
MAXLEN_SEARCH_STR = 96


def _no_membership_query(query: Query) -> Query:
    return query.filter(Member.memberships_usa == None)


def _valid_membership_query(query: Query) -> Query:
    return query.filter(
        Member.memberships_usa.any(
            NationalMembershipData.dues_paid_until > datetime.now()
        )
    )


def _expired_membership_query(query: Query) -> Query:
    return query.filter(
        Member.memberships_usa.any(
            NationalMembershipData.dues_paid_until < datetime.now()
        )
    )


def _new_member_query(query: Query) -> Query:
    return query.filter(
        Member.memberships_usa.any(
            NationalMembershipData.join_date > (datetime.now() - timedelta(weeks=4))
        )
    )


def _city_filter(qualifier: str):
    return or_(
        Member.memberships_usa.any(
            and_(
                NationalMembershipData.dues_paid_until > datetime.now(),
                NationalMembershipData.city.ilike(f'{qualifier}%'),
            )
        ),
        Member.city.ilike(f'{qualifier}%'),
    )


def _zip_filter(qualifier: str):
    return or_(
        Member.memberships_usa.any(
            and_(
                NationalMembershipData.dues_paid_until > datetime.now(),
                NationalMembershipData.zipcode.ilike(f'{qualifier}%'),
            )
        ),
        Member.zipcode.ilike(f'{qualifier}%'),
    )


def _process_location_operator(query: Query, qualifier: str) -> Query:
    return query.filter(or_(_city_filter(qualifier), _zip_filter(qualifier)))


def _process_city_operator(query: Query, qualifier: str) -> Query:
    return query.filter(_city_filter(qualifier))


def _process_zip_operator(query: Query, qualifier: str) -> Query:
    return query.filter(_zip_filter(qualifier))


def _process_is_operator(query: Query, qualifier: str) -> Query:
    if qualifier == "new":
        return _new_member_query(query)
    elif qualifier == "expired":
        return _expired_membership_query(query)
    else:
        return query


def _process_membership_operator(query: Query, qualifier: str) -> Query:
    if qualifier == "none":
        return _no_membership_query(query)
    elif qualifier == "valid":
        return _valid_membership_query(query)
    elif qualifier == "expired":
        return _expired_membership_query(query)
    elif qualifier == "new":
        return _new_member_query(query)
    else:
        return query


def _process_phone_operator(query: Query, qualifier: str) -> Query:
    return query.filter(
        Member.phone_numbers.any(PhoneNumber._raw_number.ilike(f"%{qualifier}%"))
    )


VALID_OPERATORS = {
    # 'joined': process_joined_operator,
    # 'attended': process_attended_operator,
    'is': _process_is_operator,
    'membership': _process_membership_operator,
    'location': _process_location_operator,
    'city': _process_city_operator,
    'zip': _process_zip_operator,
    'phone': _process_phone_operator,
}


def _process_operator(query: Query, operator: str) -> Query:
    # A search operator looks like "verb:text that can be separated by spaces"
    verb, qualifier = operator.split(':')
    if verb in VALID_OPERATORS:
        return VALID_OPERATORS[verb](query, qualifier)
    else:
        return query


def process_search_query(query: Query, query_str: Optional[str]) -> Query:
    if query_str is not None and len(query_str) < MAXLEN_SEARCH_STR:
        try:
            tokens = set(shlex.split(query_str))
        except Exception:
            tokens = set(query_str.split(' '))

        bare_words = set(token for token in tokens if ':' not in token)
        operators = tokens - bare_words

        for operator in operators:
            query = _process_operator(query, operator)

        for word in bare_words:
            formatted_query = f"{word}%"
            # Case-insensitive query for string in user name and email
            or_filter = or_(
                Member.first_name.ilike(formatted_query),
                Member.last_name.ilike(formatted_query),
                Member.email_address.ilike(formatted_query),
            )
            query = query.filter(or_filter)

        return query
    else:
        # Don't modify
        return query


class SearchQueryTooLongError(ValueError):
    pass


@dataclass
class QueryStringTokens:
    bare_tokens: List[str]
    operators: Dict[str, str]


def process_query_string(query_str: str) -> QueryStringTokens:
    if len(query_str) > MAXLEN_SEARCH_STR:
        raise SearchQueryTooLongError(
            f"search query longer than max length ({MAXLEN_SEARCH_STR} chars)"
        )

    try:
        tokens = set(shlex.split(query_str))
    except Exception:
        tokens = set(query_str.split(" "))

    bare_words = set(token for token in tokens if ":" not in token)
    operators = tokens - bare_words

    return QueryStringTokens(
        bare_tokens=list(bare_words),
        operators={k: v for operator in operators for k, v in operator.split(":")},
    )
