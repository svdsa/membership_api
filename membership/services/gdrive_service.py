"""Service for interacting with Google Drive
"""

import logging
import io
import httplib2
from logging import Logger
from typing import Optional
import backoff
from google.oauth2 import service_account
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaIoBaseUpload

MAX_UPLOAD_TRIES = 5

SCOPES = [
    'https://www.googleapis.com/auth/drive.metadata.readonly',
    'https://www.googleapis.com/auth/drive.file'
]


class GdriveFileUploadException(Exception):
    pass


class GdriveChunkUploadException(Exception):
    pass


class GdriveService:
    def __init__(self, logger: Logger = logging.getLogger(__name__), credentials_file: str = None):
        self.logger = logger
        credentials = service_account.Credentials.from_service_account_file(
            credentials_file,
            scopes=SCOPES
        )
        # disable cache discovery to prevent import error
        self.client = build('drive', 'v3', credentials=credentials, cache_discovery=False)

    def get_dir_id(self, dirname: str) -> Optional[str]:
        """
        Gets directory id from human readable name
        """
        query: str = f"mimeType = 'application/vnd.google-apps.folder' and name = '{dirname}'"
        page_token = None
        response = self.client.files().list(
            q=query,
            includeItemsFromAllDrives=True,
            supportsAllDrives=True,
            spaces='drive',
            fields='nextPageToken, files(id)',
            pageToken=page_token
        ).execute()
        files = response.get('files', [])
        if not files:
            return None
        if len(files) > 1:
            self.logger.warning(f"Multiple directories with name '{dirname}', using first result.")
        return files[0]['id']

    @backoff.on_exception(
        backoff.expo,
        GdriveFileUploadException,
        max_tries=MAX_UPLOAD_TRIES
    )
    def upload_bytes_to_dir(
        self,
        body: io.BytesIO,
        filename: str,
        dir_id: str,
        mime_type: str = 'application/octet-stream'
    ) -> Optional[str]:
        """
        Do in mem file upload
        """
        media = MediaIoBaseUpload(body, mimetype=mime_type, resumable=True)
        file_metadata = {
            "name": filename,
            "parents": [dir_id]
        }
        request = self.client.files().create(
            body=file_metadata,
            media_body=media,
            supportsAllDrives=True,
            fields='id'
        )
        resp = None
        try:
            resp = self.__upload_chunks(request)
        except HttpError as e:
            if e.resp.status in [404]:
                raise GdriveFileUploadException from e
            else:
                raise e

        return None if resp is None else resp.get('id')

    @backoff.on_exception(
        backoff.expo,
        GdriveChunkUploadException,
        max_time=90
    )
    def __upload_chunks(self, request) -> Optional[httplib2.Response]:
        response = None
        try:
            while response is None:
                (status, response) = request.next_chunk()
        except HttpError as e:
            if e.resp.status in [500, 502, 503, 504]:
                raise GdriveChunkUploadException from e
            else:
                raise e
        return response
