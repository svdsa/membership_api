import datetime
import logging
import random
from typing import List, Optional, Union

import dateutil.parser
from sqlalchemy import and_, or_, orm

from membership.database.models import Meeting
from membership.services.errors import ValidationError

logger = logging.getLogger(__name__)


class MeetingService:

    class ErrorCodes:
        NAME_TAKEN = "NAME_TAKEN"
        INVALID_CODE = "INVALID_CODE"
        INVALID_START_END_TIMES = "INVALID_START_END_TIMES"

    def add_meeting(
        self,
        name: str,
        committee_id: Optional[int],
        owner_id: int,
        published: bool,
        session: orm.Session,
    ) -> Meeting:
        if session.query(Meeting).filter_by(name=name).count() > 0:
            raise ValidationError(
                MeetingService.ErrorCodes.NAME_TAKEN,
                'A meeting with this name already exists.'
            )

        meeting = Meeting(
            name=name,
            committee_id=committee_id,
            owner_id=owner_id,
            published=published,
        )
        session.add(meeting)
        return meeting

    def find_meeting_by_id(
        self, meeting_id: int, session: orm.Session
    ) -> Optional[Meeting]:
        return session.query(Meeting).get(meeting_id)

    def set_meeting_fields(self, meeting: Meeting, json: dict, session: orm.Session) -> Meeting:
        if 'name' in json:
            meeting.name = json.get('name')
        if 'committee_id' in json:
            meeting.committee_id = json.get('committee_id')
        if 'landing_url' in json:
            meeting.landing_url = json.get('landing_url')
        if 'code' in json:
            meeting = self.set_meeting_code(meeting, json['code'], session)
        if 'published' in json:
            meeting.published = json.get('published')
        if 'start_time' in json:
            if json['start_time'] is None:
                meeting.start_time = None
            else:
                meeting.start_time = dateutil.parser.parse(json['start_time'])
        if 'end_time' in json:
            if json['end_time'] is None:
                meeting.end_time = None
            else:
                meeting.end_time = dateutil.parser.parse(json['end_time'])

        if (meeting.end_time is not None and meeting.start_time is not None
                and meeting.end_time < meeting.start_time):
            raise ValidationError(
                MeetingService.ErrorCodes.INVALID_START_END_TIMES,
                'The end time of the meeting must not be before the start time.'
            )

        session.add(meeting)
        session.commit()

        # Send pending invitations if the meeting is now published
        if meeting.published:
            from membership.services.meeting_invitation_service import (
                MeetingInvitationService,
            )
            MeetingInvitationService.send_pending_invitations(
                meeting_id=meeting.id,
                session=session,
            )

        return meeting

    def set_meeting_code(self,
                         meeting: Meeting,
                         code: Optional[Union[int, str]],
                         session: orm.Session) -> Meeting:
        if code is None:
            meeting.short_id = None
        elif code == 'autogenerate':
            meeting.short_id = self.generate_meeting_code(session)
        else:
            try:
                short_id = int(code)
            except (TypeError, ValueError):
                raise ValidationError(
                    MeetingService.ErrorCodes.INVALID_CODE,
                    f'Invalid meeting code: {repr(code)}.'
                    ' Expected 4 digit integer, null, or "autogenerate".'
                )
            if short_id < 1000 or short_id > 9999:
                raise ValidationError(
                    MeetingService.ErrorCodes.INVALID_CODE,
                    f'Invalid meeting code: {short_id}. Must be 4 digits long.'
                )
            meeting.short_id = short_id
        return meeting

    def generate_meeting_code(self, session: orm.Session) -> int:
        for _ in range(5):
            # Not used for cryptographic purposes, only to generate a unique meeting ID
            short_id = random.randint(1000, 9999)  # nosec
            if session.query(Meeting).filter_by(short_id=short_id).count() == 0:
                return short_id

        raise Exception('Failed to find an unused meeting code after five tries.')

    def get_last_three_meetings_impacting_eligibility(self, session: orm.Session) -> List[Meeting]:
        meetings = session\
            .query(Meeting)\
            .filter(and_(
                Meeting.start_time < datetime.datetime.now(),
                or_(Meeting.name.like('%General%'), Meeting.name.like('%Regular%'))))\
            .order_by(Meeting.start_time.desc())\
            .limit(3)\
            .all()

        return meetings
