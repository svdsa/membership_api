from datetime import datetime
from typing import List, Optional, cast

from sqlalchemy import orm, select
from sqlalchemy.sql.elements import ClauseElement
from sqlalchemy.sql.expression import or_
from sqlalchemy.sql.schema import Column

from membership.database.models import Tag
from membership.errors.exceptions import (
    AssociationExistsError,
    CreateAlreadyExistsError,
    ResourceMissingError,
)
from membership.schemas.rest.search import SxSearchTagsQuery
from membership.schemas.rest.tags import IdTag, SxTagUpdateRequest
from membership.services import search_service
from membership.util.pagination import (
    DEFAULT_PAGE_LIMIT,
    PaginatedList,
    list_pagable_elements,
)


class TagExistsError(CreateAlreadyExistsError):
    pass


class TagAssociationExistsError(AssociationExistsError):
    def __init__(self, *, target_object: Optional[str] = None) -> None:
        super().__init__(target_object=target_object, association_object="tag")


class TagNotFoundError(ResourceMissingError):
    def __init__(self):
        super().__init__(object="tag")


def from_id(
    session: orm.Session,
    id: IdTag,
) -> Optional[Tag]:
    raw_id = Tag.parse_id(id)
    existing_def = session.query(Tag).get(raw_id)
    return existing_def


def from_id_or_raise(session: orm.Session, id: IdTag) -> Tag:
    existing_def = from_id(session, id)
    if existing_def is None:
        raise TagNotFoundError()

    return existing_def


def find_existing(session: orm.Session, *, tag: str) -> Optional[Tag]:
    return session.execute(select(Tag).where(Tag.name == tag)).scalars().one_or_none()


def _create(
    session: orm.Session, *, tag: str, date_created: datetime, defer_commit: bool
) -> Tag:
    """Internal use only.
    Use `create_or_raise` or `find_existing_or_create` instead."""

    new_tag = Tag(name=tag, date_created=date_created)
    session.add(new_tag)
    session.flush()
    session.refresh(new_tag)

    if not defer_commit:
        session.commit()

    return new_tag


def create_or_raise(
    session: orm.Session,
    *,
    tag: str,
    date_created: Optional[datetime] = None,
    defer_commit: bool = True,
) -> Tag:
    """Create a tag, or raise ContactTagExistsError
    if the tag already exists in the database."""

    date_created = date_created or datetime.utcnow()

    existing_tag = find_existing(session, tag=tag)
    if existing_tag is not None:
        raise TagExistsError()
    return _create(
        session, tag=tag, date_created=date_created, defer_commit=defer_commit
    )


def find_existing_or_create(
    session: orm.Session,
    *,
    tag: str,
    date_created: Optional[datetime] = None,
    defer_commit: bool = True,
) -> Tag:
    """Find a tag named `tag`, or create it if it doesn't exist."""

    date_created = date_created or datetime.utcnow()

    existing_tag = find_existing(session, tag=tag)
    if existing_tag is not None:
        return existing_tag
    return _create(
        session, tag=tag, date_created=date_created, defer_commit=defer_commit
    )


def list_(
    session: orm.Session,
    *,
    limit: Optional[int] = DEFAULT_PAGE_LIMIT,
    starting_after: Optional[str] = None,
    ending_before: Optional[str] = None,
) -> PaginatedList[Tag]:
    limit = limit if limit is not None else DEFAULT_PAGE_LIMIT
    return list_pagable_elements(
        session,
        Tag,
        from_id_fn=from_id,
        limit=limit,
        starting_after=starting_after,
        ending_before=ending_before,
    )


def update_from_id(
    session: orm.Session,
    id: IdTag,
    *,
    update_request: SxTagUpdateRequest,
    defer_commit: bool = True,
) -> Tag:
    to_modify = from_id_or_raise(session, id)
    return update(
        session, to_modify, update_request=update_request, defer_commit=defer_commit
    )


def update(
    session: orm.Session,
    to_modify: Tag,
    *,
    update_request: SxTagUpdateRequest,
    defer_commit: bool = True,
) -> Tag:
    for key, value in update_request.dict(
        exclude_unset=True,
        exclude_none=True,
    ).items():
        setattr(to_modify, key, value)

    session.add(to_modify)
    session.flush()
    session.refresh(to_modify)

    if not defer_commit:
        session.commit()

    return to_modify


def delete_from_id(
    session: orm.Session, id: IdTag, *, defer_commit: bool = True
) -> Tag:
    to_delete = from_id_or_raise(session, id)
    return delete(session, to_delete, defer_commit=defer_commit)


def delete(session: orm.Session, to_delete: Tag, *, defer_commit: bool = True) -> Tag:
    session.delete(to_delete)
    session.flush()

    if not defer_commit:
        session.commit()

    return to_delete


def search(
    session: orm.Session,
    query: SxSearchTagsQuery,
    limit: Optional[int] = DEFAULT_PAGE_LIMIT,
    starting_after: Optional[str] = None,
    ending_before: Optional[str] = None,
) -> PaginatedList[Tag]:
    limit = limit if limit is not None else DEFAULT_PAGE_LIMIT

    processed_tokens = search_service.process_query_string(query.q)
    or_clauses: List[ClauseElement] = []
    for token in processed_tokens.bare_tokens:
        or_clauses.append(cast(Column, Tag.name).ilike(f"%{token}%"))

    return list_pagable_elements(
        session,
        Tag,
        from_id_fn=from_id,
        where=[or_(*or_clauses)],
        limit=limit,
        starting_after=starting_after,
        ending_before=ending_before,
    )
