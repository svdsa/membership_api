from datetime import datetime
from typing import List, Optional, cast

from sqlalchemy import orm, select
from sqlalchemy.sql.schema import Column

from membership.database.models import Contact, PostalAddress
from membership.errors.exceptions import AssociationExistsError, ResourceMissingError
from membership.schemas.id import IdContact, IdPostalAddress
from membership.schemas.rest.postal_addresses import (
    SxPostalAddressCreateRequest,
    SxPostalAddressUpdateRequest,
)
from membership.util.pagination import (
    DEFAULT_PAGE_LIMIT,
    PaginatedList,
    list_pagable_elements,
)


class ContactMailingAddressExistsError(AssociationExistsError):
    pass


class PostalAddressNotFoundError(ResourceMissingError):
    def __init__(self):
        super().__init__(object="postal_address")


def from_id(
    session: orm.Session,
    id: IdPostalAddress,
) -> Optional[PostalAddress]:
    raw_id = PostalAddress.parse_id(id)
    existing_def = session.query(PostalAddress).get(raw_id)
    return existing_def


def from_id_or_raise(session: orm.Session, id: IdPostalAddress) -> PostalAddress:
    existing_def = from_id(session, id)
    if existing_def is None:
        raise PostalAddressNotFoundError()

    return existing_def


def create(
    session: orm.Session,
    create: SxPostalAddressCreateRequest,
    *,
    contact: Optional[Contact] = None,
    date_created: Optional[datetime] = None,
    defer_commit: bool = True,
) -> PostalAddress:
    date_created = date_created or create.date_created or datetime.utcnow()

    addr = PostalAddress(
        date_created=date_created,
        source_created=create.source_created,
        line1=create.line1,
        line2=create.line2,
        city=create.city,
        state=create.state,
        zipcode=create.zipcode,
        consent_to_mail=create.consent_to_mail,
        contact=contact,
    )

    session.add(addr)
    session.flush()
    session.refresh(addr)

    if not defer_commit:
        session.commit()

    return addr


def bulk_create(
    session: orm.Session,
    postal_addresses: List[SxPostalAddressCreateRequest],
    *,
    contact: Optional[Contact] = None,
    date_created: Optional[datetime] = None,
) -> List[PostalAddress]:
    existing_postal_addresses: List[PostalAddress] = (
        contact.mailing_addresses if contact is not None else []
    )
    created: List[PostalAddress] = []
    for addr in postal_addresses:
        subsumed = [
            is_subsumable(base=existing, candidate=addr)
            for existing in existing_postal_addresses
        ]
        if not any(subsumed):
            result = create(session, addr, contact=contact, date_created=date_created)
            existing_postal_addresses.append(result)
            created.append(result)
    return created


def attach_to_contact(
    session: orm.Session,
    address: PostalAddress,
    *,
    contact: Contact,
    defer_commit: bool = True,
) -> PostalAddress:
    """Attach a postal address to the provided Contact"""
    existing_addresses = set(a.full_address for a in contact.mailing_addresses)
    if address.full_address in existing_addresses:
        raise ContactMailingAddressExistsError(
            association_object="postal_address", target_object="contact"
        )

    address.contact = contact
    session.add(address)
    session.flush()
    session.refresh(address)

    if not defer_commit:
        session.commit()

    return address


def attach_to_contact_from_id(
    session: orm.Session,
    postal_address: PostalAddress,
    *,
    contact_id: IdContact,
    defer_commit: bool = True,
) -> PostalAddress:
    from membership.services import contact_service

    contact = contact_service.from_id_or_raise(session, contact_id)
    return attach_to_contact(
        session, postal_address, contact=contact, defer_commit=defer_commit
    )


def list_postal_addresses(
    session: orm.Session,
    *,
    limit: Optional[int] = DEFAULT_PAGE_LIMIT,
    starting_after: Optional[str] = None,
    ending_before: Optional[str] = None,
    show_archived: Optional[bool] = False,
) -> PaginatedList[PostalAddress]:
    limit = limit if limit is not None else DEFAULT_PAGE_LIMIT
    return list_pagable_elements(
        session,
        PostalAddress,
        from_id_fn=from_id,
        where=[
            PostalAddress.date_archived == None if not show_archived else None,
        ],
        secondary_sort_columns=[
            cast(Column, PostalAddress._raw_id),
        ],
        limit=limit,
        starting_after=starting_after,
        ending_before=ending_before,
    )


def list_mailing_addresses_for_contact(
    session: orm.Session,
    *,
    contact_id: str,
    limit: Optional[int] = DEFAULT_PAGE_LIMIT,
    starting_after: Optional[str] = None,
    ending_before: Optional[str] = None,
    show_archived: Optional[bool] = False,
) -> PaginatedList[PostalAddress]:
    limit = limit if limit is not None else DEFAULT_PAGE_LIMIT
    return list_pagable_elements(
        session,
        PostalAddress,
        from_id_fn=from_id,
        where=[
            PostalAddress.contact_id == Contact.parse_id(contact_id),
            PostalAddress.date_archived == None if not show_archived else None,
        ],
        secondary_sort_columns=[
            cast(Column, PostalAddress._raw_id),
        ],
        limit=limit,
        starting_after=starting_after,
        ending_before=ending_before,
    )


def update(
    session: orm.Session,
    to_update: PostalAddress,
    *,
    update_request: SxPostalAddressUpdateRequest,
    defer_commit: bool = True,
) -> PostalAddress:
    if update_request.consent_to_mail is not None:
        to_update.consent_to_mail = update_request.consent_to_mail

    session.add(to_update)
    session.flush()
    session.refresh(to_update)

    if not defer_commit:
        session.commit()

    return to_update


def update_from_id(
    session: orm.Session,
    id: IdPostalAddress,
    *,
    contact_id: IdContact = None,
    update_request: SxPostalAddressUpdateRequest,
    defer_commit: bool = True,
) -> PostalAddress:
    to_update = from_id_or_raise(session, id)
    if contact_id is not None and to_update.contact.id != contact_id:
        raise PostalAddressNotFoundError()

    return update(
        session, to_update, update_request=update_request, defer_commit=defer_commit
    )


def archive(
    session: orm.Session,
    to_archive: PostalAddress,
    *,
    reason_archived: Optional[str] = None,
    archived: bool,
    defer_commit: bool = True,
) -> PostalAddress:
    if archived:
        to_archive.date_archived = datetime.utcnow()
        to_archive.reason_archived = reason_archived
    else:
        to_archive.date_archived = None
        to_archive.reason_archived = None

    session.add(to_archive)
    session.flush()
    session.refresh(to_archive)

    if not defer_commit:
        session.commit()

    return to_archive


def archive_from_id(
    session: orm.Session,
    id: IdPostalAddress,
    *,
    reason_archived: Optional[str] = None,
    contact_id: IdContact = None,
    archived: bool,
    defer_commit: bool = True,
) -> PostalAddress:
    to_archive = from_id_or_raise(session, id)
    if contact_id is not None and to_archive.contact.id != contact_id:
        raise PostalAddressNotFoundError(
        )

    return archive(
        session,
        to_archive,
        reason_archived=reason_archived,
        archived=archived,
        defer_commit=defer_commit,
    )


def mark_confirmed(
    session: orm.Session,
    to_confirm: PostalAddress,
    *,
    confirmed: bool,
    defer_commit: bool = True,
) -> PostalAddress:
    if confirmed:
        to_confirm.date_confirmed = datetime.utcnow()
    else:
        to_confirm.date_confirmed = None

    session.add(to_confirm)
    session.flush()
    session.refresh(to_confirm)

    if not defer_commit:
        session.commit()

    return to_confirm


def mark_confirmed_from_id(
    session: orm.Session,
    id: IdPostalAddress,
    *,
    contact_id: IdContact = None,
    confirmed: bool,
    defer_commit: bool = True,
) -> PostalAddress:
    to_confirm = from_id_or_raise(session, id)
    if contact_id is not None and to_confirm.contact.id != contact_id:
        raise PostalAddressNotFoundError(
        )

    return mark_confirmed(
        session, to_confirm, confirmed=confirmed, defer_commit=defer_commit
    )


def from_postal_code(session: orm.Session, postal_code: str) -> List[PostalAddress]:
    return (
        session.execute(
            select(PostalAddress).where(PostalAddress.postal_code == postal_code)
        )
        .scalars()
        .all()
    )


def is_subsumable(
    *, base: PostalAddress, candidate: SxPostalAddressCreateRequest
) -> bool:
    """Compares a candidate create request with an existing PostalAddress model
    to determine whether the candidate can be described by the base address."""

    # since it's possible for a zipcode to encompass multiple cities,
    # and it's possible to describe one address with multiple cities
    # we'll compare postcodes
    if candidate.zipcode != base.zipcode:
        return False

    if candidate.line1 is not None and candidate.line1 != base.line1:
        return False

    if (
        candidate.line1 is not None
        and candidate.line2 is not None
        and candidate.line2 != base.line2
    ):
        return False

    return True
