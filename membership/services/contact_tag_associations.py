from datetime import datetime
from typing import Optional, Tuple

from sqlalchemy import orm, select
from sqlalchemy.sql.expression import and_

from membership.database.models import Contact, ContactTagAssociation, Tag
from membership.schemas.rest.contacts import IdContact
from membership.schemas.rest.tags import IdTag
from membership.services import tag_service
from membership.services.tag_service import TagAssociationExistsError, TagNotFoundError


def attach_to_contact(
    session: orm.Session,
    *,
    contact: Contact,
    tag: Tag,
    date_created: Optional[datetime] = None,
    defer_commit: bool = True,
) -> Tuple[ContactTagAssociation, Contact]:
    """Associate this Tag with a Contact (i.e. tag the contact)"""

    date_created = date_created or datetime.utcnow()

    existing_tags = set(t.name for t in contact.tags)
    if tag.name in existing_tags:
        raise TagAssociationExistsError(target_object="contact")

    new_assoc = ContactTagAssociation(
        contact=contact, tag=tag, date_created=date_created
    )
    session.add(new_assoc)
    session.flush()
    session.refresh(contact)

    if not defer_commit:
        session.commit()

    return new_assoc, contact


def attach_to_contact_from_id(
    session: orm.Session,
    *,
    tag_id: IdTag,
    contact_id: IdContact,
    date_created: Optional[datetime] = None,
    defer_commit: bool = True,
) -> Tuple[ContactTagAssociation, Contact]:
    # resolve circular import
    from membership.services import contact_service

    tag = tag_service.from_id_or_raise(session, tag_id)
    contact = contact_service.from_id_or_raise(session, contact_id)
    return attach_to_contact(
        session,
        contact=contact,
        tag=tag,
        date_created=date_created,
        defer_commit=defer_commit,
    )


def detach_contact(
    session: orm.Session,
    *,
    contact: Contact,
    tag: Tag,
    defer_commit: bool = True,
) -> Tuple[ContactTagAssociation, Contact]:
    """Remove a Tag from a Contact"""

    existing_tags = set(t.name for t in contact.tags)
    if tag.name not in existing_tags:
        raise TagNotFoundError()

    to_delete = (
        session.execute(
            select(ContactTagAssociation).where(
                and_(
                    ContactTagAssociation.contact_id == contact._raw_id,
                    ContactTagAssociation.tag_id == tag._raw_id,
                )
            )
        )
        .scalars()
        .one()
    )

    session.delete(to_delete)
    session.flush()
    session.refresh(contact)

    if not defer_commit:
        session.commit()

    return to_delete, contact


def detach_contact_from_id(
    session: orm.Session,
    *,
    tag_id: IdTag,
    contact_id: IdContact,
    defer_commit: bool = True,
) -> Tuple[ContactTagAssociation, Contact]:
    # resolve circular import
    from membership.services import contact_service

    tag = tag_service.from_id_or_raise(session, tag_id)
    contact = contact_service.from_id_or_raise(session, contact_id)
    return detach_contact(
        session,
        contact=contact,
        tag=tag,
        defer_commit=defer_commit,
    )
