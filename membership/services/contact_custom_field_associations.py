from datetime import datetime
from typing import Any, List, Optional, Set

import dateutil.parser
import pydantic
from sqlalchemy import orm, select
from sqlalchemy.sql.expression import and_

from membership.database.models import (
    Contact,
    ContactCustomFieldAssociation,
    CustomFieldDefinition,
    CustomFieldType,
)
from membership.errors.exceptions import CreateAlreadyExistsError, ResourceMissingError
from membership.schemas.rest.contacts import IdContact
from membership.schemas.rest.custom_field_options import (
    SxCustomFieldChoiceOptions,
    SxCustomFieldRangeOptions,
)
from membership.schemas.rest.custom_fields import IdCustomField
from membership.schemas.rest.pydantic import ValidateUrl


class CustomFieldAssociationNotFoundError(ResourceMissingError):
    def __init__(self):
        super().__init__(object="custom_field_association")


class InvalidCustomFieldValueError(ValueError):
    def __init__(
        self, *, cf: CustomFieldDefinition, message: Optional[str] = None, value: Any
    ):
        message = message or f"Value must be {cf.field_type.name}"
        super().__init__(message)
        self.name = cf.name
        self.field_type = cf.field_type.name
        self.message = message
        self.value = value


class CustomFieldAssociationExistsError(CreateAlreadyExistsError):
    pass


def from_ids(
    session: orm.Session, *, custom_field_id: IdCustomField, contact_id: IdContact
) -> Optional[ContactCustomFieldAssociation]:
    raw_cf_id = CustomFieldDefinition.parse_id(custom_field_id)
    raw_contact_id = Contact.parse_id(contact_id)
    existing_def = (
        session.execute(
            select(ContactCustomFieldAssociation).where(
                and_(
                    ContactCustomFieldAssociation.contact_id == raw_contact_id,
                    ContactCustomFieldAssociation.custom_field_id == raw_cf_id,
                )
            )
        )
        .scalars()
        .one_or_none()
    )
    return existing_def


def from_ids_or_raise(
    session: orm.Session, *, custom_field_id: IdCustomField, contact_id: IdContact
) -> ContactCustomFieldAssociation:
    existing_def = from_ids(
        session, custom_field_id=custom_field_id, contact_id=contact_id
    )
    if existing_def is None:
        raise CustomFieldAssociationNotFoundError(
        )
    return existing_def


def list_for_contact(
    session: orm.Session,
    *,
    contact_id: IdContact,
) -> List[ContactCustomFieldAssociation]:
    return (
        session.execute(
            select(ContactCustomFieldAssociation).where(
                ContactCustomFieldAssociation.contact_id == Contact.parse_id(contact_id)
            )
        )
        .scalars()
        .all()
    )


def infer_type_from_value(value: Any) -> CustomFieldType:
    if isinstance(value, bool):
        return CustomFieldType.boolean
    elif isinstance(value, dict):
        if value.get("type") == "choice":
            return CustomFieldType.choice
        elif value.get("type") == "datetime":
            return CustomFieldType.date
        elif value.get("type") == "range":
            return CustomFieldType.range
        elif value.get("type") == "textarea":
            return CustomFieldType.textarea
        elif value.get("type") == "url":
            return CustomFieldType.url
        else:
            return CustomFieldType.object
    elif isinstance(value, int):
        return CustomFieldType.number
    elif isinstance(value, str):
        return CustomFieldType.text
    elif isinstance(value, list):
        return CustomFieldType.array
    else:
        raise ValueError("couldn't infer type from value")


def _validate_value_for_type(*, custom_field: CustomFieldDefinition, value: Any) -> Any:
    type = custom_field.field_type
    options = custom_field.field_options
    if type == CustomFieldType.choice:
        options = SxCustomFieldChoiceOptions.validate(options)
        valid_choices = set(choice.value for choice in options.choices)
        if value not in valid_choices:
            raise InvalidCustomFieldValueError(
                value=value,
                cf=custom_field,
                message=f"must choose from {list(valid_choices)}",
            )

        return value
    elif type == CustomFieldType.range:
        options = SxCustomFieldRangeOptions.validate(options)
        parsed_value = None
        error = InvalidCustomFieldValueError(
            value=value,
            cf=custom_field,
            message=f"must be integer between {options.start} and {options.end}",
        )
        if isinstance(value, str):
            parsed_value = int(value)
        elif not isinstance(value, int):
            raise error

        if (
            parsed_value is None
            or parsed_value > options.end
            or parsed_value < options.start
        ):
            raise error

        return parsed_value
    elif type == CustomFieldType.boolean:
        try:
            return bool(value)
        except Exception:
            raise InvalidCustomFieldValueError(value=value, cf=custom_field)
    elif type == CustomFieldType.date:
        try:
            return dateutil.parser.parse(value)
        except Exception:
            raise InvalidCustomFieldValueError(value=value, cf=custom_field)
    elif type == CustomFieldType.number:
        try:
            return float(value)
        except Exception:
            raise InvalidCustomFieldValueError(value=value, cf=custom_field)
    elif type == CustomFieldType.url:
        try:
            return ValidateUrl(to_validate=value).to_validate
        except pydantic.ValidationError:
            raise InvalidCustomFieldValueError(value=value, cf=custom_field)
    elif type == CustomFieldType.array:
        try:
            return list(value)
        except Exception:
            raise InvalidCustomFieldValueError(value=value, cf=custom_field)
    elif type == CustomFieldType.object:
        try:
            return dict(value)
        except Exception:
            raise InvalidCustomFieldValueError(value=value, cf=custom_field)
    elif type == CustomFieldType.text or type == CustomFieldType.textarea:
        return value
    else:
        raise InvalidCustomFieldValueError(
            value=value, cf=custom_field, message="Unknown field type"
        )


def attach_to_contact(
    session: orm.Session,
    *,
    custom_field: CustomFieldDefinition,
    contact: Contact,
    value: Any,
    date_created: datetime = None,
    defer_commit: bool = True,
) -> ContactCustomFieldAssociation:
    date_created = date_created or datetime.utcnow()

    validated_value = _validate_value_for_type(custom_field=custom_field, value=value)
    existing_custom_field_ids: Set[str] = set(cf.id for cf in contact.custom_fields)
    if custom_field.id in existing_custom_field_ids:
        raise CustomFieldAssociationExistsError()

    new_assoc = ContactCustomFieldAssociation(
        custom_field=custom_field,
        contact=contact,
        date_created=date_created,
        value=validated_value,
    )
    session.add(new_assoc)
    session.flush()
    session.refresh(new_assoc)

    if not defer_commit:
        session.commit()

    return new_assoc


def attach_to_contact_by_id(
    session: orm.Session,
    *,
    custom_field_id: IdCustomField,
    contact_id: IdContact,
    value: Any,
    date_created: datetime = None,
    defer_commit: bool = True,
) -> ContactCustomFieldAssociation:
    # break circular reference
    from membership.services import contact_service, custom_field_service

    date_created = date_created or datetime.utcnow()

    contact = contact_service.from_id_or_raise(session, contact_id)
    custom_field = custom_field_service.from_id_or_raise(session, custom_field_id)
    return attach_to_contact(
        session,
        custom_field=custom_field,
        contact=contact,
        value=value,
        date_created=date_created,
        defer_commit=defer_commit,
    )


def update(
    session: orm.Session,
    to_modify: ContactCustomFieldAssociation,
    *,
    value: Any,
    defer_commit: bool = True,
) -> ContactCustomFieldAssociation:
    validated_value = _validate_value_for_type(
        custom_field=to_modify.custom_field, value=value
    )
    to_modify.value = validated_value
    session.add(to_modify)
    session.flush()
    session.refresh(to_modify)

    if not defer_commit:
        session.commit()

    return to_modify


def update_by_id(
    session: orm.Session,
    *,
    custom_field_id: IdCustomField,
    contact_id: IdContact,
    value: Any,
    defer_commit: bool = True,
) -> ContactCustomFieldAssociation:
    to_modify = from_ids_or_raise(
        session, custom_field_id=custom_field_id, contact_id=contact_id
    )
    return update(session, to_modify, value=value, defer_commit=defer_commit)


def delete(
    session: orm.Session,
    to_delete: ContactCustomFieldAssociation,
    *,
    defer_commit: bool = True,
) -> ContactCustomFieldAssociation:
    session.delete(to_delete)
    session.flush()

    if not defer_commit:
        session.commit()

    return to_delete


def delete_by_id(
    session: orm.Session,
    *,
    custom_field_id: IdCustomField,
    contact_id: IdContact,
    defer_commit: bool = True,
) -> ContactCustomFieldAssociation:
    to_delete = from_ids_or_raise(
        session, custom_field_id=custom_field_id, contact_id=contact_id
    )
    return delete(session, to_delete, defer_commit=defer_commit)
