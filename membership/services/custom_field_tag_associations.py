from datetime import datetime
from typing import Optional, Set, Tuple

from sqlalchemy import and_, orm, select

from membership.database.models import CustomFieldDefinition, Tag, TagFieldAssociation
from membership.schemas.rest.custom_fields import IdCustomField
from membership.schemas.rest.tags import IdTag
from membership.services import tag_service
from membership.services.tag_service import TagAssociationExistsError, TagNotFoundError


class CustomFieldTagAssociationService:
    @staticmethod
    def attach_to_custom_field(
        session: orm.Session,
        *,
        custom_field: CustomFieldDefinition,
        tag: Tag,
        date_created: Optional[datetime] = None,
        defer_commit: bool = True,
    ) -> Tuple[TagFieldAssociation, CustomFieldDefinition]:
        """Associate this Tag with a Custom Field"""

        date_created = date_created or datetime.utcnow()

        custom_field_tags = custom_field.tags
        if custom_field_tags is not None:
            existing_tag_names: Set[str] = set(t.name for t in custom_field_tags)
            if tag.name in existing_tag_names:
                raise TagAssociationExistsError(target_object="custom_field")

        new_assoc = TagFieldAssociation(
            custom_field=custom_field, tag=tag, date_created=date_created
        )
        session.add(new_assoc)
        session.flush()
        session.refresh(new_assoc)

        if not defer_commit:
            session.commit()

        return new_assoc, custom_field

    @staticmethod
    def attach_to_custom_field_from_id(
        session: orm.Session,
        *,
        tag_id: IdTag,
        custom_field_id: IdCustomField,
        date_created: Optional[datetime] = None,
        defer_commit: bool = True,
    ) -> Tuple[TagFieldAssociation, CustomFieldDefinition]:
        from membership.services import custom_field_service

        tag = tag_service.from_id_or_raise(session, tag_id)
        custom_field = custom_field_service.from_id_or_raise(session, custom_field_id)
        return CustomFieldTagAssociationService.attach_to_custom_field(
            session,
            custom_field=custom_field,
            tag=tag,
            date_created=date_created,
            defer_commit=defer_commit,
        )

    @staticmethod
    def detach_custom_field(
        session: orm.Session,
        *,
        custom_field: CustomFieldDefinition,
        tag: Tag,
        defer_commit: bool = True,
    ) -> Tuple[TagFieldAssociation, CustomFieldDefinition]:
        """Remove a Tag from a Contact"""

        tag_assocs = custom_field.tags
        if tag_assocs is None:
            raise TagNotFoundError()

        existing_tags = set(t.name for t in tag_assocs)
        if tag.name not in existing_tags:
            raise TagNotFoundError()

        to_delete = (
            session.execute(
                select(TagFieldAssociation).where(
                    and_(
                        TagFieldAssociation.custom_field_id == custom_field._raw_id,
                        TagFieldAssociation.tag_id == tag._raw_id,
                    )
                )
            )
            .scalars()
            .one()
        )

        session.delete(to_delete)
        session.flush()
        session.refresh(custom_field)

        if not defer_commit:
            session.commit()

        return to_delete, custom_field

    @staticmethod
    def detach_custom_field_from_id(
        session: orm.Session,
        *,
        tag_id: IdTag,
        custom_field_id: IdCustomField,
        defer_commit: bool = True,
    ) -> Tuple[TagFieldAssociation, CustomFieldDefinition]:
        # resolve circular import
        from membership.services import custom_field_service

        tag = tag_service.from_id_or_raise(session, tag_id)
        custom_field = custom_field_service.from_id_or_raise(session, custom_field_id)
        return CustomFieldTagAssociationService.detach_custom_field(
            session,
            custom_field=custom_field,
            tag=tag,
            defer_commit=defer_commit,
        )
