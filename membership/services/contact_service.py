from datetime import datetime
from enum import Enum
from typing import Any, Dict, List, Optional, Set, Tuple, cast

from pydantic.networks import EmailStr
from sqlalchemy import orm, select
from sqlalchemy.sql.elements import ClauseElement
from sqlalchemy.sql.expression import or_
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.schema import Column

import membership.schemas.formatters.email_addresses as email_address_fmt
import membership.schemas.formatters.phone_numbers as phone_number_fmt
from membership.database.models import (
    Contact,
    ContactCustomFieldAssociation,
    ContactTagAssociation,
    CustomFieldDefinition,
    EmailAddress,
    PhoneNumber,
    PostalAddress,
    Tag,
)
from membership.errors.exceptions import BulkResourceMissingError, ResourceMissingError
from membership.schemas.rest.contacts import (
    IdContact,
    SxContactCreateRequest,
    SxContactUpdateRequest,
    SxSearchContactsQuery,
)
from membership.schemas.rest.custom_fields import IdCustomField
from membership.schemas.rest.email_addresses import SxEmailAddressCreateRequest
from membership.schemas.rest.phone_numbers import PhoneStr, SxPhoneNumberCreateRequest
from membership.schemas.rest.postal_addresses import SxPostalAddressCreateRequest
from membership.schemas.rest.tags import IdTag
from membership.services import (
    contact_custom_field_associations,
    contact_tag_associations,
    custom_field_service,
    email_address_service,
    phone_number_service,
    postal_address_service,
    search_service,
    tag_service,
)
from membership.services.tag_service import TagNotFoundError
from membership.util.arrays import compact
from membership.util.pagination import (
    DEFAULT_PAGE_LIMIT,
    PaginatedList,
    list_pagable_elements,
)


class ContactNotFoundError(ResourceMissingError):
    def __init__(self):
        super().__init__(object="contact")


class BulkContactNotFoundError(BulkResourceMissingError):
    def __init__(
        self,
        *,
        message: Optional[str] = None,
        items: Optional[List[Any]] = None,
    ):
        super(BulkContactNotFoundError, self).__init__(object="contact", items=items)


def from_id(session: orm.Session, id: IdContact) -> Optional[Contact]:
    raw_id = Contact.parse_id(id)
    return session.query(Contact).get(raw_id)


def from_id_or_raise(session: orm.Session, id: IdContact) -> Contact:
    existing_def = from_id(session, id)
    if existing_def is None:
        raise ContactNotFoundError()

    return existing_def


def bulk_from_id(session: orm.Session, ids: List[IdContact]) -> List[Optional[Contact]]:
    parsed_ids = [Contact.parse_id(id) for id in ids]
    results: List[Contact] = (
        session.execute(
            select(Contact).where(cast(Column, Contact._raw_id).in_(parsed_ids))
        )
        .scalars()
        .all()
    )
    keyed_by_id = {row.id: row for row in results}
    return [keyed_by_id.get(contact_id) for contact_id in ids]


def bulk_from_id_or_raise(session: orm.Session, ids: List[IdContact]) -> List[Contact]:
    results = bulk_from_id(session, ids)

    if not all(v is not None for v in results):
        missing = [ids[i] for i in range(len(results)) if results[i] is None]
        raise BulkContactNotFoundError(items=missing)

    return cast(List[Contact], results)


def create(
    session: orm.Session,
    create_request: SxContactCreateRequest,
    *,
    date_created: Optional[datetime] = None,
    defer_commit: bool = True,
) -> Contact:
    date_created = date_created or create_request.date_created or datetime.utcnow()

    contact = Contact(
        full_name=create_request.full_name,
        display_name=create_request.display_name,
        pronouns=create_request.pronouns,
        biography=create_request.biography,
        profile_pic=create_request.profile_pic,
        admin_notes=create_request.admin_notes,
        date_created=date_created,
        source_created=create_request.source_created,
    )

    if create_request.email_addresses is not None:
        email_addresses = email_address_fmt.normalize_email_addresses(
            create_request.email_addresses
        )
        email_address_service.bulk_create(
            session,
            email_addresses,
            contact=contact,
            date_created=date_created,
        )

    if create_request.phone_numbers is not None:
        phone_numbers = phone_number_fmt.normalize_phone_numbers(
            create_request.phone_numbers
        )
        phone_number_service.bulk_create(
            session, phone_numbers, contact=contact, date_created=date_created
        )

    if create_request.mailing_addresses is not None:
        postal_address_service.bulk_create(
            session,
            create_request.mailing_addresses,
            contact=contact,
            date_created=date_created,
        )

    if create_request.tags is not None:
        for tag_id in sorted(set(create_request.tags)):
            created_tag = tag_service.from_id_or_raise(session, id=tag_id)
            add_tag(session, contact, created_tag, date_created=date_created)

    add_custom_field_mapping(
        session, contact, create_request.custom_fields, date_created=date_created
    )

    session.add(contact)
    session.flush()
    session.refresh(contact)

    if not defer_commit:
        session.commit()

    return contact


def add_custom_field_mapping(
    session: orm.Session,
    contact: Contact,
    custom_fields: Optional[Dict[IdCustomField, Any]],
    *,
    update_existing: bool = False,
    date_created: Optional[datetime] = None,
    defer_commit: bool = True,
) -> Optional[Dict[IdCustomField, Optional[ContactCustomFieldAssociation]]]:
    date_created = date_created or datetime.utcnow()
    field_entries = custom_fields.items() if custom_fields is not None else None
    parsed_field_definitions = (
        {
            key: custom_field_service.from_id_or_raise(session, key)
            for key, value in field_entries
        }
        if field_entries is not None
        else None
    )
    parsed_field_tuples = (
        [
            (definition, custom_fields[key])
            for key, definition in parsed_field_definitions.items()
        ]
        if parsed_field_definitions is not None and custom_fields is not None
        else None
    )

    if parsed_field_tuples is None:
        return None

    added_fields = {}
    for definition, custom_field_value in parsed_field_tuples:
        try:
            added_fields[definition.id] = add_custom_field(
                session,
                contact,
                definition=definition,
                value=custom_field_value,
                date_created=date_created,
                defer_commit=defer_commit,
            )
        except contact_custom_field_associations.CustomFieldAssociationExistsError:
            if update_existing:
                assoc = contact_custom_field_associations.from_ids_or_raise(
                    session, custom_field_id=definition.id, contact_id=contact.id
                )
                added_fields[definition.id] = contact_custom_field_associations.update(
                    session, assoc, value=custom_field_value, defer_commit=defer_commit
                )
            else:
                added_fields[definition.id] = None

    return added_fields


def add_custom_field_mapping_or_raise(
    session: orm.Session,
    contact: Contact,
    custom_fields: Optional[Dict[IdCustomField, Any]],
    *,
    update_existing: bool = False,
    date_created: Optional[datetime] = None,
    defer_commit: bool = True,
) -> Dict[IdCustomField, ContactCustomFieldAssociation]:
    result = add_custom_field_mapping(
        session,
        contact,
        custom_fields,
        update_existing=update_existing,
        date_created=date_created,
        defer_commit=defer_commit,
    )

    if result is None:
        raise custom_field_service.CustomFieldNotFoundError(
        )

    for key, assoc in result.items():
        if assoc is None:
            raise contact_custom_field_associations.CustomFieldAssociationExistsError()

    return cast(Dict[IdCustomField, ContactCustomFieldAssociation], result)


def list_(
    session: orm.Session,
    *,
    limit: Optional[int] = DEFAULT_PAGE_LIMIT,
    starting_after: Optional[str] = None,
    ending_before: Optional[str] = None,
    show_archived: Optional[bool] = False,
) -> PaginatedList[Contact]:
    limit = limit if limit is not None else DEFAULT_PAGE_LIMIT
    return list_pagable_elements(
        session,
        Contact,
        from_id_fn=from_id,
        limit=limit,
        where=[
            Contact.date_archived == None if not show_archived else None,
        ],
        starting_after=starting_after,
        ending_before=ending_before,
    )


def update_from_id(
    session: orm.Session,
    id: IdContact,
    *,
    update_request: SxContactUpdateRequest,
    defer_commit: bool = True,
) -> Contact:
    to_modify = from_id_or_raise(session, id)
    return update(
        session, to_modify, update_request=update_request, defer_commit=defer_commit
    )


def update(
    session: orm.Session,
    to_modify: Contact,
    *,
    update_request: SxContactUpdateRequest,
    defer_commit: bool = True,
) -> Contact:
    for key, value in update_request.dict(
        exclude=set(["archived", "reason_archived"]),
        exclude_unset=True,
    ).items():
        setattr(to_modify, key, value)

    if update_request.archived is not None:
        to_modify = archive(
            session,
            to_modify,
            archived=update_request.archived,
            reason_archived=update_request.reason_archived,
        )

    session.add(to_modify)
    session.flush()
    session.refresh(to_modify)

    if not defer_commit:
        session.commit()

    return to_modify


def archive_from_id(
    session: orm.Session,
    id: IdContact,
    *,
    archived: bool,
    reason_archived: Optional[str] = None,
    defer_commit: bool = True,
) -> Contact:
    to_archive = from_id_or_raise(session, id)
    return archive(
        session,
        to_archive,
        archived=archived,
        reason_archived=reason_archived,
        defer_commit=defer_commit,
    )


def archive(
    session: orm.Session,
    to_archive: Contact,
    *,
    archived: bool,
    reason_archived: Optional[str] = None,
    defer_commit: bool = True,
) -> Contact:
    if archived:
        to_archive.date_archived = datetime.utcnow()
        to_archive.reason_archived = reason_archived
    else:
        to_archive.date_archived = None
        to_archive.reason_archived = None

    session.add(to_archive)
    session.flush()
    session.refresh(to_archive)

    if not defer_commit:
        session.commit()

    return to_archive


class ContactTryAddOutcome(str, Enum):
    """Successfully added to contact"""

    added = "added"

    """Subsumed by existing record on contact"""
    subsumed_by_existing = "subsumed_by_existing"

    """Conflict with record on another contact"""
    conflict_with_other = "conflict_with_other"


def try_add_mailing_address(
    session: orm.Session,
    contact: Contact,
    mailing_address: SxPostalAddressCreateRequest,
    *,
    defer_commit: bool = True,
) -> Tuple[Optional[PostalAddress], ContactTryAddOutcome]:
    existing_mailing_addresses = contact.mailing_addresses
    for addr in existing_mailing_addresses:
        if postal_address_service.is_subsumable(base=addr, candidate=mailing_address):
            return None, ContactTryAddOutcome.subsumed_by_existing

    return (
        postal_address_service.create(
            session, mailing_address, contact=contact, defer_commit=defer_commit
        ),
        ContactTryAddOutcome.added,
    )


def try_add_email_address(
    session: orm.Session,
    contact: Contact,
    email_address: SxEmailAddressCreateRequest,
    *,
    defer_commit: bool = True,
) -> Tuple[Optional[EmailAddress], ContactTryAddOutcome]:
    if has_email_address(contact, email_address.value):
        return None, ContactTryAddOutcome.subsumed_by_existing

    try:
        return (
            email_address_service.create(
                session, email_address, contact=contact, defer_commit=defer_commit
            ),
            ContactTryAddOutcome.added,
        )
    except email_address_service.EmailAddressExistsError:
        return None, ContactTryAddOutcome.conflict_with_other


def try_add_phone_number(
    session: orm.Session,
    contact: Contact,
    phone_number: SxPhoneNumberCreateRequest,
    *,
    defer_commit: bool = True,
) -> Tuple[Optional[PhoneNumber], ContactTryAddOutcome]:
    if has_phone_number(contact, phone_number.value):
        return None, ContactTryAddOutcome.subsumed_by_existing

    return (
        phone_number_service.create(
            session, phone_number, contact=contact, defer_commit=defer_commit
        ),
        ContactTryAddOutcome.added,
    )


def add_tag(
    session: orm.Session,
    contact: Contact,
    tag: Tag,
    *,
    date_created: Optional[datetime] = None,
    defer_commit: bool = True,
) -> ContactTagAssociation:
    date_created = date_created or datetime.utcnow()
    tag_assoc, contact = contact_tag_associations.attach_to_contact(
        session,
        contact=contact,
        tag=tag,
        date_created=date_created,
        defer_commit=defer_commit,
    )
    return tag_assoc


def add_custom_field(
    session: orm.Session,
    contact: Contact,
    *,
    definition: CustomFieldDefinition,
    value: Any,
    date_created: Optional[datetime] = None,
    defer_commit: bool = True,
) -> ContactCustomFieldAssociation:
    date_created = date_created or datetime.utcnow()
    return contact_custom_field_associations.attach_to_contact(
        session,
        custom_field=definition,
        contact=contact,
        value=value,
        date_created=date_created,
        defer_commit=defer_commit,
    )


def get_tag_from_id(
    session: orm.Session, *, contact_id: IdContact, tag_id: IdTag
) -> Optional[Tag]:
    contact = from_id_or_raise(session, contact_id)
    contact_tag_ids = set(
        Tag._as_id(assoc.tag_id) for assoc in contact.tag_associations
    )
    if tag_id in contact_tag_ids:
        return tag_service.from_id(session, tag_id)
    else:
        return None


def get_tag_from_id_or_raise(
    session: orm.Session, *, contact_id: IdContact, tag_id: IdTag
) -> Tag:
    tag = get_tag_from_id(session, contact_id=contact_id, tag_id=tag_id)
    if tag is None:
        raise TagNotFoundError(
        )
    return tag


def get_best_email_address(contact: Contact) -> Optional[EmailAddress]:
    emails = contact.email_addresses
    primary_emails = [e for e in emails if e.primary]
    return primary_emails[0] if len(primary_emails) > 0 else emails[0]


def get_best_email_address_or_raise(contact: Contact) -> Optional[EmailAddress]:
    email_address = get_best_email_address(contact)
    if email_address is None:
        raise email_address_service.EmailAddressNotFoundError(
        )
    return email_address


def find_by_email_address(
    session: orm.Session, email_address: EmailStr
) -> Optional[Contact]:
    addr = email_address_service.from_email_address(session, email_address)
    if addr is None:
        return None

    return addr.contact


def find_by_phone_number(session: orm.Session, phone_number: PhoneStr) -> List[Contact]:
    pns = phone_number_service.from_phone_number(session, phone_number)
    return compact([pn.contact for pn in pns])


def find_by_name(session: orm.Session, name: str) -> List[Contact]:
    return (
        session.execute(
            select(Contact).where(func.lower(Contact.full_name) == name.lower())
        )
        .scalars()
        .all()
    )


def find_by_postal_code(session: orm.Session, *, postal_code: str) -> List[Contact]:
    postal_addresses = postal_address_service.from_postal_code(
        session, postal_code=postal_code
    )
    return [addr.contact for addr in postal_addresses]


def has_phone_number(contact: Contact, number: PhoneStr) -> bool:
    self_pns = set(pn.value for pn in contact.phone_numbers)
    return len(self_pns) > 0 and number in self_pns


def has_any_phone_number(contact: Contact, numbers: Set[PhoneStr]) -> bool:
    self_pns = set(pn.value for pn in contact.phone_numbers)
    return len(self_pns) > 0 and len(numbers.intersection(self_pns)) > 0


def has_any_postal_code(contact: Contact, postal_codes: Set[str]) -> bool:
    self_postal_codes = set(addr.postal_code for addr in contact.mailing_addresses)
    return (
        len(self_postal_codes) > 0
        and len(postal_codes.intersection(self_postal_codes)) > 0
    )


def has_email_address(contact: Contact, email_address: EmailStr) -> bool:
    self_email_addresses = set(email.value for email in contact.email_addresses)
    return len(self_email_addresses) > 0 and email_address in self_email_addresses


def has_any_email_address(contact: Contact, email_addresses: Set[EmailStr]) -> bool:
    self_email_addresses = set(email.value for email in contact.email_addresses)
    return (
        len(self_email_addresses) > 0
        and len(email_addresses.intersection(self_email_addresses)) > 0
    )


def search(
    session: orm.Session,
    query: SxSearchContactsQuery,
    limit: Optional[int] = DEFAULT_PAGE_LIMIT,
    starting_after: Optional[str] = None,
    ending_before: Optional[str] = None,
) -> PaginatedList[Contact]:
    limit = limit if limit is not None else DEFAULT_PAGE_LIMIT

    try:
        email_str = EmailStr.validate(query.q)
        contact = find_by_email_address(session, EmailStr(email_str))
        if contact is not None:
            return PaginatedList.new([contact], has_more=False)
    except ValueError:
        pass

    phone_str = PhoneNumber.try_format_number(query.q)
    if phone_str is not None:
        contacts = find_by_phone_number(session, PhoneStr(phone_str))
        return PaginatedList.new(contacts, has_more=False)

    processed_tokens = search_service.process_query_string(query.q)
    or_clauses: List[ClauseElement] = []
    for token in processed_tokens.bare_tokens:
        or_clauses.append(
            # i = "ignore case" flag
            cast(Column, Contact.full_name).ilike(f"%{token}%")
        )
        or_clauses.append(
            # i = "ignore case" flag
            cast(Column, Contact.display_name).ilike(f"%{token}%")
        )

    return list_pagable_elements(
        session,
        Contact,
        from_id_fn=from_id,
        where=[or_(*or_clauses)],
        limit=limit,
        starting_after=starting_after,
        ending_before=ending_before,
    )
