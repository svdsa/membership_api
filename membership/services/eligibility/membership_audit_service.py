from typing import List

from sqlalchemy import orm
from sqlalchemy.orm import joinedload

from membership.database.models import MembershipStatusAudit, MembershipStatusAuditItemization, \
    Meeting, Member
from membership.models import MemberAsEligibleToVote
from membership.models.membership_status import MembershipStatus
from membership.repos import MeetingRepo
from membership.schemas import Json
from membership.services.eligibility import AttendeeService, SanFranciscoEligibilityService
from membership.services.member_status_util import MemberStatusUtil


class MembershipAuditService:
    membership_audit_query = r"""
        SELECT
            s.id,
            s.email_address,
            s.has_member_role,
            s.meets_attendance_criteria,
            s.is_active_in_committee,
            CASE
                WHEN s.suspended_on IS NOT NULL THEN 'SUSPENDED'
                WHEN s.left_chapter_on IS NOT NULL THEN 'LEFT_CHAPTER'
                WHEN s.dues_paid_until IS NULL AND s.has_member_role THEN 'UNKNOWN'
                WHEN s.dues_paid_until IS NULL THEN 'NON_MEMBER'
                WHEN s.dues_paid_until > :date THEN 'GOOD_STANDING'
                WHEN s.dues_paid_until > DATE_ADD(:date, INTERVAL -2 MONTH) THEN 'LEAVING_STANDING'
                ELSE 'OUT_OF_STANDING'
            END AS membership_status
        FROM (
            SELECT
                m.id,
                m.email_address,
                m.suspended_on,
                m.left_chapter_on,
                IF(chapter_member.member_id IS NULL, 0, 1)             AS has_member_role,
                IF(COALESCE(attended.attendance, 0) >= 2, 1, 0)        AS meets_attendance_criteria,
                IF(COALESCE(committee_roles.committees, 0) >= 1, 1, 0) AS is_active_in_committee,
                mu.dues_paid_until
            FROM members m
            LEFT JOIN memberships_usa mu ON m.id = mu.member_id
            LEFT JOIN (
                SELECT roles.member_id FROM roles WHERE role = 'member' AND committee_id IS NULL
            ) AS chapter_member ON chapter_member.member_id = m.id
            LEFT JOIN (
                SELECT attendees.member_id AS attended_member_id, COUNT(*) AS attendance
                FROM attendees
                INNER JOIN (
                    SELECT id AS meeting_id
                    FROM meetings
                    WHERE id in :meetings
                ) AS past3 ON attendees.meeting_id = past3.meeting_id
                GROUP BY attendees.member_id
            ) AS attended ON m.id = attended_member_id
            LEFT JOIN (
                SELECT member_id, COUNT(*) AS committees
                FROM roles
                JOIN committees c on roles.committee_id = c.id
                WHERE role = 'active' AND c.provisional=0
                GROUP BY member_id
            ) AS committee_roles ON m.id = committee_roles.member_id
        ) as s
        """

    def build_membership_audit(self,
                               session: orm.Session) -> MembershipStatusAudit:
        meeting_repository = MeetingRepo(Meeting)
        attendee_service = AttendeeService()
        eligibility_service = SanFranciscoEligibilityService(meeting_repository, attendee_service)

        members = session.query(Member).options(
            joinedload(Member.memberships_usa),
            joinedload(Member.meetings_attended),
            joinedload(Member.all_roles)
        ).all()

        eligibility_for_members = eligibility_service.members_as_eligible_to_vote(session, members)
        items = [self.map_to_db_model(x) for x in eligibility_for_members]

        audit = MembershipStatusAudit()
        audit.items = items

        return audit

    def map_to_db_model(
        self,
        member_details: MemberAsEligibleToVote
    ) -> MembershipStatusAuditItemization:
        membership = max(
            member_details.memberships_usa, key=lambda m: m.dues_paid_until,
            default=None
        )

        status = MemberStatusUtil.get_member_status(member_details, membership).name

        return MembershipStatusAuditItemization(
            member_id=member_details.id,
            membership_status=status,
            dues_are_paid=member_details.dues_are_paid,
            eligible_to_vote=member_details.is_eligible,
            meets_attendance_criteria=member_details.meets_attendance_criteria,
            is_active_in_committee=member_details.active_in_committee
        )

    @staticmethod
    def status_count(status: str, items: List[MembershipStatusAuditItemization]) -> int:
        return len(list(filter(lambda x: x.membership_status == status, items)))

    @staticmethod
    def inactive(item: MembershipStatusAuditItemization) -> bool:
        return not item.eligible_to_vote and \
               not item.meets_attendance_criteria and \
               not item.is_active_in_committee

    @staticmethod
    def non_standing(item: MembershipStatusAuditItemization) -> bool:
        return not item.eligible_to_vote and \
            item.membership_status != MembershipStatus.GOOD_STANDING.name and \
            item.membership_status != MembershipStatus.LEAVING_STANDING.name

    @staticmethod
    def ineligible_inactive(item: MembershipStatusAuditItemization) -> bool:
        return MembershipAuditService.inactive(item) and \
               not MembershipAuditService.non_standing(item)

    @staticmethod
    def ineligible_standing(item: MembershipStatusAuditItemization) -> bool:
        return not MembershipAuditService.inactive(item) and \
               MembershipAuditService.non_standing(item)

    @staticmethod
    def ineligible_both(item: MembershipStatusAuditItemization) -> bool:
        return MembershipAuditService.inactive(item) and \
               MembershipAuditService.non_standing(item)

    def audit_json(self, audit: MembershipStatusAudit) -> Json:
        total = len(audit.items)

        good_standing = self.status_count(MembershipStatus.GOOD_STANDING.name, audit.items)
        leaving_standing = self.status_count(MembershipStatus.LEAVING_STANDING.name, audit.items)
        out_of_standing = self.status_count(MembershipStatus.OUT_OF_STANDING.name, audit.items)
        unknown = self.status_count(MembershipStatus.UNKNOWN.name, audit.items)
        left = self.status_count(MembershipStatus.LEFT_CHAPTER.name, audit.items)
        suspended = self.status_count(MembershipStatus.SUSPENDED.name, audit.items)
        non_members = self.status_count(MembershipStatus.NOT_A_MEMBER.name, audit.items)

        eligible = len(list(filter(lambda x: x.eligible_to_vote, audit.items)))
        inactive = len(list(filter(self.ineligible_inactive, audit.items)))
        standing = len(list(filter(self.ineligible_standing, audit.items)))
        both = len(list(filter(self.ineligible_both, audit.items)))

        return {
            'total_individuals': total,
            'membership_breakdown': {
                'good_standing': good_standing,
                'leaving_standing': leaving_standing,
                'out_of_standing': out_of_standing,
                'unknown_standing': unknown,
                'left_chapter': left,
                'suspended': suspended,
                'non_members': non_members
            },
            'eligibility_breakdown': {
                'eligible': eligible,
                'ineligible_inactive': inactive,
                'ineligible_standing': standing,
                'ineligible_both': both
            }
        }
