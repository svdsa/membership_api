"""Operations related to committees."""

from typing import List, Optional, Sequence, cast

from sqlalchemy import orm
from sqlalchemy.sql.schema import Column

from config import EMAIL_CONNECTOR
from membership.database.models import Committee, Member, Role
from membership.services.errors import NotFoundError
from membership.util.email import get_email_connector

email_connector = get_email_connector(EMAIL_CONNECTOR)


class CommitteeNotFoundError(NotFoundError):
    pass


class CommitteeService:
    def list(
        self, session: orm.Session, *, show_inactive: bool = False
    ) -> List[Committee]:
        query = session.query(Committee)
        if show_inactive:
            query = query.filter(~Committee.inactive)
        committees = query.all()
        return committees

    def create(
        self,
        session: orm.Session,
        name: str,
        *,
        provisional: bool = False,
        admins: Sequence[str] = [],
        defer_commit: bool = True,
    ) -> Committee:
        committee = Committee(name=name)
        committee.provisional = provisional

        # TODO refactor to take Contacts directly as arguments
        # TODO refactor out to separate function
        members = (
            session.query(Member)
            .filter(cast(Column, Member.email_address).in_(admins))
            .all()
        )
        for member in members:
            role = Role(role='admin', committee=committee, member=member)
            session.add(role)

        session.add(committee)
        session.flush()
        session.refresh(committee)

        if not defer_commit:
            session.commit()

        return committee

    def from_id(self, session: orm.Session, committee_id: int) -> Optional[Committee]:
        committee = session.query(Committee).get(committee_id)
        if committee is None:
            return None
        return committee

    def from_id_or_raise(self, session: orm.Session, committee_id: int) -> Committee:
        committee = self.from_id(session, committee_id)
        if committee is None:
            raise CommitteeNotFoundError(str(committee_id))
        return committee

    def edit(
        self,
        session: orm.Session,
        committee_id: int,
        *,
        inactive: bool,
        defer_commit: bool = True,
    ) -> Committee:
        committee = session.query(Committee).get(committee_id)

        if committee is None:
            raise CommitteeNotFoundError(str(committee_id))

        committee.inactive = inactive

        if not defer_commit:
            session.commit()

        return committee
