from typing import List, Optional, Callable

from slack_sdk.models.blocks import SectionBlock, DividerBlock, ButtonElement, Block

from config.crm_config import CRM_LINK
from config.onboarding_config import GOOGLE_GROUP_WELCOME_LINK, CHAPTER_WELCOME_EMAIL_LINK, \
    MEMBER_GOOGLE_GROUP_LINK
from config.portal_config import PORTAL_URL
from config.slack_config import slack_client, SLACK_ONBOARDING_CHANNEL_ID
from membership.database.models import Member
from membership.services.onboarding.onboarding_results import OnboardingResults, OnboardingResult
from membership.services.onboarding.onboarding_status import OnboardingStatus


class SlackService:

    def notify_of_roster_import(self, new_members: List[Member]) -> None:
        notif_text = "A new roster import has completed!"

        header = SectionBlock(text="A roster import has completed. The following members are new "
                                   "and need attention!")

        d1 = DividerBlock()

        member_list = [
            SectionBlock(
                text=f"*{member.name}* - {member.email_address}",
                accessory=ButtonElement(text="Portal", url=f"{PORTAL_URL}/members/{member.id}")
            )
            for member in new_members
        ]

        d2 = DividerBlock()

        steps = [
            SectionBlock(text="*Next Steps:*"),
            SectionBlock(text="*Invite to Slack* - In sidebar, click DSA SF -> invite... -> "
                              "Members -> 'add many at once'"),
            SectionBlock(text=F"*Add to <{MEMBER_GOOGLE_GROUP_LINK}|Google Group>* - You can use "
                              f"<{GOOGLE_GROUP_WELCOME_LINK}|this email template>."),
            SectionBlock(text="*Send Welcome Email* - You can use "
                              f"<{CHAPTER_WELCOME_EMAIL_LINK}|this email template>."),
            SectionBlock(text="*Assign Reach Out Vols* - "
                              F"<{CRM_LINK}|CRM here>."),
        ]

        emails = [member.email_address for member in new_members]
        emails_csv = ""
        for i in range(0, len(emails), 10):
            emails_csv += ','.join(emails[i:i + 10])
            emails_csv += '\n\n'

        email_block = SectionBlock(text=f"```{emails_csv}```")

        blocks = [header, d1] + member_list + [d2] + steps + [email_block]

        slack_client.chat_postMessage(
            channel=SLACK_ONBOARDING_CHANNEL_ID, text=notif_text, blocks=blocks)

    def notify_of_individual_sign_up(self,
                                     member: Member,
                                     onboarding_source: str,
                                     onboarding_results: OnboardingResults) -> None:
        notif_text = "A new member has signed up!"

        dont_email = "*(Do not email)*" if member.do_not_email else ""
        dont_call = "*(Do not call)*" if member.do_not_call else ""

        phones = ",".join([f"({num.name}) {num.number}" for num in member.phone_numbers])

        steps_taken_text = self._steps_taken_as_blocks(onboarding_results)

        blocks = [
            SectionBlock(text="A new member has signed up and needs attention!"),
            DividerBlock(),
            SectionBlock(text=f"*Basic info:* {member.name} - {member.email_address} {dont_email}"),
            SectionBlock(text=f"*Phones:* {dont_call} {phones}"),
            SectionBlock(text=f"*Onboarding Source:* {onboarding_source}"),
            DividerBlock(),
            SectionBlock(text="*Steps Taken Already:* \n" + steps_taken_text)
        ]

        next_steps_text = self._next_steps_as_blocks(onboarding_results)
        blocks += [
            DividerBlock(),
            SectionBlock(text="*Next Steps:* \n" + next_steps_text)
        ]

        self._get_slack_client().chat_postMessage(
            channel=SLACK_ONBOARDING_CHANNEL_ID, text=notif_text, blocks=blocks)

    def _steps_taken_as_blocks(self, results: OnboardingResults) -> List[Block]:
        return self._as_blocks(results, self._create_result_block)

    def _next_steps_as_blocks(self, results: OnboardingResults) -> List[Block]:
        return self._as_blocks(results, self._create_next_step_block)

    def _as_blocks(self,
                   results: OnboardingResults,
                   to_block_func: Callable[[OnboardingResults], Block]) -> List[Block]:
        text = ""

        onboarding_block = to_block_func("Auth0", results.auth0_result)
        if onboarding_block:
            text += onboarding_block
            text += '\n'

        portal_block = to_block_func("Portal Email", results.portal_email_result)
        if portal_block:
            text += portal_block
            text += '\n'

        google_block = to_block_func("Google Groups", results.google_group_result)
        if google_block:
            text += google_block
            text += '\n'

        slack_block = to_block_func("Slack", results.slack_result)
        if slack_block:
            text += slack_block
            text += '\n'

        newsletter_block = to_block_func("Newsletter", results.newsletter_result)
        if newsletter_block:
            text += newsletter_block
            text += '\n'

        hustle_block = to_block_func("Hustle", results.hustle_result)
        if hustle_block:
            text += hustle_block
            text += '\n'

        crm_block = to_block_func("CRM", results.crm_result)
        if crm_block:
            text += crm_block

        return text

    def _create_result_block(self, title: str, result: OnboardingResult) -> Optional[str]:
        if result.status == OnboardingStatus.NOT_IMPLEMENTED:
            return None
        if result.message:
            return f"- *{title}:* {self._map_status(result.status)} - {result.message}"
        else:
            return f"- *{title}:* {self._map_status(result.status)}"

    def _create_next_step_block(self,
                                title: str,
                                result: OnboardingResults) -> Optional[SectionBlock]:
        not_implemented = result.status == OnboardingStatus.NOT_IMPLEMENTED
        error = result.status == OnboardingStatus.ERROR

        if not_implemented or error:
            return f"- Add to {title}"
        else:
            return None

    def _map_status(self, status: OnboardingStatus) -> str:
        if status == OnboardingStatus.SUCCESS:
            return "Success!"
        elif status == OnboardingStatus.ERROR:
            return "Error"
        elif status == OnboardingStatus.OPTED_OUT:
            return "Opted Out"
        elif status == OnboardingStatus.ALREADY_ADDED:
            return "Already Added"
        elif status == OnboardingStatus.NOT_IMPLEMENTED:
            return "Not Implemented"
        elif status == OnboardingStatus.SKIPPED:
            return "Skipped"

    def _get_slack_client(self):
        return slack_client
