from datetime import datetime
from typing import List, Optional, Set, Union, cast

from pydantic.networks import EmailStr
from sqlalchemy import orm, select
from sqlalchemy.sql.schema import Column

from membership.database.models import Contact, EmailAddress
from membership.errors.exceptions import (
    CreateAlreadyExistsError,
    CreateArchivedExistsError,
    ResourceMissingError,
)
from membership.schemas.id import IdContact, IdEmailAddress
from membership.schemas.rest.email_addresses import (
    SxEmailAddressCreateRequest,
    SxEmailAddressUpdateRequest,
)
from membership.util.pagination import (
    DEFAULT_PAGE_LIMIT,
    PaginatedList,
    list_pagable_elements,
)


class ContactEmailAddressExistsError(CreateAlreadyExistsError):
    def __init__(self):
        super().__init__(object="email_address")


class EmailAddressExistsError(CreateAlreadyExistsError):
    def __init__(self):
        super().__init__(object="email_address")


class EmailAddressArchivedExistsError(CreateArchivedExistsError):
    def __init__(self, *, existing_id: Optional[str] = None):
        super().__init__(object="email_address", existing_id=existing_id)


class EmailAddressNotFoundError(ResourceMissingError):
    def __init__(self):
        super().__init__(object="email_address")


def from_id(
    session: orm.Session,
    id: IdEmailAddress,
) -> Optional[EmailAddress]:
    raw_id = EmailAddress.parse_id(id)
    existing_def = session.query(EmailAddress).get(raw_id)
    return existing_def


def from_id_or_raise(session: orm.Session, id: IdEmailAddress) -> EmailAddress:
    existing_def = from_id(session, id)
    if existing_def is None:
        raise EmailAddressNotFoundError()

    return existing_def


def from_email_address(session: orm.Session, email: EmailStr) -> Optional[EmailAddress]:
    normalized_email = EmailAddress.normalize(EmailStr.validate(email))
    return (
        session.execute(
            select(EmailAddress).where(EmailAddress._raw_value == normalized_email)
        )
        .scalars()
        .one_or_none()
    )


def from_email_address_or_raise(session: orm.Session, email: EmailStr) -> EmailAddress:
    result = from_email_address(session, email)
    if result is None:
        raise EmailAddressNotFoundError()
    return result


def create(
    session: orm.Session,
    create: SxEmailAddressCreateRequest,
    *,
    contact: Optional[Contact] = None,
    date_created: Optional[datetime] = None,
    defer_commit: bool = True,
) -> EmailAddress:
    date_created = date_created or create.date_created or datetime.utcnow()

    existing_email = from_email_address(session, create.value)
    if existing_email is not None:
        if existing_email.date_archived:
            raise EmailAddressArchivedExistsError(existing_id=existing_email.id)
        else:
            raise EmailAddressExistsError()

    model = EmailAddress(
        value=create.value,
        primary=create.primary,
        consent_to_email=create.consent_to_email,
        consent_to_share=create.consent_to_share,
        contact=contact,
        date_created=date_created,
    )
    session.add(model)

    if not defer_commit:
        session.commit()

    return model


def bulk_create(
    session: orm.Session,
    creates: List[SxEmailAddressCreateRequest],
    *,
    contact: Optional[Contact] = None,
    date_created: Optional[datetime] = None,
    defer_commit: bool = True,
) -> List[EmailAddress]:
    existing_emails: Set[str] = (
        set(ea.value for ea in contact.email_addresses)
        if contact is not None
        else set()
    )
    created: List[EmailAddress] = []
    for req in creates:
        if req.value not in existing_emails:
            result = create(
                session,
                create=req,
                contact=contact,
                date_created=date_created,
                defer_commit=defer_commit,
            )
            existing_emails.add(req.value)
            created.append(result)
    return created


def attach_to_contact(
    session: orm.Session,
    email_address: EmailAddress,
    *,
    contact: Contact,
    defer_commit: bool = True,
) -> EmailAddress:
    existing_addresses = set(e.value for e in contact.email_addresses)
    if email_address.value in existing_addresses:
        raise ContactEmailAddressExistsError(
        )

    if email_address.primary:
        # unset primary for all other email addresses
        for e in contact.email_addresses:
            e.primary = False
            session.add(e)

    email_address.contact = contact
    session.add(email_address)
    session.flush()
    session.refresh(email_address)

    if not defer_commit:
        session.commit()

    return email_address


def attach_to_contact_by_id(
    session: orm.Session,
    email_address: EmailAddress,
    *,
    contact_id: IdContact,
    defer_commit: bool = True,
) -> EmailAddress:
    # Defeat cyclic imports
    from membership.services import contact_service

    contact = contact_service.from_id_or_raise(session, contact_id)
    return attach_to_contact(
        session, email_address, contact=contact, defer_commit=defer_commit
    )


def list_(
    session: orm.Session,
    *,
    contact_id: Optional[str] = None,
    limit: Optional[int] = DEFAULT_PAGE_LIMIT,
    starting_after: Optional[str] = None,
    ending_before: Optional[str] = None,
    show_archived: Optional[bool] = False,
) -> PaginatedList[EmailAddress]:
    limit = limit if limit is not None else DEFAULT_PAGE_LIMIT
    return list_pagable_elements(
        session,
        EmailAddress,
        from_id_fn=from_id,
        where=[
            EmailAddress.contact_id == Contact.parse_id(contact_id)
            if contact_id is not None
            else None,
            EmailAddress.date_archived == None if not show_archived else None,
        ],
        secondary_sort_columns=[
            cast(Column, EmailAddress._raw_id),
        ],
        limit=limit,
        starting_after=starting_after,
        ending_before=ending_before,
    )


def update(
    session: orm.Session,
    to_update: EmailAddress,
    *,
    update_request: SxEmailAddressUpdateRequest,
    defer_commit: bool = True,
) -> EmailAddress:
    if update_request.primary is not None:
        to_update.primary = update_request.primary
    if update_request.consent_to_email is not None:
        to_update.consent_to_email = update_request.consent_to_email
    if update_request.consent_to_share is not None:
        to_update.consent_to_share = update_request.consent_to_share

    session.add(to_update)
    session.flush()
    session.refresh(to_update)

    if not defer_commit:
        session.commit()

    return to_update


def update_from_id(
    session: orm.Session,
    id: IdEmailAddress,
    *,
    contact_id: IdContact = None,
    update_request: SxEmailAddressUpdateRequest,
    defer_commit: bool = True,
) -> EmailAddress:
    to_update = from_id_or_raise(session, id)
    if contact_id is not None and to_update.contact.id != contact_id:
        raise EmailAddressNotFoundError(
        )

    return update(
        session, to_update, update_request=update_request, defer_commit=defer_commit
    )


def archive(
    session: orm.Session,
    to_archive: EmailAddress,
    *,
    reason_archived: Optional[str],
    archived: bool,
    defer_commit: bool = True,
) -> EmailAddress:
    if archived:
        to_archive.date_archived = datetime.utcnow()
        to_archive.reason_archived = reason_archived
    else:
        to_archive.date_archived = None
        to_archive.reason_archived = None

    session.add(to_archive)
    session.flush()
    session.refresh(to_archive)

    if not defer_commit:
        session.commit()

    return to_archive


def archive_from_id(
    session: orm.Session,
    id: IdEmailAddress,
    *,
    reason_archived: Optional[str],
    contact_id: IdContact = None,
    archived: bool,
    defer_commit: bool = True,
) -> EmailAddress:
    to_archive = from_id_or_raise(session, id)
    if contact_id is not None and to_archive.contact.id != contact_id:
        raise EmailAddressNotFoundError(
        )

    return archive(
        session,
        to_archive,
        reason_archived=reason_archived,
        archived=archived,
        defer_commit=defer_commit,
    )


def mark_confirmed(
    session: orm.Session,
    to_confirm: EmailAddress,
    *,
    confirmed: bool,
    defer_commit: bool = True,
) -> EmailAddress:
    if confirmed:
        to_confirm.date_confirmed = datetime.utcnow()
    else:
        to_confirm.date_confirmed = None

    session.add(to_confirm)
    session.flush()
    session.refresh(to_confirm)

    if not defer_commit:
        session.commit()

    return to_confirm


def mark_confirmed_from_id(
    session: orm.Session,
    id: IdEmailAddress,
    *,
    contact_id: IdContact = None,
    confirmed: bool,
    defer_commit: bool = True,
) -> EmailAddress:
    to_confirm = from_id_or_raise(session, id)
    if contact_id is not None and to_confirm.contact.id != contact_id:
        raise EmailAddressNotFoundError(
        )

    return mark_confirmed(
        session, to_confirm, confirmed=confirmed, defer_commit=defer_commit
    )


def clean(candidate: Union[str, EmailStr, SxEmailAddressCreateRequest]) -> EmailStr:
    value = None
    if isinstance(candidate, SxEmailAddressCreateRequest):
        value = candidate.value
    else:
        value = candidate

    return EmailStr(EmailStr.validate(value))  # HACK is there a better way
