import time
from datetime import datetime
from typing import Any, List, Optional, Protocol, cast

import structlog
from sqlalchemy import orm

import membership.schemas.formatters.email_addresses as email_address_fmt
import membership.schemas.formatters.phone_numbers as phone_number_fmt
from membership.database.models import (
    Contact,
    ImportEvent,
    ImportEventReviewAction,
    ImportEventReviewActionType,
    SecurityPrincipal,
)
from membership.errors.exceptions import ResourceMissingError
from membership.schemas.id import (
    IdImportEvent,
    IdImportEventReviewAction,
    IdSecurityPrincipal,
)
from membership.schemas.rest.contacts import SxContactCreateRequest
from membership.schemas.rest.import_events import (
    MERGE_ACTION_TYPES,
    MERGE_LIST_ATTRIBUTES,
    ImportEventInferConfidence,
    ReviewMergeActionListType,
    ReviewMergeActionType,
    SxImportEventCreateRequest,
    SxImportEventMatchJustification,
    SxImportEventReviewActionsUpdateRequest,
    SxImportReviewMerge,
    SxImportReviewMergeAction,
    SxImportReviewMergeCollectionAction,
)
from membership.schemas.rest.postal_addresses import SxPostalAddressCreateRequest
from membership.services import (
    contact_service,
    custom_field_service,
    email_address_service,
    phone_number_service,
    tag_service,
)
from membership.util.arrays import compact, flatten
from membership.util.pagination import (
    DEFAULT_PAGE_LIMIT,
    PaginatedList,
    list_pagable_elements,
)

DEFAULT_DAYS_AGO = 7


class DuplicateEventSlugError(Exception):
    pass


class ImportEventNotFoundError(ResourceMissingError):
    def __init__(self):
        super().__init__(object="import_event")


class InvalidContactImportStateError(ValueError):
    pass


class InvalidImportEventReviewActionStateError(ValueError):
    pass


class MissingImportEventParticipantsError(ValueError):
    pass


class FnMapImportEventElemToContact(Protocol):
    def __call__(
        self, elem: Any, *, tags: List[str], custom_fields: List[str]
    ) -> SxContactCreateRequest:
        ...


class FnSpecificInferContact(Protocol):
    def __call__(
        self, session: orm.Session, elem: Any
    ) -> Optional[ImportEventReviewAction]:
        ...


def default_noop_infer_fn(
    session: orm.Session, elem: Any
) -> Optional[ImportEventReviewAction]:
    return None


def from_id(session: orm.Session, id: IdImportEvent) -> Optional[ImportEvent]:
    raw_id = ImportEvent.parse_id(id)
    return session.query(ImportEvent).get(raw_id)


def from_id_or_raise(session: orm.Session, id: IdImportEvent) -> ImportEvent:
    import_event = from_id(session, id)
    if import_event is None:
        raise ImportEventNotFoundError()
    return import_event


def list_import_events(
    session: orm.Session,
    *,
    limit: Optional[int] = DEFAULT_PAGE_LIMIT,
    starting_after: Optional[str] = None,
    ending_before: Optional[str] = None,
) -> PaginatedList[ImportEvent]:
    limit = limit if limit is not None else DEFAULT_PAGE_LIMIT
    return list_pagable_elements(
        session,
        ImportEvent,
        from_id_fn=from_id,
        limit=limit,
        starting_after=starting_after,
        ending_before=ending_before,
    )


def create(
    session: orm.Session,
    create: SxImportEventCreateRequest,
    *,
    creator: SecurityPrincipal,
    date_created: Optional[datetime] = None,
    infer_fn: FnSpecificInferContact = default_noop_infer_fn,
    create_fn: FnMapImportEventElemToContact,
    defer_commit: bool = True,
) -> ImportEvent:
    date_created = date_created or create.date_created or datetime.utcnow()

    tags = (
        [tag_service.from_id_or_raise(session, tag) for tag in create.tags]
        if create.tags is not None
        else []
    )
    custom_fields = (
        [
            custom_field_service.from_id_or_raise(session, cf)
            for cf in create.custom_fields
        ]
        if create.custom_fields is not None
        else []
    )

    event = ImportEvent(
        title=create.title,
        event_type=create.event_type,
        creator=creator,
        date_created=date_created,
        event_info=create.event_info,
    )

    if infer_fn is None or not callable(infer_fn):
        raise ValueError("Infer function for create_import_event must be callable")

    if len(create.participants) == 0:
        raise MissingImportEventParticipantsError(
            "Must have at least one participant in import event"
        )

    # Create temporary contacts and attempt to identify matches for all
    # proposed entries.
    for entry in create.participants:
        create_request = create_fn(
            entry,
            tags=[tag.name for tag in tags],
            custom_fields=[cf.id for cf in custom_fields],
        )
        specific_action = infer_fn(session, entry)
        if specific_action is not None:
            # The dev-provided inference function produced an action, let's defer to that
            specific_action.participant = create_request
            specific_action.import_event = event
            session.add(specific_action)
        else:
            # The dev-provided inference function wasn't provided or didn't produce an action
            # Let's use the general inference function
            generic_action = infer_generic_action_for_contact(
                session, create_request=create_request
            )
            generic_action.participant = create_request
            generic_action.import_event = event
            session.add(generic_action)

    session.add_all([event])

    if not defer_commit:
        session.commit()

    return event


def infer_generic_action_for_contact(
    session: orm.Session,
    *,
    create_request: SxContactCreateRequest,
) -> ImportEventReviewAction:
    """Attempt to infer an automatic action to take for this contact

    Define behavior and heuristics here to direct whether the backend proposes
    an automatic match for the contact, or instead suggest that we create
    a new account for them.
    """

    # TODO: performance!
    unique_emails = set(
        [email_address_service.clean(email) for email in create_request.email_addresses]
        if create_request.email_addresses is not None
        else []
    )
    email_matches: List[Contact] = list(
        compact(
            [
                contact_service.find_by_email_address(session, email_address=email)
                for email in unique_emails
            ]
        )
    )

    full_name_matches: List[Contact] = contact_service.find_by_name(
        session, create_request.full_name
    )

    valid_phones = set(
        compact([phone_number_service.clean(pn) for pn in create_request.phone_numbers])
        if create_request.phone_numbers is not None
        else []
    )
    if len(valid_phones) > 0:
        phone_matches: List[Contact] = list(
            flatten(
                [
                    contact_service.find_by_phone_number(session, pn)
                    for pn in valid_phones
                ]
            )
        )
    else:
        phone_matches = []

    all_matches = set(full_name_matches + phone_matches + email_matches)

    # Inspired by import_membership
    # Python uses stable sort, so multi-sort can be achieved by sorting by each comparator in
    # reverse order, as per https://docs.python.org/3/howto/sorting.html
    # Sort from "least important" to "most important"
    sorted_matches = all_matches
    sorted_matches = sorted(
        sorted_matches,
        key=lambda x: 0
        if x.full_name.lower() == create_request.full_name.lower()
        else 1,
    )
    sorted_matches = sorted(
        sorted_matches,
        key=lambda x: 0
        if create_request.phone_numbers is not None
        and contact_service.has_any_phone_number(x, valid_phones)
        else 1,
    )
    sorted_matches = sorted(
        sorted_matches,
        key=lambda x: 0
        if contact_service.has_any_email_address(x, unique_emails)
        else 1,
    )

    if len(sorted_matches) >= 1:
        most_confident_match = sorted_matches[0]
        emails_match = contact_service.has_any_email_address(
            most_confident_match, unique_emails
        )
        phones_match = contact_service.has_any_phone_number(
            most_confident_match, valid_phones
        )
        full_name_match = (
            most_confident_match.full_name.lower() == create_request.full_name.lower()
        )
        confidence: ImportEventInferConfidence = (
            ImportEventInferConfidence.strong
            if emails_match or phones_match
            else ImportEventInferConfidence.fuzzy
        )
        return ImportEventReviewAction(
            action=ImportEventReviewActionType.proposed_match,
            merge_into_contact=sorted_matches[0],
            match_justification=SxImportEventMatchJustification(
                confidence=confidence,
                strong_matches=compact(
                    [
                        "email_addresses" if emails_match else None,
                        "phone_numbers" if phones_match else None,
                    ]
                ),
                fuzzy_matches=compact(["full_name" if full_name_match else None]),
            ),
            merge=SxImportReviewMerge(
                full_name=SxImportReviewMergeAction(
                    attribute="full_name",
                    merge_action=ReviewMergeActionType.keep_existing,
                ),
                display_name=SxImportReviewMergeAction(
                    attribute="display_name",
                    merge_action=ReviewMergeActionType.keep_existing,
                ),
                pronouns=SxImportReviewMergeAction(
                    attribute="pronouns",
                    merge_action=ReviewMergeActionType.keep_existing,
                ),
                biography=SxImportReviewMergeAction(
                    attribute="biography",
                    merge_action=ReviewMergeActionType.keep_existing,
                ),
                profile_pic=SxImportReviewMergeAction(
                    attribute="profile_pic",
                    merge_action=ReviewMergeActionType.keep_existing,
                ),
                admin_notes=SxImportReviewMergeAction(
                    attribute="admin_notes",
                    merge_action=ReviewMergeActionType.keep_existing,
                ),
                email_addresses=SxImportReviewMergeCollectionAction(
                    attribute="email_addresses",
                    merge_action=ReviewMergeActionListType.add_new,
                ),
                phone_numbers=SxImportReviewMergeCollectionAction(
                    attribute="phone_numbers",
                    merge_action=ReviewMergeActionListType.add_new,
                ),
                mailing_addresses=SxImportReviewMergeCollectionAction(
                    attribute="mailing_addresses",
                    merge_action=ReviewMergeActionListType.add_new,
                ),
                tags=SxImportReviewMergeCollectionAction(
                    attribute="tags", merge_action=ReviewMergeActionListType.add_new
                ),
                custom_fields=SxImportReviewMergeCollectionAction(
                    attribute="custom_fields",
                    merge_action=ReviewMergeActionListType.add_new,
                ),
            ),
        )
    else:
        return ImportEventReviewAction(
            action=ImportEventReviewActionType.proposed_create,
        )


def update_review_actions(
    session: orm.Session,
    *,
    review_update: SxImportEventReviewActionsUpdateRequest,
    event: ImportEvent,
    defer_commit: bool = True,
) -> List[ImportEventReviewAction]:
    new_actions = {a.id: a for a in review_update.actions}
    existing_actions = event.review_actions
    updated = []

    invalid_ids = set(new_actions.keys()).difference(
        set(action.id for action in existing_actions)
    )
    if len(invalid_ids) > 0:
        raise InvalidImportEventReviewActionStateError(
            f"invalid action ids '{str(invalid_ids)}' when attempting to update import {event.id=}"
        )

    for to_update in existing_actions:
        new_action = new_actions.get(IdImportEventReviewAction.validate(to_update.id))
        if new_action is None:
            continue

        # validate and check action, and associated fields
        if new_action.action in MERGE_ACTION_TYPES:
            if new_action.merge_into_contact_id is not None:
                # validate merge_into_contact_id if specified
                if (
                    contact_service.from_id(session, new_action.merge_into_contact_id)
                    is None
                ):
                    raise InvalidImportEventReviewActionStateError(
                        "`merge_into_contact_id` is not a valid contact id"
                    )

                raw_merge_contact_id = (
                    Contact.parse_id(new_action.merge_into_contact_id)
                    if new_action.merge_into_contact_id is not None
                    else None
                )

                to_update.merge_into_contact_id = raw_merge_contact_id

            # parse and validate import review merge if specified
            new_merge = SxImportReviewMerge(
                **{
                    **(
                        to_update.merge.dict(exclude_none=True, exclude_unset=True)
                        if to_update.merge is not None
                        else {}
                    ),
                    **(
                        new_action.merge.dict(exclude_none=True, exclude_unset=True)
                        if new_action.merge is not None
                        else {}
                    ),
                }
            )

            to_update.merge = new_merge
        else:
            if new_action.merge_into_contact_id is not None:
                raise InvalidImportEventReviewActionStateError(
                    "can only specify `merge_into_contact_id` for merge actions"
                )
            if new_action.merge is not None:
                raise InvalidImportEventReviewActionStateError(
                    "can only specify `merge` for merge actions"
                )

            to_update.merge = None
            to_update.merge_into_contact_id = None

        to_update.action = new_action.action

        session.add(to_update)
        updated.append(to_update)

    session.add(event)
    session.flush()
    session.refresh(event)

    if not defer_commit:
        session.commit()

    return updated


def _apply_merge_action(session: orm.Session, action: ImportEventReviewAction) -> None:
    logger = structlog.stdlib.get_logger()

    logger = logger.bind(action_id=action.id)

    existing_contact = action.merge_into_contact
    action_participant = action.participant

    if existing_contact is None:
        raise InvalidImportEventReviewActionStateError(
            "missing existing contact to process merge to"
        )

    if action_participant is None:
        raise InvalidImportEventReviewActionStateError(
            "missing contact to process merge from"
        )

    if action.merge is None:
        raise InvalidImportEventReviewActionStateError("missing merge instructions")

    logger = logger.bind(existing_contact_id=existing_contact.id)

    for key, directive in action.merge.dict(exclude_none=True).items():
        logger = logger.bind(key=key, directive=directive)

        if directive is None:
            continue

        name = directive["attribute"]
        merge_action = directive["merge_action"]

        try:
            new_value = getattr(action_participant, name)
        except AttributeError:
            raise InvalidImportEventReviewActionStateError(
                f"unrecognized participant attribute {name}"
            )

        logger = logger.bind(name=name, new_value=new_value, merge_action=merge_action)

        if name in MERGE_LIST_ATTRIBUTES:
            if merge_action in ReviewMergeActionListType.list_types():
                if new_value is None:
                    continue

                if name == "email_addresses":
                    email_addresses = email_address_fmt.normalize_email_addresses(
                        new_value
                    )
                    for email_address in email_addresses:
                        _, result = contact_service.try_add_email_address(
                            session,
                            contact=existing_contact,
                            email_address=email_address,
                        )
                elif name == "phone_numbers":
                    phone_numbers = phone_number_fmt.normalize_phone_numbers(new_value)
                    for phone_number in phone_numbers:
                        _, result = contact_service.try_add_phone_number(
                            session,
                            contact=existing_contact,
                            phone_number=phone_number,
                        )
                elif name == "mailing_addresses":
                    for mailing_address in cast(
                        List[SxPostalAddressCreateRequest], new_value
                    ):
                        _, result = contact_service.try_add_mailing_address(
                            session,
                            contact=existing_contact,
                            mailing_address=mailing_address,
                        )
                elif name == "tags":
                    for tag in sorted(set(new_value)):
                        created_tag = tag_service.from_id_or_raise(session, tag)
                        contact_service.add_tag(session, existing_contact, created_tag)
                elif name == "custom_fields":
                    contact_service.add_custom_field_mapping(
                        session, existing_contact, new_value
                    )
                else:
                    raise InvalidImportEventReviewActionStateError(
                        "encountered unknown merge list action"
                    )
            elif merge_action == ReviewMergeActionType.keep_existing:
                # no op
                pass
            else:
                raise InvalidImportEventReviewActionStateError(
                    f"can't replace attribute {name}; can only add"
                )
        else:
            try:
                existing_value = getattr(existing_contact, name)

                if merge_action == ReviewMergeActionType.take_incoming:
                    logger.debug(
                        f"process_merge_action.merge_take_incoming_item "
                        f"{action.id=} {existing_contact.id=} "
                        f"{key=} {directive=} {name=} {merge_action=} "
                        f"{existing_value=} {new_value=}"
                    )
                    setattr(existing_contact, name, new_value)
                elif merge_action == ReviewMergeActionType.keep_existing:
                    logger.debug(
                        f"process_merge_action.merge_keep_existing_item "
                        f"{action.id=} {existing_contact.id=} "
                        f"{key=} {directive=} {name=} {merge_action=} "
                        f"{existing_value=} {new_value=}"
                    )
                elif merge_action == ReviewMergeActionType.manual_edit:
                    new_value = directive["edit_value"]
                    logger.debug(
                        f"process_merge_action.merge_manual_edit_item "
                        f"{action.id=} {existing_contact.id=} "
                        f"{key=} {directive=} {name=} {merge_action=} "
                        f"{existing_value=} {new_value=}"
                    )
                    setattr(existing_contact, name, new_value)
                elif merge_action == ReviewMergeActionListType.add_new:
                    raise InvalidImportEventReviewActionStateError(
                        f"can't add more of type {name} to a contact record"
                    )
                else:
                    raise InvalidImportEventReviewActionStateError(
                        "encountered unknown merge action"
                    )
            except Exception as e:
                logger.exception("unknown_contact_attribute", error=e)
                raise InvalidImportEventReviewActionStateError(
                    "unrecognized contact attribute"
                )

        logger.debug("process_merge_action")

        session.add(existing_contact)


def _apply_create_action(session: orm.Session, action: ImportEventReviewAction) -> None:
    logger = structlog.stdlib.get_logger()

    logger = logger.bind(action_id=action.id)

    if action.merge_into_contact is not None:
        raise InvalidImportEventReviewActionStateError(
            "review action with create directive cannot have merge contact"
        )

    contact = contact_service.create(session, create_request=action.participant)
    logger = logger.bind(contact_id=contact.id)

    logger.debug("process_create_action")


def apply_import_event(
    session: orm.Session,
    *,
    event: ImportEvent,
    reviewer_id: IdSecurityPrincipal,
    defer_commit: bool = True,
) -> ImportEvent:
    logger = structlog.stdlib.get_logger()

    timing_start = time.time_ns()

    actions = event.review_actions

    logger = logger.bind(
        event_id=event.id, reviewer_id=reviewer_id, num_actions=len(actions)
    )
    for action in actions:
        action_type = action.action.name
        logger.debug("apply_action", action_id=action.id, action_type=action_type)
        if action_type in MERGE_ACTION_TYPES:
            _apply_merge_action(session, action)
        elif (
            action.action == ImportEventReviewActionType.create_new_contact
            or action.action == ImportEventReviewActionType.proposed_create
        ):
            _apply_create_action(session, action)

    event.reviewer_id = SecurityPrincipal.parse_id(reviewer_id)
    event.date_reviewed = datetime.utcnow()
    session.add(event)
    session.flush()

    if not defer_commit:
        session.commit()

    session.refresh(event)

    logger.info("apply_import_event", timing_import_event=time.time_ns() - timing_start)

    return event


def review_action_from_id(
    session: orm.Session, id: IdImportEventReviewAction
) -> Optional[ImportEventReviewAction]:
    raw_id = ImportEventReviewAction.parse_id(id)
    return session.query(ImportEventReviewAction).get(raw_id)


def list_review_actions(
    session: orm.Session,
    event_id: IdImportEvent,
    *,
    limit: Optional[int] = None,
    starting_after: Optional[str] = None,
    ending_before: Optional[str] = None,
) -> PaginatedList[ImportEventReviewAction]:
    limit = limit if limit is not None else DEFAULT_PAGE_LIMIT
    return list_pagable_elements(
        session,
        ImportEventReviewAction,
        from_id_fn=review_action_from_id,
        where=[
            ImportEventReviewAction.import_event_id == ImportEvent.parse_id(event_id)
        ],
        limit=limit,
        starting_after=starting_after,
        ending_before=ending_before,
    )
