from datetime import datetime
from typing import List, Optional, Set, Union, cast

from sqlalchemy import orm, select
from sqlalchemy.sql.schema import Column

from membership.database.models import Contact, Member, PhoneNumber
from membership.database.statements import insert_or_ignore
from membership.errors.exceptions import (
    AssociationExistsError,
    CreateAlreadyExistsError,
    ResourceMissingError,
)
from membership.schemas.id import IdContact, IdPhoneNumber
from membership.schemas.rest.phone_numbers import (
    PhoneStr,
    SxPhoneNumberCreateRequest,
    SxPhoneNumberUpdateRequest,
)
from membership.util.pagination import (
    DEFAULT_PAGE_LIMIT,
    PaginatedList,
    list_pagable_elements,
)


class PhoneNumberExistsError(CreateAlreadyExistsError):
    pass


class ContactPhoneNumberExistsError(AssociationExistsError):
    pass


class PhoneNumberNotFoundError(ResourceMissingError):
    def __init__(self):
        super().__init__(object="phone_number")


def from_id(
    session: orm.Session,
    id: IdPhoneNumber,
) -> Optional[PhoneNumber]:
    raw_id = PhoneNumber.parse_id(id)
    existing_def = session.query(PhoneNumber).get(raw_id)
    return existing_def


def from_id_or_raise(session: orm.Session, id: IdPhoneNumber) -> PhoneNumber:
    existing_def = from_id(session, id)
    if existing_def is None:
        raise PhoneNumberNotFoundError()

    return existing_def


def new(
    number: str,
    name: Optional[str] = None,
    *,
    primary: bool = False,
    member: Optional[Member] = None,
    contact: Optional[Contact] = None,
    date_created: datetime = datetime.utcnow(),
) -> PhoneNumber:
    """Creates a phone number with a normalized format."""
    formatted_num = PhoneNumber.format_number(number)

    return PhoneNumber(
        number=formatted_num,
        name=name,
        member=member,
        primary=primary,
        contact=contact,
        date_created=date_created,
    )


def create(
    session: orm.Session,
    create: SxPhoneNumberCreateRequest,
    *,
    contact: Optional[Contact] = None,
    date_created: Optional[datetime] = None,
    defer_commit: bool = True,
) -> PhoneNumber:
    date_created = date_created or create.date_created or datetime.utcnow()
    create_dict = create.dict()
    del create_dict["date_created"]

    model = PhoneNumber(
        number=create.value,
        date_created=date_created,
        contact=contact,
        **create_dict,
    )

    session.add(model)

    if not defer_commit:
        session.commit()

    return model


def bulk_create(
    session: orm.Session,
    phone_numbers: List[SxPhoneNumberCreateRequest],
    *,
    contact: Optional[Contact] = None,
    date_created: Optional[datetime] = None,
    defer_commit: bool = True,
) -> List[PhoneNumber]:
    existing_phone_numbers: Set[str] = (
        set(pn.value for pn in contact.phone_numbers) if contact is not None else set()
    )
    created: List[PhoneNumber] = []
    for req in phone_numbers:
        if req.value not in existing_phone_numbers:
            result = create(
                session,
                req,
                contact=contact,
                date_created=date_created,
                defer_commit=defer_commit,
            )
            existing_phone_numbers.add(req.value)
            created.append(result)
    return created


def upsert(session: orm.Session, member_id: int, number: PhoneNumber) -> None:
    number.member_id = member_id
    add_all(session, [number])


def add_all(session: orm.Session, numbers: List[PhoneNumber]) -> None:
    statement = insert_or_ignore(session, PhoneNumber)
    session.execute(
        statement,
        [
            {"member_id": pn.member_id, "number": pn.number, "name": pn.name}
            for pn in numbers
        ],
    )
    session.commit()


def exists(session: orm.Session, *, member: Member, number: str) -> bool:
    formatted_number = PhoneNumber.format_number(number)
    existing_phone_numbers: List[PhoneNumber] = (
        session.query(PhoneNumber).filter_by(member_id=member.id).all()
    )

    formatted_existing = set(
        PhoneNumber.format_number(pn.number) for pn in existing_phone_numbers
    )

    return formatted_number in formatted_existing


def best_phone_for_calling(numbers: List[PhoneNumber]) -> Optional[PhoneNumber]:
    cell = next(iter([pn for pn in numbers if pn.name == "Cell"]), None)
    if cell:
        return cell

    kiosk = next(
        iter([pn for pn in numbers if pn.name == "Kiosk" or pn.name == "WebForm"]),
        None,
    )
    if kiosk:
        return kiosk

    home = next(iter([pn for pn in numbers if pn.name == "Home"]), None)
    if home:
        return home

    return next(iter(numbers), None)


def attach_to_contact(
    session: orm.Session,
    phone_number: PhoneNumber,
    *,
    contact: Contact,
    defer_commit: bool = True,
) -> PhoneNumber:
    existing_phone_numbers = set(p.number for p in contact.phone_numbers)
    if phone_number.number in existing_phone_numbers:
        raise ContactPhoneNumberExistsError(
            target_object="contact",
            association_object="phone_number",
        )

    if phone_number.primary:
        for p in contact.phone_numbers:
            p.primary = False
            session.add(p)

    phone_number.contact = contact
    session.add(phone_number)
    session.flush()
    session.refresh(phone_number)

    if not defer_commit:
        session.commit()

    return phone_number


def attach_to_contact_from_id(
    session: orm.Session,
    phone_number: PhoneNumber,
    *,
    contact_id: IdContact,
    defer_commit: bool = True,
) -> PhoneNumber:
    from membership.services import contact_service

    contact = contact_service.from_id_or_raise(session, contact_id)
    return attach_to_contact(
        session, phone_number, contact=contact, defer_commit=defer_commit
    )


def list_(
    session: orm.Session,
    *,
    contact_id: Optional[str] = None,
    limit: Optional[int] = DEFAULT_PAGE_LIMIT,
    starting_after: Optional[str] = None,
    ending_before: Optional[str] = None,
    show_archived: Optional[bool] = False,
) -> PaginatedList[PhoneNumber]:
    limit = limit if limit is not None else DEFAULT_PAGE_LIMIT
    return list_pagable_elements(
        session,
        PhoneNumber,
        from_id_fn=from_id,
        where=[
            PhoneNumber.contact_id == Contact.parse_id(contact_id)
            if contact_id is not None
            else None,
            PhoneNumber.date_archived == None if not show_archived else None,
        ],
        secondary_sort_columns=[
            cast(Column, PhoneNumber._raw_id),
        ],
        limit=limit,
        starting_after=starting_after,
        ending_before=ending_before,
    )


def update(
    session: orm.Session,
    to_update: PhoneNumber,
    *,
    update_request: SxPhoneNumberUpdateRequest,
    defer_commit: bool = True,
) -> PhoneNumber:
    if update_request.primary is not None:
        to_update.primary = update_request.primary
    if update_request.name is not None:
        to_update.name = update_request.name
    if update_request.consent_to_call is not None:
        to_update.consent_to_call = update_request.consent_to_call
    if update_request.sms_capable is not None:
        to_update.sms_capable = update_request.sms_capable
    if update_request.consent_to_text is not None:
        to_update.consent_to_text = update_request.consent_to_text
    if update_request.consent_to_share is not None:
        to_update.consent_to_share = update_request.consent_to_share

    session.add(to_update)
    session.flush()
    session.refresh(to_update)

    if not defer_commit:
        session.commit()

    return to_update


def update_from_id(
    session: orm.Session,
    id: IdPhoneNumber,
    *,
    contact_id: IdContact = None,
    update_request: SxPhoneNumberUpdateRequest,
    defer_commit: bool = True,
) -> PhoneNumber:
    to_update = from_id_or_raise(session, id)
    if contact_id is not None and to_update.contact.id != contact_id:
        raise PhoneNumberNotFoundError(
        )

    return update(
        session, to_update, update_request=update_request, defer_commit=defer_commit
    )


def archive(
    session: orm.Session,
    to_archive: PhoneNumber,
    *,
    reason_archived: Optional[str] = None,
    archived: bool,
    defer_commit: bool = True,
) -> PhoneNumber:
    if archived:
        to_archive.date_archived = datetime.utcnow()
        to_archive.reason_archived = reason_archived
    else:
        to_archive.date_archived = None
        to_archive.reason_archived = None

    session.add(to_archive)
    session.flush()
    session.refresh(to_archive)

    if not defer_commit:
        session.commit()

    return to_archive


def archive_from_id(
    session: orm.Session,
    id: IdPhoneNumber,
    *,
    reason_archived: Optional[str] = None,
    contact_id: IdContact = None,
    archived: bool,
    defer_commit: bool = True,
) -> PhoneNumber:
    to_archive = from_id_or_raise(session, id)
    if contact_id is not None and to_archive.contact.id != contact_id:
        raise PhoneNumberNotFoundError(
        )

    return archive(
        session,
        to_archive,
        reason_archived=reason_archived,
        archived=archived,
        defer_commit=defer_commit,
    )


def mark_confirmed(
    session: orm.Session,
    to_confirm: PhoneNumber,
    *,
    confirm: bool,
    defer_commit: bool = True,
) -> PhoneNumber:
    if confirm:
        to_confirm.date_confirmed = datetime.utcnow()
    else:
        to_confirm.date_confirmed = None

    session.add(to_confirm)
    session.flush()
    session.refresh(to_confirm)

    if not defer_commit:
        session.commit()

    return to_confirm


def mark_confirmed_from_id(
    session: orm.Session,
    id: IdPhoneNumber,
    *,
    contact_id: IdContact = None,
    confirm: bool,
    defer_commit: bool = True,
) -> PhoneNumber:
    to_confirm = from_id_or_raise(session, id)
    if contact_id is not None and to_confirm.contact.id != contact_id:
        raise PhoneNumberNotFoundError(
        )

    return mark_confirmed(
        session, to_confirm, confirm=confirm, defer_commit=defer_commit
    )


def clean(candidate: Union[str, SxPhoneNumberCreateRequest]) -> PhoneStr:
    value = None
    if isinstance(candidate, SxPhoneNumberCreateRequest):
        value = candidate.value
    else:
        value = candidate

    return PhoneStr(PhoneStr.validate(value))  # HACK is there a better way


def from_phone_number(session: orm.Session, pn: PhoneStr) -> List[PhoneNumber]:
    return (
        session.execute(select(PhoneNumber).where(PhoneNumber._raw_number == pn))
        .scalars()
        .all()
    )
