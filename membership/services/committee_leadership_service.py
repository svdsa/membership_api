"""Operations related to committees."""

from datetime import datetime
from typing import List, Optional

from dateutil.relativedelta import relativedelta
from sqlalchemy import orm

from membership.database.models import (
    Committee,
    CommitteeLeader,
    CommitteeLeadershipRole,
    LeadershipRole,
)
from membership.services.errors import NotFoundError, ValidationError
from membership.util import time


class CommitteeLeadershipService:
    class ErrorCodes:
        INVALID_TERM_END_DATE = "INVALID_TERM_END_DATE"

    def get_leadership_roles(
        self, session: orm.Session, committee_id: int
    ) -> Optional[List[CommitteeLeadershipRole]]:
        committee = session.query(Committee).get(committee_id)
        if committee is None:
            return None
        return committee.committee_leadership_roles

    def add_leadership_role(
        self,
        session: orm.Session,
        committee_id: int,
        title: str,
        *,
        term_limit_months: Optional[int] = None,
        defer_commit: bool = True,
    ) -> CommitteeLeadershipRole:
        leadership_role = (
            session.query(LeadershipRole)
            .filter(LeadershipRole.title == title).one_or_none()
        )
        if leadership_role is None:
            leadership_role = LeadershipRole(title=title)
            session.add(leadership_role)
        committee_leadership_role = CommitteeLeadershipRole(
            committee_id=committee_id,
            term_limit_months=term_limit_months,
        )
        committee_leadership_role.leadership_role = leadership_role
        session.add(committee_leadership_role)

        if not defer_commit:
            session.commit()

        return committee_leadership_role

    def replace_leader(
        self,
        session: orm.Session,
        committee_leadership_role_id: int,
        member_id: Optional[int],
        *,
        term_start_date: Optional[datetime] = None,
        term_end_date: Optional[datetime] = None,
        defer_commit: bool = True,
    ) -> CommitteeLeadershipRole:
        # TODO: should we validate that each member can hold at most one role per committee?
        committee_leadership_role = session.query(CommitteeLeadershipRole).get(
            committee_leadership_role_id
        )
        if committee_leadership_role is None:
            raise NotFoundError(
                f'Committee leadership role with id {committee_leadership_role_id} not found'
            )
        if term_start_date is None:
            term_start_date = time.get_current_time()
        if committee_leadership_role.current_leader is not None:
            committee_leadership_role.current_leader.term_end_date = term_start_date
        if member_id:
            if committee_leadership_role.term_limit_months:
                expected_end_date = (
                    term_start_date
                    + relativedelta(months=committee_leadership_role.term_limit_months)
                )
                if term_end_date is None:
                    term_end_date = expected_end_date
                elif term_end_date > expected_end_date:
                    raise ValidationError(
                        CommitteeLeadershipService.ErrorCodes.INVALID_TERM_END_DATE,
                        (
                            f'Term end date {term_end_date.isoformat()}'
                            f' is beyond the term limit {expected_end_date.isoformat()}'
                        )
                    )
            if term_end_date is not None and term_start_date >= term_end_date:
                raise ValidationError(
                    CommitteeLeadershipService.ErrorCodes.INVALID_TERM_END_DATE,
                    (
                        f'Term end date {term_end_date.isoformat()}'
                        f' is before the term start date {term_start_date.isoformat()}'
                    )
                )
            new_leader = CommitteeLeader(
                committee_leadership_role_id=committee_leadership_role_id,
                member_id=member_id,
                term_start_date=term_start_date,
                term_end_date=term_end_date,
            )
            session.add(new_leader)

        if not defer_commit:
            session.commit()

        return committee_leadership_role
