from typing import Sequence


class ValidationError(Exception):
    def __init__(self, key: str, message: str):
        super().__init__(message)
        self.key = key
        self.message = message


class NotFoundError(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class JsonFormatError(Exception):
    def __init__(self, path: Sequence[str], message: str):
        super().__init__(f"JsonFormatError: {message} @ {'.'.join(path)}")
        self.path = path
        self.message = message

    def __repr__(self):
        return f"JsonFormatError({repr(self.path)}, {repr(self.message)})"


class ConflictError(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message
