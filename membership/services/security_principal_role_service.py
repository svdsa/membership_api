from typing import Optional, Tuple

from sqlalchemy import orm, select

from membership.database.models import (
    SecurityPrincipal,
    SecurityPrincipalRole,
    SecurityPrincipalRoleAssociation,
)
from membership.errors.exceptions import AssociationExistsError, ResourceMissingError


class SecurityPrincipalRoleNotFoundError(ResourceMissingError):
    def __init__(self):
        super().__init__(object="security_principal_role")


class SecurityPrincipalRoleAssociationExistsError(AssociationExistsError):
    def __init__(self, *, target_object: Optional[str] = None) -> None:
        super().__init__(
            target_object=target_object, association_object="security_principal_role"
        )


def find_by_slug(session: orm.Session, slug: str) -> Optional[SecurityPrincipalRole]:
    return (
        session.execute(
            select(SecurityPrincipalRole).where(SecurityPrincipalRole.slug == slug)
        )
        .scalars()
        .one_or_none()
    )


def find_by_slug_or_raise(session: orm.Session, slug: str) -> SecurityPrincipalRole:
    spr = find_by_slug(session, slug)
    if spr is None:
        raise SecurityPrincipalRoleNotFoundError(
        )
    return spr


def attach_to_security_principal(
    session: orm.Session,
    *,
    sp: SecurityPrincipal,
    role: SecurityPrincipalRole,
    defer_commit: bool = True,
) -> Tuple[SecurityPrincipalRoleAssociation, SecurityPrincipal]:
    existing_roles = set(role.slug for role in sp.roles)
    if role.slug in existing_roles:
        raise SecurityPrincipalRoleAssociationExistsError(
            target_object="security_principal",
        )

    new_assoc = SecurityPrincipalRoleAssociation(role=role, security_principal=sp)
    session.add(new_assoc)
    session.flush()
    session.refresh(new_assoc)

    if not defer_commit:
        session.commit()

    return new_assoc, sp
