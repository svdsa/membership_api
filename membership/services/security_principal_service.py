from typing import Optional, cast

from sqlalchemy import orm
from sqlalchemy.sql.schema import Column

from membership.database.models import SecurityPrincipal, SecurityPrincipalRole
from membership.errors.exceptions import ResourceMissingError
from membership.schemas.id import IdSecurityPrincipal, IdSecurityPrincipalRole
from membership.util.pagination import (
    DEFAULT_PAGE_LIMIT,
    PaginatedList,
    list_pagable_elements,
)


class SecurityPrincipalNotFoundError(ResourceMissingError):
    def __init__(self):
        super().__init__(object="security_principal")


def from_id(
    session: orm.Session, id: IdSecurityPrincipal
) -> Optional[SecurityPrincipal]:
    raw_id = SecurityPrincipal.parse_id(id)
    return session.query(SecurityPrincipal).get(raw_id)


def from_id_or_raise(
    session: orm.Session, id: IdSecurityPrincipal
) -> SecurityPrincipal:
    security_principal = from_id(session, id)
    if security_principal is None:
        raise SecurityPrincipalNotFoundError()
    return security_principal


def role_from_id(
    session: orm.Session, id: IdSecurityPrincipalRole
) -> Optional[SecurityPrincipalRole]:
    raw_id = SecurityPrincipalRole.parse_id(id)
    return session.query(SecurityPrincipalRole).get(raw_id)


def list_(
    session: orm.Session,
    *,
    limit: Optional[int] = DEFAULT_PAGE_LIMIT,
    starting_after: Optional[str] = None,
    ending_before: Optional[str] = None,
) -> PaginatedList[SecurityPrincipal]:
    limit = limit if limit is not None else DEFAULT_PAGE_LIMIT
    return list_pagable_elements(
        session,
        SecurityPrincipal,
        from_id_fn=from_id,
        limit=limit,
        starting_after=starting_after,
        ending_before=ending_before,
    )


def list_roles(
    session: orm.Session,
    sp: SecurityPrincipal,
    *,
    limit: Optional[int] = DEFAULT_PAGE_LIMIT,
    starting_after: Optional[str] = None,
    ending_before: Optional[str] = None,
) -> PaginatedList[SecurityPrincipalRole]:
    limit = limit if limit is not None else DEFAULT_PAGE_LIMIT
    role_ids_in_sp = set([spra.role_id for spra in sp.role_associations])
    return list_pagable_elements(
        session,
        SecurityPrincipalRole,
        from_id_fn=role_from_id,
        where=[cast(Column, SecurityPrincipalRole._raw_id).in_(list(role_ids_in_sp))],
        limit=limit,
        starting_after=starting_after,
        ending_before=ending_before,
    )
