class IdCommittee(int):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> "IdCommittee":
        if not isinstance(value, int):
            raise TypeError("id must be an int")

        return cls(value)

    def __repr__(self) -> str:
        return f"IdCommittee({super().__repr__()})"


class IdContact(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> "IdContact":
        from membership.database.models import Contact

        if not isinstance(value, str):
            raise TypeError("id must be a string")

        Contact.parse_id(value)
        return cls(value)

    def __repr__(self) -> str:
        return f"IdContact({super().__repr__()})"


class IdCustomField(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> "IdCustomField":
        from membership.database.models import CustomFieldDefinition

        if not isinstance(value, str):
            raise TypeError("id must be a string")

        CustomFieldDefinition.parse_id(value)
        return cls(value)

    def __repr__(self) -> str:
        return f"IdCustomField({super().__repr__()})"


class IdEmailAddress(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> "IdEmailAddress":
        from membership.database.models import EmailAddress

        if not isinstance(value, str):
            raise TypeError("id must be a string")

        EmailAddress.parse_id(value)
        return cls(value)

    def __repr__(self) -> str:
        return f"IdEmailAddress({super().__repr__()})"


class IdImportEvent(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> "IdImportEvent":
        from membership.database.models import ImportEvent

        if not isinstance(value, str):
            raise TypeError("id must be a string")

        ImportEvent.parse_id(value)
        return cls(value)

    def __repr__(self) -> str:
        return f"IdImportEvent({super().__repr__()})"


class IdImportEventReviewAction(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> "IdImportEventReviewAction":
        from membership.database.models import ImportEventReviewAction

        if not isinstance(value, str):
            raise TypeError("id must be a string")

        ImportEventReviewAction.parse_id(value)
        return cls(value)

    def __repr__(self) -> str:
        return f"IdImportEventReviewAction({super().__repr__()})"


class IdMeeting(int):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> "IdMeeting":
        if not isinstance(value, int):
            raise TypeError("id must be an int")

        return cls(value)

    def __repr__(self) -> str:
        return f"IdMeeting({super().__repr__()})"


class IdPhoneNumber(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> "IdPhoneNumber":
        from membership.database.models import PhoneNumber

        if not isinstance(value, str):
            raise TypeError("id must be a string")

        PhoneNumber.parse_id(value)
        return cls(value)

    def __repr__(self) -> str:
        return f"IdPhoneNumber({super().__repr__()})"


class IdPostalAddress(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> "IdPostalAddress":
        from membership.database.models import PostalAddress

        if not isinstance(value, str):
            raise TypeError("id must be a string")

        PostalAddress.parse_id(value)
        return cls(value)

    def __repr__(self) -> str:
        return f"IdPostalAddress({super().__repr__()})"


class IdSecurityPrincipal(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> "IdSecurityPrincipal":
        from membership.database.models import SecurityPrincipal

        if not isinstance(value, str):
            raise TypeError("id must be a string")

        SecurityPrincipal.parse_id(value)
        return cls(value)

    def __repr__(self) -> str:
        return f"IdSecurityPrincipal({super().__repr__()})"


class IdSecurityPrincipalRole(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> "IdSecurityPrincipalRole":
        from membership.database.models import SecurityPrincipalRole

        if not isinstance(value, str):
            raise TypeError("id must be a string")

        SecurityPrincipalRole.parse_id(value)
        return cls(value)

    def __repr__(self) -> str:
        return f"IdSecurityPrincipal({super().__repr__()})"


class IdTag(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> "IdTag":
        from membership.database.models import Tag

        if not isinstance(value, str):
            raise TypeError("id must be a string")

        Tag.parse_id(value)
        return cls(value)

    def __repr__(self) -> str:
        return f"IdTag({super().__repr__()})"


class IdUser(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> "IdUser":
        from membership.database.models import User

        if not isinstance(value, str):
            raise TypeError("id must be a string")

        User.parse_id(value)
        return cls(value)

    def __repr__(self) -> str:
        return f"IdUser({super().__repr__()})"
