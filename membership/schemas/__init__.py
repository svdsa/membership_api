import inspect
from datetime import datetime
from typing import Any, Dict, List, Tuple, Union

Json = Union[str, float, int, List["Json"], Dict[str, "Json"], None]
JsonObj = Dict[str, Json]
JsonList = List[Json]


def format_datetime(dt: datetime) -> int:
    """
    Formats a datetime as milliseconds from the epoch for JavaScript
    """
    return round(dt.timestamp() * 1000)


def get_class_attributes(cls: object) -> List[Tuple[str, Any]]:
    return [
        (k, v)
        for k, v in inspect.getmembers(cls, lambda m: not inspect.isroutine(m))
        if not k.startswith("_")
    ]
