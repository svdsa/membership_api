from pydantic import BaseModel, Extra
from pydantic.generics import GenericModel
from pydantic.networks import AnyUrl
from pydantic.types import constr


class SxBase(BaseModel):
    class Config:
        extra = Extra.forbid


class SxGeneric(GenericModel):
    class Config:
        extra = Extra.forbid


class SxFrozenBase(SxBase):
    class Config:
        frozen = True


class ValidateUrl(BaseModel):
    to_validate: AnyUrl


NonEmptyStrippedStr = constr(min_length=1, strip_whitespace=True)
