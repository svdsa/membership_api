from typing import Any, Dict, List, Optional, cast  # NOQA: W401

import stripe

from membership.database.models import Member
from membership.schemas.rest.generic import error_schema
from membership.services.payments_service import InvalidSubscriptionError

create_dues_subscription_request_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['email_address', 'first_name', 'last_name', 'city', 'zip_code'],
    'properties': {
        'email_address': {
            'type': 'string',
            'pattern': r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)',
        },
        'phone_number': {'type': 'string'},
        'first_name': {'type': 'string'},
        'last_name': {'type': 'string'},
        'pronouns': {
            'type': 'string',
            'pattern': r'^(?:\w+/\w+(?:/\w+){0,3}(?:;?\w+/\w+(?:/\w+){0,3})*)?$',
        },
        'address': {'type': 'string'},
        'city': {'type': 'string'},
        'zip_code': {'type': 'string', 'pattern': r'^\d{5}(?:[-\s]?\d{4})?$'},
        'payment': {
            'type': 'object',
            'required': [
                'total_amount_cents',
                'local_amount_cents',
                'national_amount_cents',
                'recurrence',
                'stripe_payment_method_id',
            ],
            'properties': {
                'total_amount_cents': {'type': 'integer', 'minimum': 100},
                'local_amount_cents': {'type': 'integer', 'minimum': 0},
                'national_amount_cents': {'type': 'integer', 'minimum': 0},
                'recurrence': {
                    'type': 'string',
                    'enum': ['one-time', 'monthly', 'annually'],
                },
                'stripe_payment_method_id': {'type': 'string'},
            },
        },
    },
}

subscription_plan_item_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['amount', 'interval'],
    'properties': {
        'amount': {'type': 'integer', 'minimum': 0},
        'interval': {'type': 'string', 'enum': ['month', 'year']},
    },
}

create_dues_subscription_success_schema = {
    'type': 'object',
    'required': ['email_address'],
    'properties': {
        'email_address': {
            'type': 'string',
            'pattern': r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)',
        },
        'subscription': {
            'type': 'object',
            'required': ['id', 'amount', 'status', 'items', 'cancel_at_period_end'],
            'properties': {
                'id': {'type': 'string'},
                'amount': {'type': 'integer', 'minimum': 100},
                'status': {
                    'type': 'string',
                    'enum': [
                        'incomplete',
                        'incomplete_expired',
                        'trialing',
                        'active',
                        'past_due',
                        'canceled',
                        'unpaid',
                    ],
                },
                'items': {
                    'type': 'object',
                    'required': ['local', 'national'],
                    'properties': {
                        'local': subscription_plan_item_schema,
                        'national': subscription_plan_item_schema,
                    },
                },
                'cancel_at_period_end': {'type': 'boolean'},
            },
        },
    },
}

create_dues_subscription_response_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'oneOf': [create_dues_subscription_success_schema, error_schema],
}


def format_subscription_item(item: stripe.SubscriptionItem) -> Dict[str, Any]:
    return {'amount': item.plan.amount, 'interval': item.plan.interval}


def format_subscription_items(items: List[stripe.SubscriptionItem]) -> Dict[str, Any]:
    local_items = [
        format_subscription_item(item)
        for item in items
        if item.plan.nickname is not None
        and cast(str, item.plan.nickname).startswith('Auto local')
    ]
    national_items = [
        format_subscription_item(item)
        for item in items
        if item.plan.nickname is not None
        and cast(str, item.plan.nickname).startswith('Auto national')
    ]

    if len(local_items) != 1 or len(national_items) != 1:
        raise InvalidSubscriptionError(
            f"Invalid # of plans; found {len(local_items)} local, "
            f"{len(national_items)} national plans"
        )

    return {
        'local': local_items[0],
        'national': national_items[0],
    }


def format_create_member_response(
    member: Member, subscription: Optional[stripe.Subscription] = None
) -> Dict[str, Any]:
    return {
        'email_address': member.email_address,
        **(
            {
                'subscription': {
                    'id': subscription.id,
                    'amount': subscription.latest_invoice.amount_paid,
                    'status': subscription.status,
                    # subscription.items gets clobbered by dict.items, must use []
                    # see https://github.com/stripe/stripe-python/issues/494#issuecomment-440590342
                    'items': format_subscription_items(subscription['items']['data']),
                    'cancel_at_period_end': subscription.cancel_at_period_end,
                }
            }
            if subscription is not None
            else {}
        ),
    }
