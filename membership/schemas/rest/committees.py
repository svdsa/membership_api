from typing import Any, Dict

from membership.database.models import Committee, CommitteeLeadershipRole
from membership.schemas import JsonObj
from membership.schemas.rest.generic import error_schema


def format_committee(committee: Committee, viewable: bool = False) -> Dict[str, Any]:
    # TODO pydantic models
    return {
        "id": committee.id,
        "name": committee.name,
        "email": (
            committee.emails[0].email_address if len(committee.emails) == 1 else None
        ),
        "viewable": viewable,  # XXX doesn't seem to be used anywhere
        "provisional": bool(committee.provisional),
        "inactive": bool(committee.inactive),
    }


def format_committee_details(committee: Committee, viewable: bool = False) -> JsonObj:
    return {
        **(format_committee(committee, viewable)),
        'admins': [{'id': admin.id, 'name': admin.name} for admin in committee.admins],
        'members': [{'id': member.id, 'name': member.name} for member in committee.members],
        'active_members': [
            {'id': member.id, 'name': member.name}
            for member in committee.active_members
        ]
    }


def format_committee_leadership_role(role: CommitteeLeadershipRole) -> JsonObj:
    current_leader = role.current_leader
    return {
        "id": role.id,
        "committee_id": role.committee_id,
        "member_id": current_leader.member_id if current_leader else None,
        "member_name": current_leader.member.name if current_leader else None,
        "role_title": role.leadership_role.title,
        "term_start_date": (
            current_leader.term_start_date.isoformat()
            if current_leader and current_leader.term_start_date
            else None
        ),
        "term_end_date": (
            current_leader.term_end_date.isoformat()
            if current_leader and current_leader.term_end_date else None
        ),
    }


committee_leadership_role_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['id', 'committee_id', 'role_title'],
    'properties': {
        'id': {'type': 'number'},
        'committee_id': {'type': 'number'},
        'role_title': {'type': 'string'},
        'member_id': {'type': ['number', 'null']},
        'member_name': {'type': ['string', 'null']},
        'term_start_date': {'type': ['string', 'null']},
        'term_end_date': {'type': ['string', 'null']},
    }
}

get_leaders_success_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'array',
    'items': committee_leadership_role_schema,
}
get_leaders_response_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'oneOf': [get_leaders_success_schema, error_schema],
}

add_leadership_role_request_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['role_title'],
    'properties': {
        'role_title': {'type': 'string'},
        'term_limit_months': {'type': ['number', 'null']}
    }
}
add_leadership_role_response_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'oneOf': [committee_leadership_role_schema, error_schema],
}

replace_leader_request_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'properties': {
        'member_id': {'type': ['number', 'null']},
        'term_start_date': {'type': ['string', 'null']},
        'term_end_date': {'type': ['string', 'null']},
    }
}
replace_leader_response_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'oneOf': [committee_leadership_role_schema, error_schema],
}
