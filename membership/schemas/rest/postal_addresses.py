import re
from datetime import datetime
from typing import List, Literal, Optional

from membership.schemas.id import IdContact, IdPostalAddress
from membership.schemas.rest.pagination import SxList, SxListQuery
from membership.schemas.rest.pydantic import NonEmptyStrippedStr, SxBase

zipcode_regex = re.compile(r"^(\d{5})(?:[-\s]?(\d{4}))?$")


class ZipcodeStr(str):
    @classmethod
    def __get_validators__(cls):
        # one or more validators may be yielded which will be called in the
        # order to validate the input, each validator will receive as an input
        # the value returned from the previous validator
        yield cls.validate

    @classmethod
    def __modify_schema__(cls, field_schema):
        # __modify_schema__ should mutate the dict it receives in place,
        # the returned value will be ignored
        field_schema.update(
            # simplified regex here for brevity, see the wikipedia link above
            pattern=r"^\d{5}(?:[-\s]\d{4})?$",
            # some example postcodes
            examples=["94103", "94103-1234", "123456789"],
        )

    @classmethod
    def validate(cls, v):
        if not isinstance(v, str):
            raise TypeError("string required")
        m = zipcode_regex.fullmatch(v.upper())
        if not m:
            raise ValueError("invalid zipcode format")
        # you could also return a string here which would mean model.post_code
        # would be a string, pydantic won't care but you could end up with some
        # confusion since the value's type won't match the type annotation
        # exactly
        if len(m.groups()) > 2:
            return cls(f"{m.group(1)}-{m.group(2)}")
        else:
            return cls(f"{m.group(1)}")

    def __repr__(self):
        return f"ZipCodeStr({super().__repr__()})"


class SxPostalAddressBase(SxBase):
    line1: Optional[NonEmptyStrippedStr]
    line2: Optional[NonEmptyStrippedStr]
    city: Optional[NonEmptyStrippedStr]
    state: Optional[NonEmptyStrippedStr]
    zipcode: ZipcodeStr


class SxPostalAddress(SxPostalAddressBase):
    object: Literal["postal_address"] = "postal_address"
    id: IdPostalAddress

    consent_to_mail: bool

    date_created: datetime
    source_created: Optional[NonEmptyStrippedStr]
    date_archived: Optional[datetime]
    reason_archived: Optional[NonEmptyStrippedStr]

    contact: IdContact


class SxPostalAddressCreateRequest(SxPostalAddressBase):
    date_created: Optional[datetime]
    source_created: Optional[NonEmptyStrippedStr]

    consent_to_mail: Optional[bool]


class SxPostalAddressShort(SxBase):
    street_address: Optional[str]
    city: Optional[str]
    state: Optional[str]
    zipcode: ZipcodeStr
    consent_to_mail: bool
    active: bool


class SxListPostalAddressesQuery(SxListQuery):
    show_archived: Optional[bool]


class SxListPostalAddressResponse(SxList):
    data: List[SxPostalAddress]


class SxPostalAddressArchiveRequest(SxBase):
    reason_archived: Optional[NonEmptyStrippedStr]


class SxPostalAddressUpdateRequest(SxBase):
    consent_to_mail: Optional[bool]
