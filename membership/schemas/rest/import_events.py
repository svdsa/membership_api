from datetime import datetime
from enum import Enum
from typing import Any, Dict, Generic, List, Literal, Optional, Set, TypeVar

from pydantic import root_validator
from pydantic.config import Extra
from pydantic.networks import AnyHttpUrl

from membership.schemas.id import (
    IdContact,
    IdCustomField,
    IdImportEvent,
    IdImportEventReviewAction,
    IdSecurityPrincipal,
    IdTag,
)
from membership.schemas.rest.contacts import SxContactCreateRequest
from membership.schemas.rest.pagination import SxList, SxListQuery
from membership.schemas.rest.pydantic import NonEmptyStrippedStr, SxBase, SxGeneric


class ImportEventType(str, Enum):
    zoom_webhook = "zoom_webhook"
    newsletter_signup = "newsletter_signup"
    dsausa_roster = "dsausa_roster"
    website_signup = "website_signup"
    csv_import = "csv_import"


class ImportEventReviewActionType(str, Enum):
    create_new_contact = "create_new_contact"
    ignore_once = "ignore_once"
    match_manual = "match_manual"
    match_auto = "match_auto"
    proposed_match = "proposed_match"
    proposed_create = "proposed_create"

    # TODO: requires ignore table to function properly
    # IGNORE_FOREVER = "IGNORE_FOREVER"


class MissingReviewActionsError(ValueError):
    pass


class MissingMergeDirectivesError(ValueError):
    pass


class InvalidMergeDirectiveError(ValueError):
    pass


class SxImportEventInfo(SxBase):
    location: Optional[str]
    date_started: Optional[datetime]
    date_ended: Optional[datetime]
    organizer: Optional[str]
    event_id: Optional[str]  # TODO use IdEvent when that gets created

    class Config:
        extra = Extra.allow


class SxImportEventCreateRequest(SxBase):
    title: NonEmptyStrippedStr
    event_type: ImportEventType
    event_info: Optional[SxImportEventInfo]
    date_created: Optional[datetime]
    tags: Optional[List[IdTag]]
    custom_fields: Optional[List[IdCustomField]]
    participants: List[SxContactCreateRequest]


MERGE_ACTION_TYPES = set(["match_auto", "match_manual", "proposed_match"])


class ReviewMergeActionType(str, Enum):
    take_incoming = "take_incoming"
    keep_existing = "keep_existing"
    manual_edit = "manual_edit"


class ReviewMergeActionListType(str, Enum):
    take_incoming = "take_incoming"
    keep_existing = "keep_existing"
    add_new = "add_new"

    @staticmethod
    def list_types() -> Set[str]:
        return set([ReviewMergeActionListType.add_new.name])


MERGE_LIST_ATTRIBUTES = set(
    ["email_addresses", "phone_numbers", "mailing_addresses", "tags", "custom_fields"]
)


TEditValue = TypeVar("TEditValue")


class SxImportReviewMergeAction(SxGeneric, Generic[TEditValue]):
    attribute: str
    merge_action: ReviewMergeActionType
    edit_value: Optional[TEditValue] = None

    @root_validator
    def check_edit_value(cls, action: Dict[str, Any]):
        if (
            action.get("merge_action") == "manual_edit"
            and action.get("edit_value") is None
        ):
            raise ValueError(
                "if merge_action is 'manual_edit', 'edit_value' must be present"
            )
        return action


class SxImportReviewMergeCollectionAction(SxBase):
    attribute: str
    merge_action: ReviewMergeActionListType


class SxImportReviewMerge(SxBase):
    full_name: Optional[SxImportReviewMergeAction[str]]
    display_name: Optional[SxImportReviewMergeAction[str]]
    pronouns: Optional[SxImportReviewMergeAction[str]]
    biography: Optional[SxImportReviewMergeAction[str]]
    profile_pic: Optional[SxImportReviewMergeAction[AnyHttpUrl]]
    admin_notes: Optional[SxImportReviewMergeAction[str]]

    email_addresses: Optional[SxImportReviewMergeCollectionAction]
    phone_numbers: Optional[SxImportReviewMergeCollectionAction]
    mailing_addresses: Optional[SxImportReviewMergeCollectionAction]
    tags: Optional[SxImportReviewMergeCollectionAction]
    custom_fields: Optional[SxImportReviewMergeCollectionAction]

    @root_validator
    def check_attribute_names(cls, merge: Dict[str, Any]):
        for key, value in merge.items():
            if value is None:
                continue

            if not hasattr(value, "attribute"):
                raise ValueError("attribute value is not present")

            if key != value.attribute:
                raise ValueError("attribute value doesn't match merge directive key")

        return merge

    @root_validator
    def at_least_one_modifier(cls, merge: Dict[str, Any]):
        merge_actions = [
            directive.merge_action
            for directive in merge.values()
            if directive is not None
        ]
        if all(
            [action == ReviewMergeActionType.keep_existing for action in merge_actions]
        ):
            raise ValueError(
                "this merge will not do anything - "
                "choose some fields to merge or ignore this contact instead"
            )

        return merge


class ImportEventInferConfidence(str, Enum):
    strong = "strong"
    fuzzy = "fuzzy"


class SxImportEventMatchJustification(SxBase):
    confidence: ImportEventInferConfidence
    strong_matches: List[str]
    fuzzy_matches: List[str]


class SxImportEventReviewAction(SxBase):
    object: Literal["import_event_review_action"] = "import_event_review_action"
    id: IdImportEventReviewAction
    participant: SxContactCreateRequest
    import_event_id: IdImportEvent
    action: ImportEventReviewActionType
    merge_into_contact_id: Optional[IdContact]
    merge: Optional[SxImportReviewMerge]
    match_justification: Optional[SxImportEventMatchJustification]


class SxListImportEventReviewActions(SxList):
    data: List[SxImportEventReviewAction]


class SxListImportEventReviewActionsQuery(SxListQuery):
    # intentionally blank
    pass


class SxImportEventReviewActionUpdate(SxBase):
    id: IdImportEventReviewAction
    action: ImportEventReviewActionType
    merge_into_contact_id: Optional[IdContact]
    merge: Optional[SxImportReviewMerge]


class SxImportEventReviewActionsUpdateRequest(SxBase):
    actions: List[SxImportEventReviewActionUpdate]


class SxImportEvent(SxBase):
    object: Literal["import_event"] = "import_event"
    id: IdImportEvent
    creator: IdSecurityPrincipal
    date_created: datetime
    reviewer: Optional[IdSecurityPrincipal]
    date_reviewed: Optional[datetime]
    title: str
    event_type: ImportEventType
    event_info: Optional[SxImportEventInfo]
    review_actions: SxListImportEventReviewActions
    review_action_counts: Dict[str, int]


class SxListImportEventsQuery(SxListQuery):
    pass


class SxListImportEvents(SxList):
    data: List[SxImportEvent]
