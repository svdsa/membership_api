import jsonschema
from abc import ABC, abstractmethod
from dateutil import parser
from typing import Any, Callable, Dict, Generic, Union, TypeVar, Type

from membership.schemas import JsonObj

T = TypeVar('T')


class RestFormatter(Generic[T], ABC):

    def __init__(
            self,
            extract: Callable[[JsonObj], T],
            is_defined_at: Callable[[JsonObj], bool]):
        self.is_defined_at = is_defined_at
        self.extract = extract

    def __call__(self, value: JsonObj) -> T:
        return self.extract(value)

    @classmethod
    def excepting(
            cls,
            extract: Union[Callable[[JsonObj], T], Type[T]],
            nullable: bool = False) -> 'RestFormatter[T]':
        def check_if_raises(value: JsonObj) -> bool:
            if value is None and not nullable:
                return False
            try:
                extract(value)
                return True
            except Exception:
                return False
        return RestFormatter(extract, check_if_raises)


RestFormatter.number = RestFormatter.excepting(float)
RestFormatter.integer = RestFormatter.excepting(int)
RestFormatter.string = RestFormatter.excepting(str)
RestFormatter.datetime = RestFormatter.excepting(parser.parse)


class RestModel(Generic[T], ABC):
    model: Callable[[], T]
    schema: JsonObj
    type_formats: Dict[str, RestFormatter[Any]] = {
        'string': RestFormatter.string,
        'integer': RestFormatter.integer,
        'number': RestFormatter.number,
        'datetime': RestFormatter.datetime,
    }

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls.schema, dict):
            raise TypeError(f"{cls.__name__}.schema is not a dict")
        if not callable(cls.model):
            raise TypeError(f"{cls.__name__}.model is not callable")

    @classmethod
    def update(cls, o: T, json: JsonObj, validate: bool = True):
        if validate:
            jsonschema.validate(json, cls.schema)
        props = cls.schema.get('properties', {})
        required = set(cls.schema.get('required', []))
        for pkey, pdef in props.items():
            ptype = pdef.get('type')
            if ptype is None:
                raise ValueError(
                    f"Invalid {cls.__name__}.schema: '{pkey}' property must have a 'type' field"
                )
            elif isinstance(ptype, str):
                union_type = tuple([ptype])
            else:
                union_type = tuple(ptype)

            value = json.get(pkey, None)
            extracted_values = []
            for type_name in union_type:
                if type_name in cls.type_formats:
                    formatter = cls.type_formats[type_name]
                    if formatter.is_defined_at(value):
                        extracted_values.append(formatter.extract(value))
            if extracted_values:
                # TODO: What
                extracted = extracted_values[0]
                if extracted is not None or pkey not in required:
                    setattr(o, pkey, extracted)

    @classmethod
    def create(cls, json: JsonObj) -> T:
        o = cls.model()
        cls.update(o, json)
        return o

    @classmethod
    @abstractmethod
    def format(cls, o: T) -> JsonObj:
        # TODO: Design a generic solution for updating based on a schema
        pass
