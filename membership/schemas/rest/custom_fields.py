from datetime import datetime
from enum import Enum
from typing import Any, List, Literal, Optional

from pydantic import validator

from membership.schemas.id import IdCustomField
from membership.schemas.rest.contacts import IdContact
from membership.schemas.rest.custom_field_options import SxCustomFieldOptionsUnion
from membership.schemas.rest.pagination import SxList, SxListQuery
from membership.schemas.rest.pydantic import NonEmptyStrippedStr, SxBase


class CustomFieldType(str, Enum):
    boolean = "boolean"
    choice = "choice"
    date = "date"
    number = "number"
    range = "range"
    text = "text"
    textarea = "textarea"
    url = "url"
    array = "array"
    object = "object"


class SxCustomFieldDefinition(SxBase):
    object: Literal["custom_field"] = "custom_field"
    id: str

    date_created: datetime

    name: str
    description: Optional[str]
    field_type: CustomFieldType
    options: Optional[SxCustomFieldOptionsUnion]
    contacts: List[str]
    tags: List[str]

    class Config:
        json_encoders = {CustomFieldType: lambda v: v.name}


class SxCustomFieldAssociation(SxBase):
    object: Literal["custom_field_value"] = "custom_field_value"

    date_created: datetime

    custom_field: IdCustomField
    contact: IdContact

    value: Any


class SxCustomFieldAssociationCreateRequest(SxBase):
    custom_field: IdCustomField
    value: Any


class SxCustomFieldAssociationUpdateRequest(SxBase):
    value: Any


class SxListCustomFieldsQuery(SxListQuery):
    pass


class SxListCustomFieldsResponse(SxList):
    data: List[SxCustomFieldDefinition]

    class Config:
        json_encoders = {CustomFieldType: lambda v: v.name}


class SxListCustomFieldAssociationsResponse(SxList):
    data: List[SxCustomFieldAssociation]


class SxCustomFieldCreateRequest(SxBase):
    name: NonEmptyStrippedStr
    description: Optional[str]
    field_type: CustomFieldType
    options: Optional[SxCustomFieldOptionsUnion]
    date_created: Optional[datetime]


class SxCustomFieldUpdateRequest(SxBase):
    name: Optional[NonEmptyStrippedStr]
    description: Optional[str]
    options: Optional[SxCustomFieldOptionsUnion]

    # HACK there currently isn't a good way to specify an
    # optional non-nullable field in Pydantic with only type-defs
    # so we have to specify a validator
    # see https://github.com/samuelcolvin/pydantic/issues/1223
    @validator("name")
    def name_is_not_none(cls, v):
        assert v is not None, "name must not be none if provided"
        return v
