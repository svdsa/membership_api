from datetime import datetime
from typing import List, Literal, Optional, Union

from phonenumbers.phonenumberutil import NumberParseException
from pydantic import root_validator

from membership.schemas.id import IdContact, IdPhoneNumber
from membership.schemas.rest.pagination import SxList, SxListQuery
from membership.schemas.rest.pydantic import NonEmptyStrippedStr, SxBase


class PhoneStr(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value) -> "PhoneStr":
        from membership.database.models import PhoneNumber

        if not isinstance(value, str):
            raise TypeError("id must be a string")

        try:
            formatted = PhoneNumber.format_number(value)
            return cls(formatted)
        except NumberParseException as e:
            # convert to ValueError for Pydantic
            raise ValueError(e)

    def __repr__(self) -> str:
        return f"PhoneStr({super().__repr__()})"


class SxPhoneNumber(SxBase):
    object: Literal["phone_number"] = "phone_number"
    id: IdPhoneNumber

    date_created: datetime
    date_confirmed: Optional[datetime]
    date_archived: Optional[datetime]
    reason_archived: Optional[NonEmptyStrippedStr]

    value: PhoneStr
    name: Optional[NonEmptyStrippedStr]
    primary: bool

    contact: Optional[IdContact]

    sms_capable: bool
    consent_to_call: bool
    consent_to_text: bool
    consent_to_share: bool


class SxPhoneNumberCreateRequest(SxBase):
    date_created: Optional[datetime]
    value: PhoneStr
    name: Optional[NonEmptyStrippedStr]
    primary: Optional[bool]
    sms_capable: Optional[bool]
    consent_to_call: Optional[bool]
    consent_to_text: Optional[bool]
    consent_to_share: Optional[bool]


CreatePhoneNumberConvenience = Union[List[PhoneStr], List[SxPhoneNumberCreateRequest]]


class SxPhoneNumberUpdateRequest(SxBase):
    name: Optional[NonEmptyStrippedStr]
    primary: Optional[bool]
    sms_capable: Optional[bool]
    consent_to_call: Optional[bool]
    consent_to_text: Optional[bool]
    consent_to_share: Optional[bool]

    # Provide a friendly error message about how to update
    # the phone number
    @root_validator()
    def update_forbidden(cls, values):
        assert "value" not in values, (
            "Phone numbers cannot be updated once created. "
            "To update an phone number, archive the old phone number and "
            "create a new one."
        )
        return values


class SxListPhoneNumbersQuery(SxListQuery):
    show_archived: Optional[bool]


class SxListPhoneNumbersResponse(SxList):
    data: List[SxPhoneNumber]


class SxPhoneNumberArchiveRequest(SxBase):
    reason_archived: Optional[NonEmptyStrippedStr]
