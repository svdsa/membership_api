from typing import List, Literal

from membership.auth.needs import NdBase
from membership.schemas.id import IdContact, IdSecurityPrincipal, IdUser
from membership.schemas.rest.pydantic import SxBase


class SxUser(SxBase):
    object: Literal["user"] = "user"
    id: IdUser

    contact: IdContact
    security_principal: IdSecurityPrincipal
    permissions: List[NdBase]
