from membership.schemas.rest.pagination import SxListQuery


class SxSearchQuery(SxListQuery):
    q: str
    # TODO
    # sort: str
    # order: Union[Literal["asc"], Literal["desc"]]


class SxSearchTagsQuery(SxSearchQuery):
    pass
