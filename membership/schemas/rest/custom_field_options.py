from typing import List, Optional, Union

from pydantic import validator

from membership.schemas.rest.pydantic import NonEmptyStrippedStr, SxBase


class SxCustomFieldChoiceElements(SxBase):
    name: Optional[NonEmptyStrippedStr] = None
    value: NonEmptyStrippedStr

    @classmethod
    def __get_validators__(cls):
        # one or more validators may be yielded which will be called in the
        # order to validate the input, each validator will receive as an input
        # the value returned from the previous validator
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if isinstance(v, str):
            return cls(value=v)
        elif isinstance(v, dict):
            try:
                return cls(name=v.get("name"), value=v["value"])
            except KeyError:
                raise ValueError("must be string or { name?: str, value: str }")
        else:
            raise ValueError("unknown type for custom field choice options")


class SxCustomFieldChoiceOptions(SxBase):
    choices: List[SxCustomFieldChoiceElements]

    @validator("choices")
    def choices_not_empty(cls, v):
        assert len(v) > 0, "Must have at least one choice."
        return v


class SxCustomFieldRangeOptions(SxBase):
    start: int
    end: int
    step: Optional[int] = 1


SxCustomFieldOptionsUnion = Union[SxCustomFieldChoiceOptions, SxCustomFieldRangeOptions]
