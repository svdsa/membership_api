from datetime import datetime
from typing import List, Literal, Optional, Union

from pydantic import root_validator
from pydantic.networks import EmailStr

from membership.schemas.id import IdContact, IdEmailAddress
from membership.schemas.rest.pagination import SxList, SxListQuery
from membership.schemas.rest.pydantic import NonEmptyStrippedStr, SxBase


class SxEmailAddressCreateRequest(SxBase):
    date_created: Optional[datetime]
    value: EmailStr
    primary: Optional[bool]
    consent_to_email: Optional[bool]
    consent_to_share: Optional[bool]


CreateEmailAddressConvenience = Union[List[EmailStr], List[SxEmailAddressCreateRequest]]


class SxEmailAddress(SxBase):
    object: Literal["email_address"] = "email_address"
    id: IdEmailAddress

    date_created: datetime
    date_confirmed: Optional[datetime]
    date_archived: Optional[datetime]
    reason_archived: Optional[str]

    value: EmailStr
    primary: bool
    consent_to_email: bool
    consent_to_share: bool
    contact: IdContact


class SxListEmailAddressesQuery(SxListQuery):
    show_archived: Optional[bool]


class SxListEmailAddressResponse(SxList):
    data: List[SxEmailAddress]


class SxEmailAddressUpdateRequest(SxBase):
    primary: Optional[bool]
    consent_to_email: Optional[bool]
    consent_to_share: Optional[bool]

    # Provide a friendly error message about how to update
    # the email address
    @root_validator()
    def update_forbidden(cls, values):
        assert "value" not in values, (
            "Email addresses cannot be updated once created. "
            "To update an email address, archive the old email address "
            "and create a new one."
        )
        return values


class SxEmailAddressArchiveRequest(SxBase):
    reason_archived: Optional[NonEmptyStrippedStr]
