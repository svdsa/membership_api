from datetime import datetime
from typing import List, Literal, Optional

from membership.schemas.id import IdContact, IdCustomField, IdTag
from membership.schemas.rest.pagination import SxList, SxListQuery
from membership.schemas.rest.pydantic import NonEmptyStrippedStr, SxBase


class SxTag(SxBase):
    object: Literal["tag"] = "tag"
    id: IdTag

    date_created: datetime

    name: NonEmptyStrippedStr
    members: List[str]
    contacts: List[IdContact]
    custom_fields: List[IdCustomField]


class SxListTagsResponse(SxList):
    data: List[SxTag]


class SxTagUpdateRequest(SxBase):
    name: Optional[NonEmptyStrippedStr]


class SxTagAttachContactRequest(SxBase):
    contact: IdContact


class SxTagAttachCustomFieldRequest(SxBase):
    custom_field: IdCustomField


class SxListTagsQuery(SxListQuery):
    pass


class SxTagCreateRequest(SxBase):
    name: NonEmptyStrippedStr
    contacts: Optional[List[IdContact]]
    custom_fields: Optional[List[IdCustomField]]


class SxContactTagAssociationRequest(SxBase):
    tag: IdTag
