
natl_csv_file_upload_request_schema = {
    '$schema': 'http://json-schema.org/draft-07/schema#',
    'type': 'object',
    'required': ['mail-attachment', 'mail-attachment-filename'],
    'properties': {
        "mail-attachment": {"type": "string"},
        "mail-attachment-filename": {"type": "string"}
    }
}
