from datetime import datetime
from typing import Any, Dict, List, Literal, Optional

from pydantic import validator
from pydantic.networks import AnyHttpUrl

from membership.schemas.id import IdContact, IdCustomField, IdTag
from membership.schemas.rest.email_addresses import (
    CreateEmailAddressConvenience,
    SxListEmailAddressResponse,
)
from membership.schemas.rest.pagination import SxList, SxListQuery
from membership.schemas.rest.phone_numbers import (
    CreatePhoneNumberConvenience,
    SxListPhoneNumbersResponse,
)
from membership.schemas.rest.postal_addresses import (
    SxListPostalAddressResponse,
    SxPostalAddressCreateRequest,
)
from membership.schemas.rest.pydantic import NonEmptyStrippedStr, SxBase
from membership.schemas.rest.search import SxSearchQuery


class SxContactBase(SxBase):
    full_name: NonEmptyStrippedStr
    display_name: Optional[NonEmptyStrippedStr]
    pronouns: Optional[NonEmptyStrippedStr]
    biography: Optional[NonEmptyStrippedStr]
    profile_pic: Optional[AnyHttpUrl]
    admin_notes: Optional[NonEmptyStrippedStr]


class SxContact(SxContactBase):
    object: Literal["contact"] = "contact"
    id: IdContact

    date_created: datetime
    source_created: Optional[NonEmptyStrippedStr]
    date_archived: Optional[datetime]
    reason_archived: Optional[NonEmptyStrippedStr]

    email_addresses: SxListEmailAddressResponse
    phone_numbers: SxListPhoneNumbersResponse
    mailing_addresses: SxListPostalAddressResponse

    city: Optional[NonEmptyStrippedStr]
    zipcode: Optional[NonEmptyStrippedStr]

    tags: List[IdTag]
    custom_fields: Dict[IdCustomField, Any]


class SxContactCreateRequest(SxContactBase):
    # allow for date_created because we may be importing from external data sources
    date_created: Optional[datetime]
    source_created: Optional[NonEmptyStrippedStr]

    email_addresses: Optional[CreateEmailAddressConvenience]
    phone_numbers: Optional[CreatePhoneNumberConvenience]
    mailing_addresses: Optional[List[SxPostalAddressCreateRequest]]

    tags: Optional[List[IdTag]]
    custom_fields: Optional[Dict[IdCustomField, Any]]


class SxListContactCreateRequest(SxList):
    data: List[SxContactCreateRequest]


class SxContactUpdateRequest(SxBase):
    full_name: Optional[NonEmptyStrippedStr]
    display_name: Optional[NonEmptyStrippedStr]
    pronouns: Optional[NonEmptyStrippedStr]
    biography: Optional[NonEmptyStrippedStr]
    profile_pic: Optional[AnyHttpUrl]
    admin_notes: Optional[NonEmptyStrippedStr]

    source_created: Optional[NonEmptyStrippedStr]
    archived: Optional[bool]
    reason_archived: Optional[NonEmptyStrippedStr]

    # HACK there currently isn't a good way to specify an
    # optional non-nullable field in Pydantic with only type-defs
    # so we have to specify a validator
    # see https://github.com/samuelcolvin/pydantic/issues/1223
    @validator("full_name")
    def full_name_is_not_none(cls, v):
        assert v is not None, "full name must not be none if provided"
        return v


class SxListContactsQuery(SxListQuery):
    pass


class SxListContactsResponse(SxList):
    data: List[SxContact]


class SxContactArchiveRequest(SxBase):
    reason_archived: Optional[NonEmptyStrippedStr]


class SxSearchContactsQuery(SxSearchQuery):
    pass
