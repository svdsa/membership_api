from typing import Any, List, Literal, Optional

from membership.schemas.rest.pydantic import SxBase


class SxList(SxBase):
    object: Literal["list"] = "list"
    url: str
    has_more: bool
    data: List[Any]


class SxListQuery(SxBase):
    limit: Optional[int]
    starting_after: Optional[str]
    ending_before: Optional[str]
