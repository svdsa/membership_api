from typing import Any, Dict, List, cast

from flask.helpers import url_for

from membership.database.models import (
    Contact,
    ContactCustomFieldAssociation,
    CustomFieldDefinition,
    EmailAddress,
    PostalAddress,
    Tag,
    TagAssociation,
)
from membership.schemas.formatters.email_addresses import (
    format_email_address_list,
    sort_email_addresses,
)
from membership.schemas.formatters.phone_numbers import (
    format_phone_number_list,
    sort_phone_numbers,
)
from membership.schemas.formatters.postal_addresses import (
    format_postal_address_list,
    sort_postal_addresses,
)
from membership.schemas.rest.contacts import (
    SxContact,
    SxContactCreateRequest,
    SxListContactCreateRequest,
    SxListContactsResponse,
)
from membership.schemas.rest.email_addresses import SxEmailAddressCreateRequest
from membership.util.pagination import PaginatedList


def format_contact_email_address(address: EmailAddress) -> SxEmailAddressCreateRequest:
    return SxEmailAddressCreateRequest(value=address.value, primary=address.primary)


def format_contact_email_addresses(
    addresses: List[EmailAddress],
) -> List[SxEmailAddressCreateRequest]:
    return [format_contact_email_address(a) for a in addresses]


def format_contact_mailing_addresses_id(addresses: List[PostalAddress]) -> List[str]:
    return [addr.id for addr in addresses]


def format_contact_tag(assoc: TagAssociation) -> str:
    tag = cast(Tag, assoc.tag)
    return tag.id


def format_contact_tags(contact: Contact) -> List[str]:
    return [format_contact_tag(assoc) for assoc in contact.tag_associations]


def format_contact_field_key(assoc: ContactCustomFieldAssociation) -> str:
    field_def = cast(CustomFieldDefinition, assoc.custom_field)
    return field_def.id


def format_contact_fields(contact: Contact) -> Dict[str, Any]:
    if contact.custom_field_values is None or len(contact.custom_field_values) == 0:
        return {}

    return {
        format_contact_field_key(assoc): cast(
            ContactCustomFieldAssociation, assoc
        ).value
        for assoc in contact.custom_field_values
    }


def format_create_contact_request_list(
    contacts: PaginatedList[SxContactCreateRequest], route: str
) -> SxListContactCreateRequest:
    return SxListContactCreateRequest(
        url=route, has_more=contacts.has_more, data=contacts.list
    )


def format_contact(contact: Contact) -> SxContact:
    return SxContact(
        id=contact.id,
        date_created=contact.date_created,
        source_created=contact.source_created,
        date_archived=contact.date_archived,
        reason_archived=contact.reason_archived,
        full_name=contact.full_name,
        display_name=contact.display_name,
        pronouns=contact.pronouns,
        biography=contact.biography,
        profile_pic=contact.profile_pic,
        admin_notes=contact.admin_notes,
        email_addresses=format_email_address_list(
            PaginatedList.new(
                [
                    email
                    for email in sort_email_addresses(contact.email_addresses)
                    if email.date_archived is None
                ]
            ),
            url_for("contacts_api.list_email_addresses_for_contact", id=contact.id),
        ),
        phone_numbers=format_phone_number_list(
            PaginatedList.new(
                [
                    pn
                    for pn in sort_phone_numbers(contact.phone_numbers)
                    if pn.date_archived is None
                ]
            ),
            url_for("contacts_api.list_phone_numbers_for_contact", id=contact.id),
        ),
        mailing_addresses=format_postal_address_list(
            PaginatedList.new(
                [
                    addr
                    for addr in sort_postal_addresses(contact.mailing_addresses)
                    if addr.date_archived is None
                ]
            ),
            url_for("contacts_api.list_mailing_addresses_for_contact", id=contact.id),
        ),
        city=contact.city,
        zipcode=contact.zipcode,
        tags=format_contact_tags(contact),
        custom_fields=format_contact_fields(contact),
    )


def format_contacts_list(
    contacts: PaginatedList[Contact], route: str
) -> SxListContactsResponse:
    return SxListContactsResponse(
        url=route,
        has_more=contacts.has_more,
        data=[format_contact(c) for c in contacts.list],
    )
