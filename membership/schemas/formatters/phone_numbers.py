from typing import List, cast

from membership.database.models import PhoneNumber
from membership.schemas.rest.phone_numbers import (
    CreatePhoneNumberConvenience,
    SxListPhoneNumbersResponse,
    SxPhoneNumber,
    SxPhoneNumberCreateRequest,
)
from membership.util.pagination import PaginatedList


def format_phone_number(pn: PhoneNumber) -> SxPhoneNumber:
    return SxPhoneNumber(
        id=pn.id,
        date_created=pn.date_created,
        date_confirmed=pn.date_confirmed,
        date_archived=pn.date_archived,
        reason_archived=pn.reason_archived,
        value=pn.value,
        name=pn.name,
        primary=pn.primary,
        contact=pn.contact.id,
        sms_capable=pn.sms_capable,
        consent_to_call=pn.consent_to_call,
        consent_to_text=pn.consent_to_text,
        consent_to_share=pn.consent_to_share,
    )


def format_phone_number_list(
    numbers: PaginatedList[PhoneNumber], route: str
) -> SxListPhoneNumbersResponse:
    return SxListPhoneNumbersResponse(
        url=route,
        has_more=numbers.has_more,
        data=[format_phone_number(pn) for pn in numbers.list],
    )


def normalize_phone_numbers(
    pns: CreatePhoneNumberConvenience,
) -> List[SxPhoneNumberCreateRequest]:
    if all(isinstance(pn, str) for pn in pns):
        return [SxPhoneNumberCreateRequest(value=pn) for pn in pns]
    else:
        return cast(List[SxPhoneNumberCreateRequest], pns)


def sort_phone_numbers(phone_numbers: List[PhoneNumber]) -> List[PhoneNumber]:
    sorting = phone_numbers

    # sort from least significant to most significant
    sorting = sorted(sorting, key=lambda addr: addr._raw_id, reverse=True)
    sorting = sorted(sorting, key=lambda addr: addr.value, reverse=True)
    sorting = sorted(sorting, key=lambda addr: addr.date_created, reverse=True)

    return sorting
