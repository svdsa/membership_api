from itertools import groupby
from typing import Dict, List, cast

from flask.helpers import url_for

from membership.database.models import ImportEvent, ImportEventReviewAction
from membership.schemas.rest.contacts import SxContactCreateRequest
from membership.schemas.rest.import_events import (
    SxImportEvent,
    SxImportEventReviewAction,
    SxListImportEventReviewActions,
    SxListImportEvents,
)
from membership.util.pagination import PaginatedList


def format_review_action(
    action: ImportEventReviewAction,
) -> SxImportEventReviewAction:
    return SxImportEventReviewAction(
        id=action.id,
        participant=action.participant,
        import_event_id=action.import_event.id,
        action=action.action.name,
        merge_into_contact_id=action.merge_into_contact.id
        if action.merge_into_contact is not None
        else None,
        match_justification=action.match_justification,
        merge=action.merge,
    )


def summarize_review_actions(
    review_actions: List[ImportEventReviewAction],
) -> Dict[str, int]:
    def keyfn(ra: ImportEventReviewAction) -> str:
        return cast(str, ra.action.name)

    actions = groupby(sorted(review_actions, key=keyfn), key=keyfn)
    return {action_type: len(list(group)) for action_type, group in actions}


def format_import_event_review_actions_list(
    actions: PaginatedList[ImportEventReviewAction], route: str
) -> SxListImportEventReviewActions:
    return SxListImportEventReviewActions(
        url=route,
        has_more=actions.has_more,
        data=[format_review_action(ra) for ra in actions.list],
    )


def format_import_event(event: ImportEvent) -> SxImportEvent:
    return SxImportEvent(
        id=event.id,
        creator=event.creator.id,
        date_created=event.date_created,
        reviewer=event.reviewer.id if event.reviewer is not None else None,
        date_reviewed=event.date_reviewed,
        title=event.title,
        event_type=event.event_type,
        event_info=event.event_info,
        review_actions=format_import_event_review_actions_list(
            PaginatedList.new([a for a in event.review_actions]),
            url_for("imports_api.list_import_event_review_actions", event_id=event.id),
        ),
        review_action_counts=summarize_review_actions(event.review_actions),
    )


def format_import_events_list(
    import_events: PaginatedList[ImportEvent], route: str
) -> SxListImportEvents:
    return SxListImportEvents(
        url=route,
        has_more=import_events.has_more,
        data=[format_import_event(e) for e in import_events.list],
    )


def contact_import_create_fn(
    elem: SxContactCreateRequest, *, tags: List[str], custom_fields: List[str]
) -> SxContactCreateRequest:
    return elem
