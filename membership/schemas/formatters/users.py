from typing import Set

from membership.auth.needs import NdBase
from membership.database.models import Contact, SecurityPrincipal, User
from membership.schemas.rest.users import SxUser


def format_user(user: User, *, needs: Set[NdBase]) -> SxUser:
    return SxUser(
        id=user.id,
        contact=Contact._as_id(user.contact_id),
        security_principal=SecurityPrincipal._as_id(user.principal_id),
        permissions=sorted(list(needs), key=lambda n: n.slug),
    )
