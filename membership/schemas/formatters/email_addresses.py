from typing import List, cast

from membership.database.models import EmailAddress
from membership.schemas.rest.email_addresses import (
    CreateEmailAddressConvenience,
    SxEmailAddress,
    SxEmailAddressCreateRequest,
    SxListEmailAddressResponse,
)
from membership.util.pagination import PaginatedList


def format_email_address(addr: EmailAddress) -> SxEmailAddress:
    return SxEmailAddress(
        id=addr.id,
        date_created=addr.date_created,
        date_confirmed=addr.date_confirmed,
        date_archived=addr.date_archived,
        reason_archived=addr.reason_archived,
        value=addr.value,
        primary=addr.primary,
        consent_to_email=addr.consent_to_email,
        consent_to_share=addr.consent_to_share,
        contact=addr.contact.id,
    )


def format_email_address_list(
    addresses: PaginatedList[EmailAddress], route: str
) -> SxListEmailAddressResponse:
    return SxListEmailAddressResponse(
        url=route,
        has_more=addresses.has_more,
        data=[format_email_address(pn) for pn in addresses.list],
    )


def normalize_email_addresses(
    addresses: CreateEmailAddressConvenience,
) -> List[SxEmailAddressCreateRequest]:
    if all(isinstance(ea, str) for ea in addresses):
        return [SxEmailAddressCreateRequest(value=ea) for ea in addresses]
    else:
        return cast(List[SxEmailAddressCreateRequest], addresses)


def sort_email_addresses(email_addresses: List[EmailAddress]) -> List[EmailAddress]:
    sorting = email_addresses

    # sort from least significant to most significant
    sorting = sorted(sorting, key=lambda addr: addr._raw_id, reverse=True)
    sorting = sorted(sorting, key=lambda addr: addr.value, reverse=True)
    sorting = sorted(sorting, key=lambda addr: addr.date_created, reverse=True)

    return sorting
