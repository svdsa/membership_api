from typing import cast

from membership.database.models import Contact, Member, Tag, TagFieldAssociation
from membership.schemas import JsonObj
from membership.schemas.rest.tags import SxListTagsResponse, SxTag
from membership.util.pagination import PaginatedList


def DEPRECATED_format_tag(tag: Tag) -> JsonObj:
    return {"id": tag.id, "name": tag.name, "date_created": tag.date_created}


def format_tag(tag: Tag) -> SxTag:
    return SxTag(
        id=tag.id,
        date_created=tag.date_created,
        name=tag.name,
        members=[cast(Member, m).id for m in tag.members],
        contacts=[cast(Contact, c).id for c in tag.contacts],
        custom_fields=[
            cast(TagFieldAssociation, tfa).custom_field.id for tfa in tag.custom_fields
        ],
    )


def format_tags_list(tags: PaginatedList[Tag], route: str) -> SxListTagsResponse:
    return SxListTagsResponse(
        url=route,
        has_more=tags.has_more,
        data=[format_tag(t) for t in tags.list],
    )
