from typing import List

from membership.database.models import PostalAddress
from membership.schemas.rest.postal_addresses import (
    SxListPostalAddressResponse,
    SxPostalAddress,
)
from membership.util.pagination import PaginatedList


def format_postal_address(addr: PostalAddress) -> SxPostalAddress:
    return SxPostalAddress(
        id=addr.id,
        consent_to_mail=addr.consent_to_mail,
        date_created=addr.date_created,
        source_created=addr.source_created,
        date_archived=addr.date_archived,
        reason_archived=addr.reason_archived,
        line1=addr.line1,
        line2=addr.line2,
        city=addr.city,
        state=addr.state,
        zipcode=addr.zipcode,
        contact=addr.contact.id,
    )


def format_postal_address_list(
    addresses: PaginatedList[PostalAddress], route: str
) -> SxListPostalAddressResponse:
    return SxListPostalAddressResponse(
        url=route,
        has_more=addresses.has_more,
        data=[format_postal_address(pn) for pn in addresses.list],
    )


def sort_postal_addresses(postal_addresses: List[PostalAddress]) -> List[PostalAddress]:
    sorting = postal_addresses

    # sort from least significant to most significant
    sorting: List[PostalAddress] = sorted(
        sorting, key=lambda addr: addr._raw_id, reverse=True
    )
    sorting: List[PostalAddress] = sorted(
        sorting, key=lambda addr: addr.line2 or "", reverse=True
    )
    sorting: List[PostalAddress] = sorted(
        sorting, key=lambda addr: addr.line1 or "", reverse=True
    )
    sorting: List[PostalAddress] = sorted(
        sorting, key=lambda addr: addr.city or "", reverse=True
    )
    sorting: List[PostalAddress] = sorted(
        sorting, key=lambda addr: addr.postal_code, reverse=True
    )
    sorting = sorted(sorting, key=lambda addr: addr.date_created, reverse=True)

    return sorting
