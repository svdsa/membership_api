from flask.helpers import url_for

from membership.database.models import SecurityPrincipal, SecurityPrincipalRole
from membership.schemas.formatters.users import format_user
from membership.schemas.rest.security_principals import (
    SxListSecurityPrincipalRoles,
    SxListSecurityPrincipals,
    SxSecurityPrincipal,
    SxSecurityPrincipalRole,
)
from membership.util.arrays import flatten
from membership.util.pagination import PaginatedList


def format_security_principal_role(
    spr: SecurityPrincipalRole,
) -> SxSecurityPrincipalRole:
    return SxSecurityPrincipalRole(
        id=spr.id,
        slug=spr.slug,
        name=spr.name,
        description=spr.description,
        autogenerated=spr.autogenerated,
        permissions=spr.needs.__root__,
    )


def format_security_principal_roles_list(
    security_principal_roles: PaginatedList[SecurityPrincipalRole], route: str
) -> SxListSecurityPrincipalRoles:
    return SxListSecurityPrincipalRoles(
        url=route,
        has_more=security_principal_roles.has_more,
        data=[
            format_security_principal_role(spr) for spr in security_principal_roles.list
        ],
    )


def format_security_principal(sp: SecurityPrincipal) -> SxSecurityPrincipal:
    sp_roles = [spra.role for spra in sp.role_associations]
    all_sp_needs = set(
        flatten([spra.role.needs.__root__ for spra in sp.role_associations])
    )
    return SxSecurityPrincipal(
        id=sp.id,
        user=format_user(sp.user, needs=all_sp_needs) if sp.user is not None else None,
        roles=format_security_principal_roles_list(
            PaginatedList.new(sp_roles, limit=len(sp_roles)),
            route=url_for(
                "security_principals_api.list_roles_for_security_principal", sp_id=sp.id
            ),
        ),
    )


def format_security_principals_list(
    sps: PaginatedList[SecurityPrincipal], route: str
) -> SxListSecurityPrincipals:
    return SxListSecurityPrincipals(
        url=route,
        has_more=sps.has_more,
        data=[format_security_principal(sp) for sp in sps.list],
    )
