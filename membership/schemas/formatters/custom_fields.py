from typing import List

from membership.database.models import (
    Contact,
    ContactCustomFieldAssociation,
    CustomFieldDefinition,
)
from membership.schemas.rest.custom_fields import (
    SxCustomFieldAssociation,
    SxCustomFieldDefinition,
    SxListCustomFieldAssociationsResponse,
    SxListCustomFieldsResponse,
)
from membership.util.pagination import PaginatedList


def format_custom_field_contacts(cfd: CustomFieldDefinition) -> List[str]:
    if cfd.contact_ids is None:
        return []

    return [Contact._as_id(contact_id) for contact_id in cfd.contact_ids]


def format_custom_field_tags(cfd: CustomFieldDefinition) -> List[str]:
    if cfd.tags is None:
        return []

    return [tag.id for tag in cfd.tags]


def format_custom_field_definition(
    cfd: CustomFieldDefinition,
) -> SxCustomFieldDefinition:
    return SxCustomFieldDefinition(
        id=cfd.id,
        date_created=cfd.date_created,
        name=cfd.name,
        description=cfd.description,
        field_type=cfd.field_type.name,
        options=cfd.field_options,
        contacts=format_custom_field_contacts(cfd),
        tags=format_custom_field_tags(cfd),
    )


def format_custom_field_association(
    cfa: ContactCustomFieldAssociation,
) -> SxCustomFieldAssociation:
    # need to use the internal "_as_id" function to convert IDs
    # since the ConCusFieldAssoc model may have been deleted, so its
    # child elements are no longer readily accessible through property access
    return SxCustomFieldAssociation(
        date_created=cfa.date_created,
        custom_field=CustomFieldDefinition._as_id(cfa.custom_field_id),
        contact=Contact._as_id(cfa.contact_id),
        value=cfa.value,
    )


def format_custom_field_list(
    custom_fields: PaginatedList[CustomFieldDefinition], route: str
) -> SxListCustomFieldsResponse:
    return SxListCustomFieldsResponse(
        url=route,
        has_more=custom_fields.has_more,
        data=[format_custom_field_definition(cfd) for cfd in custom_fields.list],
    )


def format_custom_field_association_list(
    custom_field_associations: PaginatedList[ContactCustomFieldAssociation], route: str
) -> SxListCustomFieldAssociationsResponse:
    return SxListCustomFieldAssociationsResponse(
        url=route,
        has_more=False,
        data=[
            format_custom_field_association(cfa)
            for cfa in custom_field_associations.list
        ],
    )
