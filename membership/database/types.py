import json
import urllib.parse
from base64 import urlsafe_b64encode
from datetime import datetime
from typing import Union

import pytz
import sqlalchemy.dialects
import sqlalchemy.types as types
from Cryptodome.Random import get_random_bytes


def StdBoolean(
        display_width=1,
        **kwargs
) -> Union[sqlalchemy.dialects.mysql.TINYINT, types.String]:
    """
    Returns a boolean column that is specialized for MariaDB.

    This addresses an issue with the alembic generated migrations where the database is assumed
    to be wrong because the type is a TINYINT instead of BOOLEAN. This causes alembic to always
    incorrectly assume that boolean fields need to be re-migrated. By explicitly returning a
    TINYINT, alembic won't get confused by SQL Alchemy's optimization.
    """
    from .base import engine
    if engine.dialect.name == 'mysql':
        return sqlalchemy.dialects.mysql.TINYINT(display_width=display_width, **kwargs)
    else:
        return types.Boolean(**kwargs)


COMPACT_MAX_LENGTH = 45
DEFAULT_MAX_LENGTH = 255
DEFAULT_UTF8_COLLATION = 'utf8mb4_unicode_520_ci'


def StdString(length: int = DEFAULT_MAX_LENGTH, *args, **kwargs) -> types.String:
    from .base import engine

    if engine.dialect.name == "mysql":
        return types.String(
            length=length, collation=DEFAULT_UTF8_COLLATION, *args, **kwargs
        )
    else:
        return types.String(length=length, *args, **kwargs)


def StdText(*args, **kwargs) -> types.Text:
    from .base import engine
    if engine.dialect.name == 'mysql':
        return types.Text(collation=DEFAULT_UTF8_COLLATION, *args, **kwargs)
    else:
        return types.Text(*args, **kwargs)


def date_parser(date_str: str) -> datetime:
    return datetime.strptime(date_str, '%Y-%m-%d')


class StringyJSON(types.TypeDecorator):
    """Stores and retrieves JSON as TEXT."""

    impl = types.TEXT
    cache_ok = True

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = json.dumps(value)
        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            value = json.loads(value)
        return value


class UTCDateTime(types.TypeDecorator):
    """Make sure all values written to and read from the database as UTC"""

    impl = types.DateTime
    cache_ok = True

    def process_bind_param(self, value, dialect):
        if value is not None and value.tzinfo != pytz.utc:
            value = value.astimezone(pytz.utc)
        return value

    def process_result_value(self, value, dialect):
        if value is not None and hasattr(value, 'tzinfo'):
            if value.tzinfo:
                value = value.astimezone(pytz.utc)
            else:
                value = value.replace(tzinfo=pytz.utc)
        return value


class URLSafeToken:
    @staticmethod
    def column_default():
        return urlsafe_b64encode(get_random_bytes(32)).decode('ascii')

    @staticmethod
    def url_encoded_token(token: str):
        return urllib.parse.quote(token, safe="~()*!.'")


# TypeEngine.with_variant says "use StringyJSON instead when
# connecting to 'sqlite'"
JSON = types.JSON().with_variant(StringyJSON, 'sqlite')

IdInteger = types.BigInteger().with_variant(types.Integer(), "sqlite")
