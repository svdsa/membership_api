import inspect
import os
from typing import Any, Callable, Type, cast

import sqlalchemy.dialects.mysql
import structlog
from hashids import Hashids
from sqlalchemy import Column, engine_from_config, event, orm
from sqlalchemy.exc import DisconnectionError
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import declarative_mixin, scoped_session
from sqlalchemy.sql.selectable import TableClause

from alembic import config, script
from alembic.runtime import migration
from config import ID_SALT, settings
from membership.database.types import IdInteger

logger = structlog.stdlib.get_logger()


class DatabaseNotMigratedError(RuntimeError):
    pass


class InvalidIdError(ValueError):
    pass


def checkout_listener(dbapi_con, con_record, con_proxy):
    """
    Ensures that connections in the pool are still valid before returning them
    :param dbapi_con:
    :param con_record:
    :param con_proxy:
    :return:
    """
    try:
        try:
            dbapi_con.ping(False)
        except TypeError:
            dbapi_con.ping()
    except dbapi_con.OperationalError as exc:
        if exc.args[0] in (2006, 2013, 2014, 2045, 2055):
            raise DisconnectionError()
        else:
            raise


Base = declarative_base()
metadata: sqlalchemy.schema.MetaData = Base.metadata
engine: sqlalchemy.engine.Engine = engine_from_config(settings, prefix="")
Session: Callable[[], orm.Session] = orm.sessionmaker(
    autocommit=False, autoflush=False, bind=engine
)
db_session = scoped_session(Session)


class IdPrefixes:
    Contact = "ct"
    Tag = "tg"
    EmailAddress = "eml"
    PhoneNumber = "phn"
    CustomFieldDefinition = "cfd"
    PostalAddress = "pad"
    User = "usr"
    SecurityPrincipal = "sec"
    SecurityPrincipalRole = "spr"
    ImportEvent = "ime"
    ImportEventReviewAction = "ima"


@declarative_mixin
class TableWithMaskedId:
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    @classmethod
    def _id_prefix(cls):
        try:
            id_prefix = getattr(IdPrefixes, cls.__name__)
        except AttributeError:
            raise NotImplementedError(f"Must define ID prefix for table {cls.__name__}")

        return f"{id_prefix}_"

    @classmethod
    def _id_salt(cls):
        return f"{ID_SALT}:{cls._id_prefix()}"

    @classmethod
    def _id_hasher(cls):
        return Hashids(salt=cls._id_salt(), min_length=16)

    """Internal DB ID and primary key. Use `id` for most uses"""
    _raw_id = Column("id", IdInteger, primary_key=True, unique=True, autoincrement=True)

    @property
    def id(self) -> str:
        return self._as_id(self._raw_id)

    @classmethod
    def _as_id(cls, id: int) -> str:
        return f"{cls._id_prefix()}{cls._id_hasher().encode(id)}"

    @classmethod
    def parse_id(cls, id: str) -> int:
        if not id.startswith(cls._id_prefix()):
            raise InvalidIdError(
                f"not a valid {cls.__name__} id: '{id}' - valid ids start with {cls._id_prefix()}"
            )

        try:
            hashid_portion = id[len(cls._id_prefix()) :]
            decoded = cls._id_hasher().decode(hashid_portion)
            return decoded[0]
        except IndexError:
            raise InvalidIdError(f"No such {cls.__name__} id: '{id}'")


def check_migrations(engine):
    """check that latest migration is applied (i.e. the migration container ran
    successfully and we did not race it to startup)
    from https://stackoverflow.com/a/56085521"""

    app_root_path = os.path.abspath(os.path.dirname(__file__))
    alembic_cfg_path = os.path.normpath(
        os.path.join(app_root_path, "..", "..", "alembic.ini")
    )
    logger = structlog.stdlib.get_logger()
    logger = logger.bind(app_root_path=app_root_path, alembic_cfg_path=alembic_cfg_path)
    alembic_cfg = config.Config(alembic_cfg_path)
    script_ = script.ScriptDirectory.from_config(alembic_cfg)
    with engine.begin() as conn:
        context = migration.MigrationContext.configure(conn)
        current_revision = context.get_current_revision()
        latest_revision = script_.get_current_head()
        logger = logger.bind(
            current_revision=current_revision, latest_revision=latest_revision
        )
        if current_revision != latest_revision:
            logger.fatal("database not fully migrated")
            raise DatabaseNotMigratedError(
                inspect.cleandoc(
                    f"""
                FATAL: Database not fully migrated!

                App root path: {app_root_path}
                Alembic config path: {alembic_cfg_path}
                Found migration revision: {current_revision}
                Expected latest revision: {latest_revision}

                This may be a temporary issue caused by the API container starting
                before the migration container has a chance to finish running.

                If you continue to get this error, please check that the migration container
                ran successfully and that it was built and pushed successfully alongside the
                API container.
            """
                )
            )
        else:
            logger.unbind("app_root_path", "alembic_cfg_path")
            logger.info("Confirmed alembic migrations")


event.listen(engine, 'checkout', checkout_listener)


def col(field: Any) -> Column:
    return cast(Column, field)


def table(base: Type[Base]) -> TableClause:
    return cast(TableClause, base)
