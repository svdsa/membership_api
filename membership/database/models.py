import enum
import json
import re
from datetime import datetime, timezone
from typing import Any, List, Optional, cast

import phonenumbers
from phonenumbers.phonenumberutil import NumberParseException, PhoneNumberFormat
from pydantic.parse import Protocol
from sqlalchemy import (
    BigInteger,
    Column,
    Enum,
    ForeignKey,
    Index,
    Integer,
    and_,
    func,
    or_,
)
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
from sqlalchemy.schema import UniqueConstraint
from sqlalchemy.sql.expression import desc

from config import PORTAL_URL
from membership.auth.needs import SxRoleNeedsList
from membership.database.base import Base, TableWithMaskedId
from membership.database.constants import US_ADMIN_AREAS, US_ADMIN_AREAS_REVERSE
from membership.database.types import (
    COMPACT_MAX_LENGTH,
    JSON,
    IdInteger,
    StdBoolean,
    StdString,
    StdText,
    URLSafeToken,
    UTCDateTime,
)
from membership.schemas.rest.contacts import SxContactCreateRequest
from membership.schemas.rest.custom_field_options import (
    SxCustomFieldChoiceOptions,
    SxCustomFieldOptionsUnion,
    SxCustomFieldRangeOptions,
)
from membership.schemas.rest.custom_fields import CustomFieldType
from membership.schemas.rest.import_events import (
    ImportEventReviewActionType,
    ImportEventType,
    SxImportEventInfo,
    SxImportEventMatchJustification,
    SxImportReviewMerge,
)
from membership.util import time
from membership.util.arrays import compact
from membership.util.schema import unmodifiable


class Chapter(Base):
    __tablename__ = "chapters"

    id: int = Column(Integer, primary_key=True, unique=True)
    name: str = Column(StdString(), unique=True, nullable=False)


class NationalMembershipData(Base):
    __tablename__ = "memberships_usa"

    id: int = Column(Integer, primary_key=True, unique=True)
    date_imported: datetime = Column(UTCDateTime, nullable=False, default=func.now())
    date_updated: datetime = Column(
        UTCDateTime, nullable=False, default=func.now(), onupdate=func.now()
    )

    active: bool = Column(StdBoolean(), default=True, nullable=False)
    """
    False if membership has been deactivated.
    Note: Members can deactivate their account before their membership expires.
    """

    member_id: Optional[int] = Column(ForeignKey("members.id"))
    member: Optional["Member"] = relationship(
        "Member", back_populates="memberships_usa", uselist=False
    )

    ak_id: str = Column(StdString(COMPACT_MAX_LENGTH), unique=True, nullable=False)
    dsa_id: Optional[str] = Column(StdString(COMPACT_MAX_LENGTH), nullable=True)

    do_not_call: bool = Column(StdBoolean(), default=False, nullable=False)

    first_name: str = Column(StdString(), nullable=False)
    middle_name: Optional[str] = Column(StdString())
    last_name: str = Column(StdString(), nullable=False)

    address_line_1: Optional[str] = Column(StdString())
    address_line_2: Optional[str] = Column(StdString())
    city: str = Column(StdString(), nullable=False)  # Optional?
    country: str = Column(StdString(), nullable=False)  # Optional?
    zipcode: str = Column(StdString(10), nullable=False)  # Optional?

    @property
    def address(self) -> List[str]:
        return [
            line
            for line in (
                self.address_line_1,
                self.address_line_2,
                f"{self.city} {self.zipcode}",
            )
            if line
        ]

    # The following should be required, but national sends us an empty value here
    join_date = Column(UTCDateTime)  # Required?
    dues_paid_until = Column(UTCDateTime)  # Required?

    def __repr__(self) -> str:
        return (
            f"<Membership AK:{self.ak_id} "
            + f"Name:{self.first_name} {self.last_name} "
            + f"Exp:{self.dues_paid_until}>"
        )


class Member(Base):
    __tablename__ = "members"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        email = self.email_address
        self.normalized_email = EmailAddress.normalize(email) if email else None

    id: int = Column(Integer, primary_key=True, unique=True)
    date_created: datetime = Column(
        UTCDateTime,
        nullable=True,  # todo (Jeff): remove after we migrate everyone over
        default=func.now(),
    )

    # guest info for non-members & preferred member info for members
    # todo: consolidate first_name + last_name => nickname & full_name
    first_name: Optional[str] = Column(StdString())
    last_name: Optional[str] = Column(StdString())

    # slash-delimited pronoun declensions. multiple unknown pronoun sets joined by ";"
    pronouns: Optional[str] = Column(StdText())
    email_address: Optional[str] = Column(StdString(), unique=True)
    normalized_email: Optional[str] = Column(StdString(), unique=True)

    # preferences
    do_not_call: bool = Column(StdBoolean(), default=False, nullable=False)
    do_not_email: bool = Column(StdBoolean(), default=False, nullable=False)

    # details
    biography: Optional[str] = Column(StdText())
    phone_numbers: List["PhoneNumber"] = relationship(
        "PhoneNumber", back_populates="member"
    )
    additional_email_addresses: List["AdditionalEmailAddress"] = relationship(
        "AdditionalEmailAddress", back_populates="member"
    )
    address: Optional[str] = Column(StdString())
    city: Optional[str] = Column(StdString())
    zipcode: Optional[str] = Column(StdString())

    # admin fields
    notes = Column(StdText(), default="", nullable=False)
    dues_paid_overridden_on = Column(UTCDateTime, nullable=True)
    onboarded_on = Column(UTCDateTime, nullable=True)
    suspended_on = Column(UTCDateTime, nullable=True)
    left_chapter_on = Column(UTCDateTime, nullable=True)
    memberships_usa: List["NationalMembershipData"] = relationship(
        "NationalMembershipData", back_populates="member",
    )
    identities: List["Identity"] = relationship("Identity", back_populates="member")
    eligible_votes: List["EligibleVoter"] = relationship(
        "EligibleVoter", back_populates="member"
    )
    meetings_attended: List["Attendee"] = relationship(
        "Attendee", back_populates="member"
    )
    meetings_owned: List["Meeting"] = relationship("Meeting", back_populates="owner")
    all_roles = cast(
        List["Role"], relationship("Role", back_populates="member", viewonly=True)
    )
    email_verify_token: List["AttendeeEmailVerifyToken"] = relationship(
        "AttendeeEmailVerifyToken",
        back_populates="member",
        cascade="all, delete-orphan",
    )
    meeting_invitations: List["MeetingInvitation"] = relationship(
        "MeetingInvitation", back_populates="member",
    )
    all_committee_leaderships: List['CommitteeLeader'] = relationship(
        'CommitteeLeader', back_populates='member',
    )
    tag_associations = cast(
        List["TagAssociation"],
        relationship(
            "TagAssociation",
            back_populates="member",
            lazy="selectin",
            cascade="all,delete,delete-orphan",
        ),
    )
    tags = cast(List["Tag"], association_proxy("tag_associations", "tag"))

    principal = cast(
        Optional["SecurityPrincipal"],
        relationship(
            "SecurityPrincipal",
            uselist=False,
            back_populates="member",
            lazy="selectin",
            single_parent=True,
            cascade="all,delete-orphan",
        ),
    )
    principal_id = cast(
        Optional[int], Column(IdInteger, ForeignKey("security_principals.id"))
    )

    @property
    def name(self) -> str:
        n = ""
        if self.first_name:
            n = self.first_name
        if self.last_name:
            n += " " + self.last_name
        return n

    @property
    def roles(self) -> List["Role"]:
        return [
            r for r in self.all_roles if r.committee is None or not r.committee.inactive
        ]

    @property
    def committee_leaderships(self) -> List['CommitteeLeader']:
        return [
            leader for leader in self.all_committee_leaderships if leader.is_current
        ]

    def has_general_member_role(self) -> bool:
        return self.roles and any(
            role
            for role in self.roles
            if role.committee_id is None and role.role == "member"
        )

    def email_matches(self, email: str) -> bool:
        return (
            len(email) > 0
            and self.email_address == email
            or self.normalized_email == EmailAddress.normalize(email)
        )

    def additional_email_matches(self, email: str) -> bool:
        return email and any(
            addl
            for addl in self.additional_email_addresses
            if addl.email_address == email
        )

    def name_matches(self, first_name: str, last_name: str) -> bool:
        if not first_name or not last_name or not self.first_name or not self.last_name:
            return False

        return (
            self.first_name.lower() == first_name.lower()
            and self.last_name.lower() == last_name.lower()
        )

    def zipcode_matches(self, zipcode: str) -> bool:
        return zipcode and self.zipcode == zipcode

    def phone_matches(self, phone_number: str) -> bool:
        formatted_number = PhoneNumber.format_number(phone_number)

        return formatted_number and any(
            phone
            for phone in self.phone_numbers
            if phone.number == formatted_number
        )

    def phones_in(self, phone_numbers: List[str]) -> bool:
        formatted_numbers = [
            PhoneNumber.format_number(pn)
            for pn in phone_numbers
        ]

        self_pns = set(self.phone_numbers)
        their_pns = set([pn for pn in formatted_numbers if pn is not None])

        return len(formatted_numbers) > 0 and len(their_pns.intersection(self_pns)) > 0

    def is_deleted(self) -> bool:
        return self.email_address and self.email_address.startswith("DELETE")

    def __repr__(self) -> str:
        return (
            f"{self.name} (pn {self.pronouns}) (email {self.email_address}) "
            f"(loc {self.address} {self.city} {self.zipcode})"
        )


class PhoneNumber(Base, TableWithMaskedId):
    __tablename__ = "phone_numbers"

    date_created = cast(
        datetime,
        Column(
            UTCDateTime,
            nullable=False,
            default=func.now(),
        ),
    )
    date_confirmed = cast(Optional[datetime], Column(UTCDateTime, nullable=True))
    date_archived = cast(Optional[datetime], Column(UTCDateTime, nullable=True))
    reason_archived = cast(Optional[str], Column(StdText(), nullable=True))

    _raw_number = cast(
        str, Column("number", StdString(COMPACT_MAX_LENGTH), nullable=False)
    )
    name = cast(Optional[str], Column(StdString(), nullable=True))
    primary = cast(bool, Column(StdBoolean(), nullable=False, default=False))

    member_id: int = Column(ForeignKey("members.id"), nullable=True)
    member: Member = relationship("Member", back_populates="phone_numbers")

    contact_id = cast(int, Column(ForeignKey("contacts.id"), nullable=True))
    contact = cast("Contact", relationship("Contact", back_populates="phone_numbers"))

    sms_capable = cast(bool, Column(StdBoolean(), nullable=False, default=False))

    consent_to_call = cast(bool, Column(StdBoolean(), nullable=False, default=False))
    consent_to_text = cast(bool, Column(StdBoolean(), nullable=False, default=False))
    consent_to_share = cast(bool, Column(StdBoolean(), nullable=False, default=False))

    @property
    def number(self) -> str:
        return self._raw_number

    @number.setter
    def number(self, value: str):
        self._raw_number = PhoneNumber.format_number(value)

    @property
    def value(self) -> str:
        return self._raw_number

    @value.setter
    def value(self, input: str):
        self._raw_number = PhoneNumber.format_number(input)

    @staticmethod
    def format_number(number: str) -> str:
        raw_number = re.sub("[(). +-]*", lambda m: "", number)
        full_phone_number: str = f"+1 {number}" if len(raw_number) == 10 else number
        return cast(str, phonenumbers.format_number(
            phonenumbers.parse(full_phone_number, 'US'), PhoneNumberFormat.INTERNATIONAL
        ))

    @staticmethod
    def try_format_number(number: str) -> Optional[str]:
        try:
            return PhoneNumber.format_number(number)
        except NumberParseException:
            return None

    def __repr__(self) -> str:
        return (
            "<"
            "PhoneNumber "
            f"{self.id=} "
            f"{self.value=} "
            f"{self.primary=} "
            f"{self.consent_to_call=} "
            f"{self.consent_to_share=} "
            f"{self.consent_to_text=} "
            f"{self.date_created=} "
            f"{self.date_confirmed=} "
            f"{self.date_archived=} "
            f"{self.reason_archived=} "
            f"{self.name=} "
            f"{self.sms_capable=}"
            ">"
        )


class AdditionalEmailAddress(Base):
    __tablename__ = "additional_email_addresses"
    __table_args__ = (UniqueConstraint("member_id", "email_address"),)

    id: int = Column(Integer, primary_key=True, unique=True)
    member_id: int = Column(ForeignKey("members.id"), nullable=False)
    date_created: datetime = Column(
        UTCDateTime, nullable=False, default=func.now(),
    )
    date_verified: datetime = Column(UTCDateTime, nullable=True)

    email_address: str = Column(StdString(), nullable=False)
    name: Optional[str] = Column(StdString())
    preferred: bool = Column(StdBoolean(), default=True, nullable=False)
    verified: bool = Column(StdBoolean(), default=False, nullable=False)

    member: Member = relationship("Member", back_populates="additional_email_addresses")


class IdentityProviders:
    AK = "AK_ID"
    DSA = "DSA_ID"
    AUTH0 = "AUTH0"
    COLLECTIVE_DUES = "COLLECTIVE_DUES_SUBSCRIPTION"
    CSV = "CSV"


class Identity(Base):
    __tablename__ = "identities"
    __table_args__ = (UniqueConstraint("provider_id", "provider_name"),)

    id: int = Column(Integer, primary_key=True, unique=True)
    member_id: int = Column(ForeignKey("members.id"), nullable=False)
    date_imported: datetime = Column(
        UTCDateTime,
        nullable=True,  # todo (Jeff): remove after we run the script again on prod
        default=func.now(),
    )

    provider_name: str = Column(StdString(), nullable=False)  # one of IdentityProviders
    provider_id: str = Column(StdString(COMPACT_MAX_LENGTH), nullable=False)

    member: "Member" = relationship("Member", back_populates="identities")


class Committee(Base):
    __tablename__ = "committees"

    id: int = Column(Integer, primary_key=True, unique=True)
    name: str = Column(StdString(COMPACT_MAX_LENGTH))
    provisional: bool = Column(StdBoolean(), default=False, nullable=False)
    inactive: bool = Column(StdBoolean(), default=False, nullable=False)

    admins = cast(
        List[Member],
        relationship(
            "Member",
            secondary="roles",
            primaryjoin="and_(Committee.id==Role.committee_id, " "Role.role=='admin')",
            viewonly=True,
        ),
    )
    members = cast(
        List[Member],
        relationship(
            "Member",
            secondary="roles",
            primaryjoin="and_(Committee.id==Role.committee_id, " "Role.role=='member')",
            viewonly=True,
        ),
    )
    active_members = cast(
        List[Member],
        relationship(
            "Member",
            secondary="roles",
            primaryjoin="and_(Committee.id==Role.committee_id,Role.role=='active')",
            viewonly=True,
        ),
    )
    committee_leadership_roles: List["CommitteeLeadershipRole"] = relationship(
        "CommitteeLeadershipRole",
        back_populates="committee",
    )


class Role(Base):
    __tablename__ = "roles"
    __table_args__ = (UniqueConstraint("member_id", "committee_id", "role"),)

    id: int = Column(Integer, primary_key=True, unique=True)
    chapter_id: Optional[int] = Column(ForeignKey("chapters.id"))
    committee_id: Optional[int] = Column(ForeignKey("committees.id"))
    member_id: int = Column(ForeignKey("members.id"), nullable=False)
    role: str = Column(StdString(COMPACT_MAX_LENGTH), nullable=False)
    date_created: datetime = Column(
        UTCDateTime,
        nullable=True,  # TODO: Remove after roles are migrated on prod
        default=func.now(),
    )

    committee: "Committee" = relationship(Committee)
    member: "Member" = relationship("Member", back_populates="all_roles")


class LeadershipRole(Base):
    __tablename__ = 'leadership_roles'
    __table_args__ = (UniqueConstraint('title'),)

    id: int = Column(Integer, primary_key=True, unique=True)
    title: str = Column(StdString(), nullable=False)

    committee_leadership_roles: List['CommitteeLeadershipRole'] = relationship(
        'CommitteeLeadershipRole', back_populates='leadership_role',
    )


class CommitteeLeadershipRole(Base):
    __tablename__ = 'committee_leadership_roles'

    id: int = Column(Integer, primary_key=True, unique=True)
    committee_id: int = Column(ForeignKey('committees.id'), nullable=False)
    leadership_role_id: int = Column(ForeignKey('leadership_roles.id'), nullable=False)
    term_limit_months: int = Column(Integer, nullable=True)

    committee: 'Committee' = relationship(
        'Committee', back_populates='committee_leadership_roles'
    )
    leadership_role: 'LeadershipRole' = relationship(
        'LeadershipRole', back_populates='committee_leadership_roles',
    )
    all_committee_leaders: List['CommitteeLeader'] = relationship(
        'CommitteeLeader', back_populates='committee_leadership_role',
    )

    @property
    def current_leader(self) -> Optional['CommitteeLeader']:
        # There should be at most one current leader per CommitteeLeadershipRole
        current_leaders = [
            leader for leader in self.all_committee_leaders if leader.is_current
        ]
        return current_leaders[-1] if current_leaders else None


class CommitteeLeader(Base):
    __tablename__ = 'committee_leaders'

    id: int = Column(Integer, primary_key=True, unique=True)
    committee_leadership_role_id: int = Column(
        ForeignKey('committee_leadership_roles.id'), nullable=False,
    )
    member_id: int = Column(ForeignKey('members.id'), nullable=False)
    term_start_date: Optional[datetime] = Column(
        UTCDateTime, nullable=False, default=func.now(),
    )
    term_end_date: Optional[datetime] = Column(UTCDateTime, nullable=True)

    committee_leadership_role: 'CommitteeLeadershipRole' = relationship(
        'CommitteeLeadershipRole', back_populates='all_committee_leaders',
    )
    member: 'Member' = relationship(
        'Member', back_populates='all_committee_leaderships'
    )

    @hybrid_property
    def is_current(self):
        now = time.get_current_time()
        return self.term_start_date <= now and (
            self.term_end_date is None or self.term_end_date > now
        )

    @is_current.expression
    def is_current(cls):
        return and_(
            cls.term_start_date <= func.now(),
            or_(cls.term_end_date == None, cls.term_end_date > func.now()),
        )


class Meeting(Base):
    __tablename__ = "meetings"

    # XXX MEMBER transition this to TableWithMaskedId
    id: int = Column(Integer, primary_key=True, unique=True)
    short_id: Optional[int] = Column(Integer, nullable=True, unique=True)
    name: str = Column(StdString(), nullable=False)
    committee_id: Optional[int] = Column(ForeignKey("committees.id"))

    # XXX fix nullable
    start_time: Optional[datetime] = Column(UTCDateTime)
    end_time: Optional[datetime] = Column(UTCDateTime)
    landing_url: Optional[str] = Column(StdString(), nullable=True)
    published: bool = Column(StdBoolean(), default=True, nullable=False)

    # XXX MEMBER remove this later
    owner_id = cast(Optional[int], Column(ForeignKey("members.id"), nullable=True))
    owner = cast(
        Optional[Member], relationship("Member", back_populates="meetings_owned")
    )

    creator_id = cast(
        Optional[int], Column(ForeignKey("security_principals.id"), nullable=True)
    )
    creator = cast(Optional["SecurityPrincipal"], relationship("SecurityPrincipal"))

    attendees: List["Attendee"] = relationship("Attendee", back_populates="meeting")
    invitations: List["MeetingInvitation"] = relationship(
        "MeetingInvitation", back_populates="meeting",
    )
    agenda: "MeetingAgenda" = relationship(
        "MeetingAgenda", uselist=False, back_populates="meeting"
    )

    @hybrid_property
    def is_voting_meeting(self):
        has_label = ("General" in self.name) or ("Regular" in self.name)
        return self.committee_id is None and has_label

    @is_voting_meeting.expression
    def is_voting_meeting(cls):
        has_label = or_(cls.name.like("%General%"), cls.name.like("%Regular%"),)
        return and_(cls.committee_id == None, has_label)


class Attendee(Base):
    __tablename__ = "attendees"

    __table_args__ = (
        Index("attendee_member_id_meeting_id", "member_id", "meeting_id", unique=True),
        Index(
            "attendee_contact_id_meeting_id", "contact_id", "meeting_id", unique=True
        ),
    )

    id: int = Column(Integer, primary_key=True, unique=True)
    eligible_to_vote: Optional[bool] = Column(StdBoolean())
    verified: bool = Column(StdBoolean(), nullable=False, default=True)

    meeting_id = cast(int, Column(ForeignKey("meetings.id"), nullable=False))
    meeting = cast(Meeting, relationship("Meeting", back_populates="attendees"))

    member_id = cast(Optional[int], Column(ForeignKey("members.id"), nullable=True))
    member = cast(
        Optional[Member], relationship("Member", back_populates="meetings_attended")
    )

    contact_id = cast(Optional[int], Column(ForeignKey("contacts.id"), nullable=True))
    contact = cast(Optional["Contact"], relationship("Contact"))

    email_verify_token: "AttendeeEmailVerifyToken" = relationship(
        "AttendeeEmailVerifyToken",
        back_populates="attendee",
        cascade="all, delete-orphan",
        uselist=False,
    )


class AttendeeEmailVerifyToken(Base):
    __tablename__ = "attendee_email_verify_tokens"

    __table_args__ = (UniqueConstraint("attendee_id", "email_address", "token"),)

    id: int = Column(Integer, autoincrement=True, primary_key=True, unique=True)
    created_on: datetime = Column(UTCDateTime, default=func.now())
    email_address = cast(str, Column(StdString(), unique=True, nullable=False))
    token = cast(str, Column(StdString(), unique=True, nullable=False))

    member_id = cast(Optional[int], Column(ForeignKey("members.id"), nullable=True))
    member = cast(
        Optional[Member],
        relationship("Member", back_populates="email_verify_token", uselist=False),
    )

    contact_id = cast(Optional[int], Column(ForeignKey("contacts.id"), nullable=True))
    contact = cast(Optional["Contact"], relationship("Contact", uselist=False))

    attendee_id = cast(int, Column(ForeignKey("attendees.id"), nullable=False))
    attendee = cast(
        Attendee,
        relationship("Attendee", back_populates="email_verify_token", uselist=False),
    )


class MeetingInvitationStatus(enum.Enum):
    SEND_PENDING = "SEND_PENDING"
    NO_RESPONSE = "NO_RESPONSE"
    ACCEPTED = "ACCEPTED"
    DECLINED = "DECLINED"


class MeetingInvitation(Base):
    __tablename__ = "meeting_invitation"

    __table_args__ = (UniqueConstraint("meeting_id", "member_id"),)

    id: int = Column(Integer, primary_key=True, unique=True)

    date_created = cast(
        datetime, unmodifiable(Column(UTCDateTime, default=func.now(), nullable=False))
    )

    member_id = cast(Optional[int], Column(ForeignKey("members.id"), nullable=True))
    member = cast(
        Optional[Member],
        relationship("Member", back_populates="meeting_invitations", uselist=False),
    )

    contact_id = cast(Optional[int], Column(ForeignKey("contacts.id"), nullable=True))
    contact = cast(Optional["Contact"], relationship("Contact", uselist=False))

    meeting_id = cast(int, Column(ForeignKey("meetings.id"), nullable=False))
    meeting = cast(Meeting, relationship("Meeting", back_populates="invitations"))

    token: str = unmodifiable(
        Column(
            StdString(128),
            unique=True,
            default=lambda x: URLSafeToken.column_default(),
        )
    )
    status: MeetingInvitationStatus = Column(
        Enum(MeetingInvitationStatus), default=MeetingInvitationStatus.SEND_PENDING,
    )

    @property
    def rsvp_url(self):
        url_encoded_token = URLSafeToken.url_encoded_token(self.token)
        return f"{PORTAL_URL}/meetings/{self.meeting_id}/rsvp/{url_encoded_token}"

    @property
    def created_at(self) -> datetime:
        return self.date_created


class MeetingAgenda(Base):
    __tablename__ = "meeting_agendas"

    id: int = Column(Integer, primary_key=True, unique=True)
    meeting_id: int = Column(ForeignKey("meetings.id"), nullable=False, unique=True)
    text: str = Column(StdText(), default="", nullable=False)
    updated_at: datetime = Column(UTCDateTime, default=func.now(), nullable=False)

    meeting: "Meeting" = relationship("Meeting", back_populates="agenda", uselist=False)


class Election(Base):
    __tablename__ = "elections"

    id: int = Column(Integer, primary_key=True, unique=True)
    name: str = Column(StdString(), nullable=False, unique=True)
    status: str = Column(StdString(COMPACT_MAX_LENGTH), nullable=False, default="draft")
    number_winners: int = Column(Integer)
    voting_begins_epoch_millis: Optional[int] = Column(BigInteger, nullable=True)
    voting_ends_epoch_millis: Optional[int] = Column(BigInteger, nullable=True)
    description: Optional[str] = Column(StdText(), nullable=True)
    description_img: Optional[str] = Column(StdString(1024), nullable=True)
    author: Optional[str] = Column(StdString(), nullable=True)

    candidates: List["Candidate"] = relationship("Candidate", back_populates="election")
    sponsors: List["Sponsor"] = relationship("Sponsor", back_populates="election")
    votes: List["Vote"] = relationship("Vote", back_populates="election")
    voters: List["EligibleVoter"] = relationship(
        "EligibleVoter", back_populates="election"
    )

    @property
    def voting_begins(self) -> Optional[datetime]:
        if self.voting_begins_epoch_millis is None:
            return None
        return datetime.utcfromtimestamp(
            self.voting_begins_epoch_millis / 1000.0
        ).replace(tzinfo=timezone.utc)

    @property
    def voting_ends(self) -> Optional[datetime]:
        if self.voting_ends_epoch_millis is None:
            return None
        return datetime.utcfromtimestamp(
            self.voting_ends_epoch_millis / 1000.0
        ).replace(tzinfo=timezone.utc)


class Candidate(Base):
    __tablename__ = "candidates"

    id: int = Column(Integer, primary_key=True, unique=True)
    name: str = Column(StdString())
    election_id: int = Column(ForeignKey("elections.id"))
    image_url: Optional[str] = Column(StdString(), nullable=True)

    election: "Election" = relationship("Election", back_populates="candidates")


class Sponsor(Base):
    __tablename__ = "sponsors"

    id: int = Column(Integer, primary_key=True, unique=True)
    name: str = Column(StdString(), nullable=False)
    election_id: int = Column(ForeignKey("elections.id"), nullable=False)

    election: "Election" = relationship("Election", back_populates="sponsors")


class Vote(Base):
    __tablename__ = "votes"
    __table_args__ = (UniqueConstraint("vote_key", "election_id"),)

    id: int = Column(Integer, primary_key=True, unique=True)
    date_created: datetime = Column(
        UTCDateTime,
        nullable=True,  # todo (Jeff): remove after votes are migrated on prod
        default=func.now(),
    )
    vote_key: int = Column(Integer)
    election_id: int = Column(ForeignKey("elections.id"))

    election: "Election" = relationship("Election", back_populates="votes")
    ranking: List["Ranking"] = relationship(
        "Ranking", back_populates="vote", order_by="Ranking.rank"
    )


class Ranking(Base):
    __tablename__ = "rankings"

    id: int = Column(Integer, primary_key=True, unique=True)
    vote_id: int = Column(ForeignKey("votes.id"))
    rank: int = Column(Integer)
    candidate_id: int = Column(ForeignKey("candidates.id"))

    vote: "Vote" = relationship("Vote", back_populates="ranking")
    candidate: "Candidate" = relationship("Candidate")


class EligibleVoter(Base):
    __tablename__ = "eligible_voters"

    __table_args__ = (
        Index(
            "eligible_voter_member_election_unique",
            "member_id",
            "election_id",
            unique=True,
        ),
    )

    id: int = Column(Integer, primary_key=True, unique=True)
    member_id: int = Column(ForeignKey("members.id"))
    voted: bool = Column(StdBoolean(), nullable=False)
    election_id: int = Column(ForeignKey("elections.id"))

    member: "Member" = relationship("Member", back_populates="eligible_votes")
    election: "Election" = relationship("Election", back_populates="voters")


class AssetType(enum.Enum):
    EXTERNAL_URL = "EXTERNAL_URL"
    PRESIGNED_URL = "PRESIGNED_URL"


class AssetLabel(Base):
    __tablename__ = "asset_labels"
    __table_args__ = (UniqueConstraint("asset_id", "label"),)

    id: int = Column(Integer, primary_key=True, unique=True)
    asset_id: int = Column(ForeignKey("assets.id", ondelete="CASCADE"), nullable=False)
    label: str = Column(StdString(), nullable=False)

    asset: "Asset" = relationship("Asset", back_populates="labels")


class Asset(Base):
    __tablename__ = "assets"

    id: int = Column(Integer, primary_key=True, unique=True)
    asset_type: AssetType = Column(Enum(AssetType), nullable=False)
    last_updated: datetime = Column(UTCDateTime, nullable=False, default=func.now())
    content_type: str = Column(StdString(), nullable=False)
    title: Optional[str] = Column(StdString())
    external_url: Optional[str] = Column(StdText())
    relative_path: Optional[str] = Column(StdString(), unique=True)

    labels: List[AssetLabel] = relationship(AssetLabel, back_populates="asset")


class ProxyTokenState(enum.Enum):
    UNCLAIMED = "UNCLAIMED"
    ACCEPTED = "ACCEPTED"
    REJECTED = "REJECTED"

    @property
    def friendly_name(self) -> str:
        return self.value.lower()


class ProxyToken(Base):
    """
    Tokens that members can create
    to nominate other members to vote on their behalf at a meeting.
    """

    __tablename__ = "proxy_token"

    # A unique identifier for this token, which is used as effective authorization of this user.
    id: str = unmodifiable(
        Column(
            StdString(128),
            primary_key=True,
            unique=True,
            default=lambda x: URLSafeToken.column_default(),
        )
    )

    # ID of the member who nominating someone else to be their proxy.
    member_id: int = unmodifiable(Column(Integer, nullable=False))

    # ID of the meeting at which the Proxy will vote.
    meeting_id: int = unmodifiable(Column(Integer, nullable=False))

    # ID of the member who is being nominated.
    receiving_member_id: int = Column(Integer, nullable=True)

    # Tokens can only be used once.
    state: ProxyTokenState = Column(
        Enum(ProxyTokenState), nullable=False, default=ProxyTokenState.UNCLAIMED,
    )

    @property
    def url(self):
        url_encoded_token = URLSafeToken.url_encoded_token(self.id)
        return (
            f"{PORTAL_URL}/meetings/{self.meeting_id}/proxy-token/{url_encoded_token}"
        )


class MembershipStatusAudit(Base):
    __tablename__ = 'membership_status_audit'

    id: int = Column(Integer, primary_key=True)
    timestamp: datetime = Column(UTCDateTime, nullable=False, default=func.now())

    items = relationship('MembershipStatusAuditItemization', back_populates='audit')


class MembershipStatusAuditItemization(Base):
    __tablename__ = 'membership_status_audit_itemization'

    audit_id: int = Column(
        ForeignKey('membership_status_audit.id'), primary_key=True, nullable=False
    )
    member_id: int = Column(ForeignKey('members.id'), primary_key=True, nullable=False)

    membership_status: str = Column(StdString(128), nullable=False)
    dues_are_paid: bool = Column(StdBoolean(), nullable=False)
    eligible_to_vote: bool = Column(StdBoolean(), nullable=False)
    meets_attendance_criteria: bool = Column(StdBoolean(), nullable=False)
    is_active_in_committee: bool = Column(StdBoolean(), nullable=False)

    audit: MembershipStatusAudit = relationship(
        MembershipStatusAudit, back_populates='items'
    )

    def is_active(self) -> bool:
        return self.meets_attendance_criteria or self.is_active_in_committee


class ImportEvent(Base, TableWithMaskedId):
    __tablename__ = "import_events"

    creator_id = cast(
        int,
        Column(IdInteger, ForeignKey("security_principals.id"), nullable=False),
    )
    creator = cast(
        "SecurityPrincipal",
        relationship("SecurityPrincipal", foreign_keys=[creator_id], uselist=False),
    )
    date_created = cast(
        datetime,
        Column(
            UTCDateTime,
            nullable=False,
            default=func.now(),
        ),
    )

    # Event metadata

    title = cast(str, Column(StdString(), nullable=False))
    event_type = cast(
        ImportEventType,
        Column(
            Enum(ImportEventType),
            nullable=False,
        ),
    )
    event_info_json = Column(JSON, nullable=True)

    @property
    def event_info(self) -> Optional[SxImportEventInfo]:
        if self.event_info_json is not None:
            return SxImportEventInfo(**json.loads(cast(str, self.event_info_json)))
        else:
            return None

    @event_info.setter
    def event_info(self, value: Optional[SxImportEventInfo]):
        if value is not None:
            self.event_info_json = value.json(exclude_none=True)
        else:
            self.event_info_json = None

    # Participants

    participant_jsons = cast(
        List[str], association_proxy("review_actions", "participant_json")
    )

    @property
    def participants(self) -> List[SxContactCreateRequest]:
        values = [json.loads(j) for j in self.participant_jsons]
        return [SxContactCreateRequest(**v) for v in values]

    # Reviews

    date_reviewed = cast(
        Optional[datetime],
        Column(
            UTCDateTime,
        ),
    )
    reviewer_id = cast(
        Optional[int],
        Column(IdInteger, ForeignKey("security_principals.id"), nullable=True),
    )
    reviewer = cast(
        Optional["SecurityPrincipal"],
        relationship("SecurityPrincipal", foreign_keys=[reviewer_id], uselist=False),
    )
    review_actions = cast(
        List["ImportEventReviewAction"],
        relationship(
            "ImportEventReviewAction",
            back_populates="import_event",
            cascade="all,delete-orphan",
        ),
    )

    def __repr__(self) -> str:
        return (
            "<"
            "ImportEvent "
            f"{self.id=} "
            f"{self.title=} "
            f"{self.event_type=} "
            f"{self.event_info=} "
            f"{self.participants=} "
            f"{self.date_created=} "
            f"{self.date_reviewed=} "
            f"reviewer={self.reviewer.id if self.reviewer is not None else None} "
            f"{self.review_actions=} "
            f"{self.creator.id=}"
            ">"
        )


class ImportEventReviewAction(Base, TableWithMaskedId):
    __tablename__ = "import_event_review_actions"

    import_event_id = cast(
        int, Column(IdInteger, ForeignKey("import_events.id"), nullable=False)
    )
    import_event = cast(
        ImportEvent,
        relationship(
            "ImportEvent",
            uselist=False,
        ),
    )

    # Temporary participant storage
    participant_json = cast(str, Column(JSON, nullable=False))

    @property
    def participant(self) -> SxContactCreateRequest:
        return SxContactCreateRequest(**json.loads(self.participant_json))

    @participant.setter
    def participant(self, value: SxContactCreateRequest):
        self.participant_json = value.json(exclude_none=True)

    # Decisions
    action = cast(
        ImportEventReviewActionType,
        Column(
            Enum(ImportEventReviewActionType),
            nullable=False,
        ),
    )

    # If matched with an existing contact, merge into this contact
    merge_into_contact_id = cast(
        Optional[int], Column(IdInteger, ForeignKey("contacts.id"), nullable=True)
    )
    merge_into_contact = cast(
        Optional["Contact"],
        relationship("Contact", uselist=False, foreign_keys=[merge_into_contact_id]),
    )

    # Represent merge changes (which fields were added, changed, etc)
    match_merge_json = cast(Optional[str], Column(JSON, nullable=True))

    @property
    def merge(self) -> Optional[SxImportReviewMerge]:
        if self.match_merge_json is not None:
            return SxImportReviewMerge(**json.loads(cast(str, self.match_merge_json)))
        else:
            return None

    @merge.setter
    def merge(self, value: Optional[SxImportReviewMerge]):
        self.match_merge_json = (
            value.json(exclude_none=True) if value is not None else None
        )

    # Why did we choose to automerge
    match_justification_json = cast(Optional[str], Column(JSON, nullable=True))

    @property
    def match_justification(self) -> Optional[SxImportEventMatchJustification]:
        if self.match_justification_json is not None:
            return SxImportEventMatchJustification(
                **json.loads(cast(str, self.match_justification_json))
            )
        else:
            return None

    @match_justification.setter
    def match_justification(self, value: Optional[SxImportEventMatchJustification]):
        self.match_justification_json = (
            value.json(exclude_none=True) if value is not None else None
        )

    def __repr__(self) -> str:
        merge_into_contact = (
            self.merge_into_contact.id if self.merge_into_contact is not None else None
        )
        return (
            "<"
            f"ImportEventReviewAction event={self.import_event.id} "
            f"id={self.id} "
            f"action={self.action.name} "
            f"merge_into_contact={merge_into_contact} "
            f"merge={self.match_merge_json}"
            ">"
        )


class PostalAddress(Base, TableWithMaskedId):
    __tablename__ = "postal_addresses"

    # Administrative columns
    date_created = cast(
        datetime,
        Column(
            UTCDateTime,
            nullable=False,
            default=func.now(),
        ),
    )
    source_created = cast(Optional[str], Column(StdString(), nullable=True))
    date_archived = cast(Optional[datetime], Column(UTCDateTime, nullable=True))
    reason_archived = cast(Optional[str], Column(StdText(), nullable=True))

    # Base data
    # eventually: revise if we get international addresses :)
    line1 = cast(Optional[str], Column(StdString(), nullable=True))
    line2 = cast(Optional[str], Column(StdString(), nullable=True))
    city = cast(Optional[str], Column(StdString(), nullable=True))
    admin_area = cast(Optional[str], Column(StdString(), nullable=True))
    postal_code = cast(str, Column(StdString(24), nullable=False))

    # Consent
    consent_to_mail = cast(bool, Column(StdBoolean(), nullable=False, default=False))

    # Aliases
    @property
    def street_address(self) -> Optional[str]:
        if self.line1 is None and self.line2 is None:
            return None

        return "\n".join(compact([self.line1, self.line2]))

    @property
    def full_address(self) -> Optional[str]:
        return "\n".join(
            compact(
                [
                    self.line1,
                    self.line2,
                    " ".join(compact([self.city, self.state, self.zipcode])),
                ]
            )
        )

    @property
    def state(self) -> Optional[str]:
        return self.admin_area

    @state.setter
    def state(self, value: Optional[str]):
        if value is None:
            self.admin_area = None
            return

        if value not in US_ADMIN_AREAS:
            if value.lower() not in US_ADMIN_AREAS_REVERSE:
                raise ValueError(
                    "value is not two letter abbreviation or full state name"
                )
            else:
                value = US_ADMIN_AREAS_REVERSE[value]

        self.admin_area = value

    @property
    def zipcode(self) -> str:
        return self.postal_code

    @zipcode.setter
    def zipcode(self, value: str):
        stripped = re.sub(r"\D", "", value)
        if not str.isnumeric(stripped):
            raise ValueError("value is not numeric ZIP code")
        if len(stripped) != 5 and len(stripped) != 9:
            raise ValueError("value is not ZIP or ZIP+4")

        self.postal_code = value

    # Relationships
    contact_id = cast(
        int, Column("contact_id", IdInteger, ForeignKey("contacts.id"), nullable=True)
    )
    contact = cast(
        "Contact",
        relationship(
            "Contact",
            uselist=False,
            back_populates="mailing_addresses",
        ),
    )

    def __repr__(self) -> str:
        return (
            "<"
            "PostalAddress "
            f"{self.id=} "
            f"{self.line1=} "
            f"{self.line2=} "
            f"{self.city=} "
            f"{self.zipcode=} "
            f"{self.state=} "
            f"{self.consent_to_mail=} "
            f"{self.date_created=} "
            f"{self.source_created=} "
            f"{self.date_archived=} "
            f"{self.reason_archived=} "
            f"contact={self.contact.id if self.contact is not None else None}"
            ">"
        )


class Contact(Base, TableWithMaskedId):
    __tablename__ = "contacts"

    # Administrative columns
    date_created = cast(
        datetime,
        Column(
            UTCDateTime,
            nullable=False,
            default=func.now(),
        ),
    )
    source_created = cast(Optional[str], Column(StdString(), nullable=True))
    date_archived = cast(Optional[datetime], Column(UTCDateTime, nullable=True))
    reason_archived = cast(Optional[str], Column(StdText(), nullable=True))

    # Data columns
    full_name = cast(str, Column(StdString(), nullable=False))
    display_name = cast(Optional[str], Column(StdString(), nullable=True))
    pronouns = cast(Optional[str], Column(StdString(), nullable=True))
    biography = cast(Optional[str], Column(StdText(), nullable=True))
    profile_pic = cast(Optional[str], Column(StdText(), nullable=True))
    admin_notes = cast(Optional[str], Column(StdText(), nullable=True))

    # Contact methods
    email_addresses = cast(
        List["EmailAddress"],
        relationship(
            "EmailAddress",
            back_populates="contact",
            cascade="all,delete,delete-orphan",
            lazy="selectin",
            order_by=lambda: [
                desc(cast(Column, EmailAddress.date_created)),
                desc(cast(Column, EmailAddress.primary)),
            ],
        ),
    )

    phone_numbers = cast(
        List[PhoneNumber],
        relationship(
            "PhoneNumber",
            back_populates="contact",
            cascade="all,delete,delete-orphan",
            lazy="selectin",
            order_by=lambda: [
                desc(cast(Column, PhoneNumber.date_created)),
                desc(cast(Column, PhoneNumber.primary)),
            ],
        ),
    )

    mailing_addresses = cast(
        List[PostalAddress],
        relationship(
            "PostalAddress",
            back_populates="contact",
            cascade="all,delete,delete-orphan",
            lazy="selectin",
            order_by=lambda: desc(cast(Column, PostalAddress.date_created)),
        ),
    )

    @property
    def most_recent_valid_address(self) -> Optional[PostalAddress]:
        if len(self.mailing_addresses) == 0:
            return None

        most_recent_address = self.mailing_addresses[0]
        if most_recent_address.date_archived is not None:
            return None

        return most_recent_address

    @property
    def city(self) -> Optional[str]:
        addr = self.most_recent_valid_address
        if addr is None:
            return None

        return addr.city

    @property
    def zipcode(self) -> Optional[str]:
        addr = self.most_recent_valid_address
        if addr is None:
            return None

        return addr.zipcode

    # Tags
    tag_associations = cast(
        List["ContactTagAssociation"],
        relationship(
            "ContactTagAssociation", back_populates="contact", lazy="selectin"
        ),
    )
    tags = cast(List["Tag"], association_proxy("tag_associations", "tag"))

    # Custom fields
    custom_field_values = cast(
        List["ContactCustomFieldAssociation"],
        relationship(
            "ContactCustomFieldAssociation",
            back_populates="contact",
            cascade="all,delete,delete-orphan",
            lazy="selectin",
        ),
    )
    custom_fields = cast(
        List["CustomFieldDefinition"],
        association_proxy("custom_field_values", "custom_field"),
    )

    # User
    user = cast(
        Optional["User"],
        relationship(
            "User",
            uselist=False,
            back_populates="contact",
            cascade="all,delete,delete-orphan",
            lazy="selectin",
        ),
    )
    security_principal = cast(
        Optional["SecurityPrincipal"], association_proxy("user", "principal")
    )

    def __repr__(self) -> str:
        return (
            "<"
            "Contact "
            f"{self.id=} "
            f"{self.full_name=} "
            f"{self.display_name=} "
            f"{self.pronouns=} "
            f"{self.biography=} "
            f"{self.profile_pic=} "
            f"{self.admin_notes=} "
            f"email_addresses=[{[v.id for v in self.email_addresses]}] "
            f"phone_numbers=[{[v.id for v in self.phone_numbers]}] "
            f"mailing_addresses=[{[v.id for v in self.mailing_addresses]}] "
            f"{self.most_recent_valid_address=} "
            f"{self.city=} "
            f"{self.zipcode=} "
            f"tag_associations=[{[v.id for v in self.tag_associations]}] "
            f"tags=[{[v.id for v in self.tags]}] "
            f"custom_field_values=[{[v.id for v in self.custom_field_values]}] "
            f"custom_fields=[{[v.id for v in self.custom_fields]}] "
            f"{self.user=} "
            f"{self.security_principal=} "
            f"{self.date_created=} "
            f"{self.source_created=} "
            f"{self.date_archived=} "
            f"{self.reason_archived=}"
            ">"
        )


class EmailAddress(Base, TableWithMaskedId):
    __tablename__ = "email_addresses"

    # Administrative columns
    date_created = cast(
        datetime,
        Column(
            UTCDateTime,
            nullable=False,
            default=func.now(),
        ),
    )
    date_confirmed = cast(Optional[datetime], Column(UTCDateTime, nullable=True))
    date_archived = cast(Optional[datetime], Column(UTCDateTime, nullable=True))
    reason_archived = cast(Optional[str], Column(StdText(), nullable=True))

    _raw_value = cast(str, Column("value", StdString(), nullable=False, unique=True))
    primary = cast(bool, Column(StdBoolean(), nullable=False, default=False))

    consent_to_email = cast(bool, Column(StdBoolean(), nullable=False, default=False))
    consent_to_share = cast(bool, Column(StdBoolean(), nullable=False, default=False))

    contact_id = cast(
        int, Column("contact_id", IdInteger, ForeignKey("contacts.id"), nullable=False)
    )
    contact = cast(
        Contact,
        relationship(
            "Contact",
            uselist=False,
            back_populates="email_addresses",
        ),
    )

    @property
    def value(self) -> str:
        return self._raw_value

    @value.setter
    def value(self, input: str):
        self._raw_value = EmailAddress.normalize(input)

    @staticmethod
    def normalize(email: str) -> str:
        splitted = email.split("@")
        if len(splitted) != 2:
            raise ValueError('Invalid email address: "{}"'.format(email))

        local, host = splitted

        # only normalize by lowercasing the domain (since that portion
        # is case insensitive according to RFC 2822)
        return "".join([local, "@", host.lower()])

    def __repr__(self) -> str:
        return (
            "<"
            "EmailAddress "
            f"{self.id=} "
            f"{self.value=} "
            f"{self.primary=} "
            f"{self.consent_to_email=} "
            f"{self.consent_to_share=} "
            f"{self.date_created=} "
            f"{self.date_confirmed=} "
            f"{self.date_archived=} "
            f"{self.reason_archived=} "
            f"{self.contact.id=}"
            ">"
        )


class CustomFieldDefinition(Base, TableWithMaskedId):
    __tablename__ = "custom_fields"

    date_created = Column(
        UTCDateTime,
        nullable=False,
        default=func.now(),
    )

    tag_associations = cast(
        List["TagFieldAssociation"],
        relationship(
            "TagFieldAssociation",
            back_populates="custom_field",
            cascade="all,delete,delete-orphan",
        ),
    )
    tags = cast(Optional[List["Tag"]], association_proxy("tag_associations", "tag"))
    values = cast(
        List["ContactCustomFieldAssociation"],
        relationship(
            "ContactCustomFieldAssociation",
            back_populates="custom_field",
            cascade="all,delete,delete-orphan",
        ),
    )
    contacts = cast(Optional[List[Contact]], association_proxy("values", "contact"))
    contact_ids = cast(Optional[List[int]], association_proxy("values", "contact_id"))

    name = cast(str, Column(StdString(), nullable=False))
    description = cast(Optional[str], Column(StdText(), nullable=True))
    field_type = cast(CustomFieldType, Column(Enum(CustomFieldType), nullable=False))
    field_options_json = cast(Optional[str], Column(JSON, nullable=True))

    @property
    def field_options(self) -> Optional[SxCustomFieldOptionsUnion]:
        if self.field_options_json is not None:
            loaded = json.loads(cast(str, self.field_options_json))
            if self.field_type == CustomFieldType.choice:
                return SxCustomFieldChoiceOptions(**loaded)
            elif self.field_type == CustomFieldType.range:
                return SxCustomFieldRangeOptions(**loaded)
            else:
                raise ValueError("Unknown field options type")
        else:
            return None

    @field_options.setter
    def field_options(self, field_options: Optional[SxCustomFieldOptionsUnion]) -> None:
        self.field_options_json = (
            field_options.json(exclude_none=True) if field_options is not None else None
        )

    def __repr__(self) -> str:
        return (
            "<"
            "CustomFieldDefinition "
            f"{self.id=} "
            f"{self.name=} "
            f"{self.description=} "
            f"{self.field_type=} "
            f"{self.field_options=} "
            f"{self.date_created=} "
            f"tag_associations=[{[v.id for v in self.tag_associations]}] "
            f"tags=[{[v.id for v in self.tags] if self.tags is not None else None}] "
            f"values=[{[v.id for v in self.values]}] "
            f"contacts=[{[v.id for v in self.contacts] if self.contacts is not None else None}]"
            ">"
        )


class ContactCustomFieldAssociation(Base):
    __tablename__ = "contact_custom_fields"

    date_created = cast(
        datetime,
        Column(
            UTCDateTime,
            nullable=False,
            default=func.now(),
        ),
    )

    custom_field_id = cast(
        int,
        Column(
            IdInteger, ForeignKey("custom_fields.id"), nullable=False, primary_key=True
        ),
    )
    custom_field = cast(
        CustomFieldDefinition,
        relationship(
            "CustomFieldDefinition",
            uselist=False,
            back_populates="values",
            lazy="selectin",
        ),
    )
    contact_id = cast(
        int,
        Column(IdInteger, ForeignKey("contacts.id"), nullable=False, primary_key=True),
    )
    contact = cast(
        Contact,
        relationship("Contact", uselist=False, back_populates="custom_field_values"),
    )

    value_json = cast(str, Column(JSON, nullable=False))

    @property
    def value(self) -> Optional[Any]:
        if self.value_json is not None:
            return json.loads(cast(str, self.value_json))
        else:
            return None

    @value.setter
    def value(self, value: Optional[Any]):
        self.value_json = json.dumps(value)

    def __repr__(self) -> str:
        return (
            "<"
            "ContactCustomFieldAssociation "
            f"{self.id=} "
            f"{self.custom_field.id=} "
            f"{self.contact.id=} "
            f"{self.value=}"
            ">"
        )


class Tag(Base, TableWithMaskedId):
    __tablename__ = "tags"

    name = cast(str, Column(StdString(), unique=True, index=True))
    date_created = cast(
        datetime, Column(UTCDateTime, nullable=False, default=func.now())
    )

    member_associations = cast(
        List["TagAssociation"],
        relationship(
            "TagAssociation", back_populates="tag", cascade="all,delete,delete-orphan"
        ),
    )
    contact_associations = cast(
        List["ContactTagAssociation"],
        relationship(
            "ContactTagAssociation",
            back_populates="tag",
            cascade="all,delete,delete-orphan",
        ),
    )
    members = cast(List[Member], association_proxy("member_associations", "member"))
    contacts = cast(List[Contact], association_proxy("contact_associations", "contact"))
    custom_fields = cast(
        List["TagFieldAssociation"],
        relationship(
            "TagFieldAssociation",
            back_populates="tag",
            cascade="all,delete,delete-orphan",
        ),
    )

    def __repr__(self) -> str:
        return "<" "Tag " f"{self.id=} " f"{self.name=} " f"{self.date_created=} " ">"


class TagAssociation(Base):
    __tablename__ = "member_tags"

    date_added = Column(UTCDateTime, nullable=False, default=func.now())

    member_id = Column(Integer, ForeignKey("members.id"), primary_key=True)
    member = relationship(
        "Member", uselist=False, lazy="selectin", back_populates="tag_associations"
    )
    tag_id = Column(IdInteger, ForeignKey("tags.id"), primary_key=True)
    tag = relationship(
        "Tag", uselist=False, back_populates="member_associations", lazy="selectin"
    )


class ContactTagAssociation(Base):
    __tablename__ = "contact_tags"

    date_created = Column(UTCDateTime, nullable=False, default=func.now())

    contact_id = cast(
        int,
        Column(IdInteger, ForeignKey("contacts.id"), nullable=False, primary_key=True),
    )
    contact = cast(
        Contact,
        relationship(
            "Contact", uselist=False, lazy="selectin", back_populates="tag_associations"
        ),
    )
    tag_id = cast(
        int, Column(IdInteger, ForeignKey("tags.id"), nullable=False, primary_key=True)
    )
    tag = cast(
        Tag,
        relationship(
            "Tag", uselist=False, back_populates="contact_associations", lazy="selectin"
        ),
    )


class TagFieldAssociation(Base):
    __tablename__ = "tag_custom_fields"

    date_created = Column(
        UTCDateTime,
        nullable=False,
        default=func.now(),
    )

    tag_id = cast(
        int, Column(IdInteger, ForeignKey("tags.id"), primary_key=True, nullable=False)
    )
    tag = cast(
        Tag,
        relationship(
            "Tag", uselist=False, back_populates="custom_fields", lazy="selectin"
        ),
    )
    custom_field_id = cast(
        int,
        Column(
            IdInteger, ForeignKey("custom_fields.id"), primary_key=True, nullable=False
        ),
    )
    custom_field = cast(
        CustomFieldDefinition,
        relationship(
            "CustomFieldDefinition",
            uselist=False,
            back_populates="tag_associations",
            lazy="selectin",
        ),
    )


class User(Base, TableWithMaskedId):
    __tablename__ = "users"

    date_created = Column(
        UTCDateTime,
        nullable=False,
        default=func.now(),
    )

    contact_id = cast(int, Column(IdInteger, ForeignKey("contacts.id"), nullable=False))
    contact = cast(
        "Contact",
        relationship("Contact", uselist=False, back_populates="user", lazy="selectin"),
    )

    principal_id = cast(
        int, Column(IdInteger, ForeignKey("security_principals.id"), nullable=False)
    )
    principal = cast(
        "SecurityPrincipal",
        relationship(
            "SecurityPrincipal", uselist=False, back_populates="user", lazy="selectin"
        ),
    )

    def __repr__(self) -> str:
        return (
            "<"
            "User "
            f"{self.id=} "
            f"{self.contact.id=} "
            f"{self.principal.id=} "
            ">"
        )


class SecurityPrincipalRole(Base, TableWithMaskedId):
    __tablename__ = "security_principal_roles"

    date_created = Column(
        UTCDateTime,
        nullable=False,
        default=func.now(),
    )

    slug = cast(str, Column(StdString(), unique=True, nullable=False))
    name = cast(str, Column(StdString(), nullable=False))
    description = cast(str, Column(StdText(), nullable=False, default=""))
    autogenerated = cast(bool, Column(StdBoolean(), nullable=False))

    # association
    security_principal_associations = cast(
        List["SecurityPrincipalRoleAssociation"],
        relationship(
            "SecurityPrincipalRoleAssociation",
            uselist=True,
            back_populates="role",
        ),
    )
    security_principals = cast(
        List["SecurityPrincipal"],
        association_proxy("security_principal_associations", "security_principal"),
    )

    # needs
    needs_json = cast(str, Column(JSON, nullable=False))

    @property
    def needs(self) -> SxRoleNeedsList:
        return SxRoleNeedsList.parse_raw(
            cast(str, self.needs_json), proto=Protocol.json
        )

    @needs.setter
    def needs(self, value: SxRoleNeedsList):
        self.needs_json = value.json(exclude_none=True)

    def __repr__(self) -> str:
        return (
            "<"
            "SecurityPrincipalRole "
            f"{self.id=} "
            f"{self.slug=} "
            f"{self.name=} "
            f"{self.description=}"
            f"{self.needs_json=}"
            ">"
        )


class SecurityPrincipal(Base, TableWithMaskedId):
    __tablename__ = "security_principals"

    date_created = Column(
        UTCDateTime,
        nullable=False,
        default=func.now(),
    )

    user = cast(
        Optional[User],
        relationship(
            "User", uselist=False, back_populates="principal", lazy="selectin"
        ),
    )

    member = cast(
        Optional["Member"],
        relationship(
            "Member", uselist=False, back_populates="principal", lazy="selectin"
        ),
    )

    # roles
    role_associations = cast(
        List["SecurityPrincipalRoleAssociation"],
        relationship(
            "SecurityPrincipalRoleAssociation",
            uselist=True,
            back_populates="security_principal",
        ),
    )
    roles = cast(
        List["SecurityPrincipalRole"], association_proxy("role_associations", "role")
    )

    def __repr__(self) -> str:
        return (
            "<"
            "SecurityPrincipal "
            f"{self.id=} "
            f"{self.user=} "
            f"{self.member=} "
            ">"
        )


class SecurityPrincipalRoleAssociation(Base):
    __tablename__ = "security_principal_role_associations"

    date_created = Column(
        UTCDateTime,
        nullable=False,
        default=func.now(),
    )

    # role
    role_id = cast(
        int,
        Column(
            IdInteger,
            ForeignKey("security_principal_roles.id"),
            nullable=False,
            primary_key=True,
        ),
    )
    role = cast(
        SecurityPrincipalRole,
        relationship(
            "SecurityPrincipalRole",
            uselist=False,
            back_populates="security_principal_associations",
        ),
    )

    # security principal
    security_principal_id = cast(
        int,
        Column(
            IdInteger,
            ForeignKey("security_principals.id"),
            nullable=False,
            primary_key=True,
        ),
    )
    security_principal = cast(
        "SecurityPrincipal",
        relationship(
            "SecurityPrincipal", uselist=False, back_populates="role_associations"
        ),
    )


class SettingEntry(Base):
    __tablename__ = "settings"

    key = cast(str, Column(StdString(), unique=True, nullable=False, primary_key=True))
    value_json = cast(str, Column(JSON, nullable=False))

    @property
    def value(self) -> Any:
        return json.loads(self.value_json)

    @value.setter
    def value(self, value: Any):
        self.value_json = json.dumps(value)
