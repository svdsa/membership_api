from typing import List

from membership.models import (
    MergeMemberFlaggedEligibleVoter,
    MergeMemberFlaggedProxyToken,
)


class MergeMemberFlags:

    def __init__(
            self,
            flagged_eligible_voters: List[MergeMemberFlaggedEligibleVoter],
            flagged_proxy_tokens: List[MergeMemberFlaggedProxyToken]):
        self.flagged_eligible_voters = flagged_eligible_voters
        self.flagged_proxy_tokens = flagged_proxy_tokens
