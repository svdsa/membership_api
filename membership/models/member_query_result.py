from typing import List, Optional

from membership.models import MemberAsEligibleToVote


class MemberQueryResult:
    def __init__(
        self,
        members: List[MemberAsEligibleToVote],
        cursor: Optional[int],
        has_more: bool,
    ):
        self.members = members
        self.cursor = cursor
        self.has_more = has_more
