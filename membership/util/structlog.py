import logging
import sys

import structlog
from flask import Flask
from structlog.processors import CallsiteParameter

import config.logging_config


def init():
    """Initialize structlog and stdlib logging. Call before any logging is done (e.g. in main())."""
    # Configure for basic 12-factor logging (output JSON to stdout via structlog)
    logging.basicConfig(
        stream=sys.stdout,
        level=logging.INFO,
        format="%(message)s",
    )

    processors = [
        # ============================ NOTABLE ============================
        # Add threadlocal variables (per Flask request) to event dict.
        structlog.threadlocal.merge_threadlocal,
        # If log level is too low, abort pipeline and throw away log entry.
        structlog.stdlib.filter_by_level,
        # Add the name of the logger to event dict.
        structlog.stdlib.add_logger_name,
        # Add log level to event dict.
        structlog.stdlib.add_log_level,
        # Perform %-style formatting.
        structlog.stdlib.PositionalArgumentsFormatter(),
        # Add a timestamp in ISO 8601 format.
        structlog.processors.TimeStamper(fmt="iso"),
        # If the "stack_info" key in the event dict is true, remove it and
        # render the current stack trace in the "stack" key.
        structlog.processors.StackInfoRenderer(),
        # If the "exc_info" key in the event dict is either true or a
        # sys.exc_info() tuple, remove "exc_info" and render the exception
        # with traceback into the "exception" key.
        structlog.processors.format_exc_info,
        # If some value is in bytes, decode it to a unicode str.
        structlog.processors.UnicodeDecoder(),
        # Add callsite parameters.
        structlog.processors.CallsiteParameterAdder(
            [
                CallsiteParameter.FILENAME,
                CallsiteParameter.FUNC_NAME,
                CallsiteParameter.LINENO,
            ]
        ),
    ]

    if config.logging_config.NON_PRODUCTION:
        processors.append(structlog.dev.ConsoleRenderer())
    else:
        processors.append(structlog.processors.JSONRenderer())

    print("structlog setup", config.logging_config.PRODUCTION)

    structlog.configure(
        processors=processors,
        # `wrapper_class` is the bound logger that you get back from
        # get_logger(). This one imitates the API of `logging.Logger`.
        wrapper_class=structlog.stdlib.BoundLogger,
        # `logger_factory` is used to create wrapped loggers that are used for
        # OUTPUT. This one returns a `logging.Logger`. The final value (a JSON
        # string) from the final processor (`JSONRenderer`) will be passed to
        # the method of the same name as that you've called on the bound logger.
        logger_factory=structlog.stdlib.LoggerFactory(),
        # Effectively freeze configuration after creating the first bound
        # logger.
        cache_logger_on_first_use=config.logging_config.PRODUCTION,
    )


def generate_request_id() -> str:
    import nanoid

    # 2 year probability of collision at 1000 r/s
    # https://zelark.github.io/nano-id-cc/
    alphabet = "23456789BCDFGHJKLMNPQRSTVWXYZ"
    output = nanoid.generate(alphabet, 16)
    chunk_size = 4
    return "portalrequest_" + "-".join(
        output[i : i + chunk_size] for i in range(0, len(output), chunk_size)
    )


def register_flask_before_request(app: Flask):
    if app is None:
        raise RuntimeError("register_flask_before_request() called with app=None")

    def middleware():
        from flask import request

        structlog.threadlocal.clear_threadlocal()

        request_id = generate_request_id()
        structlog.threadlocal.bind_threadlocal(
            path=request.path,
            request_id=request_id,
            peer=request.access_route[0],
        )

    app.before_request(middleware)
