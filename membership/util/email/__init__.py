import logging
import re
from typing import Dict, List, Optional, Tuple
from urllib.parse import urljoin

import membership
import pkg_resources
import pytz
from config import (
    CHAPTER_ADDRESS,
    CHAPTER_BANNER_IMAGE_URL,
    CHAPTER_ID,
    CHAPTER_NAME,
    CHAPTER_SHORT_NAME,
    PORTAL_URL,
    SUPPORT_EMAIL_ADDRESS,
)
from membership.database.models import (
    Meeting,
    MeetingInvitation,
    Member,
    ProxyToken,
    ProxyTokenState,
)

from .email_connector import EmailConnector
from .mailgun_email_connector import MailgunEmailConnector
from .mailjet_email_connector import MailjetEmailConnector
from .smtp_email_connector import SmtpEmailConnector


def get_email_connector(connector_name: str):
    """Return an instance of the `EmailConnector` implementation specified by
    `config.EMAIL_CONNECTOR`."""

    if connector_name == "mailgun":
        return MailgunEmailConnector()
    elif connector_name == 'mailjet':
        return MailjetEmailConnector()
    elif connector_name == 'smtp':
        return SmtpEmailConnector()
    elif connector_name == "noop":
        return EmailConnector()
    else:
        logging.warning(
            f'`config.EMAIL_CONNECTOR` is set to an unsupported value {connector_name}. '
            'Defaulting to "mailgun". Please configure the environment to set this value to'
            '"mailgun", "mailjet", or "noop". '
            'In the future, an unset value will be an error. '
        )
        return MailgunEmailConnector()


WELCOME_TEMPLATE_BY_ID: Dict[str, str] = {
    'san-francisco': 'templates/sf_welcome_email.html',
    'silicon-valley': 'templates/generic_welcome_email.html',
}

WELCOME_VERIFY_TEMPLATE_BY_ID: Dict[str, str] = {
    'san-francisco': 'templates/sf_welcome_email_verification.html',
    'silicon-valley': 'templates/generic_welcome_email_verification.html',
}


def _get_welcome_template_for_id(*, chapter_id: str, verify_url: str) -> str:
    if verify_url is None:
        return WELCOME_TEMPLATE_BY_ID.get(
            chapter_id, 'templates/generic_welcome_email_verification.html'
        )
    else:
        return WELCOME_VERIFY_TEMPLATE_BY_ID.get(
            chapter_id, 'templates/generic_welcome_email_verification.html'
        )


def send_welcome_email(
    email_connector: EmailConnector,
    member: Member,
    verify_url: str,
):
    # send a welcome email with the email verification / password reset link from Auth0
    sender_name = CHAPTER_NAME
    sender_email = SUPPORT_EMAIL_ADDRESS

    filename = _get_welcome_template_for_id(
        chapter_id=CHAPTER_ID, verify_url=verify_url
    )
    template = (
        pkg_resources.resource_string(membership.__name__, filename)
        .decode("utf-8")
        .format(PORTAL_URL=PORTAL_URL)
    )

    recipient_variables = {
        member: {
            'name': member.first_name,
            'link': verify_url,
            'chapter_name': CHAPTER_NAME,
            'chapter_short_name': CHAPTER_SHORT_NAME,
        }
    }
    email_connector.send_member_emails(
        sender_name=sender_name,
        sender_email=sender_email,
        subject=f'Welcome to the {CHAPTER_SHORT_NAME} Membership Portal, %recipient.name%!',
        email_template=template,
        recipient_variables=recipient_variables,
    )


def _generate_confirm_link(token: str) -> str:
    return urljoin(PORTAL_URL, f'/verify-email/{token}')


def send_meeting_confirmation_email(
    *,
    email_connector: EmailConnector,
    member: Member,
    meeting: Meeting,
    token: str,
    email_address_override: Optional[str] = None,
    greeting: str = "Great to meet you",
):
    sender_name = CHAPTER_NAME
    sender_email = SUPPORT_EMAIL_ADDRESS
    filename = 'templates/confirm_attendance.html'
    template = pkg_resources.resource_string(membership.__name__, filename).decode(
        'utf-8'
    )

    subject = f"Please confirm your attendance at {meeting.name}"

    recipient_variables = {
        'greeting': greeting,
        'name': member.first_name,
        'meeting_name': meeting.name,
        'meeting_date': meeting.start_time.astimezone(
            tz=pytz.timezone('US/Pacific')  # HACK TODO: Assume Pacific timezone for now
        ).strftime('%A, %B %d, %Y at %I:%M %p'),
        'chapter_name': CHAPTER_NAME,
        'chapter_address': CHAPTER_ADDRESS,
        'confirm_link': _generate_confirm_link(token),
        'support_email': SUPPORT_EMAIL_ADDRESS,
        'chapter_banner_image_url': CHAPTER_BANNER_IMAGE_URL,
    }

    if email_address_override is None:
        email_connector.send_member_emails(
            sender_name=sender_name,
            sender_email=sender_email,
            subject=subject,
            email_template=template,
            recipient_variables={member: recipient_variables},
            add_unsubscribe_link=False,
        )
    else:
        email_connector.send_emails(
            sender_name=sender_name,
            sender_email=sender_email,
            subject=subject,
            email_template=template,
            recipient_variables={email_address_override: recipient_variables},
            to_emails=[email_address_override],
        )


def send_committee_request_email(
    email_connector: EmailConnector,
    name: str,
    member_email: str,
    email: str,
):
    sender_name = 'DSA SF Tech Committee'
    sender_email = SUPPORT_EMAIL_ADDRESS

    # Something weird is happening where the field is
    # not being populated via the recipient variables on prod only.
    # To deal with this the template is inlined here.
    template = f"""
    <html>
    <body>
    <p>Dear co-chairs,</p>
    <p>A member, {name}, is requesting to be added to your list of active members.</p>
    <p>If this member is active in your committee, please go to your committee's
    page in the <a href="{PORTAL_URL}">DSA SF Membership Portal</a> and mark them as active.</p>
    <p>If this is an incorrect request, you can ignore this or
    contact the member at {member_email}.</p>
    <p>If you have any questions, please email
    <a href="mailto:tech@dsasf.org">tech@dsasf.org</a></p>
    <p>Solidarity,</p>
    <p>DSA SF Tech</p>
    </body>
    </html>"""
    recipient_variables = {email: {"name": name}}
    email_connector.send_emails(
        sender_name=sender_name,
        sender_email=sender_email,
        subject='A member is requesting membership in your committee',
        email_template=template,
        recipient_variables=recipient_variables,
        to_emails=[email],
    )


def send_proxy_nomination_action(
    email_connector: EmailConnector,
    meeting: Meeting,
    nominating_member: Member,
    receiving_member: Member,
    proxy_token: ProxyToken,
):
    sender_name = 'DSA SF Tech Committee'
    sender_email = SUPPORT_EMAIL_ADDRESS

    verb = proxy_token.state
    if verb == ProxyTokenState.ACCEPTED:
        filename = "templates/proxy_nomination_accepted.html"
    else:
        filename = 'templates/proxy_nomination_rejected.html'
    template = (
        pkg_resources.resource_string(membership.__name__, filename)
        .decode("utf-8")
        .format(PORTAL_URL=PORTAL_URL)
    )

    recipient_variables = {
        nominating_member.email_address: {
            "name": nominating_member.first_name,
            "actor_name": receiving_member.first_name,
            "meeting_name": meeting.name,
            "verb": verb.friendly_name,
            "token_link": proxy_token.url,
        },
    }
    subject = (
        "%recipient.actor_name% has %recipient.verb%"
        " your proxy nomination for %recipient.meeting_name%"
    )
    email_connector.send_emails(
        sender_name=sender_name,
        sender_email=sender_email,
        subject=subject,
        email_template=template,
        recipient_variables=recipient_variables,
        to_emails=[nominating_member.email_address],
    )


def send_meeting_invitation_emails(
    email_connector: EmailConnector,
    invitations: List[MeetingInvitation],
) -> None:
    sender_name = 'DSA SF Tech Committee'
    sender_email = SUPPORT_EMAIL_ADDRESS
    filename = 'templates/meeting_invitation.html'
    template = (
        pkg_resources.resource_string(membership.__name__, filename)
        .decode("utf-8")
        .format(PORTAL_URL=PORTAL_URL)
    )
    recipient_variables = {}
    email_addresses = []
    for invitation in invitations:
        recipient_variables[invitation.member.email_address] = {
            "name": invitation.member.name,
            "meeting_name": invitation.meeting.name,
            "meeting_time": invitation.meeting.start_time.astimezone(
                tz=pytz.timezone('US/Pacific')
            ).strftime('%A, %B %d, %Y at %I:%M %p %Z'),
            "rsvp_url": invitation.rsvp_url,
        }
        email_addresses.append(invitation.member.email_address)
    email_connector.send_emails(
        sender_name=sender_name,
        sender_email=sender_email,
        subject="You've been invited to %recipient.meeting_name%",
        email_template=template,
        recipient_variables=recipient_variables,
        to_emails=email_addresses,
    )


EMAIL_GROUP_REGEX = r"([^<>]*?) <([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,})>"


def parse_combined_sender(sender: str) -> Tuple[Optional[str], Optional[str]]:
    matched = re.match(EMAIL_GROUP_REGEX, sender)
    if matched is None:
        return None, None

    groups = matched.groups()
    if len(groups) < 2:
        return None, None

    return groups[0], groups[1]
