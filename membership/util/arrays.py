from typing import Any, List, Optional, TypeVar, cast

TCompact = TypeVar("TCompact")
TFlatten = TypeVar("TFlatten")
THead = TypeVar("THead")


# There doesn't seem to be a way to exclude None from the list type...
def compact(lst: List[Optional[TCompact]]) -> List[TCompact]:
    return [e for e in lst if e is not None]


def flatten(alist: List[Any]) -> List[Any]:
    for item in alist:
        if isinstance(item, list):
            for subitem in cast(List[Any], item):
                yield subitem
        else:
            yield cast(Any, item)


def head(lst: List[THead]) -> Optional[THead]:
    """Return the first element of a list, or None if the list is empty"""
    if len(lst) < 1:
        return None
    else:
        return lst[0]


def tail(lst: List[THead]) -> Optional[THead]:
    """Return the last element of a list, or None if the list is empty"""
    if len(lst) < 1:
        return None
    else:
        return lst[-1]
