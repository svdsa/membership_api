from dataclasses import dataclass
from typing import Any, Callable, Generic, List, Optional, TypeVar, cast

from sqlalchemy import orm
from sqlalchemy.sql.expression import select
from sqlalchemy.sql.schema import Column

from membership.database.base import Base
from membership.util.arrays import compact

DEFAULT_PAGE_LIMIT = 10


class PaginationMultipleCursorsForbiddenError(Exception):
    starting_after: str
    ending_before: str

    def __init__(self, *, starting_after: str, ending_before: str) -> None:
        self.starting_after = starting_after
        self.ending_before = ending_before
        super().__init__()


class PaginationCursorFunctionRequiredError(NotImplementedError):
    pass


class PaginationCursorNotFoundError(Exception):
    pass


TElem = TypeVar("TElem")
TStr = TypeVar("TStr", bound=str)


@dataclass
class PaginatedList(Generic[TElem]):
    list: List[TElem]
    has_more: bool
    next_cursor: Optional[str]
    prev_cursor: Optional[str]

    @staticmethod
    def new(
        data: List[TElem],
        *,
        limit: int = DEFAULT_PAGE_LIMIT,
        has_more: Optional[bool] = None,
        ending_before: Optional[str] = None,
        starting_after: Optional[str] = None,
        cursor_fn: Optional[Callable[[TElem], str]] = None,
    ) -> "PaginatedList[TElem]":
        if (
            ending_before is not None or starting_after is not None
        ) and cursor_fn is None:
            raise PaginationCursorFunctionRequiredError()

        has_more = has_more or len(data) > limit
        last_limit_elems = data[len(data) - limit :]
        first_limit_elems = data[:limit]
        output_list = last_limit_elems if ending_before else first_limit_elems
        next_cursor = output_list[limit - 1] if has_more else None
        prev_cursor = (
            output_list[0]
            if len(data) > 0
            and (
                starting_after is not None
                or (ending_before is not None and len(data) > limit)
            )
            else None
        )
        return PaginatedList(
            list=output_list,
            has_more=has_more,
            next_cursor=cursor_fn(next_cursor)
            if next_cursor is not None and cursor_fn is not None
            else None,
            prev_cursor=cursor_fn(prev_cursor)
            if prev_cursor is not None and cursor_fn is not None
            else None,
        )


TPagable = TypeVar("TPagable", bound=Base)


def list_elements_created_before_reference(
    session: orm.Session,
    cls: Base,
    *,
    secondary_sort_columns: List[Column],
    limit: int,
    where: Optional[List[Any]] = None,
    options: Optional[List[Any]] = None,
    reference: TPagable,
) -> List[TPagable]:
    assert getattr(cls, "date_created") is not None

    query = (
        select(cls)
        .where(cls._raw_id < reference._raw_id)
        .order_by(
            cast(Column, cls.date_created).desc(),
            *[cast(Column, column).desc() for column in secondary_sort_columns],
        )
        .limit(limit)
    )

    if where is not None:
        for clause in where:
            query = query.where(clause)

    if options is not None:
        query = query.options(options)

    results = list(session.execute(query).scalars().all())

    return results


def list_elements_created_after_reference(
    session: orm.Session,
    cls: Base,
    *,
    secondary_sort_columns: List[Column],
    limit: int,
    where: Optional[List[Any]] = None,
    options: Optional[List[Any]] = None,
    reference: TPagable,
) -> List[TPagable]:
    assert getattr(cls, "date_created") is not None

    query = (
        select(cls)
        .where(cls._raw_id > reference._raw_id)
        .order_by(
            cast(Column, cls.date_created).asc(),
            *[cast(Column, column).asc() for column in secondary_sort_columns],
        )
        .limit(limit)
    )

    if where is not None:
        for clause in where:
            query = query.where(clause)

    if options is not None:
        query = query.options(options)

    results = list(session.execute(query).scalars().all()[::-1])

    return results


def list_elements(
    session: orm.Session,
    cls: Base,
    *,
    from_id_fn: Callable[[orm.Session, TStr], Optional[TPagable]],
    secondary_sort_columns: Optional[List[Column]] = None,
    limit: int = DEFAULT_PAGE_LIMIT,
    starting_after: Optional[str] = None,
    ending_before: Optional[str] = None,
    where: Optional[List[Any]] = None,
    options: Optional[List[Any]] = None,
) -> List[TPagable]:
    assert getattr(cls, "date_created") is not None

    if starting_after is not None and ending_before is not None:
        raise PaginationMultipleCursorsForbiddenError(
            starting_after=starting_after, ending_before=ending_before
        )

    where = compact(where) if where is not None else None
    if secondary_sort_columns is None:
        if getattr(cls, "_raw_id") is not None:
            secondary_sort_columns = [cast(Column, cls._raw_id)]
        elif getattr(cls, "id") is not None:
            secondary_sort_columns = [cast(Column, cls.id)]
        else:
            raise ValueError(
                "DB model doesn't appear to have an id column; can't infer secondary sort column"
            )

    if starting_after or ending_before:
        # after or before refers to sort order in the list (reverse chronological)
        # not absolute chronological order in time
        # i.e. `starting_after` refers to elements that were added BEFORE the reference
        # and  `ending_before` refers to elements that were added AFTER the reference
        reference_element = from_id_fn(
            session, cast(str, starting_after or ending_before)
        )

        if reference_element is None:
            raise PaginationCursorNotFoundError()

        if starting_after:
            # we want to get events that were EARLIER than the reference
            return cast(
                List[TPagable],
                list_elements_created_before_reference(
                    session,
                    cls,
                    secondary_sort_columns=secondary_sort_columns,
                    where=where,
                    limit=limit,
                    options=options,
                    reference=reference_element,
                ),
            )
        elif ending_before:
            # we want to get events that were LATER than the reference
            return cast(
                List[TPagable],
                list_elements_created_after_reference(
                    session,
                    cls,
                    secondary_sort_columns=secondary_sort_columns,
                    where=where,
                    limit=limit,
                    options=options,
                    reference=reference_element,
                ),
            )
        else:
            raise NotImplementedError("invalid state for listing elements")

    query = (
        select(cls)
        .order_by(
            cast(Column, cls.date_created).desc(),
            *[cast(Column, column).desc() for column in secondary_sort_columns],
        )
        .limit(limit)
    )

    if where is not None:
        for clause in where:
            query = query.where(clause)

    if options is not None:
        query = query.options(options)

    # TODO `query.options` is valid, waiting on improved typings
    return session.execute(query).scalars().all()


def list_pagable_elements(
    session: orm.Session,
    cls: Base,
    *,
    from_id_fn: Callable[[orm.Session, TStr], Optional[TPagable]],
    secondary_sort_columns: Optional[List[Column]] = None,
    limit: int = DEFAULT_PAGE_LIMIT,
    starting_after: Optional[str] = None,
    ending_before: Optional[str] = None,
    where: Optional[List[Any]] = None,
    options: Optional[List[Any]] = None,
) -> PaginatedList[TPagable]:
    results = list_elements(
        session,
        cls,
        from_id_fn=from_id_fn,
        secondary_sort_columns=secondary_sort_columns,
        limit=limit + 1,
        starting_after=starting_after,
        ending_before=ending_before,
        where=where,
        options=options,
    )

    return PaginatedList.new(
        results,
        limit=limit,
        ending_before=ending_before,
        starting_after=starting_after,
        cursor_fn=lambda c: c.id,
    )
