from typing import List, Optional

from membership.auth.needs import (
    NdBase,
    NdCommitteesList,
    NdContactsPrivilegedInfoReadById,
    NdContactsPrivilegedInfoReadSelf,
    NdContactsPrivilegedInfoUpdateById,
    NdContactsPrivilegedInfoUpdateSelf,
    NdElectionsRead,
    NdFullAdmin,
    NdMeetingsList,
    NdSearch,
    NdSecurityPrincipalsRead,
)
from membership.database.models import SecurityPrincipal
from membership.schemas.rest.contacts import IdContact


def contact_full_needs_by_id(contact_id: IdContact) -> List[NdBase]:
    return [
        NdContactsPrivilegedInfoReadById(contact_id=contact_id),
        NdContactsPrivilegedInfoUpdateById(contact_id=contact_id),
    ]


def user_defaults() -> List[NdBase]:
    return [
        NdCommitteesList(),
        NdElectionsRead(),
        NdMeetingsList(),
        NdContactsPrivilegedInfoReadSelf(),
        NdContactsPrivilegedInfoUpdateSelf(),
        NdSearch(),
        NdSecurityPrincipalsRead(),
    ]


def DANGEROUS_superuser_defaults() -> List[NdBase]:
    defaults: List[NdBase] = [
        NdFullAdmin(),
    ]
    return defaults


def read_self_contact_need(
    contact_id: IdContact, principal: Optional[SecurityPrincipal]
) -> Optional[NdBase]:
    if principal is None:
        return None

    if principal.user is None:
        return None

    if principal.user.contact.id != contact_id:
        return None

    return NdContactsPrivilegedInfoReadSelf()


def update_self_contact_need(
    contact_id: IdContact, principal: Optional[SecurityPrincipal]
) -> Optional[NdBase]:
    if principal is None:
        return None

    if principal.user is None:
        return None

    if principal.user.contact.id != contact_id:
        return None

    return NdContactsPrivilegedInfoUpdateSelf()
