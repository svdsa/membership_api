"""Needs for Flask-Principal (https://pythonhosted.org/Flask-Principal/)

from the docs:

A Need is the smallest grain of access control.
For example “has the admin role”, “can edit blog posts”.

Needs can have parameters to further define their scoping.
e.g. "can edit blog posts" can have a parameter for post ID to
only grant access to that single ID.
"""

from typing import List, Literal, Optional, Union

from membership.schemas.rest.contacts import IdContact
from membership.schemas.rest.custom_fields import IdCustomField
from membership.schemas.rest.pydantic import SxBase, SxFrozenBase


class NdBase(SxFrozenBase):
    slug: str


class MemberRoleNeed(NdBase):
    slug: Literal["member.has_legacy_role"] = "member.has_legacy_role"
    role: str
    committee_id: Optional[int] = None


class NdContactsAdmin(NdBase):
    slug: Literal["contacts.admin"] = "contacts.admin"


class NdContactsPrivilegedInfoReadById(NdBase):
    slug: Literal[
        "contacts.privileged_info.read_by_id"
    ] = "contacts.privileged_info.read_by_id"
    contact_id: IdContact


class NdContactsPrivilegedInfoUpdateById(NdBase):
    slug: Literal[
        "contacts.privileged_info.update_by_id"
    ] = "contacts.privileged_info.update_by_id"
    contact_id: IdContact


class NdContactsPrivilegedInfoRead(NdBase):
    slug: Literal["contacts.privileged_info.read"] = "contacts.privileged_info.read"


class NdContactsPrivilegedInfoUpdate(NdBase):
    slug: Literal["contacts.privileged_info.update"] = "contacts.privileged_info.update"


class NdContactsPrivilegedInfoReadSelf(NdBase):
    slug: Literal[
        "contacts.privileged_info.read_self"
    ] = "contacts.privileged_info.read_self"


class NdContactsPrivilegedInfoUpdateSelf(NdBase):
    slug: Literal[
        "contacts.privileged_info.update_self"
    ] = "contacts.privileged_info.update_self"


class NdContactsCreate(NdBase):
    slug: Literal["contacts.create"] = "contacts.create"


class NdContactsArchive(NdBase):
    slug: Literal["contacts.archive"] = "contacts.archive"


class NdContactsList(NdBase):
    slug: Literal["contacts.list"] = "contacts.list"


class NdAssetsAdmin(NdBase):
    slug: Literal["assets.admin"] = "assets.admin"


class NdCommitteesAdmin(NdBase):
    slug: Literal["committees.admin"] = "committees.admin"


class NdCommitteesAdminById(NdBase):
    slug: Literal["committees.admin_id"] = "committees.admin_id"
    committee_id: int


class NdCommitteesList(NdBase):
    slug: Literal["committees.list"] = "committees.list"


class NdCommitteesCreate(NdBase):
    slug: Literal["committees.create"] = "committees.create"


class NdCommitteesRead(NdBase):
    slug: Literal["committees.read"] = "committees.read"


class NdImportEventsAdmin(NdBase):
    slug: Literal["import_events.admin"] = "import_events.admin"


class NdImportEventsCreate(NdBase):
    slug: Literal["import_events.create"] = "import_events.create"


class NdImportEventsReview(NdBase):
    slug: Literal["import_events.review"] = "import_events.review"


class NdCustomFieldsAdmin(NdBase):
    slug: Literal["custom_fields.admin"] = "custom_fields.admin"


class NdCustomFieldsCreate(NdBase):
    slug: Literal["custom_fields.create"] = "custom_fields.create"


class NdCustomFieldsUpdate(NdBase):
    slug: Literal["custom_fields.update"] = "custom_fields.update"


class NdCustomFieldsUpdateById(NdBase):
    slug: Literal["custom_fields.update_id"] = "custom_fields.update_id"
    custom_field_id: IdCustomField


class NdCustomFieldsRead(NdBase):
    slug: Literal["custom_fields.read"] = "custom_fields.read"


class NdCustomFieldsDelete(NdBase):
    slug: Literal["custom_fields.delete"] = "custom_fields.delete"


class NdElectionsAdmin(NdBase):
    slug: Literal["elections.admin"] = "elections.admin"


class NdElectionsCreate(NdBase):
    slug: Literal["elections.create"] = "elections.create"


class NdElectionsRead(NdBase):
    slug: Literal["elections.read"] = "elections.read"


class NdElectionsUpdate(NdBase):
    slug: Literal["elections.update"] = "elections.update"


class NdElectionsVotersRead(NdBase):
    slug: Literal["elections.voters.read"] = "elections.voters.read"


class NdElectionsTokensCreate(NdBase):
    slug: Literal["elections.tokens.create"] = "elections.tokens.create"


class NdElectionsPaperBallotsCreate(NdBase):
    slug: Literal["elections.paper_ballots.create"] = "elections.paper_ballots.create"


class NdElectionsVotesRead(NdBase):
    slug: Literal["elections.votes.read"] = "elections.votes.read"


class NdElectionsVotesReadById(NdBase):
    slug: Literal["elections.votes.read_id"] = "elections.votes.read_id"
    election_id: int


class NdElectionsVotesCreate(NdBase):
    slug: Literal["elections.votes.create"] = "elections.votes.create"
    election_id: int


class NdMeetingsAdmin(NdBase):
    slug: Literal["meetings.admin"] = "meetings.admin"


class NdMeetingsCreate(NdBase):
    slug: Literal["meetings.create"] = "meetings.create"


class NdMeetingsUpdate(NdBase):
    slug: Literal["meetings.update"] = "meetings.update"


class NdMeetingsUpdateById(NdBase):
    slug: Literal["meetings.update_id"] = "meetings.update_id"
    meeting_id: int


class NdMeetingsUpdateByCommitteeId(NdBase):
    slug: Literal["meetings.update_committee_id"] = "meetings.update_committee_id"
    committee_id: int


class NdMeetingsList(NdBase):
    slug: Literal["meetings.list"] = "meetings.list"


class NdMeetingsDelete(NdBase):
    slug: Literal["meetings.delete"] = "meetings.delete"


class NdTagsAdmin(NdBase):
    slug: Literal["tags.admin"] = "tags.admin"


class NdTagsCreate(NdBase):
    slug: Literal["tags.create"] = "tags.create"


class NdSearch(NdBase):
    slug: Literal["search"] = "search"


class NdSecurityPrincipalsAdmin(NdBase):
    slug: Literal["security_principals.admin"] = "security_principals.admin"


class NdSecurityPrincipalsList(NdBase):
    slug: Literal["security_principals.list"] = "security_principals.list"


class NdSecurityPrincipalsRead(NdBase):
    slug: Literal["security_principals.read"] = "security_principals.read"


class NdFullAdmin(NdBase):
    slug: Literal["admin"] = "admin"


class SxRoleNeedsList(SxBase):
    __root__: List[
        Union[
            NdFullAdmin,
            NdContactsAdmin,
            NdContactsPrivilegedInfoReadById,
            NdContactsPrivilegedInfoUpdateById,
            NdContactsPrivilegedInfoRead,
            NdContactsPrivilegedInfoUpdate,
            NdContactsPrivilegedInfoReadSelf,
            NdContactsPrivilegedInfoUpdateSelf,
            NdContactsCreate,
            NdContactsArchive,
            NdContactsList,
            NdAssetsAdmin,
            NdCommitteesAdmin,
            NdCommitteesAdminById,
            NdCommitteesList,
            NdCommitteesCreate,
            NdCommitteesRead,
            NdImportEventsAdmin,
            NdImportEventsCreate,
            NdImportEventsReview,
            NdCustomFieldsAdmin,
            NdCustomFieldsCreate,
            NdCustomFieldsUpdate,
            NdCustomFieldsUpdateById,
            NdCustomFieldsRead,
            NdCustomFieldsDelete,
            NdElectionsAdmin,
            NdElectionsCreate,
            NdElectionsRead,
            NdElectionsUpdate,
            NdElectionsVotersRead,
            NdElectionsTokensCreate,
            NdElectionsPaperBallotsCreate,
            NdElectionsVotesRead,
            NdElectionsVotesReadById,
            NdElectionsVotesCreate,
            NdMeetingsAdmin,
            NdMeetingsCreate,
            NdMeetingsUpdate,
            NdMeetingsUpdateById,
            NdMeetingsUpdateByCommitteeId,
            NdMeetingsList,
            NdMeetingsDelete,
            NdTagsAdmin,
            NdTagsCreate,
            NdSearch,
            NdSecurityPrincipalsAdmin,
            NdSecurityPrincipalsList,
            NdSecurityPrincipalsRead,
        ]
    ]

    def __iter__(self):
        return iter(self.__root__)

    def __getitem__(self, item):
        return self.__root__[item]
