class ApiAuthenticationError(Exception):
    hide_presence: bool

    def __init__(self, *, hide_presence: bool) -> None:
        self.hide_presence = hide_presence
        super().__init__()


class RequestRequiresAuthenticationError(ApiAuthenticationError):
    pass


class InvalidSecurityPrincipalError(ApiAuthenticationError):
    pass


class InsufficientPermissionsError(ApiAuthenticationError):
    pass


class InvalidUserError(ApiAuthenticationError):
    pass


class CreateAuth0UserError(Exception):
    def __init__(self, status_code: int, content: str):
        self.status_code = status_code
        self.content = content
        self.message = (
            f"Failed to create user. Received status={status_code} content={content}"
        )


class InvalidJWTSecretError(Exception):
    pass


class InvalidJWTError(Exception):
    pass


class InsufficientRoleError(Exception):
    pass


class UnauthorizedError(Exception):
    pass
