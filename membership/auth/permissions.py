from typing import Optional

from flask_principal import Permission

from membership.auth.needs import (
    MemberRoleNeed,
    NdAssetsAdmin,
    NdCommitteesAdmin,
    NdCommitteesAdminById,
    NdCommitteesCreate,
    NdCommitteesList,
    NdContactsAdmin,
    NdContactsArchive,
    NdContactsCreate,
    NdContactsList,
    NdContactsPrivilegedInfoRead,
    NdContactsPrivilegedInfoReadById,
    NdContactsPrivilegedInfoUpdate,
    NdContactsPrivilegedInfoUpdateById,
    NdCustomFieldsAdmin,
    NdCustomFieldsCreate,
    NdCustomFieldsDelete,
    NdCustomFieldsRead,
    NdCustomFieldsUpdate,
    NdElectionsAdmin,
    NdElectionsCreate,
    NdElectionsRead,
    NdElectionsUpdate,
    NdElectionsVotesCreate,
    NdFullAdmin,
    NdImportEventsAdmin,
    NdImportEventsCreate,
    NdImportEventsReview,
    NdMeetingsAdmin,
    NdMeetingsCreate,
    NdMeetingsList,
    NdMeetingsUpdate,
    NdMeetingsUpdateByCommitteeId,
    NdMeetingsUpdateById,
    NdSearch,
    NdSecurityPrincipalsAdmin,
    NdSecurityPrincipalsList,
    NdSecurityPrincipalsRead,
    NdTagsAdmin,
    NdTagsCreate,
)
from membership.auth.needs_derived import (
    read_self_contact_need,
    update_self_contact_need,
)
from membership.database.models import Committee, SecurityPrincipal
from membership.schemas.id import IdCommittee, IdContact, IdMeeting
from membership.schemas.rest.pydantic import SxBase
from membership.util.arrays import compact


class SxPermissionParams(SxBase):
    contact_id: Optional[IdContact]
    committee_id: Optional[IdCommittee]
    meeting_id: Optional[IdMeeting]


# Contacts
PmContactsAdmin = Permission(
    MemberRoleNeed(role="admin"), NdFullAdmin(), NdContactsAdmin()
)
PmContactsCreate = Permission(
    MemberRoleNeed(role="admin"), NdFullAdmin(), NdContactsAdmin(), NdContactsCreate()
)
PmContactsList = Permission(
    MemberRoleNeed(role="admin"), NdFullAdmin(), NdContactsAdmin(), NdContactsList()
)
PmContactsArchive = Permission(
    MemberRoleNeed(role="admin"), NdFullAdmin(), NdContactsAdmin(), NdContactsArchive()
)

# Assets
PmAssetsAdmin = Permission(MemberRoleNeed(role="admin"), NdFullAdmin(), NdAssetsAdmin())

# Committees
PmCommitteesAdmin = Permission(
    MemberRoleNeed(role="admin"), NdFullAdmin(), NdCommitteesAdmin()
)
PmCommitteesList = Permission(
    MemberRoleNeed(role="admin"), NdFullAdmin(), NdCommitteesAdmin(), NdCommitteesList()
)
PmCommitteesListLeadership = Permission(
    MemberRoleNeed(role="admin"), NdFullAdmin(), NdCommitteesAdmin(), NdCommitteesList()
)
PmCommitteesCreate = Permission(
    MemberRoleNeed(role="admin"),
    NdFullAdmin(),
    NdCommitteesAdmin(),
    NdCommitteesCreate(),
)

# Import events
PmImportEventsAdmin = Permission(
    MemberRoleNeed(role="admin"), NdFullAdmin(), NdImportEventsAdmin()
)
PmImportEventsCreate = Permission(
    MemberRoleNeed(role="admin"),
    NdFullAdmin(),
    NdImportEventsAdmin(),
    NdImportEventsCreate(),
)
PmImportEventsReview = Permission(
    MemberRoleNeed(role="admin"),
    NdFullAdmin(),
    NdImportEventsAdmin(),
    NdImportEventsCreate(),
    NdImportEventsReview(),
)

# Custom fields
PmCustomFieldsAdmin = Permission(
    MemberRoleNeed(role="admin"), NdFullAdmin(), NdCustomFieldsAdmin()
)
PmCustomFieldsDelete = Permission(
    MemberRoleNeed(role="admin"),
    NdFullAdmin(),
    NdCustomFieldsAdmin(),
    NdCustomFieldsDelete(),
)
PmCustomFieldsCreate = Permission(
    MemberRoleNeed(role="admin"),
    NdFullAdmin(),
    NdCustomFieldsAdmin(),
    NdCustomFieldsCreate(),
)
PmCustomFieldsUpdate = Permission(
    MemberRoleNeed(role="admin"),
    NdFullAdmin(),
    NdCustomFieldsAdmin(),
    NdCustomFieldsCreate(),
    NdCustomFieldsUpdate(),
)
PmCustomFieldsRead = Permission(
    MemberRoleNeed(role="admin"),
    NdFullAdmin(),
    NdCustomFieldsAdmin(),
    NdCustomFieldsCreate(),
    NdCustomFieldsUpdate(),
    NdCustomFieldsRead(),
)

# Elections
PmElectionsAdmin = Permission(
    MemberRoleNeed(role="admin"),
    NdFullAdmin(),
    NdElectionsAdmin(),
)
PmElectionsCreate = Permission(
    MemberRoleNeed(role="admin"), NdFullAdmin(), NdElectionsAdmin(), NdElectionsCreate()
)
PmElectionsUpdate = Permission(
    MemberRoleNeed(role="admin"),
    NdFullAdmin(),
    NdElectionsAdmin(),
    NdElectionsCreate(),
    NdElectionsUpdate(),
)
PmElectionsRead = Permission(
    MemberRoleNeed(role="admin"),
    NdFullAdmin(),
    NdElectionsAdmin(),
    NdElectionsCreate(),
    NdElectionsUpdate(),
    NdElectionsRead(),
)

# Meetings
PmMeetingsAdmin = Permission(
    MemberRoleNeed(role="admin"),
    NdFullAdmin(),
    NdMeetingsAdmin(),
)
PmMeetingsCreate = Permission(
    MemberRoleNeed(role="admin"),
    NdFullAdmin(),
    NdMeetingsAdmin(),
    NdMeetingsCreate(),
)
PmMeetingsRead = Permission(
    MemberRoleNeed(role="admin"),
    NdFullAdmin(),
    NdMeetingsAdmin(),
    NdMeetingsCreate(),
    NdMeetingsList(),
)

# Tags
PmTagsAdmin = Permission(MemberRoleNeed(role="admin"), NdFullAdmin(), NdTagsAdmin())
PmTagsCreate = Permission(
    MemberRoleNeed(role="admin"), NdFullAdmin(), NdTagsAdmin(), NdTagsCreate()
)
PmTagsAttach = Permission(MemberRoleNeed(role="admin"), NdFullAdmin(), NdTagsAdmin())

# Search
PmSearch = Permission(MemberRoleNeed(role="admin"), NdFullAdmin(), NdSearch())

# Security principals
PmSecurityPrincipalsAdmin = Permission(
    MemberRoleNeed(role="admin"), NdFullAdmin(), NdSecurityPrincipalsAdmin()
)
PmSecurityPrincipalsList = Permission(
    MemberRoleNeed(role="admin"),
    NdFullAdmin(),
    NdSecurityPrincipalsAdmin(),
    NdSecurityPrincipalsList(),
)
PmSecurityPrincipalsRead = Permission(
    MemberRoleNeed(role="admin"),
    NdFullAdmin(),
    NdSecurityPrincipalsAdmin(),
    NdSecurityPrincipalsRead(),
)


def build_get_contact_permission(
    contact_id: IdContact, principal: Optional[SecurityPrincipal] = None
) -> Permission:
    read_self_need = read_self_contact_need(contact_id, principal)
    needs = compact(
        [
            MemberRoleNeed(role="admin"),
            NdFullAdmin(),
            NdContactsAdmin(),
            NdContactsPrivilegedInfoReadById(contact_id=IdContact(contact_id)),
            NdContactsPrivilegedInfoRead(),
            read_self_need,
        ]
    )

    return Permission(*needs)


def build_update_contact_permission(
    contact_id: IdContact, principal: Optional[SecurityPrincipal] = None
) -> Permission:
    update_self_need = update_self_contact_need(contact_id, principal)

    needs = compact(
        [
            MemberRoleNeed(role="admin"),
            NdFullAdmin(),
            NdContactsAdmin(),
            NdContactsPrivilegedInfoUpdateById(contact_id=IdContact(contact_id)),
            NdContactsPrivilegedInfoUpdate(),
            update_self_need,
        ]
    )

    return Permission(*needs)


def build_get_contact_email_addresses_permission(
    contact_id: IdContact, principal: SecurityPrincipal
) -> Permission:
    # TODO implement finer grained permissions
    return build_get_contact_permission(contact_id, principal)


def build_update_contact_email_addresses_permission(
    contact_id: IdContact, principal: SecurityPrincipal
) -> Permission:
    # TODO implement finer grained permissions
    return build_update_contact_permission(contact_id, principal)


def build_get_contact_phone_numbers_permission(
    contact_id: IdContact, principal: SecurityPrincipal
) -> Permission:
    # TODO implement finer grained permissions
    return build_get_contact_permission(contact_id, principal)


def build_update_contact_phone_numbers_permission(
    contact_id: IdContact, principal: SecurityPrincipal
) -> Permission:
    # TODO implement finer grained permissions
    return build_update_contact_permission(contact_id, principal)


def build_get_contact_mailing_addresses_permission(
    contact_id: IdContact, principal: SecurityPrincipal
) -> Permission:
    # TODO implement finer grained permissions
    return build_get_contact_permission(contact_id, principal)


def build_update_contact_mailing_addresses_permission(
    contact_id: IdContact, principal: SecurityPrincipal
) -> Permission:
    # TODO implement finer grained permissions
    return build_update_contact_permission(contact_id, principal)


def build_get_contact_custom_fields_permission(
    contact_id: IdContact, principal: SecurityPrincipal
) -> Permission:
    # TODO implement finer grained permissions
    return build_get_contact_permission(contact_id, principal)


def build_update_contact_custom_fields_permission(
    contact_id: IdContact, principal: SecurityPrincipal
) -> Permission:
    # TODO implement finer grained permissions
    return build_update_contact_permission(contact_id, principal)


def build_get_contact_tags_permission(
    contact_id: IdContact, principal: SecurityPrincipal
) -> Permission:
    # TODO implement finer grained permissions
    return build_get_contact_permission(contact_id, principal)


def build_update_contact_tags_permission(
    contact_id: IdContact, principal: SecurityPrincipal
) -> Permission:
    # TODO implement finer grained permissions
    return build_update_contact_permission(contact_id, principal)


def build_administer_committee_permission(committee: Committee) -> Permission:
    return build_administer_committee_permission_from_id(committee.id)


def build_administer_committee_permission_from_id(committee_id: int) -> Permission:
    return Permission(
        MemberRoleNeed(role="admin"),
        NdFullAdmin(),
        MemberRoleNeed(role="admin", committee_id=committee_id),
        NdCommitteesAdmin(),
        NdCommitteesAdminById(committee_id=committee_id),
    )


def build_submit_vote_for_election_permission_from_id(election_id: int) -> Permission:
    return Permission(
        MemberRoleNeed(role="admin"),
        NdFullAdmin(),
        NdElectionsVotesCreate(election_id=election_id),
    )


def build_update_meeting_permission_from_id(
    meeting_id: int, committee_id: Optional[int]
) -> Permission:
    needs = compact(
        [
            MemberRoleNeed(role="admin"),
            NdFullAdmin(),
            NdMeetingsAdmin(),
            NdMeetingsCreate(),
            NdMeetingsUpdate(),
            NdMeetingsUpdateById(meeting_id=meeting_id),
            NdMeetingsUpdateByCommitteeId(committee_id=committee_id)
            if committee_id is not None
            else None,
        ]
    )

    return Permission(*needs)
