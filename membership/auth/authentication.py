from typing import Optional, cast

import jwt
import structlog
from jwt.exceptions import ExpiredSignatureError
from pydantic.networks import EmailStr
from sqlalchemy import orm

from config.auth_config import NO_AUTH_EMAIL
from membership.auth.errors import (
    InvalidJWTError,
    InvalidJWTSecretError,
    UnauthorizedError,
)
from membership.database.models import SecurityPrincipal


def decode_email_from_bearer(auth_header: Optional[str], use_auth: bool) -> str:
    from config import JWT_CLIENT_ID, JWT_SECRET

    if use_auth:
        # Validate our auth0 headers.
        if not auth_header:
            raise UnauthorizedError("authorization header not found")

        if not auth_header.lower().startswith("bearer"):
            raise InvalidJWTError("auth header is not a bearer token")

        if not JWT_SECRET:
            raise InvalidJWTSecretError("JWT secret is missing or blank")

        token = auth_header.split()[1]
        try:
            token = jwt.decode(
                token, JWT_SECRET, audience=JWT_CLIENT_ID, algorithms=["HS256"]
            )
        except ExpiredSignatureError as e:
            raise e
        except Exception as e:
            raise InvalidJWTError(f"unable to decode JWT: {e}")

        raw_email = token.get("email")
        if raw_email is None:
            raise InvalidJWTError("JWT does not contain email")

        email = EmailStr.validate(raw_email)
        if email is None:
            raise UnauthorizedError("unable to extract email from JWT")

        return email

    else:
        # No validation.
        return cast(str, NO_AUTH_EMAIL)  # always available if USE_AUTH is unset


def get_security_principal(
    session: orm.Session, *, auth_header: Optional[str], use_auth: bool
) -> Optional[SecurityPrincipal]:
    from membership.database.models import Member
    from membership.services import email_address_service

    logger = structlog.stdlib.get_logger()

    if auth_header is None and use_auth:
        return None

    # TODO: Only supports lookup of security principal via JWT bearer token with email address
    # In the future, should support API key or OAuth2 bearer with client id/secret
    if use_auth:
        try:
            email = decode_email_from_bearer(auth_header, use_auth)
        except (InvalidJWTError, UnauthorizedError, ExpiredSignatureError) as e:
            logger.error("error decoding JWT", error=e)
            return None
    else:
        logger = logger.bind(use_auth=use_auth)
        email = cast(str, NO_AUTH_EMAIL)

    email_address = email_address_service.from_email_address(session, EmailStr(email))
    logger = logger.bind(email_address_id=email_address.id if email_address else None)

    if email_address is None:
        # TODO: Remove this when Members are decoupled from internal models
        member = session.query(Member).filter_by(email_address=email).one_or_none()

        if member is None:
            logger.info("security principal not found")
            return None

        sp = member.principal
        if member.principal is None:
            # HACK (2021-08-10) only to get member stuff working with new sp auth
            sp = SecurityPrincipal(member=member)
            session.add(sp)
            session.commit()
            session.refresh(sp)

        logger.info("non-email security principal found")
        return sp

    associated_contact = email_address.contact
    logger = logger.bind(
        contact_id=associated_contact.id if associated_contact else None
    )
    if associated_contact is None:
        logger.info("no contact associated with email address")
        return None

    logger = logger.bind(
        user_id=associated_contact.user.id if associated_contact.user else None
    )
    if associated_contact.user is None:
        logger.info("no user associated with contact")
        return None

    logger.info("security principal found")
    return associated_contact.user.principal
