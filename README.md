# DSA Membership API

This is the backend for the membership portal.

It uses Auth0 for authentication / authorization and MailGun for sending email.
However these features are disabled by default for local development.

## Contributing

### Common Installation

We use Docker to run and develop the API. Whether you are running the API locally for
developing the UI, or you are making changes to the API, I suggest you use Docker.
Having trouble? Check out the [troubleshooting section](#troubleshooting).

1. **[Install docker-compose](https://docs.docker.com/compose/install/)**
2. **Create your `.env` config file for the project**
   ```
   cp example.env .env
   # Edit .env and replace the NO_AUTH_EMAIL with your email address, as well as the SUPER_USER_* variables.
   # Please change MYSQL_ROOT_PASSWORD as well.
   ```

### Run with Docker

If you don't need to make code changes to the API, the easiest way to get started is to
just use docker compose to run the services. We created a dead-simple command for this:

```
make
```

On first run, this will set up the necessary databases, database tables, and docker images and containers.
On subsequnt runs, it will re-use the pieces that are already set up to get you going quickly.

### Setting up a local development environment

You may want to set up a local development environment for full editor support. These steps cover setting up a local Python environment and installing dependencies.

You still need to use the provided Docker Compose definitions to run the application.

- Install [pyenv](https://github.com/pyenv/pyenv)
  - On macOS, use Homebrew
  - On Linux / WSL, use [pyenv-installer](https://github.com/pyenv/pyenv-installer)
- Use pyenv to install a recent version of Python 3 (e.g. Python 3.8)

  On Linux / WSL:

  ```bash
  sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl git
  pyenv install 3.8
  # 3.8 or 3.9 is fine, just check what the latest bugfix release is beforehand
  ```

- Install the [poetry](https://github.com/sdispater/poetry) dependency manager

  ```bash
  curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python
  ```

- Install development headers so we can install dependencies.

  On Linux / WSL:

  ```bash
  sudo apt install default-libmysqlclient-dev python3-dev
  ```

- Install dependencies.

  ```bash
  poetry install
  ```

- Install Git hooks (to make sure you run formatting each commit)

  ```bash
  # Install the hooks inside the poetry environment
  poetry run pre-commit install

  # Make sure hooks are working properly (you should see something from `darker`)
  poetry run pre-commit
  ```

- Run app

  ```bash
  make local
  ```

  Which is just shorthand for:

  ```bash
  poetry run python flask_app.py
  ```

### Running API app on host machine w/ dockerized db/migration

- Ensure you have poetry installed. If not follow install instructions above.

- Update .env file. Whatever .env config you use to run the standard `make` command should be fine, except update `DATABASE_HOST=db` to `DATABASE_HOST=127.0.0.1`. In the multi-container compose solution that `make` runs, db resolves to the dynamically assigned IP managed by compose. When running the api on the host machine we have to specify the IP that our db container is exposed on. Note: this IP is tested on Docker for Mac, and may require tweaking based on your OS/docker config.

- Run `make dev`.

### Testing

Tests run through docker with `make unittest`.

### Adding a migration

- Edit the SQLAlchemy models at `membership/database/models.py` with your desired changes
- Run `make migration MESSAGE='message'`

### Local database access

First, ensure the database is running through Docker.

Without any changes, you can open a shell on your local machine and use [the MySQL CLI](https://mariadb.com/kb/en/mysql-command-line-client/) via the command line.

- you can connect via the command line with `mysql --user=root --host=127.0.0.1 dsa -p` and use the `MYSQL_ROOT_PASSWORD` from the `.env` file when prompted
- if you've changed the `DATABASE_PORT` in the `.env` file, you need to specify that changed port when you try to connect.

You can also use an external tool installed locally on your machine, including many GUI tools. Some examples include:

- [OmniDB (cross-platform, web based)](https://omnidb.org/en/)
- [phpMyAdmin (cross-platform, web based)](https://www.phpmyadmin.net/)
- [sequel pro (macOS, native)](https://www.sequelpro.com/)

If you want to debug using the database container, you can always open a shell into the container like so:

- run `docker-compose exec db bash` when in this repo to get a shell inside the container
- run your `mysql` cli commands as desired

### Debug with Docker

If you want to run shell in a docker container, run

```
docker-compose run --entrypoint=/bin/sh {name-in-docker-compose.yml}
```

If you to install the API locally, use the `make dev` command instead of `make`. This will start
the database, run any necessary migrations, and then start the app in debug mode so that it will
pickup live updates when you save any changes.

The API can be health checked locally

```
curl http://localhost:8080/health
# Should see {"health": true}
```

### Troubleshooting

Help! I'm seeing some error. What do I do?

1. **Error** Can't install Python 3.6.1 on Mac OSX, I'm seeing `zlib not available`
   **Fix**
   Download and install or upgrade XCode, and install the command line tools.

```
xcode-select --install
```

1. I have a local MySQL database already running on port 3306 and Docker is giving me a bind error when I try to run `make docker-api`

- Change the `DATABASE_PORT` to a different port that isn't in use. This is the port Docker tries to bind on your local machine for database debugging purposes. This won't affect the API, but if you want to access the database on your local machine, you'll have to specify the same port.

1. When writing tests, a test is telling me that a table doesn't exist when it obviously does. E.g. `sqlalchemy.exc.OperationalError: (sqlite3.OperationalError) no such table: email_addresses`

- Make sure that your test isn't freezegun'd into the future. Only use past dates with freezegun, lest weird time-travel stuff starts happening.
