"""add tags

Revision ID: b745811bc9b9
Revises: ce3102554bd7
Create Date: 2020-10-25 17:31:23.572464

"""
import sqlalchemy as sa

import membership.database.types
from alembic import op

# revision identifiers, used by Alembic.
revision = 'b745811bc9b9'
down_revision = 'ce3102554bd7'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "tags",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column(
            "name",
            sa.String(length=255, collation="utf8mb4_unicode_520_ci"),
            nullable=True,
        ),
        sa.Column(
            "date_added", membership.database.types.UTCDateTime(), nullable=False
        ),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("id"),
    )
    op.create_index(op.f("ix_tags_name"), "tags", ["name"], unique=True)
    op.create_table(
        "member_tags",
        sa.Column("member_id", sa.Integer(), nullable=False),
        sa.Column("tag_id", sa.Integer(), nullable=False),
        sa.Column(
            "date_added", membership.database.types.UTCDateTime(), nullable=False
        ),
        sa.ForeignKeyConstraint(
            ["member_id"],
            ["members.id"],
        ),
        sa.ForeignKeyConstraint(
            ["tag_id"],
            ["tags.id"],
        ),
        sa.PrimaryKeyConstraint("member_id", "tag_id"),
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('member_tags')
    op.drop_index(op.f('ix_tags_name'), table_name='tags')
    op.drop_table('tags')
    # ### end Alembic commands ###
