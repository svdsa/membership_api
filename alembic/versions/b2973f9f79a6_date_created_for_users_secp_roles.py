"""date_created for users secp roles

Revision ID: b2973f9f79a6
Revises: c9cf8bab8897
Create Date: 2021-10-22 02:37:05.419971

"""
import sqlalchemy as sa

import membership.database.types
from alembic import op

# revision identifiers, used by Alembic.
revision = "b2973f9f79a6"
down_revision = "c9cf8bab8897"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "security_principal_role_associations",
        sa.Column(
            "date_created", membership.database.types.UTCDateTime(), nullable=False
        ),
    )
    op.add_column(
        "security_principal_roles",
        sa.Column(
            "date_created", membership.database.types.UTCDateTime(), nullable=False
        ),
    )
    op.add_column(
        "security_principals",
        sa.Column(
            "date_created", membership.database.types.UTCDateTime(), nullable=False
        ),
    )
    op.add_column(
        "users",
        sa.Column(
            "date_created", membership.database.types.UTCDateTime(), nullable=False
        ),
    )
    # ### end Alembic commands ###


def downgrade():
    op.drop_column("users", "date_created")
    op.drop_column("security_principals", "date_created")
    op.drop_column("security_principal_roles", "date_created")
    op.drop_column("security_principal_role_associations", "date_created")
    # ### end Alembic commands ###
