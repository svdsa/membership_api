from datetime import datetime
from typing import List, Optional, Tuple
from unittest.mock import MagicMock

import pytz
from freezegun import freeze_time

from config import SUPER_USER_EMAIL
from membership.database.base import Session, engine, metadata
from membership.database.models import (
    Attendee,
    Committee,
    Meeting,
    Member,
    NationalMembershipData,
    Role,
)
from membership.repos import MeetingRepo
from membership.services.attendee_service import AttendeeService
from membership.services.eligibility import SanFranciscoEligibilityService


class TestSanFranciscoEligibilityService:
    def setup(self):
        metadata.create_all(engine)

        session = Session()
        m = Member()
        # make the owner id 2 to not conflict with hardcoded member id 1 in tests below
        m.id = 2
        m.email_address = SUPER_USER_EMAIL
        m.first_name = "Eugene"
        m.last_name = "Debs"
        session.add(m)
        role = Role()
        role.member = m
        role.role = 'admin'
        session.add(role)
        session.commit()

        committee = Committee(
            id=1,
            name='Committee on Committees',
            provisional=False,
        )

        session.add_all([committee])
        session.commit()
        session.close()

    def teardown(self):
        metadata.drop_all(engine)

    def make_meetings(self) -> List[Meeting]:
        return [
            Meeting(
                id=1,
                name='General Meeting 1',
                short_id=1,
                start_time=datetime(2017, 1, 1).astimezone(pytz.utc),
                end_time=datetime(2017, 1, 1).astimezone(pytz.utc),
                owner_id=2
            ),
            Meeting(
                id=2,
                name='General Meeting 2',
                short_id=2,
                start_time=datetime(2017, 2, 1).astimezone(pytz.utc),
                end_time=datetime(2017, 2, 1).astimezone(pytz.utc),
                owner_id=2
            ),
            Meeting(
                id=3,
                name='General Meeting 3',
                short_id=3,
                start_time=datetime(2017, 3, 1).astimezone(pytz.utc),
                end_time=datetime(2017, 3, 1).astimezone(pytz.utc),
                owner_id=2
            )
        ]

    def make_member(
        self,
        with_membership: bool = False
    ) -> Tuple[Member, Optional[NationalMembershipData]]:
        member = Member(
            id=2,
            email_address="debs@dsasf.org",
            first_name="Eugene",
            last_name="Debs",
            memberships_usa=[]
        )

        membership = None
        if with_membership:
            membership = NationalMembershipData(id=200)
            member.memberships_usa.append(membership)

        return member, membership

    def make_committee(self, is_provisional: bool = False) -> Committee:
        return Committee(
            id=30,
            provisional=is_provisional
        )

    @freeze_time("2020-10-01")
    def test_member_with_dues_paid_and_active_in_committee(self):
        meetings = self.make_meetings()

        meeting_repo = MeetingRepo(Meeting)
        meeting_repo.most_recent = MagicMock(return_value=meetings)

        member, membership = self.make_member(with_membership=True)
        membership.dues_paid_until = datetime(2020, 11, 1).astimezone(pytz.utc)

        committee = self.make_committee()
        committee_role = Role(
            id=2000,
            committee_id=committee.id,
            committee=committee,
            member_id=member.id,
            role='active'
        )
        member.all_roles.append(committee_role)

        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repo, attendee_service)

        result = service.members_as_eligible_to_vote(Session(), [member])[0]

        assert result.is_eligible
        assert result.dues_are_paid
        assert not result.meets_attendance_criteria
        assert result.active_in_committee

    @freeze_time("2020-10-01")
    def test_member_with_dues_paid_and_has_attended_meetings(self):
        meetings = self.make_meetings()

        meeting_repo = MeetingRepo(Meeting)
        meeting_repo.most_recent = MagicMock(return_value=meetings)

        member, membership = self.make_member(with_membership=True)
        membership.dues_paid_until = datetime(2020, 11, 1).astimezone(pytz.utc)

        attendee1 = Attendee(
            meeting_id=meetings[0].id,
            member=member
        )
        attendee2 = Attendee(
            meeting_id=meetings[1].id,
            member=member
        )
        member.meetings_attended = [
            attendee1,
            attendee2
        ]

        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repo, attendee_service)

        result = service.members_as_eligible_to_vote(Session(), [member])[0]

        assert result.is_eligible
        assert result.dues_are_paid
        assert result.meets_attendance_criteria
        assert not result.active_in_committee

    @freeze_time("2020-10-01")
    def test_member_with_lapsed_dues(self):
        meetings = self.make_meetings()

        meeting_repo = MeetingRepo(Meeting)
        meeting_repo.most_recent = MagicMock(return_value=meetings)

        member, membership = self.make_member(with_membership=True)
        # day before grace period kicks in for "LEAVING_STANDING"
        membership.dues_paid_until = datetime(2020, 8, 31).astimezone(pytz.utc)

        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repo, attendee_service)

        result = service.members_as_eligible_to_vote(Session(), [member])[0]

        assert not result.is_eligible
        assert not result.dues_are_paid

    @freeze_time("2020-10-01")
    def test_member_with_leaving_standing_is_eligible(self):
        meetings = self.make_meetings()

        meeting_repo = MeetingRepo(Meeting)
        meeting_repo.most_recent = MagicMock(return_value=meetings)

        member, membership = self.make_member(with_membership=True)
        membership.dues_paid_until = datetime(2020, 9, 1).astimezone(pytz.utc)

        committee = self.make_committee()
        committee_role = Role(
            id=2000,
            committee_id=committee.id,
            committee=committee,
            member_id=member.id,
            role='active'
        )
        member.all_roles.append(committee_role)

        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repo, attendee_service)

        result = service.members_as_eligible_to_vote(Session(), [member])[0]

        assert result.is_eligible
        assert result.dues_are_paid

    @freeze_time("2020-10-01")
    def test_member_with_dues_overridden_is_eligible(self):
        meetings = self.make_meetings()

        meeting_repo = MeetingRepo(Meeting)
        meeting_repo.most_recent = MagicMock(return_value=meetings)

        member, membership = self.make_member(with_membership=True)
        membership.dues_paid_until = datetime(2020, 1, 1).astimezone(pytz.utc)
        member.dues_paid_overridden_on = datetime(2020, 9, 1).astimezone(pytz.utc)

        committee = self.make_committee()
        committee_role = Role(
            id=2000,
            committee_id=committee.id,
            committee=committee,
            member_id=member.id,
            role='active'
        )
        member.all_roles.append(committee_role)

        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repo, attendee_service)

        result = service.members_as_eligible_to_vote(Session(), [member])[0]

        assert result.is_eligible
        assert result.dues_are_paid

    @freeze_time("2020-10-01")
    def test_suspended_member_is_not_eligible(self):
        meetings = self.make_meetings()

        meeting_repo = MeetingRepo(Meeting)
        meeting_repo.most_recent = MagicMock(return_value=meetings)

        member, membership = self.make_member(with_membership=True)
        membership.dues_paid_until = datetime(2021, 1, 1).astimezone(pytz.utc)
        member.suspended_on = datetime(2020, 1, 1).astimezone(pytz.utc)

        committee = self.make_committee()
        committee_role = Role(
            id=2000,
            committee_id=committee.id,
            committee=committee,
            member_id=member.id,
            role='active'
        )
        member.all_roles.append(committee_role)

        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repo, attendee_service)

        result = service.members_as_eligible_to_vote(Session(), [member])[0]

        assert not result.is_eligible

    @freeze_time("2020-10-01")
    def test_member_who_left_is_not_eligible(self):
        meetings = self.make_meetings()

        meeting_repo = MeetingRepo(Meeting)
        meeting_repo.most_recent = MagicMock(return_value=meetings)

        member, membership = self.make_member(with_membership=True)
        membership.dues_paid_until = datetime(2021, 1, 1).astimezone(pytz.utc)
        member.left_chapter_on = datetime(2020, 1, 1).astimezone(pytz.utc)

        committee = self.make_committee()
        committee_role = Role(
            id=2000,
            committee_id=committee.id,
            committee=committee,
            member_id=member.id,
            role='active'
        )
        member.all_roles.append(committee_role)

        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repo, attendee_service)

        result = service.members_as_eligible_to_vote(Session(), [member])[0]

        assert not result.is_eligible

    @freeze_time("2020-10-01")
    def test_non_member_is_not_eligible(self):
        meetings = self.make_meetings()

        meeting_repo = MeetingRepo(Meeting)
        meeting_repo.most_recent = MagicMock(return_value=meetings)

        member, membership = self.make_member(with_membership=False)

        committee = self.make_committee()
        committee_role = Role(
            id=2000,
            committee_id=committee.id,
            committee=committee,
            member_id=member.id,
            role='active'
        )
        member.all_roles.append(committee_role)

        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repo, attendee_service)

        result = service.members_as_eligible_to_vote(Session(), [member])[0]

        assert not result.is_eligible

    @freeze_time("2020-10-01")
    def test_member_with_unknown_and_overridden_dues_is_eligible(self):
        meetings = self.make_meetings()

        meeting_repo = MeetingRepo(Meeting)
        meeting_repo.most_recent = MagicMock(return_value=meetings)

        member, membership = self.make_member(with_membership=False)
        member.onboarded_on = datetime(2020, 1, 1).astimezone(pytz.utc)
        member.dues_paid_overridden_on = datetime(2021, 1, 1).astimezone(pytz.utc)

        committee = self.make_committee()
        committee_role = Role(
            id=2000,
            committee_id=committee.id,
            committee=committee,
            member_id=member.id,
            role='active'
        )
        member.all_roles.append(committee_role)

        attendee_service = AttendeeService()
        service = SanFranciscoEligibilityService(meeting_repo, attendee_service)

        result = service.members_as_eligible_to_vote(Session(), [member])[0]

        assert result.is_eligible
