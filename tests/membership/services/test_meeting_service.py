import dateutil.parser
import pytest

from config import SUPER_USER_EMAIL
from membership.database.base import Session, engine, metadata
from membership.database.models import Member
from membership.services.errors import ValidationError
from membership.services.meeting_service import MeetingService


class TestMeetingService:
    def setup(self):
        metadata.create_all(engine)
        session = Session()
        m = Member()
        m.email_address = SUPER_USER_EMAIL
        m.first_name = "Eugene"
        m.last_name = "Debs"
        session.add(m)
        session.commit()
        self.owner_id = m.id

    def teardown_method(cls, method):
        metadata.drop_all(engine)

    def test_add_chapter_meeting(self):
        session = Session()

        name = 'Chapter Meeting'

        service = MeetingService()
        result = service.add_meeting(name, None, self.owner_id, True, session)
        session.commit()

        assert result.id == 1
        assert result.name == name

    def test_add_committee_meeting(self):
        session = Session()

        name = 'Committee Meeting'
        committee_id = 1

        service = MeetingService()
        result = service.add_meeting(name, committee_id, self.owner_id, True, session)
        session.commit()

        assert result.id == 1
        assert result.name == name
        assert result.committee_id == committee_id

    def test_set_meeting_code(self):
        session = Session()
        service = MeetingService()

        meeting = service.add_meeting('Chapter Meeting', None, self.owner_id, True, session)

        code = 1234
        result = service.set_meeting_code(meeting, code, session)

        assert result.short_id == code

    def test_unset_meeting_code(self):
        session = Session()
        service = MeetingService()

        meeting = service.add_meeting('Chapter Meeting', None, self.owner_id, True, session)

        code = None
        result = service.set_meeting_code(meeting, code, session)

        assert result.short_id == code

    def test_set_meeting_code_invalid(self):
        session = Session()
        service = MeetingService()

        meeting = service.add_meeting('Chapter Meeting', None, self.owner_id, True, session)

        with pytest.raises(ValidationError):
            service.set_meeting_code(meeting, 123, session)

    def test_set_meeting_code_autogenerate(self):
        session = Session()
        service = MeetingService()

        meeting = service.add_meeting('Chapter Meeting', None, self.owner_id, True, session)
        meeting = service.set_meeting_code(meeting, 'autogenerate', session)

        assert 1000 <= meeting.short_id <= 9999

    def test_set_meeting_code_existing_code(self):
        session = Session()
        service = MeetingService()

        short_id = 1234

        meeting = service.add_meeting('Chapter Meeting', None, self.owner_id, True, session)
        meeting = service.set_meeting_code(meeting, short_id, session)
        meeting = service.set_meeting_code(meeting, 'autogenerate', session)

        assert meeting.short_id != short_id

    def test_set_meeting_times(self):
        session = Session()
        service = MeetingService()

        meeting = service.add_meeting('Chapter Meeting', None, self.owner_id, True, session)
        meeting = service.set_meeting_fields(meeting, {
            'start_time': '2020-05-25T19:00:00+0000',
            'end_time': '2020-05-25T20:00:00+0000'
        }, session)
        assert meeting.start_time == dateutil.parser.parse('2020-05-25T19:00:00+0000')
        assert meeting.end_time == dateutil.parser.parse('2020-05-25T20:00:00+0000')

    def test_set_meeting_end_time_before_start_time(self):
        session = Session()
        service = MeetingService()

        meeting = service.add_meeting('Chapter Meeting', None, self.owner_id, True, session)

        with pytest.raises(ValidationError):
            service.set_meeting_fields(meeting, {
                'start_time': '2020-05-25T19:00:00+0000',
                'end_time': '2020-05-25T16:00:00+0000'
            }, session)
