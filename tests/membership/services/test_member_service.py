from datetime import datetime
from typing import List

import pytest
import pytz
from freezegun import freeze_time
from overrides import overrides
from sqlalchemy import orm

from membership.database.base import Session, engine, metadata
from membership.database.models import Member, NationalMembershipData, PhoneNumber
from membership.models import MemberAsEligibleToVote
from membership.services.crm.crm_service import CrmService
from membership.services.eligibility import EligibilityService
from membership.services.mailchimp.mailchimp_action_generator import (
    MailchimpActionGenerator,
)
from membership.services.mailchimp.mailchimp_service import MailchimpService
from membership.services.member_service import MemberService
from membership.services.onboarding.onboarding_service import OnboardingService
from membership.services.role_service import RoleService
from membership.services.slack.slack_service import SlackService

test_emails = [
    ("leon.trotsky24@gmail.com", "leon.trotsky24@gmail.com"),
    ("test.e.m.a.i.l@OUTLOOK.com", "test.e.m.a.i.l@outlook.com"),
]

mailchimp_service = MailchimpService()
mailchimp_action_gen = MailchimpActionGenerator()
crm_service = CrmService()
slack_service = SlackService()


class FakeEligibilityService(EligibilityService):
    def __init__(self) -> None:
        pass

    @overrides
    def members_as_eligible_to_vote(
        self, session: orm.Session, members: List[Member]
    ) -> List[MemberAsEligibleToVote]:
        return [
            MemberAsEligibleToVote(
                member,
                is_eligible=True,
                message="message",
                dues_are_paid=True,
                meets_attendance_criteria=True,
                active_in_committee=True
            )
            for member in members
        ]


class TestMemberService:
    def setup(self):
        metadata.create_all(engine)

    def teardown(self):
        metadata.drop_all(engine)

    def test_get_by_id(self):
        session = Session()
        target = test_emails[0][0]
        member = Member(email_address=target)
        session.add(member)
        session.commit()

        onboarding_service = OnboardingService(
            mailchimp_service, mailchimp_action_gen, crm_service, slack_service)
        ms = MemberService(FakeEligibilityService(), RoleService(), onboarding_service)
        m = ms.find_by_id(session, member.id)
        assert m.email_address == target

    def test_get_by_email(self):
        session = Session()
        target = test_emails[0][0]
        session.add(Member(email_address=target))
        session.commit()

        onboarding_service = OnboardingService(
            mailchimp_service, mailchimp_action_gen, crm_service, slack_service)
        ms = MemberService(FakeEligibilityService(), RoleService(), onboarding_service)
        m = ms.find_by_email(target, session)
        assert m.email_address == target

    def test_normalize_email(self):
        for original, expected in test_emails:
            m = Member(email_address=original)
            assert m.normalized_email == expected

    def test_normalize_email_invalid_email(self):
        with pytest.raises(ValueError):
            Member(email_address="lparsons@invalid@domain.com")

    def populate_with_n_members(self, n):
        session = Session()
        for i in range(0, n):
            m = Member()
            m.first_name = "First%d" % i
            m.last_name = "Last%d" % i
            m.email_address = "user%d@foo.com" % i
            session.add(m)
        session.commit()

    def test_query_all(self):
        num_members = 10
        self.populate_with_n_members(num_members)
        session = Session()

        onboarding_service = OnboardingService(
            mailchimp_service, mailchimp_action_gen, crm_service, slack_service)
        ms = MemberService(FakeEligibilityService(), RoleService(), onboarding_service)
        member_query_result = ms.all(session)

        assert len(member_query_result.members) == num_members

    def test_query_all_no_members(self):
        session = Session()

        onboarding_service = OnboardingService(
            mailchimp_service, mailchimp_action_gen, crm_service, slack_service)
        ms = MemberService(FakeEligibilityService(), RoleService(), onboarding_service)
        member_query_result_1 = ms.all(session)

        assert len(member_query_result_1.members) == 0

        # Now populate with some members
        self.populate_with_n_members(3)

        # Note, even though the previous query didn't return anything, passing
        # in the returned cursor to a subsequent query is valid (and will
        # return all new members)
        member_query_result_2 = ms.query(
            cursor=member_query_result_1.cursor,
            page_size=10,
            query_str=None,
            session=session,
        )

        assert len(member_query_result_2.members) == 3
        assert not member_query_result_2.has_more

    def test_query_paginated(self):
        session = Session()
        page_size = 5
        self.populate_with_n_members(12)

        onboarding_service = OnboardingService(
            mailchimp_service, mailchimp_action_gen, crm_service, slack_service)
        ms = MemberService(FakeEligibilityService(), RoleService(), onboarding_service)

        member_query_result_1 = ms.query(
            cursor=None, page_size=page_size, query_str=None, session=session
        )
        member_query_result_2 = ms.query(
            cursor=member_query_result_1.cursor,
            page_size=page_size,
            query_str=None,
            session=session,
        )
        member_query_result_3 = ms.query(
            cursor=member_query_result_2.cursor,
            page_size=page_size,
            query_str=None,
            session=session,
        )

        assert len(member_query_result_1.members) == 5
        assert len(member_query_result_2.members) == 5
        assert len(member_query_result_3.members) == 2

        assert member_query_result_1.has_more
        assert member_query_result_2.has_more
        assert not member_query_result_3.has_more

    def test_query_with_search_filter(self):
        session = Session()
        num_members = page_size = 12
        self.populate_with_n_members(num_members)

        onboarding_service = OnboardingService(
            mailchimp_service, mailchimp_action_gen, crm_service, slack_service)
        ms = MemberService(FakeEligibilityService(), RoleService(), onboarding_service)
        first_name_query_result = ms.query(
            cursor=None, page_size=page_size, query_str="First", session=session
        )
        last_name_query_result = ms.query(
            cursor=None, page_size=page_size, query_str="Last", session=session
        )
        email_prefix_query_result = ms.query(
            cursor=None, page_size=page_size, query_str="user1", session=session
        )
        email_suffix_query_result = ms.query(
            cursor=None, page_size=page_size, query_str="foo.com", session=session
        )

        # Should return users with names 'First1', 'First10' and 'First11'
        first_name_query_result_2 = ms.query(
            cursor=None, page_size=page_size, query_str="First1", session=session
        )

        assert len(first_name_query_result.members) == num_members
        assert len(last_name_query_result.members) == num_members
        assert len(email_prefix_query_result.members) == 3
        assert len(email_suffix_query_result.members) == 0
        assert len(first_name_query_result_2.members) == 3

    def test_query_with_search_filter_returns_no_results(self):
        session = Session()
        num_members = page_size = 10
        self.populate_with_n_members(num_members)

        onboarding_service = OnboardingService(
            mailchimp_service, mailchimp_action_gen, crm_service, slack_service)
        ms = MemberService(FakeEligibilityService(), RoleService(), onboarding_service)
        first_name_query_result = ms.query(
            cursor=None, page_size=page_size, query_str="ASDF", session=session
        )

        assert len(first_name_query_result.members) == 0

    def test_paginate_with_search_filter(self):
        session = Session()
        num_members = 12
        self.populate_with_n_members(num_members)

        onboarding_service = OnboardingService(
            mailchimp_service, mailchimp_action_gen, crm_service, slack_service)
        ms = MemberService(FakeEligibilityService(), RoleService(), onboarding_service)

        # Overall search should return users with names 'First1', 'First10' and 'First11'
        first_name_query_result_1 = ms.query(
            cursor=None, page_size=2, query_str="First1", session=session
        )
        first_name_query_result_2 = ms.query(
            cursor=first_name_query_result_1.cursor,
            page_size=2,
            query_str="First1",
            session=session,
        )

        assert len(first_name_query_result_1.members) == 2
        assert len(first_name_query_result_2.members) == 1

        assert first_name_query_result_1.has_more
        assert not first_name_query_result_2.has_more

    def test_create_member(self):
        first_name = "Test"
        last_name = "User"
        email = "test@example.com"
        pronouns = "they/them"
        number = "(123)-456-7890"
        address = "10 Fake St"
        city = "City"
        zipcode = "94117"

        onboarding_service = OnboardingService(
            mailchimp_service, mailchimp_action_gen, crm_service, slack_service)
        ms = MemberService(FakeEligibilityService(), RoleService(), onboarding_service)

        session = Session()
        member = ms.create_member(
            session,
            first_name=first_name,
            last_name=last_name,
            email_address=email,
            pronouns=pronouns,
            phone_number=number,
            address=address,
            city=city,
            zipcode=zipcode,
            defer_commit=False)

        m = session.query(Member).get(member.id)
        assert m.first_name == first_name
        assert m.last_name == last_name
        assert m.email_address == email
        assert m.pronouns == pronouns
        assert m.address == address
        assert m.city == city
        assert m.zipcode == zipcode

        n = (
            session.query(PhoneNumber)
            .filter(PhoneNumber._raw_number == "+1 1234567890")
            .first()
        )
        assert n

    @freeze_time("2020-10-01")
    def test_upgrade_member(self):
        member = Member(
            id=100,
            first_name="Test",
            last_name="User",
            email_address="test@example.com",
            suspended_on=datetime(2020, 1, 1).astimezone(pytz.UTC),
            left_chapter_on=datetime(2020, 1, 1).astimezone(pytz.UTC),
            memberships_usa=[
                NationalMembershipData(
                    id=101,
                    member_id=100,
                    ak_id="test_id",
                    first_name="Test",
                    last_name="User",
                    city="city",
                    country="USA",
                    zipcode="94117",
                    dues_paid_until=datetime(2020, 5, 1).astimezone(pytz.UTC))
            ]
        )

        onboarding_service = OnboardingService(
            mailchimp_service, mailchimp_action_gen, crm_service, slack_service)
        ms = MemberService(FakeEligibilityService(), RoleService(), onboarding_service)
        session = Session()
        session.add(member)
        session.commit()

        ms.upgrade_to_national_member(member, True, session)

        assert not member.suspended_on
        assert not member.left_chapter_on
        assert member.dues_paid_overridden_on == datetime(2020, 10, 1).astimezone(pytz.UTC)
