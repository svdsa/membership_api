from datetime import datetime
from unittest.mock import patch

import pytest

from config import SUPER_USER_EMAIL
from membership.database.base import Session, engine, metadata
from membership.database.models import Committee, Member, NationalMembershipData, Role
from membership.models import MemberAuthContext, UnauthorizedError
from membership.services.committee_service import CommitteeService

# XXX Skip this entire file when testing
# Rationale: Test endpoints instead of services
pytestmark = pytest.mark.skip

committee_service = CommitteeService()


class TestCommitteeService:
    def setup(self):
        metadata.create_all(engine)

        session = Session()
        m = Member()
        m.email_address = SUPER_USER_EMAIL
        session.add(m)
        role1 = Role(member=m, role='member', date_created=datetime(2016, 1, 1))
        role2 = Role(member=m, role='admin', date_created=datetime(2016, 1, 1))
        session.add_all([role1, role2])
        membership = NationalMembershipData(
                member=m,
                ak_id='73750',
                first_name='Huey',
                last_name='Newton',
                address_line_1='123 Main St',
                city='Oakland',
                country='United States',
                zipcode='94612',
                dues_paid_until=datetime(3020, 6, 1, 0),
              )
        session.add(membership)

        committee = Committee(id=1, name='Testing Committee')
        inactive_committee = Committee(id=2, name='Inactive Committee', inactive=True)

        session.add_all([committee, inactive_committee])

        session.commit()

        self.session = session
        self.member = m
        self.ctx = MemberAuthContext(self.member, self.session)

    def teardown(self):
        metadata.drop_all(engine)


class TestCommitteeServiceList(TestCommitteeService):
    def test_list_returns_all_committees_admin(self):
        # self.ctx is an admin session
        assert committee_service.list(self.ctx) == [{
            'id': 1,
            'name': 'Testing Committee',
            'email': SUPER_USER_EMAIL,
            'viewable': True,
            'provisional': False,
            'inactive': False,
        }, {
            'id': 2,
            'name': 'Inactive Committee',
            'email': None,
            'viewable': True,
            'provisional': False,
            'inactive': True,
        }]

    def test_list_returns_active_committees_non_admin(self):
        non_admin = Member(id=2)
        non_admin_ctx = MemberAuthContext(non_admin, self.session)
        assert committee_service.list(non_admin_ctx) == [{
            'id': 1,
            'name': 'Testing Committee',
            'email': SUPER_USER_EMAIL,
            'viewable': False,
            'provisional': False,
            'inactive': False,
        }]


class TestCommitteeServiceCreate(TestCommitteeService):
    def test_create_creates_a_new_committee_and_returns_the__details(self):
        name = 'Committee on Mock Data'
        committee_details = committee_service.create(
            self.ctx,
            name=name,
            admins=[SUPER_USER_EMAIL],
        )
        committee = self.session.query(Committee).filter_by(name=name).one_or_none()
        assert committee is not None

        assert committee_details == {
            'id': committee.id,
            'name': 'Committee on Mock Data',
            'email': None,
            'viewable': True,
            'provisional': False,
            'inactive': False,
            'admins': [{'id': 1, 'name': ''}],
            'members': [],
            'active_members': []
        }

        assert self.session.query(Role).filter_by(
            committee=committee,
            member_id=1,
            role='admin',
        ).count() == 1


class TestCommitteeServiceGet(TestCommitteeService):
    def test_get_returns_committee_info(self):
        assert committee_service.from_id(self.ctx, 1) == {
            'id': 1,
            'name': 'Testing Committee',
            'email': SUPER_USER_EMAIL,
            'viewable': True,
            'provisional': False,
            'inactive': False,
            'admins': [],
            'members': [],
            'active_members': []
        }

    def test_get_returns_none_for_missing_committee(self):
        assert committee_service.from_id(self.ctx, -1) is None


class TestCommitteeServiceRequestMembership(TestCommitteeService):
    def test_request_membership_returns_true_on_success(self):
        assert committee_service.request_membership(
            self.ctx,
            committee_id=1,
            name=self.ctx.requester.name,
            email=self.ctx.requester.email_address,
        )

    def test_request_membership_returns_none_when_committee_has_no_email(self):
        assert (
            committee_service.request_membership(
                self.ctx,
                committee_id=-1,
                name=self.ctx.requester.name,
                email=self.ctx.requester.email_address,
            )
            is None
        )

    @patch(
        'membership.services.committee_service.send_committee_request_email',
        autospec=True,
        side_effect=Exception(),
    )
    def test_request_membership_returns_false_when_sending_email_raises_an_exception(self, _):
        assert not committee_service.request_membership(
            self.ctx,
            committee_id=1,
            name=self.ctx.requester.name,
            email=self.ctx.requester.email_address,
        )


class TestCommitteeServiceEdit(TestCommitteeService):
    def test_admins_edit_inactive(self):
        # self.ctx is an admin session
        assert committee_service.edit(self.ctx, 1, inactive=True) == {
            'id': 1,
            'name': 'Testing Committee',
            'email': SUPER_USER_EMAIL,
            'viewable': True,
            'provisional': False,
            'inactive': True,
            'admins': [],
            'members': [],
            'active_members': []
        }

    def test_non_admin_cant_edit_inactive(self):
        non_admin = Member(id=2)
        non_admin_ctx = MemberAuthContext(non_admin, self.session)
        with pytest.raises(UnauthorizedError):
            committee_service.edit(non_admin_ctx, 1, inactive=True)
