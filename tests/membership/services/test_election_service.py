from membership.database.base import Session, engine, metadata
from membership.database.models import Election
from membership.services.election_service import ElectionService


class TestElectionService:

    def setup_method(cls, method):
        metadata.create_all(engine)

    def teardown_method(cls, method):
        metadata.drop_all(engine)

    def test_find_election_by_id_with_existing(self):
        session = Session()

        election = Election(name='test-1')
        session.add(election)
        session.commit()

        service = ElectionService()
        result = service.find_election_by_id(session, 1)

        assert result.id == 1
        assert result.name == 'test-1'

        result = service.find_election_by_id(session, 2)
        assert result is None

    def test_transition_election_as_with_valid_transition(self):
        session = Session()

        test_election = Election(
            id=1,
            name='test',
            status='draft'
        )

        session.add(test_election)
        session.commit()

        service = ElectionService()

        (success, election) = service.transition_election_as(session, 'publish', test_election.id)

        assert success
        assert election.status == 'published'

    def test_transition_election_as_with_invalid_transition(self):
        session = Session()

        test_election = Election(
            id=1,
            name='test',
            status='draft'
        )

        session.add(test_election)
        session.commit()

        service = ElectionService()

        (success, election) = service.transition_election_as(session, 'bedazzle', test_election.id)

        assert not success
        assert election.status == 'draft'
