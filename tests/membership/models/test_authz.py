from unittest import TestCase

from membership.models.authz import Authorization, AzRole, UnauthorizedError


class TestAzRole(TestCase):

    def test_equality_of_equal_values(self):
        r1 = AzRole('a', 1)
        r2 = AzRole('a', 1)
        assert r1 == r2

    def test_equality_of_different_values(self):
        r1 = AzRole('a', 1)
        r2 = AzRole('b', 2)
        assert r1 != r2


class TestAuthorization(TestCase):

    def test_equality_of_equal_values(self):
        az1 = Authorization(1, [AzRole('a', 1)])
        az2 = Authorization(1, [AzRole('a', 1)])
        assert az1 == az2

    def test_equality_of_different_values(self):
        az1 = Authorization(1, [AzRole('a', 1)])
        az2 = Authorization(2, [AzRole('b', 2)])
        assert az1 != az2

    def test_raise_exception_when_member_id_absent(self):
        with self.assertRaises(ValueError):
            Authorization(None, [])

    def test_has_role_without_committee_id_matches(self):
        az = Authorization(1, [AzRole('a'), AzRole('b')])
        assert az.has_role('b')

    def test_has_role_without_committee_id_fails_when_given_different_role_name(self):
        az = Authorization(1, [AzRole('a'), AzRole('b')])
        assert not az.has_role('c')

    def test_has_role_without_committee_id_fails_when_given_any_committee_id(self):
        az = Authorization(1, [AzRole('a'), AzRole('b')])
        assert not az.has_role('b', committee_id=1)

    def test_has_role_with_committee_id_matches(self):
        az = Authorization(1, [AzRole('a', 1), AzRole('b', 1), AzRole('b', 2)])
        assert az.has_role('b', committee_id=1)

    def test_has_role_with_committee_id_fails_when_given_a_different_role_name(self):
        az = Authorization(1, [AzRole('a', 1), AzRole('b', 1), AzRole('b', 2)])
        assert not az.has_role('c', committee_id=1)

    def test_has_role_with_committee_id_fails_when_given_a_different_committee_id(self):
        az = Authorization(1, [AzRole('a', 1), AzRole('b', 1), AzRole('b', 2)])
        assert not az.has_role('b', committee_id=3)

    def test_has_role_with_member_id_matches_regardless_of_roles(self):
        az = Authorization(1, [])
        assert az.has_role('anything', member_id=1)

    def test_has_role_with_member_id_fallback_success(self):
        az = Authorization(2, [AzRole('a', 1), AzRole('b', 1), AzRole('b', 2)])
        assert az.has_role('b', committee_id=2, member_id=1)

    def test_has_role_with_member_id_fallback_failure(self):
        az = Authorization(2, [AzRole('a', 1), AzRole('b', 1), AzRole('b', 2)])
        assert not az.has_role('b', committee_id=3, member_id=1)

    def test_verify_general_admin(self):
        az = Authorization(1, [AzRole('member'), AzRole('admin')])
        az.verify_admin()

    def test_verify_committee_admin(self):
        az = Authorization(1, [AzRole('member'), AzRole('member', 1), AzRole('admin', 1)])
        with self.assertRaises(UnauthorizedError):
            az.verify_admin()

    def test_verify_non_admin(self):
        az = Authorization(1, [AzRole('member'), AzRole('member', 1)])
        with self.assertRaises(UnauthorizedError):
            az.verify_admin()

    def test_verify_general_admin_for_committee(self):
        az = Authorization(1, [AzRole('member'), AzRole('admin')])
        az.verify_admin(1)

    def test_verify_committee_admin_for_committee(self):
        az = Authorization(1, [AzRole('member'), AzRole('member', 1), AzRole('admin', 1)])
        az.verify_admin(1)

    def test_verify_non_admin_for_committee(self):
        az = Authorization(1, [AzRole('member'), AzRole('member', 1)])
        with self.assertRaises(UnauthorizedError):
            az.verify_admin()
