import pytest

from membership.database.base import (
    IdPrefixes,
    Session,
    TableWithMaskedId,
    engine,
    metadata,
)
from membership.web.base_app import app


class TestMaskedId:
    @pytest.fixture
    def tables(self):
        metadata.create_all(engine)
        yield
        metadata.drop_all(engine)

    @pytest.fixture
    def client(self, tables):
        app.testing = True
        return app.test_client()

    @pytest.fixture
    def session(self, tables):
        session = Session()
        yield session
        session.close()

    class TestSanity:
        def test_id_prefixes(self):
            for cls in TableWithMaskedId.__subclasses__():
                assert hasattr(IdPrefixes, cls.__name__), (
                    f"{cls.__name__} is missing from IdPrefixes. "
                    "You must manually define a prefix to use the "
                    "TableWithMaskedId mixin.",
                )
