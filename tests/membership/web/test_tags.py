import json
from datetime import datetime
from http import HTTPStatus
from typing import cast

import devtools
import pytest
import pytz
from freezegun import freeze_time
from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse

from config import SUPER_USER_EMAIL
from membership.database.base import Session, engine, metadata
from membership.database.models import Member, PhoneNumber, Role, Tag, TagAssociation
from membership.web.base_app import app
from tests.flask_utils import delete_json, get_json, patch_json, post_json


class TestOldTag:

    member_id = 4927

    def setup(self):
        metadata.create_all(engine)
        self.app = Client(app, BaseResponse)
        self.app = app.test_client()
        self.app.testing = True

        session = Session()

        tag1 = Tag(
            name="Tag 1", date_created=datetime(2020, 10, 1).astimezone(pytz.utc)
        )
        tag2 = Tag(
            name="Tag 2", date_created=datetime(2020, 10, 1).astimezone(pytz.utc)
        )

        m = Member(
            id=self.member_id,
            email_address=SUPER_USER_EMAIL,
            phone_numbers=[
                PhoneNumber(member_id=self.member_id, name="Home", number="425-123-2345")
            ],
        )

        roles = [
            Role(
                member=m,
                role="member",
                date_created=datetime(2016, 1, 1).astimezone(pytz.utc),
            ),
            Role(
                member=m,
                role="admin",
                date_created=datetime(2016, 1, 1).astimezone(pytz.utc),
            ),
        ]

        tassoc_1 = TagAssociation(tag=tag1, member=m)
        tassoc_2 = TagAssociation(tag=tag2, member=m)

        session.add_all([tag1, tag2, m, tassoc_1, tassoc_2] + roles)

        session.commit()
        session.close()

    def teardown(self):
        metadata.drop_all(engine)

    @freeze_time("2020-10-01")
    def test_get_tags_should_return_all_tags(self):
        response = self.app.get("/tags", content_type="application/json")
        status = response.status_code
        payload = json.loads(response.data)

        assert status == 200
        assert payload == {
            "data": [
                {
                    "id": "tg_GwV4q0dZ7Op3WMeD",
                    "object": "tag",
                    "name": "Tag 2",
                    "date_created": datetime(2020, 10, 1)
                    .astimezone(pytz.utc)
                    .isoformat(),
                    "contacts": [],
                    "custom_fields": [],
                    "members": ["4927"],
                },
                {
                    "id": "tg_An709PbyGdEJVKro",
                    "object": "tag",
                    "name": "Tag 1",
                    "date_created": datetime(2020, 10, 1)
                    .astimezone(pytz.utc)
                    .isoformat(),
                    "contacts": [],
                    "custom_fields": [],
                    "members": ["4927"],
                },
            ],
            "has_more": False,
            "object": "list",
            "url": "/tags",
        }

    @freeze_time("2020-10-01")
    def test_delete_tag_should_remove_tag_and_member_associations(self):
        response = self.app.delete(
            "/tags/tg_An709PbyGdEJVKro", content_type="application/json"
        )
        status = response.status_code

        assert status == 200

        session = Session()

        tags = session.query(Tag).all()
        member = session.query(Member).get(self.member_id)

        assert len(tags) == 1
        assert member is not None
        assert len(member.tags) == 1

    @pytest.mark.skip
    @freeze_time("2020-10-01")
    def test_get_members_with_tag_should_return_members_with_tag(self):
        response = self.app.get(
            "/tags/tg_An709PbyGdEJVKro/members", content_type="application/json"
        )
        status = response.status_code
        payload = json.loads(response.data)

        assert status == 200
        assert len(payload) == 1
        assert payload[0]['id'] == self.member_id


class TestTags:
    @pytest.fixture
    def tables(self):
        metadata.create_all(engine)
        yield
        metadata.drop_all(engine)

    @pytest.fixture
    def client(self, tables):
        app.testing = True
        return app.test_client()

    @pytest.fixture
    def session(self, tables):
        session = Session()
        yield session
        session.close()

    @pytest.fixture
    def user(self, session):
        # TODO remove in favor of User type
        admin_member = Member(
            first_name="Admin",
            last_name="McAdminFace",
            email_address=SUPER_USER_EMAIL,
        )
        session.add(admin_member)
        role = Role(
            member=admin_member,
            role="admin",
        )
        session.add(role)
        session.commit()

        first_member = session.query(Member).first()
        return first_member

    class TestCreate:
        @freeze_time("2021-07-01")
        def test_create_tag_minimal(self, session, client, user):
            payload = {
                "name": "Very Cool",
            }
            response = post_json(client, "/tags", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CREATED
            assert response.json == {
                "id": "tg_An709PbyGdEJVKro",
                "object": "tag",
                "name": "Very Cool",
                "date_created": "2021-07-01T00:00:00+00:00",
                "contacts": [],
                "custom_fields": [],
                "members": [],
            }

        @freeze_time("2021-07-01")
        def test_create_tag_with_contact(self, session, client, user):
            contact_payload = {"full_name": "Alice Bob"}
            response = post_json(client, "/contacts", contact_payload)
            assert response.json is not None
            devtools.debug(response.json)
            contact_id = response.json["id"]
            assert response.status_code == HTTPStatus.CREATED

            payload = {"name": "Very Cool", "contacts": [contact_id]}
            response = post_json(client, "/tags", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CREATED
            assert response.json == {
                "id": "tg_An709PbyGdEJVKro",
                "object": "tag",
                "name": "Very Cool",
                "date_created": "2021-07-01T00:00:00+00:00",
                "contacts": [contact_id],
                "custom_fields": [],
                "members": [],
            }

    class TestCreateErrors:
        @freeze_time("2021-07-01")
        def test_create_tag_existing(self, session, client, user):
            payload = {
                "name": "Very Cool",
            }
            response = post_json(client, "/tags", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CREATED

            response = post_json(client, "/tags", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CONFLICT

        @freeze_time("2021-07-01")
        def test_create_tag_nonexistent_contact(self, session, client, user):
            payload = {"name": "Very Cool", "contacts": ["ct_1rAEnlwZAQekpgqD"]}
            response = post_json(client, "/tags", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.BAD_REQUEST

    class TestUpdate:
        @freeze_time("2021-07-01")
        def test_update_minimal(self, session, client, user):
            payload = {
                "name": "Very Cool",
            }
            response = post_json(client, "/tags", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CREATED
            assert response.json is not None
            tag_id = response.json["id"]

            payload = {
                "name": "Very Legal",
            }
            response = patch_json(client, f"/tags/{tag_id}", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.OK
            assert response.json == {
                "id": "tg_An709PbyGdEJVKro",
                "object": "tag",
                "name": "Very Legal",
                "date_created": "2021-07-01T00:00:00+00:00",
                "contacts": [],
                "custom_fields": [],
                "members": [],
            }

        @freeze_time("2021-07-01")
        def test_update_attach_contact(self, session, client, user):
            payload = {
                "name": "Very Cool",
            }
            response = post_json(client, "/tags", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CREATED
            assert response.json is not None
            tag_id = response.json["id"]

            contact_payload = {"full_name": "Alice Bob"}
            response = post_json(client, "/contacts", contact_payload)
            devtools.debug(response.json)
            assert response.json is not None
            contact_id = response.json["id"]
            assert response.status_code == HTTPStatus.CREATED

            payload = {"contact": contact_id}
            response = post_json(client, f"/tags/{tag_id}/contacts", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.OK
            attached_contact_json = {
                "id": "ct_1rAEnlwZAQekpgqD",
                "object": "contact",
                "full_name": "Alice Bob",
                "display_name": None,
                "pronouns": None,
                "biography": None,
                "profile_pic": None,  # noqa: E501
                "admin_notes": None,
                "email_addresses": {
                    "data": [],
                    "has_more": False,
                    "object": "list",
                    "url": "/contacts/ct_1rAEnlwZAQekpgqD/email_addresses",
                },
                "phone_numbers": {
                    "data": [],
                    "has_more": False,
                    "object": "list",
                    "url": "/contacts/ct_1rAEnlwZAQekpgqD/phone_numbers",
                },
                "mailing_addresses": {
                    "data": [],
                    "has_more": False,
                    "object": "list",
                    "url": "/contacts/ct_1rAEnlwZAQekpgqD/mailing_addresses",
                },
                "city": None,
                "zipcode": None,
                "tags": ["tg_An709PbyGdEJVKro"],
                "custom_fields": {},
                "date_created": "2021-07-01T00:00:00+00:00",
                "source_created": None,
                "date_archived": None,
                "reason_archived": None,
            }
            assert response.json == attached_contact_json

            response = get_json(client, f"/contacts/{contact_id}")
            assert response.status_code == HTTPStatus.OK
            assert response.json == attached_contact_json

        @freeze_time("2021-07-01")
        def test_update_detach_contact(self, session, client, user):
            contact_payload = {"full_name": "Alice Bob"}
            response = post_json(client, "/contacts", contact_payload)
            assert response.json is not None
            devtools.debug(response.json)
            contact_id = response.json["id"]
            assert response.status_code == HTTPStatus.CREATED

            payload = {"name": "Very Cool", "contacts": [contact_id]}
            response = post_json(client, "/tags", payload)
            assert response.json is not None
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CREATED
            tag_id = response.json["id"]

            response = get_json(client, f"/contacts/{contact_id}")
            assert response.status_code == HTTPStatus.OK
            attached_contact_json = {
                "id": "ct_1rAEnlwZAQekpgqD",
                "object": "contact",
                "full_name": "Alice Bob",
                "display_name": None,
                "pronouns": None,
                "biography": None,
                "profile_pic": None,  # noqa: E501
                "admin_notes": None,
                "email_addresses": {
                    "data": [],
                    "has_more": False,
                    "object": "list",
                    "url": "/contacts/ct_1rAEnlwZAQekpgqD/email_addresses",
                },
                "phone_numbers": {
                    "data": [],
                    "has_more": False,
                    "object": "list",
                    "url": "/contacts/ct_1rAEnlwZAQekpgqD/phone_numbers",
                },
                "mailing_addresses": {
                    "data": [],
                    "has_more": False,
                    "object": "list",
                    "url": "/contacts/ct_1rAEnlwZAQekpgqD/mailing_addresses",
                },
                "city": None,
                "zipcode": None,
                "tags": ["tg_An709PbyGdEJVKro"],
                "custom_fields": {},
                "date_created": "2021-07-01T00:00:00+00:00",
                "source_created": None,
                "date_archived": None,
                "reason_archived": None,
            }
            assert response.json == attached_contact_json

            response = delete_json(client, f"/tags/{tag_id}/contacts/{contact_id}")
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.OK
            detached_contact_json = {
                "id": "ct_1rAEnlwZAQekpgqD",
                "object": "contact",
                "full_name": "Alice Bob",
                "display_name": None,
                "pronouns": None,
                "biography": None,
                "profile_pic": None,  # noqa: E501
                "admin_notes": None,
                "email_addresses": {
                    "data": [],
                    "has_more": False,
                    "object": "list",
                    "url": "/contacts/ct_1rAEnlwZAQekpgqD/email_addresses",
                },
                "phone_numbers": {
                    "data": [],
                    "has_more": False,
                    "object": "list",
                    "url": "/contacts/ct_1rAEnlwZAQekpgqD/phone_numbers",
                },
                "mailing_addresses": {
                    "data": [],
                    "has_more": False,
                    "object": "list",
                    "url": "/contacts/ct_1rAEnlwZAQekpgqD/mailing_addresses",
                },
                "city": None,
                "zipcode": None,
                "tags": [],
                "custom_fields": {},
                "date_created": "2021-07-01T00:00:00+00:00",
                "source_created": None,
                "date_archived": None,
                "reason_archived": None,
            }
            assert response.json == detached_contact_json

            response = get_json(client, f"/contacts/{contact_id}")
            assert response.status_code == HTTPStatus.OK
            assert response.json == detached_contact_json

        @freeze_time("2021-07-01")
        def test_update_attach_custom_field(self, session, client, user):
            payload = {
                "name": "Very Cool",
            }
            response = post_json(client, "/tags", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CREATED
            assert response.json is not None
            tag_id = response.json["id"]

            custom_field_payload = {"name": "Ice cream flavor", "field_type": "text"}
            response = post_json(client, "/custom_fields", custom_field_payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CREATED
            assert response.json is not None
            custom_field_id = response.json["id"]

            payload = {"custom_field": custom_field_id}
            response = post_json(client, f"/tags/{tag_id}/custom_fields", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.OK
            attached_custom_field_response = {
                "id": "cfd_G6lnAeM13r01a49Z",
                "object": "custom_field",
                "name": "Ice cream flavor",
                "field_type": "text",
                "description": None,
                "options": None,
                "contacts": [],
                "tags": [tag_id],
                "date_created": "2021-07-01T00:00:00+00:00",
            }
            assert response.json == attached_custom_field_response

            response = get_json(client, f"/custom_fields/{custom_field_id}")
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.OK
            assert response.json == attached_custom_field_response

        @freeze_time("2021-07-01")
        def test_update_detach_custom_field(self, session, client, user):
            custom_field_payload = {"name": "Ice cream flavor", "field_type": "text"}
            response = post_json(client, "/custom_fields", custom_field_payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CREATED
            assert response.json is not None
            custom_field_id = response.json["id"]

            payload = {"name": "Very Cool", "custom_fields": [custom_field_id]}
            response = post_json(client, "/tags", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CREATED
            assert response.json is not None
            tag_id = response.json["id"]

            response = get_json(client, f"/custom_fields/{custom_field_id}")
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.OK
            assert response.json == {
                "id": "cfd_G6lnAeM13r01a49Z",
                "object": "custom_field",
                "name": "Ice cream flavor",
                "field_type": "text",
                "description": None,
                "options": None,
                "contacts": [],
                "tags": [tag_id],
                "date_created": "2021-07-01T00:00:00+00:00",
            }

            response = delete_json(
                client, f"/tags/{tag_id}/custom_fields/{custom_field_id}"
            )
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.OK
            detached_custom_field_response = {
                "id": "cfd_G6lnAeM13r01a49Z",
                "object": "custom_field",
                "name": "Ice cream flavor",
                "field_type": "text",
                "description": None,
                "options": None,
                "contacts": [],
                "tags": [],
                "date_created": "2021-07-01T00:00:00+00:00",
            }
            assert response.json == detached_custom_field_response

            response = get_json(client, f"/custom_fields/{custom_field_id}")
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.OK
            assert response.json == detached_custom_field_response

        def test_attach_contact_uniqueness(self, session, client, user):
            payload = {
                "name": "Very Cool",
            }
            response = post_json(client, "/tags", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CREATED
            assert response.json is not None
            tag_id = response.json["id"]

            contact_payload = {"full_name": "Alice Bob"}
            response = post_json(client, "/contacts", contact_payload)
            assert response.json is not None
            devtools.debug(response.json)
            contact_id = response.json["id"]
            assert response.status_code == HTTPStatus.CREATED

            payload = {"contact": contact_id}
            response = post_json(client, f"/tags/{tag_id}/contacts", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.OK

            payload = {"tag": tag_id}
            response = post_json(client, f"/contacts/{contact_id}/tags", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CONFLICT

        def test_tag_field_association(self, session, client, user):
            payload = {
                "name": "Very Cool",
            }
            response = post_json(client, "/tags", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CREATED
            assert response.json is not None
            tag_id = response.json["id"]

            custom_field_payload = {"name": "Ice cream flavor", "field_type": "text"}
            response = post_json(client, "/custom_fields", custom_field_payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CREATED
            assert response.json is not None
            custom_field_id = response.json["id"]

            payload = {"custom_field": custom_field_id}
            response = post_json(client, f"/tags/{tag_id}/custom_fields", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.OK

            response = get_json(client, f"/custom_fields/{custom_field_id}")
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.OK

            payload = {"custom_field": custom_field_id}
            response = post_json(client, f"/tags/{tag_id}/custom_fields", payload)
            devtools.debug(response.json)
            assert response.status_code == HTTPStatus.CONFLICT

    class TestPagination:
        @freeze_time("2021-07-01")
        def test_correct_link_header(self, session, client, user):
            for i in range(11):
                payload = {
                    "name": f"Tag #{i}",
                }
                response = post_json(client, "/tags", payload)
                devtools.debug(response.json)
                assert response.status_code == HTTPStatus.CREATED

            response = get_json(client, "/tags")
            assert response.status_code == HTTPStatus.OK
            assert response.headers["Link"] is not None
            assert cast(str, response.headers["Link"]).startswith(
                "</tags?starting_after="
            )
