import json
from datetime import datetime
from http import HTTPStatus
from io import BytesIO

import pytest
import pytz
from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse

from config import MAILGUN_HTTP_WEBHOOK_KEY, SUPER_USER_EMAIL
from membership.database.base import Session, engine, metadata
from membership.database.models import (
    Attendee,
    Committee,
    Meeting,
    Member,
    NationalMembershipData,
    PhoneNumber,
    Role,
    Tag,
    TagAssociation,
)
from membership.util.email.mailgun_email_connector import MailgunEmailConnector
from membership.web.base_app import app
from tests.flask_utils import delete_json, get_json, patch_json, post_json, put_json


@pytest.mark.skip
class TestWebMembers:
    def setup(self):
        metadata.create_all(engine)
        self.app = Client(app, BaseResponse)
        self.app = app.test_client()
        self.app.testing = True

        # set up for auth
        session = Session()

        tag1 = Tag(
            name="Tag 1", date_created=datetime(2020, 10, 1).astimezone(pytz.utc)
        )
        tag2 = Tag(
            name="Tag 2", date_created=datetime(2020, 10, 1).astimezone(pytz.utc)
        )

        m = Member(
            id=1,
            dues_paid_overridden_on=datetime(2020, 1, 1),
            onboarded_on=datetime(2020, 1, 2),
        )
        m.email_address = SUPER_USER_EMAIL
        m.notes = "Some notes on this member"
        m.city = "Oakland"
        m.zipcode = "94612"
        m.address = "123 Main St"
        tassoc_1 = TagAssociation(tag=tag1, member=m)
        tassoc_2 = TagAssociation(tag=tag2, member=m)
        m.tag_associations = [tassoc_1, tassoc_2]
        session.add_all([tag1, tag2, m, tassoc_1, tassoc_2])
        role1 = Role(
            member=m,
            role="member",
            date_created=datetime(2016, 1, 1).astimezone(pytz.utc))
        role2 = Role(member=m, role="admin", date_created=datetime(2016, 1, 1).astimezone(pytz.utc))
        session.add_all([role1, role2])
        membership = NationalMembershipData(
            member=m,
            ak_id="73750",
            first_name="Huey",
            last_name="Newton",
            address_line_1="123 Main St",
            city="Oakland",
            country="United States",
            zipcode="94612",
            dues_paid_until=datetime(2028, 6, 1, 0).astimezone(pytz.utc)
        )
        session.add(membership)

        phone_number = PhoneNumber(member_id=1, name="Home", number="425-123-2345")
        session.add(phone_number)

        meeting1 = Meeting(
            id=1,
            short_id=1,
            name="General Meeting 1",
            start_time=datetime(2017, 1, 1, 0).astimezone(pytz.utc),
            end_time=datetime(2017, 1, 1, 1).astimezone(pytz.utc),
            owner_id=m.id,
        )
        meeting2 = Meeting(
            id=2,
            short_id=2,
            name="General Meeting 2",
            start_time=datetime(2017, 2, 1, 0).astimezone(pytz.utc),
            end_time=datetime(2017, 2, 1, 1).astimezone(pytz.utc),
            owner_id=m.id,
        )
        meeting3 = Meeting(
            id=3,
            short_id=3,
            name="General Meeting 3",
            start_time=datetime(2017, 3, 1, 0).astimezone(pytz.utc),
            end_time=datetime(2017, 3, 1, 1).astimezone(pytz.utc),
            owner_id=m.id,
        )
        meeting4 = Meeting(
            id=4,
            short_id=None,
            name="General Meeting 4",
            start_time=datetime(2017, 4, 1, 0).astimezone(pytz.utc),
            end_time=datetime(2017, 4, 1, 1).astimezone(pytz.utc),
            owner_id=m.id,
        )

        attendee2 = Attendee(meeting=meeting2, member=m)
        attendee3 = Attendee(meeting=meeting3, member=m)

        committee = Committee(id=1, name="Testing Committee")
        # Even tho the member is on this committee,
        # it never is mentioned in their API responses because it's inactive
        inactive_committee = Committee(id=2, name="Inactive Committee", inactive=True)
        inactive_role = Role(
            member=m,
            role="member",
            date_created=datetime(2016, 1, 1).astimezone(pytz.utc),
            committee_id=2,
        )

        session.add_all(
            [
                meeting1,
                attendee2,
                attendee3,
                meeting4,
                committee,
                inactive_committee,
                inactive_role,
            ]
        )

        session.commit()
        session.close()

    def teardown(self):
        metadata.drop_all(engine)

    def test_member_list(self):
        params = {
            "page_size": 1,
            "cursor": None,
        }
        response = self.app.get(
            "/member/list", content_type="application/json", query_string=params
        )
        result = json.loads(response.data)
        expected = {
            "members": [
                {
                    "id": 1,
                    "name": "",
                    "email": SUPER_USER_EMAIL,
                    "phone_numbers": ["+1 425-123-2345"],
                    "city": "Oakland",
                    "zipcode": "94612",
                    "eligibility": {
                        "is_eligible": True,
                        "message": "eligible (Feb, Mar)",
                        "dues_are_paid": True,
                        "meets_attendance_criteria": True,
                        "active_in_committee": False,
                        "num_votes": 1,
                    },
                    "membership": {
                        "active": True,
                        "ak_id": "73750",
                        "do_not_call": False,
                        "first_name": "Huey",
                        "middle_name": None,
                        "last_name": "Newton",
                        "city": "Oakland",
                        "zipcode": "94612",
                        "join_date": None,
                        "phone_numbers": ["+1 425-123-2345"],
                        "status": 'GOOD_STANDING',
                        "dues_paid_until": datetime(2028, 6, 1, 0).astimezone(pytz.utc).isoformat(),
                        "dues_paid_overridden_on": datetime(2020, 1, 1).astimezone(
                            pytz.utc
                        ).isoformat()
                    },
                }
            ],
            "has_more": False,
            "cursor": 1,
        }

        assert result == expected

    def do_search_test(
        self, params, should_match_user, expected_has_more, expected_cursor
    ):
        response = self.app.get(
            "/member/search", content_type="application/json", query_string=params
        )

        response_code = response.status_code
        assert response_code == 200

        result = json.loads(response.data)

        members = []

        if should_match_user:
            members += [
                {
                    "id": 1,
                    "name": "",
                    "email": SUPER_USER_EMAIL,
                    "phone_numbers": ["+1 425-123-2345"],
                    "city": "Oakland",
                    "zipcode": "94612",
                    "eligibility": {
                        "is_eligible": True,
                        "message": "eligible (Feb, Mar)",
                        "dues_are_paid": True,
                        "meets_attendance_criteria": True,
                        "active_in_committee": False,
                        "num_votes": 1,
                    },
                    "membership": {
                        "active": True,
                        "ak_id": "73750",
                        "do_not_call": False,
                        "first_name": "Huey",
                        "middle_name": None,
                        "last_name": "Newton",
                        "city": "Oakland",
                        "zipcode": "94612",
                        "join_date": None,
                        "phone_numbers": ["+1 425-123-2345"],
                        "status": 'GOOD_STANDING',
                        "dues_paid_until": datetime(2028, 6, 1, 0).astimezone(pytz.utc).isoformat(),
                        "dues_paid_overridden_on": datetime(2020, 1, 1).astimezone(
                            pytz.utc
                        ).isoformat()
                    },
                }
            ]
        expected = {
            "members": members,
            "has_more": expected_has_more,
            "cursor": expected_cursor,
        }

        assert result == expected

    def test_member_search_null_cursor(self):
        assert "@" in SUPER_USER_EMAIL
        query_str = SUPER_USER_EMAIL.split("@", 1)[
            0
        ]  # query for part of email before @
        params = {
            "query": query_str,
            "page_size": 1,
            "cursor": None,
        }

        self.do_search_test(
            params=params,
            should_match_user=True,
            expected_has_more=False,
            expected_cursor=1,
        )

    def test_member_search_nonnull_cursor(self):
        assert "@" in SUPER_USER_EMAIL
        query_str = SUPER_USER_EMAIL.split("@", 1)[
            0
        ]  # query for part of email before @
        params = {
            "query": query_str,
            "page_size": 1,
            "cursor": 1,
        }

        self.do_search_test(
            params=params,
            should_match_user=False,
            expected_has_more=False,
            expected_cursor=1,
        )

    def test_member_search_name(self):
        session = Session()

        member = session.query(Member).limit(1).one_or_none()
        member.first_name = "Huey"
        member.last_name = "Newton"
        session.add(member)
        session.commit()

        m2 = Member(
            id=1,
            dues_paid_overridden_on=datetime(2020, 1, 1),
            onboarded_on=datetime(2020, 1, 2),
        )
        m2.email_address = SUPER_USER_EMAIL
        m2.notes = "Some notes on this member"
        m2.city = "San Andreas"
        m2.zipcode = "94696"
        m2.address = "123 High St"
        session.add_all([m2])

        role1 = Role(
            member=m2,
            role="member",
            date_created=datetime(2016, 1, 1).astimezone(pytz.utc),
        )
        session.add_all([role1])

        membership = NationalMembershipData(
            member=m2,
            ak_id="12345",
            first_name="Squeamish",
            last_name="Ossifrage",
            address_line_1="123 High St",
            city="San Andreas",
            country="United States",
            zipcode="94696",
            dues_paid_until=datetime(2028, 6, 1, 0).astimezone(pytz.utc),
        )
        session.add(membership)

        query_str = "Huey"
        params = {
            "query": query_str,
            "page_size": 1,
            "cursor": None,
        }

        response = self.app.get(
            "/member/search", content_type="application/json", query_string=params
        )

        response_code = response.status_code
        assert response_code == 200

        result = json.loads(response.data)

        members = [
            {
                "id": 1,
                "name": "Huey Newton",
                "email": SUPER_USER_EMAIL,
                "city": "Oakland",
                "zipcode": "94612",
                "phone_numbers": ["+1 425-123-2345"],
                "eligibility": {
                    "is_eligible": True,
                    "message": "eligible (Feb, Mar)",
                    "dues_are_paid": True,
                    "meets_attendance_criteria": True,
                    "active_in_committee": False,
                    "num_votes": 1,
                },
                "membership": {
                    "active": True,
                    "ak_id": "73750",
                    "do_not_call": False,
                    "first_name": "Huey",
                    "middle_name": None,
                    "last_name": "Newton",
                    "city": "Oakland",
                    "zipcode": "94612",
                    "join_date": None,
                    "phone_numbers": ["+1 425-123-2345"],
                    "status": "GOOD_STANDING",
                    "dues_paid_until": datetime(2028, 6, 1, 0)
                    .astimezone(pytz.utc)
                    .isoformat(),
                    "dues_paid_overridden_on": datetime(2020, 1, 1)
                    .astimezone(pytz.utc)
                    .isoformat(),
                },
            }
        ]
        expected = {
            "members": members,
            "has_more": False,
            "cursor": 1,
        }

        assert result == expected

    def test_member_search_name_negative(self):
        query_str = "Squeamish"
        params = {
            "query": query_str,
            "page_size": 1,
            "cursor": None,
        }

        response = self.app.get(
            "/member/search", content_type="application/json", query_string=params
        )

        response_code = response.status_code
        assert response_code == 200

        result = json.loads(response.data)

        members = []
        expected = {
            "members": members,
            "has_more": False,
            "cursor": 0,
        }

        assert result == expected

    def test_member_search_zipcode(self):
        session = Session()

        member = session.query(Member).limit(1).one_or_none()
        member.zipcode = "90001"
        session.add(member)
        session.commit()

        query_str = "zip:90001"
        params = {
            "query": query_str,
            "page_size": 1,
            "cursor": None,
        }

        response = self.app.get(
            "/member/search", content_type="application/json", query_string=params
        )

        response_code = response.status_code
        assert response_code == 200

        result = json.loads(response.data)

        members = [
            {
                "id": 1,
                "name": "",
                "email": SUPER_USER_EMAIL,
                "city": "Oakland",
                "zipcode": "90001",
                "phone_numbers": ["+1 425-123-2345"],
                "eligibility": {
                    "is_eligible": True,
                    "message": "eligible (Feb, Mar)",
                    "dues_are_paid": True,
                    "meets_attendance_criteria": True,
                    "active_in_committee": False,
                    "num_votes": 1,
                },
                "membership": {
                    "active": True,
                    "ak_id": "73750",
                    "do_not_call": False,
                    "first_name": "Huey",
                    "middle_name": None,
                    "last_name": "Newton",
                    "city": "Oakland",
                    "zipcode": "94612",
                    "join_date": None,
                    "phone_numbers": ["+1 425-123-2345"],
                    "status": 'GOOD_STANDING',
                    "dues_paid_until": datetime(2028, 6, 1, 0).astimezone(pytz.utc).isoformat(),
                    "dues_paid_overridden_on": datetime(2020, 1, 1).astimezone(
                        pytz.utc
                    ).isoformat()
                },
            }
        ]
        expected = {
            "members": members,
            "has_more": False,
            "cursor": 1,
        }

        assert result == expected

    def test_member_search_city(self):
        session = Session()

        member = session.query(Member).limit(1).one_or_none()
        member.city = "Berkeley"
        session.add(member)
        session.commit()

        query_str = "city:Berkeley"
        params = {
            "query": query_str,
            "page_size": 1,
            "cursor": None,
        }

        response = self.app.get(
            "/member/search", content_type="application/json", query_string=params
        )

        response_code = response.status_code
        assert response_code == 200

        result = json.loads(response.data)

        members = [
            {
                "id": 1,
                "name": "",
                "email": SUPER_USER_EMAIL,
                "phone_numbers": ["+1 425-123-2345"],
                "city": "Berkeley",
                "zipcode": "94612",
                "eligibility": {
                    "is_eligible": True,
                    "message": "eligible (Feb, Mar)",
                    "dues_are_paid": True,
                    "meets_attendance_criteria": True,
                    "active_in_committee": False,
                    "num_votes": 1,
                },
                "membership": {
                    "active": True,
                    "ak_id": "73750",
                    "do_not_call": False,
                    "first_name": "Huey",
                    "middle_name": None,
                    "last_name": "Newton",
                    "city": "Oakland",
                    "zipcode": "94612",
                    "join_date": None,
                    "phone_numbers": ["+1 425-123-2345"],
                    "status": 'GOOD_STANDING',
                    "dues_paid_until": datetime(2028, 6, 1, 0).astimezone(pytz.utc).isoformat(),
                    "dues_paid_overridden_on": datetime(2020, 1, 1).astimezone(
                        pytz.utc
                    ).isoformat()
                },
            }
        ]
        expected = {
            "members": members,
            "has_more": False,
            "cursor": 1,
        }

        assert result == expected

    def test_member_search_phone_numbers(self):
        session = Session()

        member = session.query(Member).limit(1).one_or_none()
        ph = PhoneNumber(member=member, number=PhoneNumber.format_number("234-535-3456"))
        session.add(ph)
        session.commit()

        assert "@" in SUPER_USER_EMAIL
        query_str = "phone:3456"
        params = {
            "query": query_str,
            "page_size": 1,
            "cursor": None,
        }

        response = self.app.get(
            "/member/search", content_type="application/json", query_string=params
        )

        response_code = response.status_code
        assert response_code == 200

        result = json.loads(response.data)

        members = [
            {
                "id": 1,
                "name": "",
                "email": SUPER_USER_EMAIL,
                "phone_numbers": ["+1 425-123-2345", "+1 234-535-3456"],
                "city": "Oakland",
                "zipcode": "94612",
                "eligibility": {
                    "is_eligible": True,
                    "message": "eligible (Feb, Mar)",
                    "dues_are_paid": True,
                    "meets_attendance_criteria": True,
                    "active_in_committee": False,
                    "num_votes": 1,
                },
                "membership": {
                    "active": True,
                    "ak_id": "73750",
                    "do_not_call": False,
                    "first_name": "Huey",
                    "middle_name": None,
                    "last_name": "Newton",
                    "city": "Oakland",
                    "zipcode": "94612",
                    "join_date": None,
                    "phone_numbers": ["+1 425-123-2345", "+1 234-535-3456"],
                    "status": 'GOOD_STANDING',
                    "dues_paid_until": datetime(2028, 6, 1, 0).astimezone(pytz.utc).isoformat(),
                    "dues_paid_overridden_on": datetime(2020, 1, 1).astimezone(
                        pytz.utc
                    ).isoformat()
                },
            }
        ]
        expected = {
            "members": members,
            "has_more": False,
            "cursor": 1,
        }

        assert result == expected

    def test_member(self):
        response = get_json(self.app, "/member")
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            "id": 1,
            "info": {
                "first_name": None,
                "last_name": None,
                "email_address": SUPER_USER_EMAIL,
                "address": "123 Main St",
                "city": "Oakland",
                "zipcode": "94612",
                "phone_numbers": [{"phone_number": "+1 425-123-2345", "name": "Home"}],
            },
            "roles": [
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "admin",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat()
                },
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "member",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat()
                },
            ]
        }

    def test_member_details(self):
        response = get_json(self.app, "/member/details")
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            "id": 1,
            "do_not_call": False,
            "do_not_email": False,
            "is_eligible": True,
            "info": {
                "first_name": None,
                "last_name": None,
                "email_address": SUPER_USER_EMAIL,
                "address": "123 Main St",
                "city": "Oakland",
                "zipcode": "94612",
                "phone_numbers": [{"phone_number": "+1 425-123-2345", "name": "Home"}],
            },
            "notes": "Some notes on this member",
            "membership": {
                "address": ["123 Main St", "Oakland 94612"],
                "phone_numbers": ["+1 425-123-2345"],
                "dues_paid_until": datetime(2028, 6, 1).astimezone(pytz.utc).isoformat(),
                "dues_paid_overridden_on": datetime(2020, 1, 1).astimezone(pytz.utc).isoformat(),
                "status": "GOOD_STANDING"
            },
            "roles": [
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "admin",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat()
                },
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "member",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat()
                },
            ],
            "meetings": [
                {"meeting_id": 2, "name": "General Meeting 2"},
                {"meeting_id": 3, "name": "General Meeting 3"},
            ],
            "votes": [],
            "onboarded_on": datetime(2020, 1, 2).astimezone(pytz.utc).isoformat(),
            "suspended_on": None,
            "left_chapter_on": None,
            "dues_are_paid": True,
            "meets_attendance_criteria": True,
            "active_in_committee": False,
            'tags': [
                {
                    "date_created": "2020-10-01T00:00:00+00:00",
                    "id": "tg_An709PbyGdEJVKro",
                    "name": "Tag 1",
                },
                {
                    "date_created": "2020-10-01T00:00:00+00:00",
                    "id": "tg_GwV4q0dZ7Op3WMeD",
                    "name": "Tag 2",
                },
            ],
        }

    def test_member_info(self):
        response = get_json(self.app, "/admin/member/details?member_id=1")
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            "id": 1,
            "do_not_call": False,
            "do_not_email": False,
            "is_eligible": True,
            "info": {
                "first_name": None,
                "last_name": None,
                "email_address": SUPER_USER_EMAIL,
                "address": "123 Main St",
                "city": "Oakland",
                "zipcode": "94612",
                "phone_numbers": [{"phone_number": "+1 425-123-2345", "name": "Home"}],
            },
            "notes": "Some notes on this member",
            "membership": {
                "address": ["123 Main St", "Oakland 94612"],
                "phone_numbers": ["+1 425-123-2345"],
                "dues_paid_until": datetime(2028, 6, 1).astimezone(pytz.utc).isoformat(),
                "dues_paid_overridden_on": datetime(2020, 1, 1).astimezone(pytz.utc).isoformat(),
                "status": "GOOD_STANDING"
            },
            "roles": [
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "admin",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat(),
                },
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "member",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat(),
                },
            ],
            "meetings": [
                {"meeting_id": 2, "name": "General Meeting 2"},
                {"meeting_id": 3, "name": "General Meeting 3"},
            ],
            "votes": [],
            "onboarded_on": datetime(2020, 1, 2).astimezone(pytz.utc).isoformat(),
            "suspended_on": None,
            "left_chapter_on": None,
            "dues_are_paid": True,
            "meets_attendance_criteria": True,
            "active_in_committee": False,
            'tags': [
                {
                    "date_created": "2020-10-01T00:00:00+00:00",
                    "id": "tg_An709PbyGdEJVKro",
                    "name": "Tag 1",
                },
                {
                    "date_created": "2020-10-01T00:00:00+00:00",
                    "id": "tg_GwV4q0dZ7Op3WMeD",
                    "name": "Tag 2",
                },
            ],
        }

    def test_add_member(self):
        first_name = "Eugene"
        last_name = "Debs"
        email_address = "debs.1855@gmail.com"
        phone_number = "+1 234-345-4567"
        payload = {
            "email_address": email_address,
            "first_name": first_name,
            "last_name": last_name,
            "phone_number": phone_number,
        }
        response = post_json(self.app, "/member", payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result["status"] == "success"
        assert result["data"] == {
            "account_created": False,
            "email_sent": False,
            "member": {
                "id": 2,
                "info": {
                    "first_name": first_name,
                    "last_name": last_name,
                    "email_address": email_address,
                    "phone_numbers": [{"name": None, "phone_number": phone_number}],
                },
                "roles": [],
            },
            "role_created": False
        }

        session = Session()
        member = session.query(Member).filter_by(email_address=email_address).one()
        assert member.first_name == "Eugene"
        assert member.last_name == "Debs"
        assert member.email_address == "debs.1855@gmail.com"
        assert member.normalized_email == "debs.1855@gmail.com"
        assert member.date_created is not None

    def test_add_member_multi_phones(self):
        first_name = "Eugene"
        last_name = "Debs"
        email_address = "debs.1855@gmail.com"
        phone_numbers = ["+1 234-345-4567", '+1 345-456-5678']
        payload = {
            "email_address": email_address,
            "first_name": first_name,
            "last_name": last_name,
            "phone_numbers": phone_numbers
        }
        response = post_json(self.app, "/member", payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result["status"] == "success"
        assert result["data"] == {
            "account_created": False,
            "email_sent": False,
            "member": {
                "id": 2,
                "info": {
                    "first_name": first_name,
                    "last_name": last_name,
                    "email_address": email_address,
                    "phone_numbers": [
                        {"name": None, "phone_number": num} for num in phone_numbers
                    ],
                },
                "roles": [],
            },
            "role_created": False
        }

        session = Session()
        member = session.query(Member).filter_by(email_address=email_address).one()
        assert member.first_name == "Eugene"
        assert member.last_name == "Debs"
        assert member.email_address == "debs.1855@gmail.com"
        assert member.normalized_email == "debs.1855@gmail.com"
        assert member.date_created is not None

    def test_update_member_dnc(self):
        payload = {"do_not_call": True, "do_not_email": True}
        response = put_json(self.app, "/member", payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result["status"] == "success"
        assert result["data"] == {
            "id": 1,
            "do_not_call": True,
            "do_not_email": True,
            "is_eligible": True,
            "info": {
                "first_name": None,
                "last_name": None,
                "email_address": SUPER_USER_EMAIL,
                "phone_numbers": [{"phone_number": "+1 425-123-2345", "name": "Home"}],
                "address": "123 Main St",
                "city": "Oakland",
                "zipcode": "94612",
            },
            "notes": "Some notes on this member",
            "membership": {
                "address": ["123 Main St", "Oakland 94612"],
                "phone_numbers": ["+1 425-123-2345"],
                "dues_paid_until": datetime(2028, 6, 1).astimezone(pytz.utc).isoformat(),
                "dues_paid_overridden_on": datetime(2020, 1, 1).astimezone(pytz.utc).isoformat(),
                "status": "GOOD_STANDING"
            },
            "roles": [
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "admin",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat(),
                },
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "member",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat(),
                },
            ],
            "meetings": [
                {"meeting_id": 2, "name": "General Meeting 2"},
                {"meeting_id": 3, "name": "General Meeting 3"},
            ],
            "votes": [],
            "onboarded_on": datetime(2020, 1, 2).astimezone(pytz.utc).isoformat(),
            "suspended_on": None,
            "left_chapter_on": None,
            "dues_are_paid": True,
            "meets_attendance_criteria": True,
            "active_in_committee": False,
            'tags': [
                {
                    "date_created": "2020-10-01T00:00:00+00:00",
                    "id": "tg_An709PbyGdEJVKro",
                    "name": "Tag 1",
                },
                {
                    "date_created": "2020-10-01T00:00:00+00:00",
                    "id": "tg_GwV4q0dZ7Op3WMeD",
                    "name": "Tag 2",
                },
            ],
        }

    def test_add_update_member_non_json(self):
        response = self.app.post("/member", data=None)
        assert response.status_code == 400

        response = self.app.put("/member", data=None)
        assert response.status_code == 400

    def test_make_admin(self):
        payload = {"email_address": SUPER_USER_EMAIL, "committee": 1}
        response = post_json(self.app, "/admin", payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {"status": "success"}

        session = Session()
        assert (
                session.query(Role)
                .filter_by(member_id=1, committee_id=1, role="admin")
                .count()
                == 1
        )

    def test_make_admin_non_json(self):
        response = self.app.post("/admin", data=None)
        assert response.status_code == 400

    def test_add_committee_member_role(self):
        payload = {
            "member_id": 1,
            "committee_id": 1,
            "role": "member",
        }
        response = post_json(self.app, "/member/role", payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {"status": "success"}

    def test_add_committee_admin_role(self):
        session = Session()
        committee_member_role = Role(member_id=1, committee_id=1, role="member")
        committee_admin_role = Role(member_id=1, committee_id=1, role="admin")
        session.add(committee_member_role)
        session.commit()
        payload = {
            "member_id": committee_admin_role.member_id,
            "committee_id": committee_admin_role.committee_id,
            "role": committee_admin_role.role,
        }
        response = post_json(self.app, "/member/role", payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {"status": "success"}

    def test_add_chapter_member_role_duplicate(self):
        payload = {
            "member_id": 1,
            "committee_id": None,
            "role": "admin",
        }
        response = post_json(self.app, "/member/role", payload=payload)
        assert response.status_code == 409

    def test_add_member_role_non_json(self):
        response = self.app.post("/admin", data=None)
        assert response.status_code == 400

    def test_remove_member_role(self):
        payload = {
            "member_id": 1,
            "committee_id": None,
            "role": "admin",
        }
        response = delete_json(self.app, "/member/role", payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {"status": "success"}

    def test_remove_member_role_nonexistent(self):
        payload = {
            "member_id": 1,
            "committee_id": 1,
            "role": "admin",
        }
        response = delete_json(self.app, "/member/role", payload=payload)
        assert response.status_code == 404

    def test_remove_member_role_non_json(self):
        response = self.app.delete("/member/role", data=None)
        assert response.status_code == 400

    def test_get_notes(self):
        response = get_json(self.app, "/admin/member/details?member_id=1")
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert json_response["notes"] == "Some notes on this member"

    def test_set_notes(self):
        """Set and re-read notes to verify integrity"""
        payload = {"notes": "These are some notes for the member"}
        response = put_json(self.app, "/admin/member/notes?member_id=1", payload)
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert json_response["notes"] == "These are some notes for the member"

    def test_set_notes_bad_response(self):
        payload = "These are some notes for the member"
        response = put_json(self.app, "/admin/member/notes?member_id=1", payload)
        assert response.status_code == 400

    def test_update_member_details(self):
        payload = {
            "first_name": "Huey",
            "last_name": "Newton",
            "pronouns": "he/him;they/them",
            "address": "345 Grain St",
            "city": "Fruitvale",
            "zipcode": "94601"
        }
        response = patch_json(self.app, "/admin/member?member_id=1", payload)
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert json_response == {
            "data": {
                "id": 1,
                "do_not_call": False,
                "do_not_email": False,
                "is_eligible": True,
                "info": {
                    "first_name": "Huey",
                    "last_name": "Newton",
                    "pronouns": "he/him;they/them",
                    "email_address": SUPER_USER_EMAIL,
                    "address": "345 Grain St",
                    "city": "Fruitvale",
                    "zipcode": "94601",
                    "phone_numbers": [
                        {"phone_number": "+1 425-123-2345", "name": "Home"}
                    ],
                },
                "notes": "Some notes on this member",
                "membership": {
                    "address": ["123 Main St", "Oakland 94612"],
                    "phone_numbers": ["+1 425-123-2345"],
                    "dues_paid_until": datetime(2028, 6, 1).astimezone(pytz.utc).isoformat(),
                    "dues_paid_overridden_on": datetime(2020, 1, 1).astimezone(
                        pytz.utc
                    ).isoformat(),
                    "status": "GOOD_STANDING"
                },
                "roles": [
                    {
                        "committee": "general",
                        "committee_name": "general",
                        "committee_id": -1,
                        "committee_inactive": False,
                        "role": "admin",
                        "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat(),
                    },
                    {
                        "committee": "general",
                        "committee_name": "general",
                        "committee_id": -1,
                        "committee_inactive": False,
                        "role": "member",
                        "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat(),
                    },
                ],
                "meetings": [
                    {"meeting_id": 2, "name": "General Meeting 2"},
                    {"meeting_id": 3, "name": "General Meeting 3"},
                ],
                "votes": [],
                "onboarded_on": datetime(2020, 1, 2).astimezone(pytz.utc).isoformat(),
                "suspended_on": None,
                "left_chapter_on": None,
                "dues_are_paid": True,
                "meets_attendance_criteria": True,
                "active_in_committee": False,
                'tags': [
                    {
                        "date_created": "2020-10-01T00:00:00+00:00",
                        "id": "tg_An709PbyGdEJVKro",
                        "name": "Tag 1",
                    },
                    {
                        "date_created": "2020-10-01T00:00:00+00:00",
                        "id": "tg_GwV4q0dZ7Op3WMeD",
                        "name": "Tag 2",
                    },
                ],
            },
            "status": "success"
        }

    def test_update_member_details_unknown_field(self):
        payload = {"horoscope": "aquarius"}
        response = patch_json(self.app, "/admin/member?member_id=1", payload)
        assert response.status_code == 400

    def test_add_phone_number(self):
        payload = {"phone_number": "408-224-5555", "name": "Cell"}
        response = post_json(
            self.app, "/admin/member/phone_numbers?member_id=1", payload
        )
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert len(json_response) == 2
        assert json_response == [
            {"phone_number": "+1 425-123-2345", "name": "Home"},
            {"phone_number": "+1 408-224-5555", "name": "Cell"},
        ]

    def test_add_phone_number_invalid_request(self):
        payload = "425-123-2345"
        response = post_json(
            self.app, "/admin/member/phone_numbers?member_id=1", payload
        )
        assert response.status_code == 400

    def test_add_phone_number_conflict(self):
        payload = {"phone_number": "425-123-2345", "name": "Other home"}
        response = post_json(
            self.app, "/admin/member/phone_numbers?member_id=1", payload
        )
        assert response.status_code == 409

    def test_delete_phone_number(self):
        payload = {"phone_number": "425-123-2345"}
        response = delete_json(
            self.app, "/admin/member/phone_numbers?member_id=1", payload
        )
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert len(json_response) == 0

    def test_add_and_delete_phone_number(self):
        payload = {"phone_number": "408-224-5555", "name": "Cell"}
        response = post_json(
            self.app, "/admin/member/phone_numbers?member_id=1", payload
        )
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert len(json_response) == 2
        assert json_response == [
            {"phone_number": "+1 425-123-2345", "name": "Home"},
            {"phone_number": "+1 408-224-5555", "name": "Cell"},
        ]
        payload = {"phone_number": "408-224-5555"}
        response = delete_json(
            self.app, "/admin/member/phone_numbers?member_id=1", payload
        )
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert len(json_response) == 1
        assert json_response == [
            {"phone_number": "+1 425-123-2345", "name": "Home"},
        ]

    def test_delete_phone_number_non_existent(self):
        payload = {"phone_number": "808-808-8080"}
        response = delete_json(
            self.app, "/admin/member/phone_numbers?member_id=1", payload
        )
        assert response.status_code == 404

    def test_add_email_address(self):
        payload = {
            "email_address": "hello@world.com",
        }
        response = post_json(
            self.app, "/admin/member/email_addresses?member_id=1", payload
        )
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert len(json_response) == 1
        assert json_response == [
            {
                "email_address": "hello@world.com",
                "name": None,
                "preferred": False,
                "verified": False,
            }
        ]

    def test_delete_email_address(self):
        payload = {
            "email_address": "hello@world.com",
        }
        response = post_json(
            self.app, "/admin/member/email_addresses?member_id=1", payload
        )
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert len(json_response) == 1
        assert json_response == [
            {
                "email_address": "hello@world.com",
                "name": None,
                "preferred": False,
                "verified": False,
            }
        ]
        response = delete_json(
            self.app, "/admin/member/email_addresses?member_id=1", payload
        )
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert len(json_response) == 0

    def test_delete_email_address_not_found(self):
        payload = {
            "email_address": "hello@world.com",
        }
        response = delete_json(
            self.app, "/admin/member/email_addresses?member_id=1", payload
        )
        assert response.status_code == 404

    def test_add_tag_should_add_tag_and_member_association(self):
        response = put_json(self.app, "/member/1/tags", {
            'tags': ['test1', 'test2']
        })
        assert response.status_code == 200

        session = Session()

        tags = session.query(Tag).all()
        assert len(tags) > 0

        tag_names = [tag.name for tag in tags]
        assert 'test1' in tag_names
        assert 'test2' in tag_names

        member = session.query(Member).get(1)
        assert member is not None

        tag_names = [tag.name for tag in member.tags]
        assert 'test1' in tag_names
        assert 'test2' in tag_names

    def test_remove_tag_should_remove_member_tags(self):
        response = delete_json(self.app, "/member/1/tags", {
            'tags': ['Tag 1', 'Tag 2']
        })
        assert response.status_code == 200

        session = Session()

        member = session.query(Member).get(1)
        assert member is not None

        tag_ids = [tag.id for tag in member.tags]
        assert 10 not in tag_ids
        assert 11 not in tag_ids

    def test_import_members(self):
        with open("tests/membership/web/members.csv", "rb") as import_file:
            payload = {
                "file": (BytesIO(import_file.read()), "members.csv"),
                "next_reg_meeting": "2020-10-01"
            }
            response = self.app.put(
                "/import", data=payload, content_type="multipart/form-data"
            )
            assert response.status_code == 200

    def test_import_natl_csv_missing_signature(self):
        import io
        import zipfile

        stream = io.BytesIO()
        with zipfile.ZipFile(stream, "w") as zipfp:
            zipfp.write("tests/membership/web/members.csv", "members.csv")
        stream.seek(0)

        response = self.app.post(
            "/import/auto/natl",
            content_type="multipart/form-data",
            data={
                "attachment-1": (stream.read(), "members.zip"),
                "timestamp": "1587356400",
                "token": "invalid_token",
            },
        )
        assert response.status_code == HTTPStatus.UNAUTHORIZED

    # def test_import_natl_csv_bare_file(self):
    #     session = Session()

    #     with open("tests/membership/web/members.csv", "rb") as stream:
    #         response = self.app.post(
    #             "/import/auto/natl",
    #             content_type="multipart/form-data",
    #             data={
    #                 "attachment-1": (stream, "members.csv"),
    #                 "signature": MailgunEmailConnector.generate_signature(
    #                     signing_key=MAILGUN_HTTP_WEBHOOK_KEY,
    #                     token="some-random-test-token",
    #                     timestamp="1587356400",
    #                 ),
    #                 "timestamp": "1587356400",
    #                 "token": "some-random-test-token",
    #             },
    #         )
    #     assert response.status_code == HTTPStatus.OK

    #     event = (
    #         session.query(ReviewableEvent)
    #         .filter_by(event_type=ReviewableEventType.DSAUSA_EMAIL)
    #         .one()
    #     )
    #     assert event != None
    #     assert event.slug.startswith("natl-csv-")

    #     participant = (
    #         session.query(ReviewableEventParticipant)
    #         .filter_by(email_address="janelee@example.com")
    #         .one()
    #     )
    #     assert participant != None
    #     assert participant.first_name == "Jane"
    #     assert participant.last_name == "Lee"
    #     assert "415-789-2345" in participant.phone_numbers

    # def test_import_natl_csv(self):
    #     session = Session()

    #     import io
    #     import zipfile

    #     stream = io.BytesIO()
    #     with zipfile.ZipFile(stream, "w") as zipfp:
    #         zipfp.write("tests/membership/web/members.csv", "members.csv")
    #     stream.seek(0)

    #     response = self.app.post(
    #         "/import/auto/natl",
    #         content_type="multipart/form-data",
    #         data={
    #             "attachment-1": (stream, "members.zip"),
    #             "signature": MailgunEmailConnector.generate_signature(
    #                 signing_key=MAILGUN_HTTP_WEBHOOK_KEY,
    #                 token="some-random-test-token",
    #                 timestamp="1587356400",
    #             ),
    #             "timestamp": "1587356400",
    #             "token": "some-random-test-token",
    #         },
    #     )
    #     assert response.status_code == HTTPStatus.OK

    #     event = (
    #         session.query(ReviewableEvent)
    #         .filter_by(event_type=ReviewableEventType.DSAUSA_EMAIL)
    #         .one()
    #     )
    #     assert event != None
    #     assert event.slug.startswith("natl-csv-")

    #     participant = (
    #         session.query(ReviewableEventParticipant)
    #         .filter_by(email_address="janelee@example.com")
    #         .one()
    #     )
    #     assert participant != None
    #     assert participant.first_name == "Jane"
    #     assert participant.last_name == "Lee"
    #     assert participant.phone_numbers is not None
    #     assert "415-789-2345" in participant.phone_numbers

    def test_import_natl_csv_empty(self):
        import io
        import zipfile

        stream = io.BytesIO()
        with zipfile.ZipFile(stream, "w") as zipfp:
            zipfp.write("tests/membership/web/members-empty.csv", "members.csv")
        stream.seek(0)

        response = self.app.post(
            "/import/auto/natl",
            content_type="multipart/form-data",
            data={
                "attachment-1": (stream, "members.zip"),
                "signature": MailgunEmailConnector.generate_signature(
                    signing_key=MAILGUN_HTTP_WEBHOOK_KEY,
                    token="some-random-test-token",
                    timestamp="1587356400",
                ),
                "timestamp": "1587356400",
                "token": "some-random-test-token",
            },
        )
        assert response.status_code == HTTPStatus.NOT_ACCEPTABLE

    def test_import_natl_csv_wrong_zip_name(self):
        import io
        import zipfile

        stream = io.BytesIO()
        with zipfile.ZipFile(stream, "w") as zipfp:
            zipfp.write("tests/membership/web/members-empty.csv", "wrong-entry.csv")
        stream.seek(0)

        response = self.app.post(
            "/import/auto/natl",
            content_type="multipart/form-data",
            data={
                "attachment-1": (stream, "members.zip"),
                "signature": MailgunEmailConnector.generate_signature(
                    signing_key=MAILGUN_HTTP_WEBHOOK_KEY,
                    token="some-random-test-token",
                    timestamp="1587356400",
                ),
                "timestamp": "1587356400",
                "token": "some-random-test-token",
            },
        )
        assert response.status_code == HTTPStatus.NOT_ACCEPTABLE

    def test_import_natl_csv_malformed(self):
        import io

        stream = io.BytesIO()
        stream.write(b"???")

        stream.seek(0)

        response = self.app.post(
            "/import/auto/natl",
            content_type="multipart/form-data",
            data={
                "attachment-1": (stream, "members.zip"),
                "signature": MailgunEmailConnector.generate_signature(
                    signing_key=MAILGUN_HTTP_WEBHOOK_KEY,
                    token="some-random-test-token",
                    timestamp="1587356400",
                ),
                "timestamp": "1587356400",
                "token": "some-random-test-token",
            },
        )
        assert response.status_code == HTTPStatus.NOT_ACCEPTABLE
