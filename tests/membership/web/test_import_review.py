from datetime import datetime
from http import HTTPStatus

import devtools
import pytest
from faker import Faker
from freezegun.api import freeze_time
from sqlalchemy import orm

from config.database_config import SUPER_USER_EMAIL
from membership.auth.needs import SxRoleNeedsList
from membership.auth.needs_derived import DANGEROUS_superuser_defaults, user_defaults
from membership.database.base import Session, engine, metadata
from membership.database.models import (
    Contact,
    EmailAddress,
    SecurityPrincipal,
    SecurityPrincipalRole,
    SecurityPrincipalRoleAssociation,
    User,
)
from membership.web.base_app import app
from tests.flask_utils import get_json, patch_json, post_json, put_json
from tests.membership.util.constants import LOCAL_ZIP_CITY_LIST


def _generate_contact(fake: Faker):
    name = fake.name()
    zipcode, city = fake.random.choice(LOCAL_ZIP_CITY_LIST)
    return {
        "full_name": name,
        "display_name": name.split(" ")[0],
        "email_addresses": [fake.ascii_safe_email()],
        "phone_numbers": [fake.phone_number()],
        "mailing_addresses": [{"zipcode": zipcode, "city": city, "state": "CA"}],
    }


EXPECTED_RESPONSE_IMPORT_EVENT_2P = {
    "object": "import_event",
    "id": "ime_ZP9BEa0l40kpdjLW",
    "creator": "sec_xdA1WQVyr5oj9J2O",
    "date_created": "2021-04-21T00:00:00+00:00",
    "title": "Pass the PRO Act Phone Bank",
    "event_type": "csv_import",
    "event_info": {
        "location": "Online",
        "date_started": "2021-04-20T18:00:00-07:00",
        "event_id": None,
        "date_ended": "2021-04-20T19:00:00-07:00",
        "organizer": "Some Organization",
    },
    "reviewer": None,
    "date_reviewed": None,
    "review_action_counts": {"proposed_create": 2},
    "review_actions": {
        "object": "list",
        "url": "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
        "has_more": False,
        "data": [
            {
                "object": "import_event_review_action",
                "id": "ima_dNkOaJpVnpVqljYW",
                "import_event_id": "ime_ZP9BEa0l40kpdjLW",
                "action": "proposed_create",
                "merge_into_contact_id": None,
                "merge": None,
                "match_justification": None,
                "participant": {
                    "full_name": "Norma Fisher",
                    "display_name": "Norma",
                    "email_addresses": ["ysullivan@example.com"],
                    "phone_numbers": ["+1 593-824-2194 ext. 8924"],
                    "mailing_addresses": [
                        {
                            "city": "Orinda",
                            "state": "CA",
                            "zipcode": "94563",
                            "line1": None,
                            "line2": None,
                            "source_created": None,
                            "consent_to_mail": None,
                            "date_created": None,
                        }
                    ],
                    "admin_notes": None,
                    "biography": None,
                    "tags": None,
                    "custom_fields": None,
                    "profile_pic": None,
                    "pronouns": None,
                    "date_created": None,
                    "source_created": None,
                },
            },
            {
                "object": "import_event_review_action",
                "id": "ima_KQkvzo5P29nWDmgL",
                "import_event_id": "ime_ZP9BEa0l40kpdjLW",
                "action": "proposed_create",
                "merge_into_contact_id": None,
                "merge": None,
                "match_justification": None,
                "participant": {
                    "full_name": "Benjamin Jefferson",
                    "display_name": "Benjamin",
                    "email_addresses": ["wcastro@example.com"],
                    "phone_numbers": ["+1 938-778-4080 ext. 16097"],
                    "mailing_addresses": [
                        {
                            "city": "Berkeley",
                            "state": "CA",
                            "zipcode": "94705",
                            "line1": None,
                            "line2": None,
                            "source_created": None,
                            "consent_to_mail": None,
                            "date_created": None,
                        }
                    ],
                    "admin_notes": None,
                    "biography": None,
                    "tags": None,
                    "custom_fields": None,
                    "profile_pic": None,
                    "pronouns": None,
                    "date_created": None,
                    "source_created": None,
                },
            },
        ],
    },
}


class TestImportReview:
    @pytest.fixture
    def tables(self):
        metadata.create_all(engine)
        yield
        metadata.drop_all(engine)

    @pytest.fixture
    def client(self, tables):
        app.testing = True
        return app.test_client()

    @pytest.fixture
    def session(self, tables):
        session = Session()

        yield session

        session.close()

    @pytest.fixture
    def admin_user(self, session: orm.Session) -> User:
        # 🛸
        first_contact = Contact(
            date_created=datetime.fromisoformat("2021-01-01T04:21:09"),
            full_name="Carlos Marcos",
            display_name="Carlos",
            pronouns="he/him;they/them",
        )
        email_address = EmailAddress(
            date_created=datetime.fromisoformat("2021-01-01T04:21:09"),
            value=SUPER_USER_EMAIL,
            contact=first_contact,
        )
        security_principal = SecurityPrincipal()
        admin_needs = SxRoleNeedsList(__root__=DANGEROUS_superuser_defaults())
        assert admin_needs is not None
        role = SecurityPrincipalRole(
            slug="test_admin", name="Test Admin", autogenerated=True, needs=admin_needs
        )

        association = SecurityPrincipalRoleAssociation(
            role=role, security_principal=security_principal
        )
        user = User(contact=first_contact, principal=security_principal)
        session.add_all(
            [first_contact, email_address, security_principal, user, role, association]
        )
        session.commit()

        return user

    @pytest.fixture
    def regular_user(self, session: orm.Session) -> User:
        # 🛸
        first_contact = Contact(
            date_created=datetime.fromisoformat("2021-01-01T00:00:00"),
            full_name="Roberta Songthrush",
            display_name="Bertie",
            pronouns="she/her",
        )
        email_address = EmailAddress(
            date_created=datetime.fromisoformat("2021-01-01T04:21:09"),
            value="bertie@example.bird",
            contact=first_contact,
        )
        security_principal = SecurityPrincipal()
        user_needs = SxRoleNeedsList(__root__=user_defaults())
        assert user_needs is not None
        role = SecurityPrincipalRole(
            slug="everyone", name="Everyone", autogenerated=True, needs=user_needs
        )
        association = SecurityPrincipalRoleAssociation(
            role=role, security_principal=security_principal
        )
        user = User(contact=first_contact, principal=security_principal)
        session.add_all(
            [first_contact, email_address, security_principal, user, role, association]
        )
        session.commit()

        return user

    @pytest.fixture
    @freeze_time("2021-04-22")
    def import_event(self, client, request, admin_user, faker: Faker):
        attendance_num_participants = request.node.get_closest_marker(
            "attendance_num_participants"
        )
        if attendance_num_participants is None:
            # Handle missing marker in some way...
            num_participants = 1
        else:
            num_participants = attendance_num_participants.args[0]

        tag_request = {
            "name": "Phone bank attendee",
        }
        response = post_json(client, "/tags", tag_request)
        assert response.status_code == HTTPStatus.CREATED
        assert response.json is not None
        tag_id = response.json["id"]

        create_request = {
            "title": "Pass the PRO Act Phone Bank",
            "event_type": "csv_import",
            "event_info": {
                "location": "Online",
                "date_started": "2021-04-20T18:00:00-07:00",
                "date_ended": "2021-04-20T19:00:00-07:00",
                "organizer": "Some Organization",
            },
            "date_created": "2021-04-21T00:00:00+00:00",
            "tags": [tag_id],
            "participants": [_generate_contact(faker) for i in range(num_participants)],
        }
        response = post_json(client, "/imports", create_request)
        assert response.status_code == HTTPStatus.CREATED
        return response.json

    class TestModels:
        def test_sanity_merge_action_types(self):
            from membership.schemas.rest.import_events import (
                MERGE_ACTION_TYPES,
                ImportEventReviewActionType,
            )

            action_types = set(e.value for e in ImportEventReviewActionType)
            assert all(m in action_types for m in MERGE_ACTION_TYPES)

        def test_sanity_merge_list_attributes(self):
            from membership.schemas.rest.contacts import SxContact
            from membership.schemas.rest.import_events import MERGE_LIST_ATTRIBUTES

            contact_attributes = set(SxContact.schema()["properties"].keys())
            for k in MERGE_LIST_ATTRIBUTES:
                assert k in contact_attributes

        def test_sanity_import_review_merge(self):
            from membership.schemas.rest.contacts import SxContact
            from membership.schemas.rest.import_events import SxImportReviewMerge

            contact_attributes = set(SxContact.schema()["properties"].keys())
            for k in SxImportReviewMerge.schema()["properties"].keys():
                assert k in contact_attributes

    class TestCreate:
        @freeze_time("2021-04-20")
        def test_create_event(self, session, client, tables, admin_user):
            post_response = post_json(
                client,
                "/imports",
                {
                    "title": "International Enjoyment Day",
                    "date_created": "2021-04-20T04:20:00",
                    "event_type": "csv_import",
                    "event_info": {"location": "San Jose International Airport"},
                    "participants": [
                        {
                            "full_name": "Gritty McGritFace",
                            "display_name": "Comrade Gritty",
                            "email_addresses": ["gritty@example.com"],
                            "phone_numbers": ["(425) 555-6423"],
                            "mailing_addresses": [
                                {"city": "San Jose", "state": "CA", "zipcode": "95123"}
                            ],
                        }
                    ],
                },
            )
            assert post_response.status_code == HTTPStatus.CREATED
            response = get_json(client, "/imports/ime_ZP9BEa0l40kpdjLW")
            assert response.status_code == HTTPStatus.OK
            assert response.json == {
                "object": "import_event",
                "id": "ime_ZP9BEa0l40kpdjLW",
                "creator": "sec_xdA1WQVyr5oj9J2O",
                "date_created": "2021-04-20T04:20:00+00:00",
                "title": "International Enjoyment Day",
                "event_type": "csv_import",
                "event_info": {
                    "location": "San Jose International Airport",
                    "date_started": None,
                    "date_ended": None,
                    "event_id": None,
                    "organizer": None,
                },
                "reviewer": None,
                "date_reviewed": None,
                "review_actions": {
                    "object": "list",
                    "url": "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                    "has_more": False,
                    "data": [
                        {
                            "object": "import_event_review_action",
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "import_event_id": "ime_ZP9BEa0l40kpdjLW",
                            "action": "proposed_create",
                            "merge_into_contact_id": None,
                            "merge": None,
                            "match_justification": None,
                            "participant": {
                                "full_name": "Gritty McGritFace",
                                "display_name": "Comrade Gritty",
                                "email_addresses": ["gritty@example.com"],
                                "phone_numbers": ["+1 425-555-6423"],
                                "mailing_addresses": [
                                    {
                                        "city": "San Jose",
                                        "state": "CA",
                                        "zipcode": "95123",
                                        "line1": None,
                                        "line2": None,
                                        "source_created": None,
                                        "consent_to_mail": None,
                                        "date_created": None,
                                    }
                                ],
                                "admin_notes": None,
                                "biography": None,
                                "tags": None,
                                "custom_fields": None,
                                "profile_pic": None,
                                "pronouns": None,
                                "date_created": None,
                                "source_created": None,
                            },
                        },
                    ],
                },
                "review_action_counts": {"proposed_create": 1},
            }

    class TestCreateErrors:
        @freeze_time("2021-04-20")
        def test_create_event_no_title(self, session, client, tables, admin_user):
            post_response = post_json(
                client,
                "/imports",
                {
                    "date_created": "2021-04-20T04:20:00",
                    "event_type": "csv_import",
                    "event_info": {"location": "San Jose International Airport"},
                    "participants": [
                        {
                            "first_name": "Gritty",
                            "last_name": "McGritFace",
                            "email_address": "gritty@gmail.com",
                            "phone_numbers": ["(425) 555-6423"],
                            "city": "San Jose",
                            "zipcode": "95113",
                        }
                    ],
                },
            )
            assert post_response.status_code == HTTPStatus.BAD_REQUEST

        @freeze_time("2021-04-20")
        def test_create_event_no_participants(
            self, session, client, tables, admin_user
        ):
            post_response = post_json(
                client,
                "/imports",
                {
                    "title": "International Enjoyment Day",
                    "date_created": "2021-04-20T04:20:00",
                    "event_type": "csv_import",
                    "event_info": {"location": "San Jose International Airport"},
                    "participants": [],
                },
            )
            assert post_response.status_code == HTTPStatus.BAD_REQUEST
            response = get_json(client, "/imports/ime_ZP9BEa0l40kpdjLW")
            assert response.status_code == HTTPStatus.NOT_FOUND

    class TestGet:
        @pytest.mark.attendance_num_participants(2)
        def test_list_events(self, import_event, session, client, admin_user):
            response = get_json(client, "/imports")
            assert response.status_code == HTTPStatus.OK
            assert response.json == {
                "object": "list",
                "url": "/imports",
                "has_more": False,
                "data": [EXPECTED_RESPONSE_IMPORT_EVENT_2P],
            }

        def test_get_attendance_empty(self, session, client, admin_user):
            response = get_json(client, "/imports")
            assert response.status_code == HTTPStatus.OK
            assert response.json == {
                "object": "list",
                "url": "/imports",
                "has_more": False,
                "data": [],
            }

        @pytest.mark.attendance_num_participants(2)
        @freeze_time("2021-04-20")
        def test_get_event(self, import_event, client):
            assert import_event is not None
            response = get_json(client, "/imports/ime_ZP9BEa0l40kpdjLW")
            assert response.status_code == HTTPStatus.OK
            assert response.json == EXPECTED_RESPONSE_IMPORT_EVENT_2P

    class TestGetErrors:
        def test_get_bad_event(self, import_event, client):
            response = get_json(client, "/imports/ime_ZP9123456kpdjLW")
            assert response.status_code == HTTPStatus.NOT_FOUND
            devtools.debug(response.json)

    class TestUpdate:
        @pytest.mark.attendance_num_participants(2)
        @freeze_time("2021-04-20")
        def test_update_actions_no_change(
            self, import_event, client, session, admin_user
        ):
            existing_review_response = get_json(client, "/imports/ime_ZP9BEa0l40kpdjLW")
            assert existing_review_response.status_code == HTTPStatus.OK
            assert existing_review_response.json is not None
            assert existing_review_response.json["date_reviewed"] == None
            assert existing_review_response.json["reviewer"] == None
            existing_review_actions = existing_review_response.json["review_actions"]

            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [],
                },
            )
            assert response.status_code == HTTPStatus.OK
            assert response.json is not None
            assert len(response.json["data"]) == 0

            updated_review_response = get_json(client, "/imports/ime_ZP9BEa0l40kpdjLW")
            assert updated_review_response.status_code == HTTPStatus.OK
            assert updated_review_response.json is not None
            assert updated_review_response.json["date_reviewed"] == None
            assert updated_review_response.json["reviewer"] == None
            assert (
                updated_review_response.json["review_actions"]
                == existing_review_actions
            )

        @pytest.mark.attendance_num_participants(1)
        def test_update_actions_multiple_times(
            self, import_event, session, client, admin_user
        ):
            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "create_new_contact",
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.OK
            assert response.json == {
                "object": "list",
                "url": "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                "has_more": False,
                "data": [
                    {
                        "object": "import_event_review_action",
                        "id": "ima_dNkOaJpVnpVqljYW",
                        "import_event_id": "ime_ZP9BEa0l40kpdjLW",
                        "action": "create_new_contact",
                        "merge_into_contact_id": None,
                        "merge": None,
                        "match_justification": None,
                        "participant": {
                            "full_name": "Norma Fisher",
                            "display_name": "Norma",
                            "email_addresses": ["ysullivan@example.com"],
                            "phone_numbers": ["+1 593-824-2194 ext. 8924"],
                            "mailing_addresses": [
                                {
                                    "city": "Orinda",
                                    "state": "CA",
                                    "zipcode": "94563",
                                    "line1": None,
                                    "line2": None,
                                    "source_created": None,
                                    "consent_to_mail": None,
                                    "date_created": None,
                                }
                            ],
                            "admin_notes": None,
                            "biography": None,
                            "tags": None,
                            "custom_fields": None,
                            "profile_pic": None,
                            "pronouns": None,
                            "date_created": None,
                            "source_created": None,
                        },
                    }
                ],
            }

            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {"id": "ima_dNkOaJpVnpVqljYW", "action": "ignore_once"}
                    ],
                },
            )
            assert response.status_code == HTTPStatus.OK
            assert response.json == {
                "object": "list",
                "url": "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                "has_more": False,
                "data": [
                    {
                        "object": "import_event_review_action",
                        "id": "ima_dNkOaJpVnpVqljYW",
                        "import_event_id": "ime_ZP9BEa0l40kpdjLW",
                        "action": "ignore_once",
                        "merge_into_contact_id": None,
                        "merge": None,
                        "match_justification": None,
                        "participant": {
                            "full_name": "Norma Fisher",
                            "display_name": "Norma",
                            "email_addresses": ["ysullivan@example.com"],
                            "phone_numbers": ["+1 593-824-2194 ext. 8924"],
                            "mailing_addresses": [
                                {
                                    "city": "Orinda",
                                    "state": "CA",
                                    "zipcode": "94563",
                                    "line1": None,
                                    "line2": None,
                                    "source_created": None,
                                    "consent_to_mail": None,
                                    "date_created": None,
                                }
                            ],
                            "admin_notes": None,
                            "biography": None,
                            "tags": None,
                            "custom_fields": None,
                            "profile_pic": None,
                            "pronouns": None,
                            "date_created": None,
                            "source_created": None,
                        },
                    }
                ],
            }
            assert response.status_code == HTTPStatus.OK

        @pytest.mark.attendance_num_participants(2)
        @freeze_time("2021-04-20")
        def test_update_actions_skip_all(
            self, import_event, client, session, admin_user
        ):
            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "ignore_once",
                        },
                        {
                            "id": "ima_KQkvzo5P29nWDmgL",
                            "action": "ignore_once",
                        },
                    ],
                },
            )
            assert response.status_code == HTTPStatus.OK
            assert response.json == {
                "object": "list",
                "url": "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                "has_more": False,
                "data": [
                    {
                        "object": "import_event_review_action",
                        "id": "ima_dNkOaJpVnpVqljYW",
                        "import_event_id": "ime_ZP9BEa0l40kpdjLW",
                        "action": "ignore_once",
                        "merge_into_contact_id": None,
                        "merge": None,
                        "match_justification": None,
                        "participant": {
                            "full_name": "Norma Fisher",
                            "display_name": "Norma",
                            "email_addresses": ["ysullivan@example.com"],
                            "phone_numbers": ["+1 593-824-2194 ext. 8924"],
                            "mailing_addresses": [
                                {
                                    "city": "Orinda",
                                    "state": "CA",
                                    "zipcode": "94563",
                                    "line1": None,
                                    "line2": None,
                                    "source_created": None,
                                    "consent_to_mail": None,
                                    "date_created": None,
                                }
                            ],
                            "admin_notes": None,
                            "biography": None,
                            "tags": None,
                            "custom_fields": None,
                            "profile_pic": None,
                            "pronouns": None,
                            "date_created": None,
                            "source_created": None,
                        },
                    },
                    {
                        "object": "import_event_review_action",
                        "id": "ima_KQkvzo5P29nWDmgL",
                        "import_event_id": "ime_ZP9BEa0l40kpdjLW",
                        "action": "ignore_once",
                        "merge_into_contact_id": None,
                        "merge": None,
                        "match_justification": None,
                        "participant": {
                            "full_name": "Benjamin Jefferson",
                            "display_name": "Benjamin",
                            "email_addresses": ["wcastro@example.com"],
                            "phone_numbers": ["+1 938-778-4080 ext. 16097"],
                            "mailing_addresses": [
                                {
                                    "city": "Berkeley",
                                    "state": "CA",
                                    "zipcode": "94705",
                                    "line1": None,
                                    "line2": None,
                                    "source_created": None,
                                    "consent_to_mail": None,
                                    "date_created": None,
                                }
                            ],
                            "admin_notes": None,
                            "biography": None,
                            "tags": None,
                            "custom_fields": None,
                            "profile_pic": None,
                            "pronouns": None,
                            "date_created": None,
                            "source_created": None,
                        },
                    },
                ],
            }

        @pytest.mark.attendance_num_participants(1)
        @freeze_time("2021-04-20")
        def test_update_actions_change_type_erases_merge_info(
            self, import_event, client, session, admin_user
        ):
            import_event_id = import_event["id"]

            response = patch_json(
                client,
                f"/imports/{import_event_id}/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "match_manual",
                            "merge_into_contact_id": "ct_1rAEnlwZAQekpgqD",
                            "merge": {
                                "full_name": {
                                    "attribute": "full_name",
                                    "merge_action": "take_incoming",
                                }
                            },
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.OK

            response = patch_json(
                client,
                f"/imports/{import_event_id}/review_actions",
                {
                    "actions": [
                        {"id": "ima_dNkOaJpVnpVqljYW", "action": "ignore_once"}
                    ],
                },
            )
            assert response.status_code == HTTPStatus.OK
            assert response.json is not None
            assert len(response.json["data"]) == 1
            assert response.json["data"][0]["action"] == "ignore_once"
            assert response.json["data"][0]["merge_into_contact_id"] == None
            assert response.json["data"][0]["merge"] == None

    class TestReviewConfirm:
        @pytest.mark.attendance_num_participants(1)
        @freeze_time("2021-04-20")
        def test_merge_on_email(self, client, session, admin_user, regular_user, faker):
            # Create contact
            email_address = "ysullivan@example.com"
            create_contact_response = post_json(
                client,
                "/contacts",
                {
                    "full_name": "Name",
                    "display_name": "Name",
                    "email_addresses": [email_address],
                },
            )
            assert create_contact_response.status_code == 201
            assert create_contact_response.json is not None
            contact_id = create_contact_response.json["id"]

            # Make sure the contact has no items
            get_contact_response = get_json(client, f"/contacts/{contact_id}")
            assert get_contact_response.status_code == 200
            assert get_contact_response.json is not None
            assert len(get_contact_response.json["email_addresses"]["data"]) == 1
            assert (
                get_contact_response.json["email_addresses"]["data"][0]["value"]
                == email_address
            )
            assert len(get_contact_response.json["phone_numbers"]["data"]) == 0
            assert len(get_contact_response.json["mailing_addresses"]["data"]) == 0
            assert get_contact_response.json["city"] == None

            # Create an import event with the same email address
            contact = _generate_contact(faker)
            contact["email_addresses"] = [email_address]
            create_request = {
                "title": "Test CSV",
                "event_type": "csv_import",
                "date_created": "2021-04-21T00:00:00+00:00",
                "participants": [contact],
            }
            create_import_response = post_json(client, "/imports", create_request)
            assert create_import_response.status_code == 201
            assert create_import_response.json is not None
            assert (
                create_import_response.json["review_actions"]["data"][0]["action"]
                == "proposed_match"
            )
            assert (
                create_import_response.json["review_actions"]["data"][0][
                    "merge_into_contact_id"
                ]
                == contact_id
            )
            assert create_import_response.json["review_actions"]["data"][0][
                "match_justification"
            ] == {
                "confidence": "strong",
                "strong_matches": ["email_addresses"],
                "fuzzy_matches": [],
            }
            import_event_id = create_import_response.json["id"]

            # Update the review
            review_request = {
                "actions": [
                    {
                        "id": "ima_dNkOaJpVnpVqljYW",
                        "action": "match_auto",
                    },
                ]
            }
            review_event_response = patch_json(
                client, f"/imports/{import_event_id}/review_actions", review_request
            )
            assert review_event_response.status_code == 200
            assert review_event_response.json is not None
            assert review_event_response.json["data"][0]["action"] == "match_auto"

            # Confirm the review
            confirm_response = put_json(client, f"/imports/{import_event_id}/confirm")
            assert confirm_response.status_code == HTTPStatus.OK

            # Confirm that the contact got modified
            get_contact_response = get_json(client, f"/contacts/{contact_id}")
            assert get_contact_response.status_code == 200
            assert get_contact_response.json is not None

            assert get_contact_response.json["full_name"] == "Name"

            assert len(get_contact_response.json["email_addresses"]["data"]) == 1
            assert (
                get_contact_response.json["email_addresses"]["data"][0]["value"]
                == email_address
            )

            assert len(get_contact_response.json["phone_numbers"]["data"]) == 1
            assert len(get_contact_response.json["mailing_addresses"]["data"]) == 1
            assert get_contact_response.json["city"] == "Orinda"

        @pytest.mark.attendance_num_participants(1)
        @freeze_time("2021-04-20")
        def test_merge_colliding_custom_field_nondestructive(
            self, client, session, admin_user, regular_user, faker
        ):
            # create custom field
            custom_field_payload = {"name": "Ice cream flavor", "field_type": "text"}
            response = post_json(client, "/custom_fields", custom_field_payload)
            assert response.status_code == HTTPStatus.CREATED
            assert response.json is not None
            custom_field_id = response.json["id"]

            # Create contact
            email_address = "ysullivan@example.com"
            create_contact_response = post_json(
                client,
                "/contacts",
                {
                    "full_name": "Name",
                    "display_name": "Name",
                    "email_addresses": [email_address],
                    "custom_fields": {custom_field_id: "Chocolate"},
                },
            )
            assert create_contact_response.status_code == 201
            assert create_contact_response.json is not None
            assert create_contact_response.json["custom_fields"] is not None
            assert len(create_contact_response.json["custom_fields"]) == 1
            contact_id = create_contact_response.json["id"]

            # Create an import event with the same email address and custom field
            contact = _generate_contact(faker)
            contact["email_addresses"] = [email_address]
            contact["custom_fields"] = {custom_field_id: "Vanilla"}
            create_request = {
                "title": "Test CSV",
                "event_type": "csv_import",
                "date_created": "2021-04-21T00:00:00+00:00",
                "participants": [contact],
            }
            create_import_response = post_json(client, "/imports", create_request)
            assert create_import_response.status_code == 201
            assert create_import_response.json is not None
            assert (
                create_import_response.json["review_actions"]["data"][0]["action"]
                == "proposed_match"
            )
            assert (
                create_import_response.json["review_actions"]["data"][0][
                    "merge_into_contact_id"
                ]
                == contact_id
            )
            assert create_import_response.json["review_actions"]["data"][0][
                "match_justification"
            ] == {
                "confidence": "strong",
                "strong_matches": ["email_addresses"],
                "fuzzy_matches": [],
            }
            import_event_id = create_import_response.json["id"]

            # Confirm the review
            confirm_response = put_json(client, f"/imports/{import_event_id}/confirm")
            assert confirm_response.status_code == HTTPStatus.OK

            # Confirm that the contact got modified
            get_contact_response = get_json(client, f"/contacts/{contact_id}")
            assert get_contact_response.status_code == 200
            assert get_contact_response.json is not None

            assert get_contact_response.json["full_name"] == "Name"

            assert len(get_contact_response.json["email_addresses"]["data"]) == 1
            assert (
                get_contact_response.json["email_addresses"]["data"][0]["value"]
                == email_address
            )
            assert len(get_contact_response.json["custom_fields"]) == 1

            # Confirm the contact custom field was not modified (non-destructive default)
            assert (
                get_contact_response.json["custom_fields"][custom_field_id]
                == "Chocolate"
            )

    class TestInference:
        @pytest.mark.attendance_num_participants(1)
        @freeze_time("2021-04-20")
        def test_merge_decide_between_phone_and_email(
            self, client, session, admin_user, faker
        ):
            # Create contact
            email_address = "ysullivan@example.com"
            create_contact_response = post_json(
                client,
                "/contacts",
                {
                    "full_name": "Email Contact",
                    "display_name": "Email",
                    "email_addresses": [email_address],
                },
            )
            assert create_contact_response.status_code == 201
            assert create_contact_response.json is not None
            email_contact_id = create_contact_response.json["id"]

            create_contact_response = post_json(
                client,
                "/contacts",
                {
                    "full_name": "Phone Contact",
                    "display_name": "Phone",
                    "phone_numbers": ["+1 415-555-1234"],
                },
            )
            assert create_contact_response.status_code == 201

            # Make sure the contact has no items
            get_contact_response = get_json(client, f"/contacts/{email_contact_id}")
            assert get_contact_response.status_code == 200
            assert get_contact_response.json is not None
            assert len(get_contact_response.json["email_addresses"]["data"]) == 1
            assert (
                get_contact_response.json["email_addresses"]["data"][0]["value"]
                == email_address
            )
            assert len(get_contact_response.json["phone_numbers"]["data"]) == 0
            assert len(get_contact_response.json["mailing_addresses"]["data"]) == 0
            assert get_contact_response.json["city"] == None

            # Create an import event with the same email address
            contact = _generate_contact(faker)
            contact["phone_numbers"] = ["415-555-1234"]
            contact["email_addresses"] = [email_address]
            assert email_address in contact["email_addresses"]
            create_request = {
                "title": "Test CSV",
                "event_type": "csv_import",
                "date_created": "2021-04-21T00:00:00+00:00",
                "participants": [contact],
            }
            create_import_response = post_json(client, "/imports", create_request)
            assert create_import_response.status_code == 201
            assert create_import_response.json is not None
            assert (
                create_import_response.json["review_actions"]["data"][0]["action"]
                == "proposed_match"
            )
            assert (
                create_import_response.json["review_actions"]["data"][0][
                    "merge_into_contact_id"
                ]
                == email_contact_id
            )
            assert create_import_response.json["review_actions"]["data"][0][
                "match_justification"
            ] == {
                "confidence": "strong",
                "strong_matches": ["email_addresses"],
                "fuzzy_matches": [],
            }

        @pytest.mark.attendance_num_participants(1)
        @freeze_time("2021-04-20")
        def test_merge_decide_between_phone_and_name(
            self, client, session, admin_user, faker
        ):
            # Create contact
            create_contact_response = post_json(
                client,
                "/contacts",
                {
                    "full_name": "Squeamish Ossifrage",
                    "display_name": "Squeamish",
                },
            )
            assert create_contact_response.status_code == 201

            create_contact_response = post_json(
                client,
                "/contacts",
                {
                    "full_name": "Phone Contact",
                    "display_name": "Phone",
                    "phone_numbers": ["+1 415-555-1234"],
                },
            )
            assert create_contact_response.status_code == 201
            assert create_contact_response.json is not None
            phone_contact_id = create_contact_response.json["id"]

            # Create an import event with the same email address
            contact = _generate_contact(faker)
            contact["phone_numbers"] = ["415-555-1234"]
            contact["full_name"] = "Squeamish Ossifrage"
            create_request = {
                "title": "Test CSV",
                "event_type": "csv_import",
                "date_created": "2021-04-21T00:00:00+00:00",
                "participants": [contact],
            }
            create_import_response = post_json(client, "/imports", create_request)
            assert create_import_response.status_code == 201
            assert create_import_response.json is not None
            assert (
                create_import_response.json["review_actions"]["data"][0]["action"]
                == "proposed_match"
            )
            assert (
                create_import_response.json["review_actions"]["data"][0][
                    "merge_into_contact_id"
                ]
                == phone_contact_id
            )
            assert create_import_response.json["review_actions"]["data"][0][
                "match_justification"
            ] == {
                "confidence": "strong",
                "strong_matches": ["phone_numbers"],
                "fuzzy_matches": [],
            }

        @pytest.mark.attendance_num_participants(1)
        @freeze_time("2021-04-20")
        def test_merge_same_name_email_vs_phone(
            self, client, session, admin_user, faker
        ):
            # Create contact
            create_contact_response = post_json(
                client,
                "/contacts",
                {
                    "full_name": "Squeamish Ossifrage",
                    "display_name": "Squeamish",
                    "email_addresses": ["squeamish@example.com"],
                },
            )
            assert create_contact_response.status_code == 201
            assert create_contact_response.json is not None
            email_contact_id = create_contact_response.json["id"]

            create_contact_response = post_json(
                client,
                "/contacts",
                {
                    "full_name": "Squeamish Ossifrage",
                    "display_name": "Squeamish",
                    "phone_numbers": ["+1 415-555-1234"],
                },
            )
            assert create_contact_response.status_code == 201

            # Create an import event with the same email address
            contact = _generate_contact(faker)
            contact["phone_numbers"] = ["415-555-1234"]
            contact["full_name"] = "Squeamish Ossifrage"
            contact["email_addresses"] = ["squeamish@example.com"]
            create_request = {
                "title": "Test CSV",
                "event_type": "csv_import",
                "date_created": "2021-04-21T00:00:00+00:00",
                "participants": [contact],
            }
            create_import_response = post_json(client, "/imports", create_request)
            assert create_import_response.status_code == 201
            assert create_import_response.json is not None
            assert (
                create_import_response.json["review_actions"]["data"][0]["action"]
                == "proposed_match"
            )
            assert (
                create_import_response.json["review_actions"]["data"][0][
                    "merge_into_contact_id"
                ]
                == email_contact_id
            )
            assert create_import_response.json["review_actions"]["data"][0][
                "match_justification"
            ] == {
                "confidence": "strong",
                "strong_matches": ["email_addresses"],
                "fuzzy_matches": ["full_name"],
            }

    class TestUpdateErrors:
        @pytest.mark.attendance_num_participants(1)
        def test_update_actions_merge_bad_review_action_id(
            self, import_event, session, client, admin_user
        ):
            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {"id": "ima_KQkvzo5P29nWDmgL", "action": "create_new_contact"}
                    ]
                },
            )
            assert response.status_code == HTTPStatus.BAD_REQUEST

            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {"id": "ima_dNkOaJpVnpVqljYW", "action": "create_new_contact"}
                    ]
                },
            )
            assert response.status_code == HTTPStatus.OK

        @pytest.mark.attendance_num_participants(1)
        def test_update_actions_merge_bad_contact_id(
            self, import_event, session, client, admin_user
        ):
            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "match_auto",
                            "merge_into_contact_id": "ct_2eOl9vxOmwMEAk4j",
                            "merge": {
                                "full_name": {
                                    "attribute": "full_name",
                                    "merge_action": "manual_edit",
                                    "edit_value": "Some Weirdo",
                                }
                            },
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.BAD_REQUEST

        @pytest.mark.attendance_num_participants(1)
        def test_update_actions_merge_unknown_add(
            self, import_event, session, client, admin_user
        ):
            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "match_auto",
                            "merge_into_contact_id": "ct_1rAEnlwZAQekpgqD",
                            "merge": {
                                "full_name": {
                                    "attribute": "full_name",
                                    "merge_action": "add_new",
                                }
                            },
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.BAD_REQUEST

        @pytest.mark.attendance_num_participants(1)
        def test_update_actions_merge_missing_attribute(
            self, import_event, session, client, admin_user
        ):
            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "match_auto",
                            "merge_into_contact_id": "ct_1rAEnlwZAQekpgqD",
                            "merge": {
                                "full_name": {
                                    "merge_action": "take_incoming",
                                }
                            },
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.BAD_REQUEST

            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "match_auto",
                            "merge_into_contact_id": "ct_1rAEnlwZAQekpgqD",
                            "merge": {
                                "full_name": {
                                    "attribute": "full_name",
                                    "merge_action": "take_incoming",
                                }
                            },
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.OK

        @pytest.mark.attendance_num_participants(1)
        def test_update_actions_merge_unknown_attribute(
            self, import_event, session, client, admin_user
        ):
            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "match_auto",
                            "merge_into_contact_id": "ct_1rAEnlwZAQekpgqD",
                            "merge": {
                                "something": {
                                    "attribute": "something",
                                    "merge_action": "take_incoming",
                                }
                            },
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.BAD_REQUEST

            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "match_auto",
                            "merge_into_contact_id": "ct_1rAEnlwZAQekpgqD",
                            "merge": {
                                "display_name": {
                                    "attribute": "display_name",
                                    "merge_action": "take_incoming",
                                }
                            },
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.OK

        @pytest.mark.attendance_num_participants(1)
        def test_update_actions_merge_mismatched_attribute(
            self, import_event, session, client, admin_user
        ):
            faulty_request = {
                "actions": [
                    {
                        "id": "ima_dNkOaJpVnpVqljYW",
                        "action": "match_auto",
                        "merge_into_contact_id": "ct_1rAEnlwZAQekpgqD",
                        "merge": {
                            "full_name": {
                                "attribute": "display_name",
                                "merge_action": "take_incoming",
                            }
                        },
                    }
                ],
            }
            response = patch_json(
                client, "/imports/ime_ZP9BEa0l40kpdjLW/review_actions", faulty_request
            )
            assert response.status_code == HTTPStatus.BAD_REQUEST

            faulty_request["actions"][0]["merge"]["full_name"][
                "attribute"
            ] = "full_name"
            response = patch_json(
                client, "/imports/ime_ZP9BEa0l40kpdjLW/review_actions", faulty_request
            )
            assert response.status_code == HTTPStatus.OK

        @pytest.mark.attendance_num_participants(1)
        def test_update_actions_merge_directive_edits_should_contain_values(
            self, import_event, session, client, admin_user
        ):
            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "match_auto",
                            "merge_into_contact_id": "ct_1rAEnlwZAQekpgqD",
                            "merge": {
                                "full_name": {
                                    "attribute": "full_name",
                                    "merge_action": "manual_edit",
                                }
                            },
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.BAD_REQUEST

            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "match_auto",
                            "merge_into_contact_id": "ct_1rAEnlwZAQekpgqD",
                            "merge": {
                                "full_name": {
                                    "attribute": "full_name",
                                    "merge_action": "take_incoming",
                                }
                            },
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.OK

        @pytest.mark.attendance_num_participants(1)
        def test_update_actions_create_cannot_have_merge(
            self, import_event, session, client, admin_user
        ):
            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "create_new_contact",
                            "merge": {
                                "full_name": {
                                    "attribute": "full_name",
                                    "merge_action": "take_incoming",
                                }
                            },
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.BAD_REQUEST

            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "create_new_contact",
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.OK

        @pytest.mark.attendance_num_participants(1)
        def test_update_actions_ignore_cannot_have_merge(
            self, import_event, session, client, admin_user
        ):
            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "ignore_once",
                            "merge": {
                                "full_name": {
                                    "attribute": "full_name",
                                    "merge_action": "take_incoming",
                                }
                            },
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.BAD_REQUEST

            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "ignore_once",
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.OK

        @pytest.mark.attendance_num_participants(1)
        def test_update_actions_merge_directive_must_have_modifiers(
            self, import_event, session, client, admin_user
        ):
            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "match_auto",
                            "merge_into_contact_id": "ct_1rAEnlwZAQekpgqD",
                            "merge": {
                                "full_name": {
                                    "attribute": "full_name",
                                    "merge_action": "keep_existing",
                                }
                            },
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.BAD_REQUEST

            response = patch_json(
                client,
                "/imports/ime_ZP9BEa0l40kpdjLW/review_actions",
                {
                    "actions": [
                        {
                            "id": "ima_dNkOaJpVnpVqljYW",
                            "action": "match_auto",
                            "merge_into_contact_id": "ct_1rAEnlwZAQekpgqD",
                            "merge": {
                                "full_name": {
                                    "attribute": "full_name",
                                    "merge_action": "take_incoming",
                                }
                            },
                        }
                    ],
                },
            )
            assert response.status_code == HTTPStatus.OK

    class TestConfirmReviewErrors:
        @pytest.mark.attendance_num_participants(1)
        def test_confirm_review_only_once(
            self, import_event, session, client, admin_user
        ):
            response = put_json(client, "/imports/ime_ZP9BEa0l40kpdjLW/confirm")
            assert response.status_code == HTTPStatus.OK
            assert response.json is not None
            assert (
                response.json["review_actions"]["data"][0]["action"]
                == "proposed_create"
            )

            contact_list_response = get_json(client, "/contacts")
            assert contact_list_response.status_code == HTTPStatus.OK
            assert contact_list_response.json is not None
            devtools.debug(contact_list_response.json)
            assert len(contact_list_response.json["data"]) == 2

            response = put_json(client, "/imports/ime_ZP9BEa0l40kpdjLW/confirm")
            assert response.status_code == HTTPStatus.FORBIDDEN
