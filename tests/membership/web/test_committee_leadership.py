import json
from datetime import datetime
from unittest.mock import patch

import pytz
from freezegun import freeze_time

from config import SUPER_USER_EMAIL
from membership.database.base import Session, engine, metadata
from membership.database.models import (
    Committee,
    CommitteeLeader,
    CommitteeLeadershipRole,
    LeadershipRole,
    Member,
    Role,
)
from membership.web.base_app import app
from tests.flask_utils import get_json, patch_json, post_json


class TestCommitteeLeadershipService:
    def setup(self):
        metadata.create_all(engine)

        session = Session()
        m = Member(first_name="Karl", last_name="Marx", email_address=SUPER_USER_EMAIL)
        session.add(m)
        committee = Committee(id=1, name='Testing Committee')
        role1 = Role(member=m, role='member', date_created=datetime(2016, 1, 1))
        role1.committee = committee
        role2 = Role(member=m, role='admin', date_created=datetime(2016, 1, 1))
        role2.committee = committee
        session.add_all([committee, role1, role2])
        leadership_role = LeadershipRole(title="Co-Chair")
        committee_leadership_role = CommitteeLeadershipRole(term_limit_months=6)
        committee_leadership_role.committee = committee
        committee_leadership_role.leadership_role = leadership_role
        committe_leader = CommitteeLeader(
            term_start_date=datetime(2020, 4, 20),
            term_end_date=datetime(2020, 10, 20),
        )
        committe_leader.committee_leadership_role = committee_leadership_role
        committe_leader.member = m
        session.add_all([leadership_role, committee_leadership_role, committe_leader])
        session.commit()

        self.member = m
        self.committee = committee
        self.app = app.test_client()
        self.app.testing = True

    def teardown(self):
        metadata.drop_all(engine)


class TestCommitteeLeadershipServiceList(TestCommitteeLeadershipService):
    @freeze_time("2020-10-01")
    def test_list_returns_all_committee_leaders(self):
        response = get_json(self.app, '/committee/1/leaders')
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == [{
            'id': 1,
            'committee_id': 1,
            'member_id': 1,
            'member_name': 'Karl Marx',
            'role_title': 'Co-Chair',
            'term_start_date': '2020-04-20T00:00:00+00:00',
            'term_end_date': '2020-10-20T00:00:00+00:00',
        }]

    @freeze_time("2020-10-01")
    def test_list_can_404(self):
        response = get_json(self.app, '/committee/2/leaders')
        assert response.status_code == 404


class TestCommitteeLeadershipServiceAdd(TestCommitteeLeadershipService):
    @freeze_time("2020-10-01")
    def test_add_new_title(self):
        response = post_json(self.app, '/committee/1/leaders', payload={
            'role_title': 'Vice Chair'
        })
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == {
            'id': 2,
            'committee_id': 1,
            'member_id': None,
            'member_name': None,
            'role_title': 'Vice Chair',
            'term_start_date': None,
            'term_end_date': None,
        }
        session = Session()
        assert len(session.query(LeadershipRole).all()) == 2
        leadership_role = (
            session.query(LeadershipRole)
            .filter(LeadershipRole.title == 'Vice Chair').one()
        )
        assert leadership_role
        clr = (
            session.query(CommitteeLeadershipRole)
            .filter(CommitteeLeadershipRole.leadership_role_id == leadership_role.id)
            .one()
        )
        # We didn't specify a term limit for this one
        assert clr.term_limit_months is None

    @freeze_time("2020-10-01")
    def test_add_existing_title(self):
        response = post_json(self.app, '/committee/1/leaders', payload={
            'role_title': 'Co-Chair',
            'term_limit_months': 12,
        })
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == {
            'id': 2,
            'committee_id': 1,
            'member_id': None,
            'member_name': None,
            'role_title': 'Co-Chair',
            'term_start_date': None,
            'term_end_date': None,
        }
        session = Session()
        assert len(session.query(LeadershipRole).all()) == 1
        leadership_role = (
            session.query(LeadershipRole)
            .filter(LeadershipRole.title == 'Co-Chair').one()
        )
        assert leadership_role is not None
        clrs = session.query(CommitteeLeadershipRole)\
            .filter(CommitteeLeadershipRole.leadership_role_id == leadership_role.id)\
            .all()
        assert len(clrs) == 2
        clr = clrs[-1]
        assert clr.term_limit_months == 12

    @freeze_time("2020-10-01")
    def test_add_no_title(self):
        response = post_json(self.app, '/committee/1/leaders', payload={
            'term_limit_months': 12,
        })
        assert response.status_code == 400
        response = post_json(self.app, '/committee/1/leaders', payload={
            'role_title': '',
            'term_limit_months': 12,
        })
        assert response.status_code == 400


class TestCommitteeLeadershipServiceReplace(TestCommitteeLeadershipService):
    @patch(
        'membership.util.time.get_current_time',
        return_value=datetime(2020, 9, 20).replace(tzinfo=pytz.utc)
    )
    def test_replace(self, get_current_time_mock):
        session = Session()
        new_leader = Member(first_name="Fred", last_name="Engels")
        session.add(new_leader)
        session.commit()
        response = patch_json(self.app, '/committee/1/leaders/1', payload={
            'member_id': new_leader.id,
        })
        result = json.loads(response.data)
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == {
            'id': 1,
            'committee_id': 1,
            'member_id': new_leader.id,
            'member_name': 'Fred Engels',
            'role_title': 'Co-Chair',
            'term_start_date': '2020-09-20T00:00:00+00:00',
            'term_end_date': '2021-03-20T00:00:00+00:00',
        }
        tenures = session.query(CommitteeLeader).all()
        assert len(tenures) == 2
        # We ended Marx's leadership early
        marx_tenure = tenures[0]
        assert marx_tenure.term_end_date == datetime(2020, 9, 20).astimezone(pytz.utc)
        assert marx_tenure.committee_leadership_role == tenures[1].committee_leadership_role

    @patch(
        'membership.util.time.get_current_time',
        return_value=datetime(2020, 9, 20).replace(tzinfo=pytz.utc)
    )
    def test_replace_with_no_one(self, get_current_time_mock):
        response = patch_json(self.app, '/committee/1/leaders/1', payload={
            'member_id': None,
        })
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == {
            'id': 1,
            'committee_id': 1,
            'member_id': None,
            'member_name': None,
            'role_title': 'Co-Chair',
            'term_start_date': None,
            'term_end_date': None,
        }
        session = Session()
        tenures = session.query(CommitteeLeader).all()
        assert len(tenures) == 1
        # We ended Marx's leadership early
        marx_tenure = tenures[0]
        assert marx_tenure.term_end_date == datetime(2020, 9, 20).astimezone(pytz.utc)

    def test_replace_non_existent_committee(self):
        # Non-existent committee 404s
        response = patch_json(self.app, '/committee/404/leaders/1', payload={
            'member_id': None,
        })
        assert response.status_code == 404

    def test_replace_non_existent_clr(self):
        # Non-existent committee leadership role 404s
        response = patch_json(self.app, '/committee/1/leaders/404', payload={
            'member_id': None,
        })
        assert response.status_code == 404

    def test_replace_committee_clr_mismatch(self):
        # A committee ID and committee leadership role ID not being related
        session = Session()
        other_committee = Committee(id=2, name='Other Committee')
        session.add(other_committee)
        session.commit()
        response = patch_json(self.app, '/committee/2/leaders/1', payload={
            'member_id': None,
        })
        assert response.status_code == 404

    @patch(
        'membership.util.time.get_current_time',
        return_value=datetime(2020, 9, 20).replace(tzinfo=pytz.utc)
    )
    def test_replace_end_date_validation(self, get_current_time_mock):
        # too-early end date 400s
        session = Session()
        new_leader = Member(first_name="Fred", last_name="Engels")
        session.add(new_leader)
        session.commit()
        response = patch_json(self.app, '/committee/1/leaders/1', payload={
            'member_id': new_leader.id,
            'term_start_date': '2020-09-20T00:00:00+00:00',
            'term_end_date': '2020-03-21T00:00:00+00:00',
        })
        assert response.status_code == 400

        # too-late end date 400s
        response = patch_json(self.app, '/committee/1/leaders/1', payload={
            'member_id': new_leader.id,
            'term_start_date': '2020-09-20T00:00:00+00:00',
            'term_end_date': '2021-03-21T00:00:00+00:00',
        })
        assert response.status_code == 400
