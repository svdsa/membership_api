import json
from datetime import datetime
from unittest.mock import patch

import pytest
from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse

from config import SUPER_USER_EMAIL, SUPER_USER_FIRST_NAME, SUPER_USER_LAST_NAME
from membership.database.base import Session, engine, metadata
from membership.database.models import Member, Role
from membership.web.base_app import app
from tests.flask_utils import get_json, patch_json, post_json


# TODO write full endpoint tests
@pytest.mark.skip("Mocked internal responses")
class TestCommittees:
    def setup(self):
        metadata.create_all(engine)
        self.app = Client(app, BaseResponse)
        self.app = app.test_client()
        self.app.testing = True

        # set up for auth
        session = Session()
        m = Member()
        m.email_address = SUPER_USER_EMAIL
        m.first_name = SUPER_USER_FIRST_NAME
        m.last_name = SUPER_USER_LAST_NAME
        session.add(m)
        role1 = Role(member=m, role='member', date_created=datetime(2016, 1, 1))
        role2 = Role(member=m, role='admin', date_created=datetime(2016, 1, 1))
        session.add_all([role1, role2])

        session.commit()
        session.close()

    def teardown(self):
        metadata.drop_all(engine)

    @patch('membership.web.committees.committee_service', autospec=True)
    def test_get_committees(self, committee_service):
        committee_list = [{
            'id': 1,
            'name': 'Testing Committee',
            'email': SUPER_USER_EMAIL,
            'viewable': True,
            'provisional': False,
        }]

        committee_service.list.return_value = committee_list

        response = get_json(self.app, '/committee/list')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == committee_list

    @patch('membership.web.committees.committee_service', autospec=True)
    def test_add_committee(self, committee_service):
        new_committee_details = {
            'id': 2,
            'name': 'Committee on Mock Data',
            'email': None,
            'viewable': True,
            'provisional': False,
            'admins': [{'id': 1, 'name': ''}],
            'members': [],
            'active_members': []
        }

        committee_service.create.return_value = new_committee_details

        payload = {
            'name': 'Committee on Mock Data',
            'admin_list': [SUPER_USER_EMAIL]
        }
        response = post_json(self.app, '/committee', payload=payload)
        assert response.status_code == 200
        assert committee_service.create.call_args[1] == {
            'name': payload['name'],
            'provisional': False,
            'admins': payload['admin_list'],
        }

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert result['created'] == new_committee_details

    @patch('membership.web.committees.committee_service', autospec=True)
    def test_get_committee(self, committee_service):
        committee_info = {
            'id': 1,
            'name': 'Testing Committee',
            'email': SUPER_USER_EMAIL,
            'viewable': True,
            'provisional': False,
            'admins': [],
            'members': [],
            'active_members': []
        }

        committee_service.get.return_value = committee_info

        response = get_json(self.app, '/committee/1')
        assert response.status_code == 200
        assert committee_service.get.call_args[1] == {
            'committee_id': 1,
        }

        result = json.loads(response.data)
        assert result == committee_info

    @patch('membership.web.committees.committee_service', autospec=True)
    def test_get_committee_not_found(self, committee_service):
        committee_service.get.return_value = None
        response = get_json(self.app, '/committee/1')
        assert response.status_code == 404

    @patch('membership.web.committees.committee_service', autospec=True)
    def test_request_committee_membership_where_committee_has_email(self, committee_service):
        committee_service.request_membership.return_value = True
        response = post_json(self.app, '/committee/1/member_request')
        assert response.status_code == 200
        assert committee_service.request_membership.call_args[1] == {
            'committee_id': 1,
            'member_name': '{} {}'.format(SUPER_USER_FIRST_NAME, SUPER_USER_LAST_NAME),
            'member_email': SUPER_USER_EMAIL,
        }

    @patch('membership.web.committees.committee_service', autospec=True)
    def test_request_committee_membership_no_email_with_committee_bad_response(
        self,
        committee_service,
    ):
        committee_service.request_membership.return_value = None
        response = post_json(self.app, '/committee/2/member_request')
        assert response.status_code == 400

    @patch('membership.web.committees.committee_service', autospec=True)
    def test_edit_inactive_status(self, committee_service):
        committee_info = {
            'id': 1,
            'name': 'Testing Committee',
            'email': SUPER_USER_EMAIL,
            'viewable': True,
            'provisional': False,
            'inactive': True,
            'admins': [],
            'members': [],
            'active_members': []
        }
        committee_service.edit.return_value = committee_info
        response = patch_json(self.app, '/committee/1', {'inactive': True})
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == committee_info
        assert committee_service.edit.call_args[0][1] == 1
        assert committee_service.edit.call_args[1] == {
            'inactive': True,
        }
