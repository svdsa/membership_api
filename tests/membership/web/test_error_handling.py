import devtools
import pytest
import structlog.threadlocal
from structlog.testing import capture_logs

from membership.database.base import Session, engine, metadata
from membership.web.base_app import app
from tests.flask_utils import get_json


class TestErrorHandling:
    @pytest.fixture
    def tables(self):
        metadata.create_all(engine)
        yield
        metadata.drop_all(engine)

    @pytest.fixture
    def client(self, tables):
        app.testing = True
        return app.test_client()

    @pytest.fixture
    def session(self, tables):
        session = Session()
        yield session
        session.close()

    def test_404_error(self, client):
        with capture_logs() as logs:
            response = get_json(client, "/something-that-will-never-exist")
            devtools.debug(response.json)
            assert response.status_code == 404
            assert response.json == {
                "object": "error",
                "type": "invalid_request_error",
                "error_code": "unknown_route",
                "status_code": 404,
                "friendly_message": "Unknown API endpoint",
                "params": None,
            }

        assert len(logs) >= 1
        devtools.debug(logs)
        assert logs[-1]["log_level"] == "error"
        assert logs[-1]["object"] == "error"
        assert logs[-1]["type"] == "invalid_request_error"
        assert logs[-1]["error_code"] == "unknown_route"
        assert logs[-1]["status_code"] == 404

        # These don't exist in the captured log output because
        # capture_log doesn't run any structlog processors
        request_ctx = structlog.threadlocal.get_threadlocal()
        assert request_ctx["path"] == "/something-that-will-never-exist"
        assert request_ctx["request_id"] is not None
        assert request_ctx["request_id"].startswith("portalrequest_")
