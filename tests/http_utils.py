import httpretty
import json
from typing import Any, Dict, List, Set, Tuple, Union
from urllib.parse import urlencode, parse_qsl


class RequestChecker:
    """ This class is for testing that http requests are as expected.
    """

    class MockRequest:
        def __init__(self,
                     method: str,
                     url: str,
                     request_body: Union[List[Dict[str, Any]], Dict[str, Any]] = None,
                     response_body: Union[List[Dict[str, Any]], Dict[str, Any]] = None,
                     params: Dict[str, Any] = None) -> None:
            self.method = method
            self.url = url
            self.request_body = request_body if request_body else {}
            self.response_body = response_body
            self.params = params

    def __init__(self) -> None:
        self.urls = set()  # type: Set[Tuple[str, str]]
        self.requests = []  # type: List[RequestChecker.MockRequest]

    def add_request(self,
                    method: str,
                    url: str,
                    request_body: Union[List[Dict[str, Any]], Dict[str, Any]] = None,
                    response_body: Union[List[Dict[str, Any]], Dict[str, Any]] = None,
                    params: Dict[str, Any] = None):
        """ Add an expected http request. Requests are expected in the order they are added.
        :param method: the expected http method
        :param url: the expected base url
        :param request_body: the expected body of the http request
        :param response_body: the body of the http response to be returned
        :param params: any expected parameters of the http request
        """
        if (method, url) not in self.urls:
            # register the call back if it's not already registered
            httpretty.register_uri(
                method,
                url,
                content_type='application/json',
                body=self.callback,
                forcing_headers={'content-type': 'application/json'})
            self.urls.add((method, url))
        self.requests.append(
            RequestChecker.MockRequest(method, url, request_body, response_body, params))

    def callback(self, request: httpretty.core.HTTPrettyRequest, uri: str,
                 headers: Dict[str, str]) -> Tuple[int, Dict[str, str], str]:
        """
        This method is a callback passed to httpretty. It checks the request against the next
        expected request and returns associated response if it's correct.
        """
        expected_request = self.requests.pop(0)
        # check the request is the expected method
        assert expected_request.method == request.method
        # check query params and strip from the url if there are any
        if expected_request.params:
            qs_index = uri.find('?')
            qs = uri[qs_index + 1:]
            uri = uri[:qs_index]
            assert urlencode(expected_request.params) == qs
        assert expected_request.url == uri
        # check the request body if there is one or one is expected
        if request.body or expected_request.request_body:
            if request.headers.get('content_type', '') == 'application/json':
                assert json.loads(request.body.decode('utf-8')) == expected_request.request_body
            else:
                assert parse_qsl(request.body.decode('utf-8')) == expected_request.request_body
        return 200, headers, json.dumps(expected_request.response_body)

    def check_empty(self) -> None:
        """
        Checks that all expected requests have been received
        """
        assert len(self.requests) == 0
