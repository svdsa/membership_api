from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.scoping import scoped_session

from membership.database import base


def pytest_configure(config):
    base.engine = create_engine('sqlite://', pool_size=10, pool_recycle=3600)
    base.Session = sessionmaker(bind=base.engine)
    base.db_session = scoped_session(base.Session)
    base.metadata.create_all(base.engine)
    base.check_migrations = lambda b: None
