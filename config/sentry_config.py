from typing import Optional
from config.lib import from_env

SENTRY_DSN: Optional[str] = from_env.get_str('SENTRY_DSN', None)
