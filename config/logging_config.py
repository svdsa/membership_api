import logging
from typing import Callable

from config import from_env

validate_log_level: Callable[[str], int] = logging._checkLevel  # NOQA

LOGGING_ROOT_LEVEL: str = from_env.get_str(
    'LOGGING_ROOT_LEVEL',
    logging.DEBUG,
    convert=validate_log_level,
)

# Whether we're running in a hot-reload watch mode
FLASK_DEBUG: bool = from_env.get_bool("FLASK_DEBUG", False, lambda env: env == "1")
FLASK_ENV: str = from_env.get_str("FLASK_ENV", "production")
NON_PRODUCTION: bool = FLASK_DEBUG or FLASK_ENV != "production"
PRODUCTION: bool = FLASK_DEBUG is False or FLASK_ENV == "production"
