from __future__ import absolute_import

from os import path

import structlog

logger = structlog.stdlib.get_logger()

try:
    from dotenv import load_dotenv
    dotenv_path = path.normpath(path.join(path.dirname(__file__), '..', '.env'))
    logger = logger.bind(dotenv_path=dotenv_path)
    if path.exists(dotenv_path):
        load_dotenv(dotenv_path)
        logger.info("Loaded dotenv file")
    else:
        logger.warning("Couldn't find dotenv file")
except ImportError:
    from config.logging_config import FLASK_DEBUG, FLASK_ENV, NON_PRODUCTION

    if NON_PRODUCTION:
        # non-production settings should have .env files
        raise RuntimeError("Couldn't import dotenv")
    else:
        logger.info(
            "dotenv package missing, skipping dotenv import",
            flask_env=FLASK_ENV,
            flask_debug=FLASK_DEBUG,
        )
