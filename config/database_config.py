from config.lib import from_env

MYSQL_DATABASE: str = from_env.get_str("MYSQL_DATABASE", "dsa")
MYSQL_ROOT_PASSWORD: str = from_env.get_str(
    "MYSQL_ROOT_PASSWORD", ValueError("Password is required for local development")
)
DATABASE_HOST: str = from_env.get_str("DATABASE_HOST", "db")
DATABASE_URL: str = from_env.get_str(
    "DATABASE_URL",
    f"mysql://root:{MYSQL_ROOT_PASSWORD}@{DATABASE_HOST}:3306/{MYSQL_DATABASE}?charset=utf8",
)
DATABASE_CONNECT_TIMEOUT: int = from_env.get_int("DATABASE_CONNECT_TIMEOUT", 30)

# Must specify charset to allow UTF-8 non-basic multilingual plane characters (e.g. emoji)
DATABASE_CHARSET = "utf8mb4"
settings = {
    "url": DATABASE_URL,
    "pool_size": 10,
    "pool_recycle": 3600,
    "connect_args": {
        "connect_timeout": DATABASE_CONNECT_TIMEOUT,
        "charset": DATABASE_CHARSET,
    },
    "pool_pre_ping": True,
}

# Migration configs
SUPER_USER_FIRST_NAME: str = from_env.get_str("SUPER_USER_FIRST_NAME", "Joe")
SUPER_USER_LAST_NAME: str = from_env.get_str("SUPER_USER_LAST_NAME", "Schmoe")
SUPER_USER_EMAIL: str = from_env.get_str("SUPER_USER_EMAIL", "joe.schmoe@example.com")
SUPER_USER_CHAPTER_NAME: str = from_env.get_str(
    "SUPER_USER_CHAPTER_NAME", "Local Chapter"
)

# ID hashing config
ID_SALT: str = from_env.get_str("ID_SALT", "3wZJiXNshbRtTPV8")
