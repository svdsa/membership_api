from config.lib import from_env

ENABLE_GDRIVE = from_env.get_bool('ENABLE_GDRIVE', False)
GDRIVE_CREDENTIALS_FILE = from_env.get_str('GDRIVE_CREDENTIALS_FILE', './gdrive_credentials.json')
GDRIVE_NATL_DIR_NAME = from_env.get_str('GDRIVE_NATL_DIR_NAME', 'dsa_data')
