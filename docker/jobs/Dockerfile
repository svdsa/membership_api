# syntax=docker/dockerfile:1.3
# inspired by https://github.com/python-poetry/poetry/issues/1178#issuecomment-517812277

FROM python:3.9-slim AS base

LABEL maintainer "tech@dsasf.org"

# Requires running with the context from $DSASF_HOME/membership_api (see README.md for settings):
#     docker build . -f docker/migrate/Dockerfile

# Place apt dependencies that the app needs here
RUN apt-get update \
  && apt-get install --no-install-recommends -y \
  default-libmysqlclient-dev \
  netcat \
  # used in ci test script
  make \
  curl \
  build-essential \
  libffi-dev \
  libssl-dev \
  cargo \
  python3-dev \
  && rm -rf /var/lib/apt/lists/*

FROM base AS poetry

ENV VIRTUAL_ENV=/opt/venv \
  POETRY_VERSION=1.1.4

# Place apt dependencies that the build process needs here
RUN pip install --no-cache-dir -U pip \
  && pip install --no-cache-dir "poetry==1.1.4" \
  poetry --version \
  # configure poetry
  && python -m venv $VIRTUAL_ENV \
  && poetry config virtualenvs.create false \
  # cleanup
  && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY poetry.lock pyproject.toml ./

ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  PATH="$VIRTUAL_ENV/bin:$PATH"

RUN . /opt/venv/bin/activate && poetry install --no-interaction --no-ansi -vvv

FROM poetry AS runtime

WORKDIR /usr/src/app

COPY --from=poetry /opt/venv /opt/venv
COPY config ./config
COPY membership ./membership
COPY jobs ./jobs
COPY flask_app.py ./
COPY runjob.py ./

EXPOSE 8080

ENTRYPOINT ["python", "runjob.py"]
