#!/usr/bin/env python3

import argparse
import csv
import io  # NOQA: F401
import json
import re
import sys
from datetime import datetime
from math import ceil
from typing import IO, Dict, Iterator, List, Optional

import pytz
from overrides import overrides

# typedef
from sqlalchemy import orm

from config.crm_config import CRM_ENABLED
from config.slack_config import SLACK_ENABLED
from jobs import Job
from jobs.auth0_invites import SendAuth0Invites
from jobs.roster.ambiguous_link import AmbiguousLink
from jobs.roster.csv_record import CSVRecord
from jobs.roster.import_err import ImportErr
from jobs.roster.import_results import ImportResults
from jobs.roster.session_cache import RosterImportSessionCache
from jobs.roster.session_proxy import SessionProxy
from membership.database.base import engine
from membership.database.models import Member, NationalMembershipData, Role
from membership.services.crm.crm_service import CrmService
from membership.services.slack.slack_service import SlackService

Row = Dict[str, str]
Rows = List[Row]


class ImportNationalMembership(Job):
    """
    Imports the national membership list.

    Gracefully handles:

    1. Existing Member:
        Inserts or updates existing Membership without touching Member
        Adds missing identities
        Adds member role if missing

    2. No Existing Member:
        Inserts or updates Membership
        Inserts Member with
        Adds identities
        Adds member role

    Matches existing member on normalized email or first_name + last_name
    """

    date = datetime.today().strftime('%Y-%m-%d')
    next_reg_meeting = None
    description = "Upload national membership roll"
    auth0_job = SendAuth0Invites()
    crm = CrmService()
    slack_service = SlackService()

    @overrides
    def build_parser(self, parser: argparse.ArgumentParser) -> None:
        parser.add_argument(
            "input",
            nargs="?",
            type=argparse.FileType("r", encoding="utf_8"),
            default=sys.stdin,
        )
        parser.add_argument(
            "-o", "--output", type=argparse.FileType("w"), default=sys.stdout
        )
        parser.add_argument("-d", "--date", default=datetime.today().strftime('%Y-%m-%d'))
        parser.add_argument("-f", "--force", action="store_true")

    @overrides
    def run(self, config: dict) -> None:
        self.date = config.get("date")
        # validate date format
        if re.compile(r"\d{4}-\d{2}-\d{2}").match(self.date) is None:
            print("Invalid date format, please use YYYY-MM-DD")
            return
        outfile: IO[str] = config.get("output", sys.stdout)
        row_iter: Optional[io.BufferedReader] = config.get("input")
        if row_iter is None:
            raise ValueError(
                f"config['input'] must be an iterator over a CSV file, received None "
                f"(provided keys: {config.keys()})"
            )
        reader = csv.DictReader(row_iter)

        maker = orm.sessionmaker(bind=engine, expire_on_commit=False)
        cache = RosterImportSessionCache(maker)
        csv_records = self.parse_records(iter(reader), outfile)

        self.import_all(
            csv_records,
            cache,
            self.auth0_job,
            outfile
        )

    def parse_records(self, row_iter: Iterator[Dict[str, str]], out: IO[str]) -> List[CSVRecord]:
        print("Parsing membership file...", file=out)
        csv_records = self.parse_csv_records(row_iter)

        print("Found %s records. Processing them now..." % len(csv_records), file=out)
        return csv_records

    def import_all(
            self,
            csv_records: List[CSVRecord],
            cache: RosterImportSessionCache,
            auth0_job: SendAuth0Invites,
            out: IO[str] = sys.stdout,
    ) -> ImportResults:
        results = ImportResults()

        i = 0
        bucket = 0
        ten_percent = ceil(len(csv_records) / 10)
        for csv_record in csv_records:
            if 1 < ten_percent < i:
                i = 0
                bucket += 1
                print("Processed %s%% of the records..." % (bucket * 10), file=out)

            i += 1
            results.processed += 1

            try:

                dues_changed = False

                # Check if there is already a copy of the person's national record in our database.
                # If so update it to reflect the latest changes, otherwise create a new copy.
                national_record = self.link_csv_record_to_national_record(
                    csv_record, cache.national_records)
                if not national_record:
                    national_record = csv_record.to_national_record()
                    cache.session.add(national_record)
                    dues_changed = True
                    results.memberships_created.add(national_record.id)
                else:
                    dues_changed = self.update_national_record(
                        csv_record, national_record, results, cache.session)

                # Check if a local chapter record exists for a member. If not, create one. If so,
                # update the existing record with changes.
                chapter_record_matches = self.link_csv_record_to_chapter_records_and_sort(
                    csv_record, cache.chapter_records)
                chapter_record = None
                if not chapter_record_matches:
                    chapter_record = csv_record.to_chapter_record()
                    cache.session.add(chapter_record)
                    results.members_created.add(chapter_record.id)
                    cache.session.flush()  # so an id can be generated for linking
                else:
                    # In some cases, the csv record could be linked to multiple local chapter
                    # records. This could happen in scenarios where a user signed up locally under
                    # different email account but the same first and last name. This script will
                    # spit out all national members with multiple accounts so that the script runner
                    # can manually merge the records later.
                    if len(chapter_record_matches) > 1:
                        results.ambiguous_links.append(
                            AmbiguousLink(csv_record, [m.id for m in chapter_record_matches]))

                    chapter_record = next(iter(chapter_record_matches))

                    # The dues_paid_overridden_on flag is used by admins to mark that someone has
                    # confirmed their dues, but we haven't gotten the update from National yet. If
                    # the natl record changed at all since the flag was set, that means we've
                    # received the latest info (barring some weird edge cases I think we can safely
                    # ignore). Once the info comes in, we reset the admin overrides
                    if dues_changed:
                        chapter_record.dues_paid_overridden_on = None

                    self.update_chapter_record(csv_record, chapter_record, results, cache.session)

                # Make sure national record is linked to the chapter record.
                national_record.member_id = chapter_record.id

                # I don't understand what our identities table is used for but I left this here
                # because it's probably important.
                self.add_identities_if_new(
                    csv_record, chapter_record, chapter_record_matches, results, cache.session)

                # Add any new phone numbers.
                self.add_or_update_phone_numbers(csv_record, chapter_record, results, cache.session)

                # If this is a new member and everything has gone well to this point then we start
                # the new member onboarding process. This will:
                #   - create an Auth0 account
                #   - send them an email with instructions for logging in
                #   - add them to the CCC Onboarding Team's new member call list (CRM)
                if not chapter_record.onboarded_on and dues_changed:
                    auth0_job.send_invite(chapter_record, out)

                    chapter_record.onboarded_on = datetime.now(pytz.utc)
                    results.members_onboarded.add(chapter_record.id)

                cache.session.commit()
            except Exception as e:
                results.errors.append(ImportErr(e, csv_record))
                cache.session.rollback()
                cache = cache.refresh_session()

        self.integrations(cache, results, out)

        return results

    def integrations(self, cache: RosterImportSessionCache, results: ImportResults, out: IO[str]):
        cache = cache.refresh_session()
        member_cache = {m.id: m for m in cache.chapter_records}

        new_members = [member_cache.get(member_id) for member_id in results.members_onboarded]

        if CRM_ENABLED:
            crm_results = self.crm.bulk_sync_members(new_members, 'national import')
            print(f"{json.dumps(crm_results.to_json())}", file=out)
            print()

        if SLACK_ENABLED:
            self.slack_service.notify_of_roster_import(new_members)
            print("Sent notif to slack!")
            print()

        # print instructions for manual Google Group invitation
        print("You must MANUALLY add the following members to the Google Group at this link:\n",
              file=out)
        print("https://groups.google.com/forum/#!managemembers/dsasf-members/add\n", file=out)
        print("Here is a welcome message template:", file=out)
        print("""
            ------ START ------
            Welcome to the DSA SF Google Group! As a member, you'll have access to both the
            public calendar (https://dsasf.org/calendar/public) and the private calendar
            (https://dsasf.org/calendar/private) as well as our Google Drive
            (https://dsasf.org/drive).

            You can subscribe to the calendars with these two links:
            https://dsasf.org/subscribe-public-calendar &
            https://dsasf.org/subscribe-private-calendar

            You’ll also receive membership-specific emails from this address. If you reply or send a
            message to the Google Group, the post will be sent for review by an admin.

            Thanks for joining!
            🌹 Socialism will win 🌹
            ------- END -------
            """, file=out)
        print("Note you can only invite max 10 email addresses at a time,", file=out)
        print("so they are displayed grouped for you to copy/paste:\n", file=out)
        emails = [m.email_address for m in new_members]
        for i in range(0, len(emails), 10):
            print(",".join(emails[i:i + 10]), file=out)
            print("", file=out)

        print()
        print()
        print("Full results summary:")
        print(f"\n{json.dumps(results.to_json(member_cache))}", file=out)

    def parse_csv_records(self, rows: List[Row]) -> List[CSVRecord]:
        return [CSVRecord.parse_csv_row(row) for row in rows]

    def link_csv_record_to_national_record(
            self,
            csv_record: CSVRecord,
            national_records: List[NationalMembershipData]
    ) -> NationalMembershipData:
        return next((nat for nat in national_records if nat.ak_id == csv_record.ak_id), None)

    # Links the csv record to chapter records based on a number of factors and sorts them in order
    # of most likely to be the record the member actually uses. Matching criteria in preference
    # order:
    # - Has the general member role
    # - Chapter record matches the csv record based on email address
    # - Chapter record has an additional email that matches csv record
    # Note, we use a hack where we mark an email with the prefix "DELETE_" if it no longer should
    # be associated.  This function will ignore those records.
    def link_csv_record_to_chapter_records_and_sort(
            self,
            csv_record: CSVRecord,
            chapter_records: List[Member]
    ) -> Optional[Member]:
        matches = set()

        for rec in chapter_records:
            if rec.is_deleted():
                continue

            if rec.email_matches(csv_record.email):
                matches.add(rec)
            elif rec.name_matches(csv_record.first_name, csv_record.last_name):
                matches.add(rec)
            elif rec.additional_email_matches(csv_record.email):
                matches.add(rec)

        # Python uses stable sort, so multi-sort can be achieved by sorting by each comparator in
        # reverse order, as per https://docs.python.org/3/howto/sorting.html
        sorted_matches = sorted(
            matches,
            key=lambda x: 0 if x.additional_email_matches(csv_record.email) else 1)
        sorted_matches = sorted(
            sorted_matches,
            key=lambda x: 0 if x.email_matches(csv_record.email) else 1)
        sorted_matches = sorted(
            sorted_matches,
            key=lambda x: 0 if x.has_general_member_role() else 1)

        return sorted_matches

    def get_member_roles(self, member: Member):
        return list(filter(lambda r: r.role == "member" and r.committee_id is None, member.roles))

    def create_role(self, member: Member):
        return Role(role="member", committee_id=None, member_id=member.id)

    def update_national_record(
            self,
            csv_record: CSVRecord,
            national_record: NationalMembershipData,
            results: ImportResults,
            session: SessionProxy
    ) -> bool:
        dues_changed = national_record.dues_paid_until != csv_record.dues_paid_until

        national_record.first_name = csv_record.first_name
        national_record.middle_name = csv_record.middle_name
        national_record.last_name = csv_record.last_name
        national_record.do_not_call = national_record.do_not_call or csv_record.do_not_call
        national_record.address_line_1 = csv_record.address_line_1
        national_record.address_line_2 = csv_record.address_line_2
        national_record.city = csv_record.city
        national_record.country = csv_record.country
        national_record.zipcode = csv_record.zipcode
        national_record.join_date = csv_record.join_date
        national_record.dues_paid_until = csv_record.dues_paid_until
        national_record.active = csv_record.active

        if session.is_modified(national_record):
            results.memberships_updated.add(national_record.id)

        # Force update so the "date_updated" field reflects that the record was processed, even if
        # none of the fields actually changed.
        session.add(national_record)

        return dues_changed

    def update_chapter_record(
            self,
            csv_record: CSVRecord,
            chapter_record: Member,
            results: ImportResults,
            session: SessionProxy
    ):
        if not chapter_record.first_name:
            chapter_record.first_name = csv_record.first_name
        if not chapter_record.last_name:
            chapter_record.last_name = csv_record.last_name
        chapter_record.do_not_call = chapter_record.do_not_call or csv_record.do_not_call

        if session.is_modified(chapter_record):
            results.members_updated.add(chapter_record.id)

    def add_identities_if_new(self,
                              csv_record: CSVRecord,
                              best_matching_chapter_record: Member,
                              matching_chapter_records: List[Member],
                              results: ImportResults,
                              session: SessionProxy):
        ids = csv_record.get_identities(best_matching_chapter_record.id)

        for id in ids:
            matches = [
                x
                for chapter_record in matching_chapter_records for x in chapter_record.identities
                if x.provider_name == id.provider_name and x.provider_id == id.provider_id
            ]
            if not any(matches):
                session.add(id)
                results.identities_added.add(id.id)

    def add_or_update_phone_numbers(self,
                                    csv_record: CSVRecord,
                                    chapter_record: Member,
                                    results: ImportResults,
                                    session: SessionProxy):
        numbers = csv_record.get_phone_numbers(chapter_record.id, results)

        for number in numbers:
            matches = [
                x
                for x in chapter_record.phone_numbers
                if x.number == number.number
            ]

            if not any(matches):
                session.add(number)
                results.phone_numbers_added.add(number)
                continue

            for existing in chapter_record.phone_numbers:
                existing.name = number.name
