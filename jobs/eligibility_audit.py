import argparse

from overrides import overrides

from jobs import Job
from membership.database.base import Session
from membership.services.eligibility import MembershipAuditService
from membership.services.meeting_service import MeetingService


class MembershipAudit(Job):
    description = "Analyzes some aggregate statistics around our current eligibility numbers and" \
                  "stores a snapshot of the data in a table."
    meeting_service = MeetingService()
    membership_audit_service = MembershipAuditService()

    @overrides
    def build_parser(self, parser: argparse.ArgumentParser) -> None:
        parser.add_argument(
            "-o",
            "--older-audit",
            nargs=1,
            type=int
        ),
        parser.add_argument(
            "-n",
            "--newer-audit",
            nargs=1,
            type=int
        )

    @overrides
    def run(self, config: dict) -> None:
        session = Session()

        newer_audit = config.get("newer_audit")
        if newer_audit is None:
            newer_audit = self.membership_audit_service.get_membership_audit_id(session, 0)
            print("Using membership audit with id %s as base audit" % str(newer_audit))
        if newer_audit is None:
            print("No membership audits have been created yet.")

        older_audit = config.get("older_audit")
        if older_audit is None:
            older_audit = self.membership_audit_service.get_membership_audit_id(session, 1)
            print("Using membership audit with id %s as comparison audit" % str(older_audit))
        if older_audit is None:
            print("Only one membership audit has been created so far. Aggregate details will be "
                  "recorded, but no status changes can be reported.")
        elif newer_audit < older_audit:
            print("New audit param must have a higher id than the older audit param.")
            return

        try:
            print("Building audit report...")
            audit = self.membership_audit_service.get_eligibility_audit(
                session, newer_audit, older_audit)

            print("Result summary: ")
            print(audit)

            print("Writing to db...")
            session.add(audit)
            session.commit()

            print("Success!")
        except Exception as e:
            print("Something went wrong:")
            print(e)
