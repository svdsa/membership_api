from typing import List

from sqlalchemy import orm
from sqlalchemy.orm import joinedload

from membership.database.models import NationalMembershipData, Member


class RosterImportSessionCache:

    def __init__(self, session_maker):
        self.session_maker = session_maker
        self.session = self.session_maker()

        self.national_records = self._load_national_records(self.session)
        self.chapter_records = self._load_chapter_records(self.session)

    def refresh_session(self) -> 'RosterImportSessionCache':
        return RosterImportSessionCache(self.session_maker)

    def _load_national_records(self, session: orm.Session) -> List[NationalMembershipData]:
        return session.query(NationalMembershipData).all()

    def _load_chapter_records(self, session: orm.Session) -> List[Member]:
        return session.query(Member).options(
            joinedload(Member.additional_email_addresses),
            joinedload(Member.all_roles),
            joinedload(Member.phone_numbers),
            joinedload(Member.identities)
        ).all()
