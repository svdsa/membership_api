from typing import List

import jobs.roster.csv_record


class AmbiguousLink:

    def __init__(
            self,
            # Using full path to avoid circular import
            csv_record: 'jobs.roster.csv_record.CSVRecord',
            matching_chapter_records: List[int]
    ):
        self.csv_record = csv_record
        self.matching_chapter_records = matching_chapter_records
