from overrides import overrides

from jobs import Job
from membership.database.base import Session
from membership.database.models import Member
from membership.services.onboarding.onboarding_results import OnboardingResults, OnboardingResult
from membership.services.onboarding.onboarding_status import OnboardingStatus
from membership.services.slack.slack_service import SlackService


class SlackTest(Job):

    @overrides
    def run(self, config: dict) -> None:
        service = SlackService()

        session = Session()

        member = session.query(Member).first()

        onboarding_results = OnboardingResults(
            auth0_result=OnboardingResult(OnboardingStatus.SUCCESS),
            portal_email_result=OnboardingResult(OnboardingStatus.SUCCESS),
            google_group_result=OnboardingResult(OnboardingStatus.NOT_IMPLEMENTED),
            slack_result=OnboardingResult(OnboardingStatus.ERROR,
                                          "There was a problem with the api"),
            newsletter_result=OnboardingResult(OnboardingStatus.ALREADY_ADDED),
            hustle_result=OnboardingResult(OnboardingStatus.OPTED_OUT),
            crm_result=OnboardingResult(OnboardingStatus.SUCCESS)
        )

        service.notify_of_individual_sign_up(member, "Public Website", onboarding_results)
