#!/usr/bin/env python3

import argparse
import sys
from typing import Optional

from overrides import overrides
from pydantic.networks import EmailStr

from jobs import Job
from membership.database.base import Session
from membership.database.models import SecurityPrincipal, User
from membership.schemas.rest.contacts import (
    SxContactCreateRequest,
    SxContactUpdateRequest,
)
from membership.schemas.rest.email_addresses import SxEmailAddressCreateRequest
from membership.services import contact_service, security_principal_role_service
from membership.util.arrays import compact, head


class AddUserJob(Job):
    @overrides
    def build_parser(self, parser: argparse.ArgumentParser) -> None:
        parser.add_argument("--email-addresses", nargs="+", required=True)
        parser.add_argument("--roles", nargs="?", action="append")
        parser.add_argument("--full-name", type=str)
        parser.add_argument("--display-name", type=str)
        parser.add_argument("--force-update", default=False, action="store_true")

    @overrides
    def run(self, config: dict) -> None:
        session = Session()

        try:
            email_strs = config["email_addresses"]
            print(email_strs)
            email_addresses = [EmailStr(ea) for ea in email_strs]
            contacts = compact(
                [
                    contact_service.find_by_email_address(session, ea)
                    for ea in email_addresses
                ]
            )

            if len(contacts) > 1:
                raise ValueError(
                    f"Provided email addresses {email_addresses=} "
                    "match more than one contact "
                    f"{[contact.full_name for contact in contacts]}"
                )

            role_strs = set(config.get("roles") or [])
            role_strs.add("everyone")
            print(role_strs)

            sp_roles = [
                security_principal_role_service.find_by_slug_or_raise(session, slug)
                for slug in role_strs
            ]
            contact = head(contacts)
            user: Optional[User] = contact.user if contact is not None else None
            sp: Optional[SecurityPrincipal] = (
                contact.security_principal if contact is not None else None
            )

            assert (sp is None) == (user is None)

            if contact is None:
                if (full_name := config.get("full_name")) is None:
                    raise ValueError(
                        "Must provide a value for `--full-name` if creating a new user"
                    )

                create_request = SxContactCreateRequest(
                    full_name=full_name,
                    display_name=config.get("display_name"),
                    email_addresses=email_addresses,
                    source_created="cli",
                )
                contact = contact_service.create(session, create_request=create_request)
                sp = SecurityPrincipal()
                user = User(contact=contact, principal=sp)
                for spr in sp_roles:
                    security_principal_role_service.attach_to_security_principal(
                        session, sp=sp, role=spr
                    )
            else:
                force_update = config.get("force_update")
                if not force_update:
                    print(
                        f"Contact already exists (id={contact.id}), "
                        "pass `--force-update` to update this existing user"
                    )
                    sys.exit(3)
                else:
                    update_request = SxContactUpdateRequest(
                        full_name=config["full_name"],
                        display_name=config.get("display_name"),
                    )
                    contact = contact_service.update(
                        session, contact, update_request=update_request
                    )

                    for ea in email_addresses:
                        contact_service.try_add_email_address(
                            session, contact, SxEmailAddressCreateRequest(value=ea)
                        )

                    if user is None:
                        assert sp is None
                        sp = SecurityPrincipal()
                        user = User(contact=contact, principal=sp)

                    if sp is not None:
                        for spr in sp_roles:
                            security_principal_role_service.attach_to_security_principal(
                                session, sp=sp, role=spr
                            )

            session.add_all([contact, user, sp])
            session.commit()

        except Exception as e:
            session.rollback()
            print(f"error: {e}", file=sys.stderr)
            sys.exit(1)
