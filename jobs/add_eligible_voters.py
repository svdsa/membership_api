#!/usr/bin/env python3

import argparse
import sys
from typing import IO, List, Optional  # noqa: F401

from overrides import overrides
from sqlalchemy import orm

from jobs import Job
from membership.database.base import Session
from membership.database.models import Election, EligibleVoter, Meeting
from membership.repos import MeetingRepo
from membership.services.attendee_service import AttendeeService
from membership.services.crm.crm_service import CrmService
from membership.services.eligibility import SanFranciscoEligibilityService
from membership.services.mailchimp.mailchimp_action_generator import (
    MailchimpActionGenerator,
)
from membership.services.mailchimp.mailchimp_service import MailchimpService
from membership.services.member_service import MemberService
from membership.services.onboarding.onboarding_service import OnboardingService
from membership.services.role_service import RoleService
from membership.services.slack.slack_service import SlackService

mailchimp_service = MailchimpService()
mailchimp_action_gen = MailchimpActionGenerator()
crm_service = CrmService()
slack_service = SlackService()


class AddEligibleVoters(Job):
    @overrides
    def build_parser(self, parser: argparse.ArgumentParser) -> None:
        parser.add_argument('-e', type=int)
        parser.add_argument('-o', '--output', type=argparse.FileType('w'), default=sys.stdout)

    @overrides
    def run(self, config: dict) -> None:
        election_id: Optional[int] = config.get('e')
        out: IO[str] = config.get('output', sys.stdout)

        session = Session()
        election = None if election_id is None else session.query(Election).get(election_id)

        eligible_member_ids = self.eligible_member_ids(session, election)

        out.write('%d members found: ' % len(eligible_member_ids))
        out.write(', '.join(map(str, eligible_member_ids)) + "\n")
        if election is None:
            return
        resp = input('Add to %s? (Y/n) ' % election.name)
        if not resp or resp.lower() != 'y':
            return

        self.add_voters(election_id, eligible_member_ids, session)

    def eligible_member_ids(self, session: orm.Session, election: Optional[Election]) -> List[int]:
        meeting_repository = MeetingRepo(Meeting)
        attendee_service = AttendeeService()
        eligibility_service = SanFranciscoEligibilityService(
            meetings=meeting_repository,
            attendee_service=attendee_service
        )
        onboarding_service = OnboardingService(
            mailchimp_service, mailchimp_action_gen, crm_service, slack_service)
        member_service = MemberService(eligibility_service, RoleService(), onboarding_service)

        members_result = member_service.all(session)
        eligible_members = eligibility_service.members_as_eligible_to_vote(
            session,
            members_result.members
        )

        if election is None:
            existing_member_ids = {}
        else:
            existing_eligible_voters = session \
                .query(EligibleVoter).filter(EligibleVoter.election == election).all()
            existing_member_ids = {voter.member_id for voter in existing_eligible_voters}

        return [
            member.id
            for member in eligible_members
            if member.is_eligible and member.id not in existing_member_ids
        ]

    def add_voters(self, election_id: int, member_ids: List[int], session: orm.Session) -> None:
        for member_id in member_ids:
            eligible_voter = EligibleVoter(
                member_id=member_id,
                election_id=election_id,
                voted=False
            )
            session.add(eligible_voter)
        session.commit()
