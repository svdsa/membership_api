from overrides import overrides

from jobs import Job
from membership.database.base import Session
from membership.services.eligibility import MembershipAuditService
from membership.services.meeting_service import MeetingService


class MembershipAudit(Job):
    description = "Generate audit report and save results to db."
    meeting_service = MeetingService()
    membership_audit_service = MembershipAuditService()

    @overrides
    def run(self, config: dict) -> None:
        try:
            session = Session()

            print("Building audit report...")
            audit = self.membership_audit_service.build_membership_audit(session)

            print("Result summary: ")
            json = self.membership_audit_service.audit_json(audit)
            print(str(json))

            print("Writing to db...")
            session.add(audit)
            session.commit()

            print("Success!")
        except Exception as e:
            print("Something went wrong:")
            print(e)
